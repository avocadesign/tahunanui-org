<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/**
 * Add a stylesheet for TinyMCE
 *
 * @since 2.0.0
 */
//add_editor_style( 'css/editor-style.css' );

/**
 * Add a stylesheet for TinyMCE
 *
 * @since 2.0.0
 */
// add_editor_style( 'css/editor-style.css' );


/*
 * Tiny MCE tweaks
 *
 * Adding format button and adding formats
 */

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'torlesse_style_select' ) ) {
	function torlesse_style_select( $buttons ) {
		array_push( $buttons, 'styleselect' );
		return $buttons;
	}
}
add_filter( 'mce_buttons', 'torlesse_style_select' );

// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'torlesse_styles_dropdown' ) ) {
	
	function torlesse_styles_dropdown( $init ) {
	    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Blockquote=blockquote;';
	
	    $style_formats = array (
	        array( 'title' => 'Lead Paragraph', 'selector' => 'p', 'classes' => 'lead' ),
	        array( 'title' => 'Button Link', 'selector' => 'a', 'classes' => 'button' )
	    );
	    
	    // Merge old & new styles
		$settings['style_formats_merge'] = true;
	
	    $init['style_formats'] = json_encode( $style_formats );
	
	    $init['style_formats_merge'] = false;
	    return $init;
	}
}
add_filter( 'tiny_mce_before_init', 'torlesse_styles_dropdown' );