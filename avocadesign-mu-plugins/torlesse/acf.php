<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );


/*
 * Options page setup
 *
 * http://www.advancedcustomfields.com/resources/options-page/
 */
function torlesse_acf_options() {
	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page(array(
			'page_title' 	=> 'Contact Details',
			'menu_title'	=> 'Contact Details',
			'menu_slug' 	=> 'contact-details',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
		//acf_add_options_sub_page(array(
		//	'page_title' 	=> 'Theme Header Settings',
		//	'menu_title'	=> 'Header',
		//	'parent_slug'	=> 'theme-general-settings',
		//));
		
	}

}

add_action( 'plugins_loaded', 'torlesse_acf_options' );

