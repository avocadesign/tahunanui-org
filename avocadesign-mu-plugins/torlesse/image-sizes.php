<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

update_option('medium_size_w', 600);
update_option('medium_size_h', 9999);
update_option('large_size_w', 1000);
update_option('large_size_h', 9999);

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'medium-height', 9999, 400 );
	add_image_size( 'small', 350, 9999 );
	add_image_size( 'small-height', 9999, 233 );
	add_image_size( 'small-square', 233, 233, true );
}

add_filter( 'image_size_names_choose', 'insert_custom_image_sizes' );
function insert_custom_image_sizes( $sizes ) {
	global $_wp_additional_image_sizes;
	if ( empty($_wp_additional_image_sizes) ) {
		return $sizes;
	}
		
	foreach ( $_wp_additional_image_sizes as $id => $data ) {
		if ( !isset($sizes[$id]) ) {
			$sizes[$id] = ucfirst( str_replace( '-', ' ', $id ) );
		}
	}

	return $sizes;
}

// set the image quality to maximum
add_filter('jpeg_quality', create_function('$quality', 'return 100;'));

add_action('added_post_meta', 'torlesse_update_jpeg_quality', 10, 4);

function torlesse_update_jpeg_quality($meta_id, $attach_id, $meta_key, $attach_meta) {

    if ($meta_key == '_wp_attachment_metadata') {

        $post = get_post($attach_id);

        if ($post->post_mime_type == 'image/jpeg' && is_array($attach_meta['sizes'])) {

            $pathinfo = pathinfo($attach_meta['file']);
            $uploads = wp_upload_dir();
            $dir = $uploads['basedir'] . '/' . $pathinfo['dirname'];

            foreach ($attach_meta['sizes'] as $size => $value) {

                $image = $dir . '/' . $value['file'];
                $resource = imagecreatefromjpeg($image);

                if ( $size == 'medium' || $size == 'medium-height' ) {
                    imagejpeg($resource, $image, 75);
                } elseif ( $size == 'small' || $size == 'small-height' || $size == 'small-square' )  {
                    imagejpeg($resource, $image, 85);
                } elseif ($size == 'large') {
	                imagejpeg($resource, $image, 65);
                } else {
                    // set the jpeg quality for the rest of sizes
                    imagejpeg($resource, $image, 100);
                }

                // or you can skip a paticular image size
                // and set the quality for the rest:
                // if ($size == 'splash') continue;

                imagedestroy($resource);
            }
        }
    }
}

