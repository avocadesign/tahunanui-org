<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/**
 * Disable admin bar on the frontend of your website
 * for subscribers.
 */
function torlesse_disable_admin_bar() {
	if( ! current_user_can('edit_posts') )
		add_filter('show_admin_bar', '__return_false');
}
add_action( 'after_setup_theme', 'torlesse_disable_admin_bar' );

/**
 * Redirect back to homepage and not allow access to
 * WP admin for Subscribers.
 */
function torlesse_redirect_admin(){
	if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
		wp_redirect( site_url() );
		exit;
	}
}
add_action( 'admin_init', 'torlesse_redirect_admin' );
