<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/*
 * Shortcode to display an Call to Action button
 * 
 * [cta-button colour="blue" title="click me" subtitle="go on now" url="http://www.example.com" newwindow="yes"]
 */
add_shortcode( 'cta-button', 'torlesse_button_shortcode' );
function torlesse_button_shortcode( $atts ) {
       extract( shortcode_atts(
               array(
                       'colour' => '',
                       'title' => 'Title',
                       'subtitle' => '',
                       'url' => '',
                       'newwindow' => 'no'
               ),
               $atts
       ));
       
       if( !empty( $atts["subtitle"] ) ) {
	       $button_subtitle = '<span class="subtitle">' . $atts["subtitle"] . '</span>';
       }
       
       if( !empty( $atts["colour"] ) ) {
	       $button_colour = ' ' . $atts["colour"];
       }
       
       // Open links in new window if specified
       if( $atts["newwindow"] == "yes" ) {
	       $newwindow = 'target="_blank"';
       } else {
	       $newwindow = '';
       }
       
       return '<a href="' . $url . '" class="button' . $button_colour . '" ' . $newwindow . '>' . $title . $button_subtitle . '</a>';
       
       
}