<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/*
*	Flexible Content Layouts for Primary Sidebar
*
*	Set on ACF options page under primary sidebar tab
*/



/*
*  Loop through a Flexible Content field and display it's content with different views for different layouts
*/

function torlesse_do_sidebar_blocks_primary() {
	
	while( has_sub_field( "sidebar_blocks_primary" , "options") ):
 
		if(get_row_layout() == "block_text_area"): // layout: Text Area
			
			echo '<section class="widget torlesse-content-block primary-sidebar text-block ' . get_sub_field( 'css_class' , 'options' ) . '">';
			
			echo '<h3 class="widget-title widgettitle">';
			the_sub_field( 'heading');
			echo '</h3>';
			?>
	 
			<div class="block-content">
				<?php the_sub_field("text"); ?>
			</div>
	 
			<?php if( get_sub_field( 'show_call_to_action_link' , 'options' ) === true ): ?>
			
				<a href="<?php the_sub_field("link"); ?>" class="button call-to-action">
					<?php the_sub_field("link_text"); ?>
				</a>
			
			<?php endif;
			
			echo '</section>';
			
		elseif(get_row_layout() == "block_contact_details"): // layout: Contact Details
			
			
			$show_phone 		  = get_sub_field( 'show_phone_number' , 'options' );
			$show_mobile 		  = get_sub_field( 'show_mobile_number' , 'options' );
			$show_address 		  = get_sub_field( 'show_address' , 'options' );		
			$show_postal_address  = get_sub_field( 'show_postal_address' , 'options' );
			$show_map 			  = get_sub_field( 'show_map' , 'options' );
			$show_social_media	  = get_sub_field( 'show_social_media_links' , 'options' );
			
			$display_fields = array(
				'show_phone' 				=> $show_phone,
				'show_mobile' 				=> $show_mobile,
				'show_address' 				=> $show_address,
				'show_postal_address'		=> $show_postal_address,
				'show_map'					=> $show_map,
				'show_social_media'			=> $show_social_media,
			);
			
			echo '<section class="widget torlesse-content-block primary-sidebar contact-details ' . get_sub_field( 'css_class' , 'options' ) . '">';
			
			echo '<h3 class="widget-title widgettitle">';
			the_sub_field( 'contact_heading' , 'options');
			echo '</h3>';
			
			torlesse_do_contact_details( $display_fields );
			
			echo '</section>';
			
		endif;
	 
	endwhile;

}
 
