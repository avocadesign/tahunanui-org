<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/*
*	Contact Details Functions
*
*	used anytime primary contact details need to be displayed
* 	Set on ACF options page
*/

	/*
	* 	Phone number
	*/
	function torlesse_do_contact_phone() {

		if( get_field( 'contact_primary_phone' , 'option' ) ) {

			echo '<div class="phone-number" itemprop="telephone">';
			the_field( 'contact_primary_phone', 'option' );
			echo '</div>';

		}

	}

	/*
	* 	Mobile number
	*/
	function torlesse_do_contact_mobile() {

		if( get_field( 'contact_primary_mobile' , 'option' ) ) {

			echo '<div class="mobile-number" itemprop="telephone">';
			the_field( 'contact_primary_mobile', 'option' );
			echo '</div>';

		}

	}

	/*
	* 	Email Address
	*/
	function torlesse_do_contact_email() {

		if( get_field( 'contact_email_address' , 'option' ) ) {

			echo '<div class="email-address" itemprop="email">';
			echo '<a href="mailto:' . get_field( 'contact_email_address', 'option' ) . '">' . get_field( 'contact_email_address', 'option' ) . '</a>';
			echo '</div>';
		}

	}

	/*
	 * 	Create Primary Address
	 *
	 *	All fields loaded conditionally
	 * 	Address Schema form Schema.org
	 */
	function torlesse_do_contact_address() {

		if( get_field( 'contact_primary_address' , 'option' ) ) {

				while( has_sub_field( 'contact_primary_address' , 'option' ) ): ?>

					<div class="primary-address">

						<?php if( get_sub_field( 'heading' , 'option' ) ): ?>
					  		<h3><?php the_sub_field('heading'); ?></h3>
					  	<?php endif; ?>

					  	<?php if( get_sub_field( 'description' , 'option' ) ): ?>
					  		<span><?php the_sub_field('description'); ?></span>
					  	<?php endif; ?>

					  	<div class="address-full" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

						  	<div class="street-address-wrap" itemprop="streetAddress">

						  		<?php if( get_sub_field( 'street_address' , 'option' ) ): ?>
						  			<div class="street-address"><?php the_sub_field('street_address'); ?></div>
						  		<?php endif; ?>

						  		<?php if( get_sub_field( 'suburb' , 'option' ) ): ?>
						  			<div class="suburb"><?php the_sub_field('suburb'); ?></div>
						  		<?php endif; ?>

								  	<div >

								  		<?php if( get_sub_field( 'city' , 'option' ) ): ?>
								  			<span class="city" itemprop="addressLocality"><?php the_sub_field('city'); ?>, </span>
								  		<?php endif; ?>


									  	<?php if( get_sub_field( 'post_code' , 'option' ) ): ?>
									  		<span class="post-code" itemprop="postalCode"><?php the_sub_field('post_code'); ?></span>
									  	<?php endif; ?>

								  	</div>

							  	<?php if( get_sub_field( 'region' , 'option' ) ): ?>
								  		<div class="region" itemprop="addressRegion"><?php the_sub_field('region'); ?> </div>
								<?php endif; ?>

							  	<?php if( get_sub_field( 'country' , 'option' ) ): ?>
							  		<div class="country" itemprop="addressCountry"><?php the_sub_field('country'); ?></div>
							  	<?php endif; ?>
							</div>

						</div>
					</div>

			<?php endwhile;

		}

	}

	/*
	* 	Postal Address
	*/
	function torlesse_do_contact_postal_address() {

		if( get_field( 'contact_primary_postal_address' , 'option' ) && get_field( 'show_second_location' , 'option' ) == true ) {

				while( has_sub_field( 'contact_primary_postal_address' , 'option' ) ): ?>

					<div class="postal-address">

						<?php if( get_sub_field( 'heading' , 'option' ) ): ?>
					  		<h3 itemprop="name"><?php the_sub_field('heading'); ?></h3>
					  	<?php endif; ?>

					  	<?php if( get_sub_field( 'description' , 'option' ) ): ?>
					  		<span itemprop="description"><?php the_sub_field('description'); ?></span>
					  	<?php endif; ?>

					  	<div class="address-full">

						  	<div class="street-address-wrap">

						  		<?php if( get_sub_field( 'street_address' , 'option' ) ): ?>
						  			<div class="street-address"><?php the_sub_field('street_address'); ?></div>
						  		<?php endif; ?>

						  		<?php if( get_sub_field( 'suburb' , 'option' ) ): ?>
						  			<div class="suburb"><?php the_sub_field('suburb'); ?></div>
						  		<?php endif; ?>

						  	</div>

						  	<div class="city-postcode">

						  		<?php if( get_sub_field( 'city' , 'option' ) ): ?>
						  			<span class="city" itemprop="addressLocality"><?php the_sub_field('city'); ?>, </span>
						  		<?php endif; ?>


							  	<?php if( get_sub_field( 'post_code' , 'option' ) ): ?>
							  		<span class="post-code" itemprop="postalCode"><?php the_sub_field('post_code'); ?></span>
							  	<?php endif; ?>

						  	</div>

						  	<?php if( get_sub_field( 'region' , 'option' ) ): ?>
							  		<div class="region" itemprop="addressRegion"><?php the_sub_field('region'); ?>, </div>
							<?php endif; ?>


						  	<?php if( get_sub_field( 'country' , 'option' ) ): ?>
						  		<div class="country"><?php the_sub_field('country'); ?></div>
						  	<?php endif; ?>

						</div>
					</div>

			<?php endwhile;

		}

	}

	/**
	 *	Loop through social media
	 *
	 *	ACF repeater
	*/
	function torlesse_do_social_media_links() {

		if( get_field( 'social_network_links' , 'option' )) {

		 	echo '<div class="social-network-links-wrap">';

			while( has_sub_field( 'social_network_links' , 'option' ) ) {
				echo '<a href="' . get_sub_field('social_network_link') . '" class="social-network-link ' . get_sub_field('social_network') . '" target="_blank">' . get_sub_field('social_network_link_text') . '</a>';
			echo '<br />';
			}

			echo '</div>';

		}

	}

	/**
	 *	Loop through social media without text (for use with just icons)
	 *
	 *	ACF repeater
	*/
	function torlesse_do_social_media_links_icons() {

		if( get_field( 'social_network_links' , 'option' )) {

		 	echo '<div class="social-network-links-wrap">';

			while( has_sub_field( 'social_network_links' , 'option' ) ) {
				echo '<a href="' . get_sub_field('social_network_link') . '" class="social-network-link ' . get_sub_field('social_network') . '" target="_blank"></a>';
			}
			echo '<br />';
			echo '</div>';

		}

	}

	/**
	 *	Link to contact page
	 *
	 *	ACF fields set in "Additional Content"
	*/
	function torlesse_do_contact_page_link() {

		echo '<a href="' . get_field( 'contact_primary_page_link', 'option') . '" class="button contact-page-link">' . get_field( 'contact_primary_page_link_text', 'option') . '</a>';

	}

	function torlesse_do_contact_details( $display_fields ) {

		$show_postal_addess = get_field( 'show_second_location' , 'option' );

		$display_fields = shortcode_atts(
							array(
								'show_phone' 				=> true,
								'show_mobile' 				=> true,
								'show_address' 				=> true,
								'show_postal_address'		=> $show_postal_addess,
								'show_contact_link'			=> true,
								'show_social_media'			=> true,
								'show_map'					=> true,
							), $display_fields
						);

		$show_phone = $display_fields['show_phone'];

		?>
			<div class="full-contact-details" itemscope itemtype="http://schema.org/LocalBusiness">

				<?php  if( $display_fields['show_phone']           == true ) { torlesse_do_contact_phone();           } ?>
				<?php  if( $display_fields['show_mobile']          == true ) { torlesse_do_contact_mobile();          } ?>
				<?php  if( $display_fields['show_address']         == true ) { torlesse_do_contact_address();         } ?>
				<?php  if( $display_fields['show_postal_address']  == true ) { torlesse_do_contact_postal_address();  } ?>
				<?php  if( $display_fields['show_contact_link']    == true ) { torlesse_do_contact_page_link();  	  } ?>
				<?php  if( $display_fields['show_social_media']    == true ) { torlesse_do_social_media_links();      } ?>

			</div>
		<?php
	}

	/**
	 *	Contact Page Setup
	 *
	 *	Override the page layout of Contact page and display contact fields
	*/
	function torlesse_do_contact_page() {

		if( get_field('toggle_contact_details') ) {

			while(has_sub_field('toggle_contact_details'))	{
				$show_phone 		  = get_sub_field( 'show_phone');
				$show_mobile 		  = get_sub_field( 'show_mobile');
				$show_address 		  = get_sub_field( 'show_address');
				$show_postal_address  = get_sub_field( 'show_postal_address');
				$show_social_media	  = get_sub_field( 'show_social_media');
				$show_map	  		  = get_sub_field( 'show_map');

			}

		}


		$display_fields = array(
			'show_phone' 				=> $show_phone,
			'show_mobile' 				=> $show_mobile,
			'show_address' 				=> $show_address,
			'show_postal_address'		=> $show_postal_address,
			'show_social_media'			=> $show_social_media,
			'show_contact_link'			=> false,
		);

		?>
			<div class="contact-page-columns-wrap">
				<div class="contact-page-left one-half first">
					<?php
						echo '<h2>' . get_field( 'contact_details_heading') . '</h2>';

						torlesse_do_contact_details( $display_fields );
					?>
					<div class="contact-us-page-content">
						<?php the_content(); ?>
					</div>
				</div>

				<div class="contact-page-right one-half">
					<?php
						$form = get_field('email_form');
						gravity_form_enqueue_scripts($form->id, true);
						gravity_form($form->id, true, true, false, '', true, 1);
					?>
				</div>

			</div>

		<?php
	}
