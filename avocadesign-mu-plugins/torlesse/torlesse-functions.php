<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

include_once( dirname(__FILE__) . '/editor-tweaks.php' );
include_once( dirname(__FILE__) . '/acf.php' );
include_once( dirname(__FILE__) . '/shortcodes.php' );
include_once( dirname(__FILE__) . '/contact-details.php' );
include_once( dirname(__FILE__) . '/sidebars.php' );
include_once( dirname(__FILE__) . '/image-sizes.php' );
include_once( dirname(__FILE__) . '/hide-admin.php' );

/****
 * Avoca Design Admin Customisations
 *
 	* Help link and logo in footer
 	* Custom login logo
 	* Hide admin bar in the frontend
 *
 * Since Version: 0.1.0
 */
 	// Help link and footer  in wordpress footer
	function remove_footer_admin () {
		echo '<img src="/wp-content/avocadesign-mu-plugins/torlesse/images/adlogo.32x32.png" width="32px" height="32px" style="float:left;padding-right:10px;margin-top:-7px;" />For support <a href="/wp-admin/admin.php?page=wp-help-documents">visit the site help</a> or contact <a href="http://www.avocadesign.co.nz">Avoca Design</a>';
	}
	add_filter('admin_footer_text', 'remove_footer_admin');

	// Custom Login Logo
	function custom_login_logo() {
	    echo '<style type="text/css">'.
	             '.login h1 a { background-image:url(/wp-content/avocadesign-mu-plugins/torlesse/images/login-logo.png) !important;
	             		 background-size: 320px 82px !important;
	             		 width:320px !important;
	             		 height:82px !important; }'.
	         '</style>';
	}
	//Uncomment this line to use a custom logo for the login
	//add_action( 'login_head', 'custom_login_logo' );

	// Hide admin bar on the frontend (uncomment to use)
	//add_filter('show_admin_bar', '__return_false'); 

/*
 * White Label Soliloquy Slideshow Plugin
 *
 * Since Version: 0.1.0
 */
	add_filter( 'tgmsp_strings', 'tgm_custom_slider_strings' );
	function tgm_custom_slider_strings( $strings ) {

		$strings['add_slider']		 = 'Add Slider';
		$strings['addon_intro']		 = 'The following Addons can be used to extend the functionality of your slider.';
		$strings['addons_page_title']	 = 'Slider Addons';
		$strings['advanced_help']	 = 'Slider Advanced';
		$strings['attachment_red']	 = 'Images with a red overlay cannot be inserted into this slider. This slider uses attachments to handle slider images, and because WordPress currently only allows you to attach an image to one post, images that have already been attached to another post cannot be used in this slider.';
		$strings['main_help']		 = 'Soliloquy utilizes custom post types in order to handle slider instances. Each slider instance has its own separate images, attributes and settings. You can get started by clicking the "Add New" button beside the page title.';
		$strings['meta_instructions']	 = 'Slider Instructions';
		$strings['meta_settings']	 = 'Slider Settings';
		$strings['no_updates']		 = 'All of your slider plugins are up to date. Update checks are performed automatically when you visit or reload this page.';
		$strings['page_title']		 = 'Slider Settings';
		$strings['slider_choose'] 	 = 'Choose Your Slider';
		$strings['slider_select'] 	 = 'Please select a slider.';
		$strings['slider_select_desc'] 	 = 'Select a slider below from the list of available sliders and then click \'Insert\' to place the slider into the editor.';
		$strings['slider_select_insert'] = 'Insert Slider';
		$strings['slider_select_cancel'] = 'Cancel Slider Insertion';
		$strings['updates_page_title']	 = 'Slider Updates';
		$strings['sidebar_help_support'] = 'Slider Support';
		$strings['sidebar_help_contact'] = 'Contact Slider Support';

		return $strings;

	}

	add_filter( 'tgmsp_post_type_labels', 'tgm_custom_slider_labels' );
	function tgm_custom_slider_labels( $labels ) {

		$labels['name'] 		= __( 'Sliders' );
		$labels['singular_name']	= __( 'Slider' );
		$labels['add_new'] 		= __( 'Add New' );
		$labels['add_new_item'] 	= __( 'Add New Slider' );
		$labels['edit_item']		= __( 'Edit Slider' );
		$labels['new_item'] 		= __( 'New Slider' );
		$labels['view_item'] 		= __( 'View Slider' );
		$labels['search_items'] 	= __( 'Search Sliders' );
		$labels['not_found'] 		= __( 'No Sliders found' );
		$labels['not_found_in_trash'] 	= __( 'No Sliders found in trash' );
		$labels['menu_name'] 	        = __( 'Sliders' );

		return $labels;

	}

	add_filter( 'tgmsp_slider_messages', 'tgm_custom_slider_messages' );
	function tgm_custom_slider_messages( $messages ) {

		$messages[1] = 'Slider updated.';
		$messages[2] = 'Slider custom field updated.';
		$messages[3] = 'Slider custom field deleted.';
		$messages[4] = 'Slider updated.';
		$messages[5] = isset( $_GET['revision'] ) ? sprintf( 'Slider restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false;
		$messages[6] = 'Slider published.';
		$messages[7] = 'Slider saved.';
		$messages[8] = 'Slider submitted.';
		$messages[9] = sprintf( 'Slider scheduled for: %1$s.', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) );
		$messages[10] = 'Slider draft updated.';

		return $messages;

	}

	/*
 * Adding the Featured Image (full size) to all single posts above the post title (Working well)
 *
 * Source http://sridharkatakam.com/adding-featured-images-post-titles-minimum-pro/
 */
add_action ( 'genesis_entry_header', 'sk_show_featured_image_single' );
function sk_show_featured_image_single() {
	if ( is_single() && has_post_thumbnail() ) {
	echo '<div class="single-thumbnail">';
	genesis_image( array( 'size' => 'single-thumbnail' ) );
	echo '</div>';
	}
}
