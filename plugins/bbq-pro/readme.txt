=== BBQ Pro ===

Plugin Name: BBQ Pro
Plugin URI: https://plugin-planet.com/bbq-pro/
Description: The fastest WordPress firewall plugin. Advanced protection against malicious requests.
Tags: security, protect, firewall, php, eval, malicious, url, request, blacklist
Author: Jeff Starr
Contributors: specialk
Author URI: https://plugin-planet.com/
Donate link: http://m0n.co/donate
Requires at least: 4.1
Tested up to: 4.6
Stable tag: trunk
Version: 1.5
Text Domain: bbq-pro
Domain Path: /languages
License: BBQ Pro is comprised of two parts (see "License" section below for details)

The fastest WordPress firewall plugin. Advanced protection against malicious requests.



== Description ==

> BBQ = Block Bad Queries

[BBQ Pro](https://plugin-planet.com/bbq-pro/) helps keep your WordPress site safe and secure by blocking attacks and bad requests. This helps to conserve precious server resources like memory and bandwidth. BBQ Pro is built to be extensible, flexible, and blazing fast. It checks all incoming traffic and quietly blocks any URI requests that contain nasty stuff like `eval(`, `base64(`, `exec(`, and other malicious nonsense. BBQ Pro is fully customizable, giving you control over every pattern and rule. You can edit, remove, add, and/or test BBQ patterns via easy-to-use settings screens.

**Features**

* Plug-n-play functionality
* No configuration required
* Born of simplicity, no frills
* Lightweight, fast and flexible
* Advanced protection against malicious requests
* Works silently behind the scenes to protect your site
* Advanced configuration via easy-to-use settings screen
* Scans all incoming traffic, option to disable for logged-in users
* Option to specify a redirect URL for blocked requests
* Option to display a custom message for all blocked requests
* Set your own Status Code for blocked requests
* Customize (add/remove/edit) BBQ patterns to suit your security strategy
* Tracks the number of times each BBQ pattern blocks a request
* Built-in "test" buttons to test each BBQ pattern
* Scan for malicious strings in the Request URI, Query String, User Agent, IP Address, and Referrer
* Add your own custom patterns to BBQ to protect against new threats and unwanted requests
* Includes tools to reset options, patterns, and statistics
* Powered by [5G Blacklist](https://perishablepress.com/5g-blacklist-2013/), [6G Blacklist](https://perishablepress.com/6g-beta/), and 10+ years of hands-on security experience
* NEW! Optional whitelisting of IP addresses

BBQ Pro is the premium version of the [Block Bad Queries (BBQ) WordPress plugin](https://wordpress.org/plugins/block-bad-queries/).



== Screenshots ==

[Screenshots available at Plugin Planet](https://plugin-planet.com/bbq-pro/#screenshots)



== Installation ==

= Installing BBQ Pro =

1. Download a zipped copy of BBQ Pro from Plugin Planet
2. Unzip and upload the `/bbq-pro/` folder to `/wp-content/plugins/`
3. Visit the WordPress Plugins screen to activate BBQ Pro
4. Visit BBQ Pro License to activate the license
5. Visit BBQ Pro Settings to configure options

Step 5 is optional; by default BBQ Pro works just like the free version of BBQ, silently protecting your site with no configuration required. To customize the plugin, visit the BBQ Settings and BBQ Patterns.

Note: BBQ includes complete inline documentation; click the "Help" tab in the upper-right corner of any BBQ settings screen for more information.

[Get started using BBQ Pro](https://plugin-planet.com/bbq-pro-quick-start/)

[More info on installing WP plugins](http://codex.wordpress.org/Managing_Plugins#Installing_Plugins)



== Upgrade Notice ==

__Upgrades:__ Your purchase of USP Pro includes free lifetime upgrades, which include new features, bug fixes, and other improvements. When an upgrade is available, WordPress will notify you in the Admin Area. When you see that there is an update available, just click "Update" and WordPress will perform the upgrade automatically. Note that you can [download the latest version of USP Pro at Plugin Planet](https://plugin-planet.com/download-purchased-plugin/) anytime at your convenience.

**New BBQ Patterns**

When new patterns are available, they can be enabled via "Reset Patterns" under the Tools menu. 

**Uninstall/Reset**

At any time you may visit the "Tools" screen to reset default settings, patterns, and statistics. 

Also, uninstalling the plugin from the WP Plugins screen results in the removal of all settings and data from the WP database. 



== Usage ==

* Install, activate, and done -- no configuration required for basic BBQ protection
* To configure plugin settings, visit BBQ's "Settings" screen
* To customize the patterns used to block bad requests, visit BBQ's "Patterns" screen
* To reset settings, patterns, and statistics, visit BBQ's "Tools" screen
* To enter your license and enable the plugin, visit BBQ's "License" screen

Note: BBQ includes complete inline documentation; click the "Help" tab in the upper-right corner of any BBQ settings screen for more information.



== Resources ==

= Getting started =

* [BBQ Pro Homepage](https://plugin-planet.com/bbq-pro/)
* [BBQ Pro Quick Start Guide](https://plugin-planet.com/bbq-pro-quick-start/)
* [BBQ Pro readme.txt](https://plugin-planet.com/wp/files/bbq-pro/readme.txt)
* [BBQ Pro Settings](https://plugin-planet.com/bbq-pro-settings/)
* [BBQ Pro FAQs](https://plugin-planet.com/bbq-pro-faqs/)

= Further resources =

* [BBQ Pro Docs](https://plugin-planet.com/docs/bbq/)
* [BBQ Pro Forum](https://plugin-planet.com/forum/bbq/)
* [BBQ Pro Tutorials](https://plugin-planet.com/category/tuts+bbq-pro/)
* [BBQ Pro News](https://plugin-planet.com/category/news+bbq-pro/)

= Feedback and downloads =

* [Bug reports, help requests, and feedback](https://plugin-planet.com/bbq-pro/#contact)
* [Log in to your account for current downloads](https://plugin-planet.com/wp/wp-login.php)

= Screenshots and more =

* [Learn more about BBQ Pro](https://plugin-planet.com/bbq-pro/)
* [Screenshots and more available](https://plugin-planet.com/bbq-pro/#screenshots)



== Frequently Asked Questions ==

[Check out the BBQ Pro FAQs at Plugin Planet &raquo;](https://plugin-planet.com/bbq-pro-faqs/)



== License ==

License: BBQ Pro is comprised of two parts:

* Part 1: Its PHP code is licensed under the GPL (v2 or later), like WordPress. More info @ http://www.gnu.org/licenses/

* Part 2: Everything else (e.g., CSS, HTML, JavaScript, images, design) is licensed according to the purchased license. More info @ https://plugin-planet.com/bbq-pro/

Without prior written consent from Monzilla Media, you must NOT directly or indirectly: license, sub-license, sell, resell, or provide for free any aspect or component of Part 2.

Further license information is available in the plugin directory, `/license/`, and online @ https://plugin-planet.com/wp/files/bbq-pro/license.txt

Upgrades: Your purchase of BBQ Pro includes free lifetime upgrades, which include new features, bug fixes, and other improvements. 

Copyright: 2016 Monzilla Media



== Changelog ==

= 1.5 (2016/08/16) =

New patterns available! See "Upgrade Notice" in readme.txt.

* Improved IP-detection protocols for better accuracy
* Added setting to optionally whitelist any IP addresses
* Added new filter hook, `bbq_ip_filter`
* Updates [WP Admin Notices](https://digwp.com/2016/05/wordpress-admin-notices/)
* Replaced `_e()` with `esc_html_e()` or `esc_attr_e()`
* Replaced `__()` with `esc_html__()` or `esc_attr__()`
* Improved translation support
* Renamed `/lang/` to `/languages/`
* Generated new translation template
* Changed text-domain from "bbq" to "bbq-pro"
* Obscured BBQ Pro License field (optional toggle)
* Replaced BBQ Pro icon with hi-rez/retina version
* Added ".aspx" to Basic patterns
* Replaced "proc/self/environ" with "self/environ" in Basic patterns
* Removed "docomo" from Advanced UA patterns
* Added "seekerspider" to Advanced UA patterns
* Replaced "muieblackcat" with "muieblack" in Advanced patterns
* Added "/shell.php", "benchmark(", "sleep(", "&pws=0", ".bak" to Advanced patterns
* Tested on WordPress 4.6

= 1.4 (2016/03/28) =

New patterns available! See "Upgrade Notice" in readme.txt.

* Added "sitesucker" to Basic UA patterns
* Added "base64(" to Basic Request URI and Query String patterns
* Renamed bbq_core to bbq__core to avoid conflict with free version
* Improved system checks for required WP version and free version
* Added (array) to $bbq_patterns loop in bbq-core.php
* Updated handling of license update status
* Updated License screen interface
* Updated plugin updater class to 1.6.3
* Tested on WordPress 4.5 beta

= 1.3.1 (2015/11/23) =

* Added isset() to check for new strict_mode setting

= 1.3 (2015/11/18) =

New patterns available! See "Upgrade Notice" in readme.txt.

* Added !$array['enable'] to loop() function
* rawurldecode() no longer enabled by default
* Added "Strict Mode" to enable rawurldecode()
* Added missing validation for limit_request
* Updated Contextual Help tab information
* Added "acapbot" and "semalt" to UA patterns
* Added "morfeus" and "snoopy" to UA patterns
* Added "__hdhdhd.php" to URI patterns
* update heading hierarchy on settings page
* Added settings_errors() to settings page
* Now using admin_notices for alerts
* Admin notices now dismissible
* Added bbq_patterns_admin_notice()
* Added bbq_tools_admin_notice()
* Added bbq_license_admin_notice()
* Added bbq_scan hook (can be used for [logging](https://plugin-planet.com/bbq-pro-log-requests/))
* Added total active pattern counts
* Added bbq_active_count() function
* Changed hook for bbq_reset_defaults to admin_init
* Update heading hierarchy on settings page
* Updated translation template file
* Updated minimum version requirement
* General code cleanup and testing
* Tested with WordPress 4.4 beta

= 1.2 (2015/07/15) =

* Bugfix: Admin Area inaccessible to non-admin-level users

= 1.1 (2015/07/05) =

* Bugfix: disabled patterns not disabled
* Updated contextual help
* New language template

= 1.0 (2015/06/25) =

* Initial release


