<?php 
/* Row Extended Setting CSS */
$rows_object = $nodes['rows'];

foreach ( $nodes['rows'] as $row_object ) {
								
	$id = $row_object->node;
	$row = $row_object->settings;

	if( $row->separator_shape == 'round_split' ) {
		if( $row->separator_position == 'top' ||  $row->separator_position == 'top_bottom' ) { ?>
			.fl-node-<?php echo $id; ?> .uabb-top-row-separator.uabb-round-split:before {
			    background-color: <?php echo $row->separator_color; ?>;
			    height: <?php echo $row->separator_shape_height; ?>px;
			    top: 0px;
			    bottom: auto;
			    border-radius: 0 0 50px 0 !important;
			}

			.fl-node-<?php echo $id; ?> .uabb-top-row-separator.uabb-round-split:after {
			    background-color: <?php echo $row->separator_color; ?>;
			    height: <?php echo $row->separator_shape_height; ?>px;
			    left: 50%;
			    top: 0px;
			    bottom: auto;
			    border-radius: 0 0 0 50px !important;
			}
		<?php }

		if ( $row->separator_position == 'bottom' || $row->separator_position == 'top_bottom' ) { ?>
			.fl-node-<?php echo $id; ?> .uabb-bottom-row-separator.uabb-round-split:before {
			    background-color: <?php echo $row->separator_color; ?>;
			    height: <?php echo $row->separator_shape_height; ?>px;
			    top: auto;
			    bottom: 0px;
			    border-radius: 0 50px 0 0 !important;
			}

			.fl-node-<?php echo $id; ?> .uabb-bottom-row-separator.uabb-round-split:after {
			    background-color: <?php echo $row->separator_color; ?>;
			    height: <?php echo $row->separator_shape_height; ?>px;
			    left: 50%;
			    top: auto;
			    bottom: 0px;
			    border-radius: 50px 0 0 0 !important;
			}
<?php 
		} 
	}
}
?>

/* Column Extended Settings CSS*/

<?php
/* Column Extended Settings CSS*/
$columns = $nodes['columns'];

foreach($columns as $id=>$col) {
	
	$parent = $nodes['groups'][$col->parent]->parent; 

?>
	<?php if($global_settings->responsive_enabled) : // Responsive Sizes ?>
		<?php if(!empty($col->settings->border_type)) : // Border ?>
		<?php if($col->settings->hide_border_mobile == 'yes') : ?>
		@media(max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
			.fl-builder-content .fl-node-<?php echo $col->node; ?> .fl-col-content {
				border: none;
			}
		}
		<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>

<?php } ?>
/* Column End Extended Settings CSS*/