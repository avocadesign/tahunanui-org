(function( $ ) {

	$(document).ready(function() {

		var btn_content		= 'fl-builder-add-content-button',
			btn_rows 		= 'fl-builder-add-ultimate-rows-button',
			btn_templates 	= 'fl-builder-add-ultimate-templates-button',
			panel_content 	= 'fl-builder-panel-content-rows',
			panel_rows 		= 'fl-builder-panel-ultimate-rows';

		//	Add class to panel
		$( '.fl-builder-panel').not('.' + panel_rows).addClass('fl-builder-panel-content-rows');

		// Focus Search
		$('#module_search').focus();

		//	Hide - Rows & Templates
		$( '.' + panel_rows ).hide();

		//	Content
		$( '.' + btn_content ).click(function(event) {
			$( '.' + panel_content ).stop(true, true).show().animate({ right: '0px' }, 0);
			$( '.' + panel_rows ).stop(true, true).animate({ right: '-350px' }, 0, function(){ $( '.' + panel_rows ).hide(); });
			$('#module_search').focus();
		});

		//	Rows
		$( '.' + btn_rows ).click(function(event) {
			console.log('insect');
			$( '.' + panel_content ).stop(true, true).animate({ right: '-350px' }, 0, function(){ $( '.' + panel_content ).hide(); });
			$( '.' + panel_rows ).stop(true, true).show().animate({ right: '0' }, 0);
			$('#section_search').focus();
		});

		/**
		 * Quick Search
		 *
		 * Functionality works for BOTH (Module / Section) search...
		 */
		$('#module_search, #section_search').keyup(function(){
			
			var parent = $( this ).closest('.fl-builder-panel');
			var rex = new RegExp( $(this).val(), 'i');
	        parent.find('.fl-builder-block').hide();
	        parent.find('.fl-builder-block > .fl-builder-block-title').filter(function () {
	        	var data_type = $(this).html();
	        	return rex.test( data_type );
            }).parent('.fl-builder-block').show().closest('.fl-builder-blocks-section').addClass('fl-active');

            if( $.trim( $(this).val() ) == '' ) {
				parent.find('.fl-builder-blocks-section').removeClass('fl-active');            	
            }
	    });
	    $('.fl-builder-blocks-section').click(function(event) {
	    	$('#module_search, #section_search').val('');
	    	$( this ).find('.fl-builder-block').show();
	    });
	});

})( jQuery );
