UABB_Admin = {

	init: function()
	{
		UABB_Admin._toggleGlobal();
		UABB_Admin._togglePanel();

		jQuery('.uabb-global-enable').on('click', 	UABB_Admin._toggleGlobal );
		jQuery('.uabb-load-templates').on('click',	UABB_Admin._togglePanel );
	},

	_toggleGlobal: function() {
		var checked = jQuery('.uabb-global-enable').is(':checked');
		if( checked ) {
			jQuery('.uabb-admin-fields').show();
		} else {
			jQuery('.uabb-admin-fields').hide();
		}		
	},

	_togglePanel: function() {
		var checked = jQuery('.uabb-load-templates').is(':checked');
		if( checked ) {
			jQuery('.uabb-enabled-panels').closest('p').show();
		} else {
			jQuery('.uabb-enabled-panels').closest('p').hide();
		}		
	}
}

jQuery(document).ready(function( $ ) {

	UABB_Admin.init();

	/**
	 * 	Reload UABB IconFonts
	 */
	jQuery('.uabb-reload-icons').on('click', function() {

		jQuery(this).find('i').addClass('uabb-reloading-iconfonts');

		var data = {
			'action': 'uabb_reload_icons'
		};

		//	Reloading IconFonts
		jQuery.post( uabb.ajax_url, data, function(response) {
			if( response == 'success' ) {
				console.log('Reloading: ');
				location.reload(true);
			}
		});

	});

	/**
	 * 	Colorpicker Initiate
	 */

	var colorpicker = $('.uabb-wp-colopicker');

	if( colorpicker.length )
	{
		colorpicker.each(function(index) {
	    	$(this).wpColorPicker();
		});
	}	
});
