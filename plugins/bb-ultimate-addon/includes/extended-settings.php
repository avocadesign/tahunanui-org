<?php 
	/**
	 * Function to extend row settings for row separator
	 */
	function uabb_extend_row_settings( $form, $id ) {
	
		$row_setting_arr = array(
			'title'         => __('Effects', 'uabb'),
			'sections'      => array(
				'general'       => array(
					'title'         => __( 'Row Separator', 'uabb' ),
					'fields'        => array(
						'separator_shape' => array(
							'type'          => 'select',
							'label'         => __('Type', 'uabb'),
							'default'       => 'none',
							'options'       => array(
								'none'						=>	__( 'None', 'uabb' ),
								'triangle_svg'				=>	__( 'Triangle', 'uabb' ),
								'xlarge_triangle'			=>	__( 'Big Triangle', 'uabb' ),
								'xlarge_triangle_left'		=>	__( 'Big Triangle Left', 'uabb' ),
								'xlarge_triangle_right'		=>	__( 'Big Triangle Right', 'uabb' ),
								'circle_svg'				=>	__( 'Half Circle', 'uabb' ),
								'xlarge_circle'				=>	__( 'Curve Center', 'uabb' ),
								'curve_up'					=>	__( 'Curve Left', 'uabb' ),
								'curve_down'				=>	__( 'Curve Right', 'uabb' ),
								'tilt_left'					=>	__( 'Tilt Left', 'uabb' ),
								'tilt_right'				=>	__( 'Tilt Right', 'uabb' ),
								'round_split'				=>	__( 'Round Split', 'uabb' ),
								'waves'						=>	__( 'Waves', 'uabb' ),
								'clouds'					=>	__( 'Clouds', 'uabb' ),
								'multi_triangle'			=>	__( 'Multi Triangle', 'uabb' ),
							),
							'toggle'	=> array(
								'triangle_svg'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'xlarge_triangle'			=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'xlarge_triangle_left'		=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'xlarge_triangle_right'		=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'circle_svg'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'xlarge_circle'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'curve_up'					=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'curve_down'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'tilt_left'					=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'tilt_right'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'round_split'				=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'waves'						=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'clouds'					=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
								'multi_triangle'			=> array(
									'fields'		=> array( 'separator_position' ,'separator_shape_height', 'separator_color' )
								),
							)
						),
						'separator_position' => array(
							'type'          => 'select',
							'label'         => __('Position', 'uabb'),
							'default'       => 'top',
							'options'       => array(
								'top'					=>	__( 'Top', 'uabb' ),
								'bottom'				=>	__( 'Bottom', 'uabb' ),
								'top_bottom'			=>	__( 'Top & Bottom', 'uabb' ),
							)
						),
						'separator_shape_height'   => array(
							'type'          => 'text',
							'label'         => __('Size', 'uabb'),
							'default'       => '60',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
							'placeholder'   => '60'
						),
						'separator_color'          => array(
							'type'          => 'uabb-color',
							'label'         => __('Background', 'uabb'),
							'default'		=> '#ffffff',
							'help'			=> __('Mostly, this should be background color of your adjacent row section. (Default - White)', 'uabb'),
							/*'preview'         => array(
								'type'            => 'text',
								'selector'        => '.uabb-button-text'
							)*/
						),
					)
				),
			)
		);
		
		if ( $id == 'row' ) {
			
			//$form['tabs']['effects'] = $row_setting_arr;
			$form['tabs'] = array_merge(
				array_slice( $form['tabs'], 0, 1 ), 
				array( 'effect' => $row_setting_arr ),
				array_slice( $form['tabs'], 1 ) 
			);
			/*print_r( $form['tabs'] );*/
		}
		return $form;
	}

	/**
	 * Function to append Custom Vertical Alignment Setting in Column
	 */
	function uabb_extend_column_settings( $form, $id ) {

		if($id == 'col') {
			$column_form = array(
				'tabs' => array(
					'style' => array(
						'sections' => array(
							'general' => array(
								'fields'  => array(
									'content_alignment'  => array(
										'options' => array(
											'bottom'   	=> __('Bottom', 'uabb')
										),
										/*'preview'	=> array(
											'type'          => 'css',
										    'selector'      => '.fl-col-content',
										    'property'      => 'justify-content',
										)*/
									)
								)
							),
							'border'       => array(
								'title'         => __('Border', 'fl-builder'),
								'fields'        => array(
									'border_type'   => array(
										'toggle'        => array(
											''              => array(
												'fields'        => array()
											),
											'solid'         => array(
												'fields'        => array('hide_border_mobile')
											),
											'dashed'        => array(
												'fields'        => array('hide_border_mobile')
											),
											'dotted'        => array(
												'fields'        => array('hide_border_mobile')
											),
											'double'        => array(
												'fields'        => array('hide_border_mobile')
											)
										),
										'preview'         => array(
											'type'            => 'none'
										)
									),
									'hide_border_mobile'	=> array(
										'type'          => 'select',
										'label'         => __('Hide Border on Mobile', 'uabb'),
										'default'       => 'no',
										'options'       => array(
											'no'        	=> __('No', 'uabb'),
											'yes'         	=> __('Yes', 'uabb')
										),
										'preview'		=> array(
											'type'			=> 'none'
										)
									)
								)
							),
						)
					)
				)
			);
			$form = array_replace_recursive( ( array )$form, ( array )$column_form );
		}
		return $form;
	}