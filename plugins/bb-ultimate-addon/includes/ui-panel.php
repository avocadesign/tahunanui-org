<?php
/**
 * 	Row and Module UNCATEGORIZED Templates
 *
 * 	Showing Row and Module Templates from Local stored templates ( If agency version is installed ) and from templates.dat.
 *
 *	1. Getting Uncategorized rows from - Local 
 *	2. Getting Uncategorized rows from - templates.dat file
 *	3. Getting Uncategorized modules from - Local 
 *	4. Getting Uncategorized modules from - templates.dat file
 */
?>

<div class="fl-builder-panel">
	<div class="fl-builder-panel-actions">
		<i class="fl-builder-panel-close fa fa-times"></i>
	</div>
	<div class="fl-builder-panel-content-wrap fl-nanoscroller">
		<div class="fl-builder-panel-content fl-nanoscroller-content">
			<div class="fl-builder-blocks">

				<!-- Search Module -->
				<div id="fl-builder-blocks-rows" class="fl-builder-blocks-section">
					<input type="text" id="module_search" placeholder="Search Module..." style="width: 100%;">
					<div class="filter-count"></div>
				</div><!-- Search Module -->

				<div id="fl-builder-blocks-rows" class="fl-builder-blocks-section">
					<span class="fl-builder-blocks-section-title">
						<?php _e('Row Layouts', 'fl-builder'); ?>
						<i class="fa fa-chevron-down"></i>
					</span>
					<div class="fl-builder-blocks-section-content fl-builder-rows">
						<span class="fl-builder-block fl-builder-block-row" data-cols="1-col"><span class="fl-builder-block-title"><?php _e('1 Column', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="2-cols"><span class="fl-builder-block-title"><?php _e('2 Columns', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="3-cols"><span class="fl-builder-block-title"><?php _e('3 Columns', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="4-cols"><span class="fl-builder-block-title"><?php _e('4 Columns', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="5-cols"><span class="fl-builder-block-title"><?php _e('5 Columns', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="6-cols"><span class="fl-builder-block-title"><?php _e('6 Columns', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="left-sidebar"><span class="fl-builder-block-title"><?php _e('Left Sidebar', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="right-sidebar"><span class="fl-builder-block-title"><?php _e('Right Sidebar', 'fl-builder'); ?></span></span>
						<span class="fl-builder-block fl-builder-block-row" data-cols="left-right-sidebar"><span class="fl-builder-block-title"><?php _e('Left &amp; Right Sidebar', 'fl-builder'); ?></span></span>
					</div>
				</div>

				<div class="fl-builder-blocks-separator"></div>

				<?php foreach($categories as $title => $modules) : ?>
				<div id="fl-builder-blocks-<?php echo FLBuilderModel::get_module_category_slug( $title ); ?>" class="fl-builder-blocks-section">
					<span class="fl-builder-blocks-section-title">
						<?php echo $title; ?>
						<i class="fa fa-chevron-down"></i>
					</span>
					<?php if($title == __('WordPress Widgets', 'fl-builder')) : ?>
					<div class="fl-builder-blocks-section-content fl-builder-widgets">
						<?php foreach($modules as $module) : ?>
						<span class="fl-builder-block fl-builder-block-module" data-type="widget" data-widget="<?php echo $module->class; ?>"><span class="fl-builder-block-title"><?php echo $module->name; ?></span></span>
						<?php endforeach; ?>
					</div>
					<?php else : ?>
					<div class="fl-builder-blocks-section-content fl-builder-modules">
						<?php foreach($modules as $module) : ?>
						<span class="fl-builder-block fl-builder-block-module" data-type="<?php echo $module->slug; ?>"><span class="fl-builder-block-title"><?php echo $module->name; ?></span></span>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php endforeach; ?>

				<div class="fl-builder-blocks-separator"></div>

				<?php if (true !== FL_BUILDER_LITE) :

					$saved_rows    = FLBuilderModel::get_node_templates( 'row' );
					$saved_modules = FLBuilderModel::get_node_templates( 'module' );
					?>

					<?php if ( ! FLBuilderModel::is_post_user_template( 'row' ) ) : ?>
					<div id="fl-builder-blocks-saved-rows" class="fl-builder-blocks-section fl-builder-blocks-node-template">
						<span class="fl-builder-blocks-section-title">
							<?php _e('Saved Rows', 'fl-builder'); ?>
							<i class="fa fa-chevron-down"></i>
						</span>
						<div class="fl-builder-blocks-section-content fl-builder-saved-rows">
							<?php if ( 0 === count( $saved_rows ) ) : ?>
							<span class="fl-builder-block-no-node-templates"><?php _e( 'No saved rows found.', 'fl-builder' ); ?></span>
							<?php endif; ?>
							<?php foreach ( $saved_rows as $saved_row ) : ?>
							<span class="fl-builder-block fl-builder-block-saved-row<?php if ( $saved_row['global'] ) echo ' fl-builder-block-global'; ?>" data-id="<?php echo $saved_row['id']; ?>">
								<span class="fl-builder-block-title"><?php echo $saved_row['name']; ?></span>
								<?php if ( $saved_row['global'] ) : ?>
								<div class="fl-builder-badge fl-builder-badge-global">
									<?php _ex( 'Global', 'Indicator for global node templates.', 'fl-builder' ); ?>
								</div>
								<?php endif; ?>
								<span class="fl-builder-node-template-actions">
									<a class="fl-builder-node-template-edit" href="<?php echo add_query_arg( 'fl_builder', '', $saved_row['link'] ); ?>" target="_blank">
										<i class="fa fa-wrench"></i>
									</a>
									<a class="fl-builder-node-template-delete" href="javascript:void(0);">
										<i class="fa fa-times"></i>
									</a>
								</span>
							</span>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>

					<div id="fl-builder-blocks-saved-modules" class="fl-builder-blocks-section fl-builder-blocks-node-template">
						<span class="fl-builder-blocks-section-title">
							<?php _e('Saved Modules', 'fl-builder'); ?>
							<i class="fa fa-chevron-down"></i>
						</span>
						<div class="fl-builder-blocks-section-content fl-builder-saved-modules">
							<?php if ( 0 === count( $saved_modules ) ) : ?>
							<span class="fl-builder-block-no-node-templates"><?php _e( 'No saved modules found.', 'fl-builder' ); ?></span>
							<?php endif; ?>
							<?php foreach ( $saved_modules as $saved_module ) : ?>
							<span class="fl-builder-block fl-builder-block-saved-module<?php if ( $saved_module['global'] ) echo ' fl-builder-block-global'; ?>" data-id="<?php echo $saved_module['id']; ?>">
								<span class="fl-builder-block-title"><?php echo $saved_module['name']; ?></span>
								<?php if ( $saved_module['global'] ) : ?>
								<div class="fl-builder-badge fl-builder-badge-global">
									<?php _ex( 'Global', 'Indicator for global node templates.', 'fl-builder' ); ?>
								</div>
								<?php endif; ?>
								<span class="fl-builder-node-template-actions">
									<a class="fl-builder-node-template-edit" href="<?php echo $saved_module['link']; ?>" target="_blank">
										<i class="fa fa-wrench"></i>
									</a>
									<a class="fl-builder-node-template-delete" href="javascript:void(0);">
										<i class="fa fa-times"></i>
									</a>
								</span>
							</span>
							<?php endforeach; ?>
						</div>
					</div>

				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
