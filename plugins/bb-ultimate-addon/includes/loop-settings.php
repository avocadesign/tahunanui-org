<?php 

FLBuilderModel::default_settings($settings, array(
	'post_type' => 'post',
	'order_by'  => 'date',
	'order'     => 'DESC',
	'offset'    => 0,
	'users'     => '',
	'show_featured_image' => 'yes',
	'show_meta' => 'yes',
	'show_author' => 'yes',
	'show_date' => 'yes',
	'show_categories' => 'yes',
	'show_tags' => 'yes',
	'show_comments' => 'yes',
	'excerpt_count' => '300',
	'featured_image_size' => 'full'
));

?>
<div id="uabb-settings-section-general" class="fl-loop-builder uabb-settings-section">

	<table class="fl-form-table">
	<?php 
	
	// Post type
	FLBuilder::render_settings_field('post_type', array(
		'type'          => 'post-type',
		'label'         => __('Post Type', 'uabb'),
	), $settings); 
	
	// Order by
	FLBuilder::render_settings_field('order_by', array(
		'type'          => 'select',
		'label'         => __('Order By', 'uabb'),
		'options'       => array(
			'ID'            => __('ID', 'uabb'),
			'date'          => __('Date', 'uabb'),
			'modified'      => __('Date Last Modified', 'uabb'),
			'title'         => __('Title', 'uabb'),
			'author'        => __('Author', 'uabb'),
			'comment_count' => __('Comment Count', 'uabb'),
			'menu_order'    => __('Menu Order', 'uabb'),
			'rand'        	=> __('Random', 'uabb'),
		)
	), $settings); 
	
	// Order
	FLBuilder::render_settings_field('order', array(
		'type'          => 'select',
		'label'         => __('Order', 'uabb'),
		'default'       => 'DESC',
		'options'       => array(
			'DESC'          => __('Descending', 'uabb'),
			'ASC'           => __('Ascending', 'uabb'),
		)
	), $settings); 
	
	// Offset
	/*FLBuilder::render_settings_field('offset', array(
		'type'          => 'text',
		'label'         => _x('Offset', 'How many posts to skip.', 'uabb'),
		'default'       => '0',
		'size'          => '4',
		'help'          => __('Skip this many posts that match the specified criteria.', 'uabb')
	), $settings); */

	//Excerpt Count
	FLBuilder::render_settings_field('excerpt_count', array(
		'type' => 'text',
        'label' => __('Excerpt Count', 'uabb'),
        'help' => __('Limit Content Characters Count', 'uabb'),
        'default' => '300',
        'size' => '8',
	), $settings);

	?>
	</table>
</div>

<div id="uabb-settings-section-filter" class="uabb-settings-section">
	<h3 class="fl-builder-settings-title"><?php _e('Filter', 'uabb'); ?></h3>
	<?php foreach(FLBuilderLoop::post_types() as $slug => $type) : ?>
		<table class="fl-form-table fl-loop-builder-filter fl-loop-builder-<?php echo $slug; ?>-filter" <?php if($slug == $settings->post_type) echo 'style="display:table;"'; ?>>
		<?php 
		
		// Posts
		FLBuilder::render_settings_field('posts_' . $slug, array(
			'type'          => 'suggest',
			'action'        => 'fl_as_posts',
			'data'          => $slug,
			'label'         => $type->label,
			'help'          => sprintf(__('Enter a comma separated list of %s. Only these %s will be shown.', 'uabb'), $type->label, $type->label)
		), $settings); 
		
		// Taxonomies
		$taxonomies = FLBuilderLoop::taxonomies($slug);
		
		foreach($taxonomies as $tax_slug => $tax) {

			FLBuilder::render_settings_field('tax_' . $slug . '_' . $tax_slug, array(
				'type'          => 'suggest',
				'action'        => 'fl_as_terms',
				'data'          => $tax_slug,
				'label'         => $tax->label,
				'help'          => sprintf(__('Enter a comma separated list of %s. Only posts with these %s will be shown.', 'uabb'), $tax->label, $tax->label)
			), $settings); 
		}
		
		?>
		</table>
	<?php endforeach; ?>
	<table class="fl-form-table">
	<?php

	// Author
	FLBuilder::render_settings_field('users', array(
		'type'          => 'suggest',
		'action'        => 'fl_as_users',
		'label'         => __('Authors', 'uabb'),
		'help'          => __('Enter a comma separated list of authors usernames. Only posts with these authors will be shown.', 'uabb')
	), $settings); 
	
	?>
	</table>
</div>

<div id="uabb-settings-section-image_settings" class="uabb-settings-section">
	<h3 class="fl-builder-settings-title"><?php _e('Image Settings', 'uabb'); ?></h3>
	<table class="fl-form-table">
	<?php

	// Show featured image
	FLBuilder::render_settings_field('show_featured_image', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Image', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);

	// Featured image settings
	FLBuilder::render_settings_field('featured_image_size', array(
		'type'          => 'select',
        'label'         => __( 'Featured Image Size', 'uabb' ),
        'default'       => 'full',
        'help'			=> __( 'For Custom make sure cache is clear.', 'uabb' ),
        'options'       => array(
            'full' => __( 'Full', 'uabb' ),
            'large' => __( 'Large', 'uabb' ),
            'medium' => __( 'Medium', 'uabb' ),
            'thumbnail' => __( 'Thumbnail', 'uabb' ),
            'custom' => __( 'Custom', 'uabb' ),
        ),
	), $settings);


	// Featured image custom sizes
	FLBuilder::render_settings_field('featured_image_size_width', array(
		'type'          => 'text',
        'label'         => __( 'Custom Image Width', 'uabb' ),
        'description'   => 'px',
        'size'          => '8'
	), $settings);

	// Featured image custom sizes
	FLBuilder::render_settings_field('featured_image_size_height', array(
		'type'          => 'text',
        'label'         => __( 'Custom Image Height', 'uabb' ),
        'description'   => 'px',
        'size'          => '8'
	), $settings);
	
	?>
	</table>
</div>

<div id="uabb-settings-section-meta_settings" class="uabb-settings-section">
	<h3 class="fl-builder-settings-title"><?php _e('Meta Settings', 'uabb'); ?></h3>
	<table class="fl-form-table">
	<?php

	// Show Meta
	FLBuilder::render_settings_field('show_meta', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Meta', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
        'toggle'    => array(
	        'yes'    => array(
	            'fields'    => array( 'show_author', 'show_date', 'show_categories', 'show_tags', 'show_comments', 'meta_color' ),
	        )
	    ),
	), $settings);


	// Show Author
	FLBuilder::render_settings_field('show_author', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Author', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);

	// Show Author
	FLBuilder::render_settings_field('show_date', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Date', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);

	// Show Categories
	FLBuilder::render_settings_field('show_categories', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Categories', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);

	// Show Tags
	FLBuilder::render_settings_field('show_tags', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Tags', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);	    

	// Show Comments
	FLBuilder::render_settings_field('show_comments', array(
		'type'          => 'uabb-toggle-switch',
        'label'         => __( 'Show Comments', 'uabb' ),
        'default'       => 'yes',
        'options'       => array(
            'yes'       => 'Yes',
            'no'        => 'No',
        ),
	), $settings);


	// Show Comments
	FLBuilder::render_settings_field('meta_color', 
		BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
            array(
            	'label'         => __('Meta link color', 'uabb'),
            )
		), 
		$settings
	);
	
	?>
	</table>
</div>