<?php
/**
 * 	Row and Module Categorized & UnCategorized Templates
 *
 * 	Showing Row and Module Templates from Local stored templates ( If agency version is installed ) and from templates.dat.
 *
 * categorized rows from - templates.dat
 * uncategorized rows from - templates.dat file
 * categorized modules from - templates.dat
 * uncategorized modules from - templates.dat file
 */

?>

<div class="fl-builder-panel fl-builder-panel-ultimate-rows">
	<div class="fl-builder-panel-actions">
		<i class="fl-builder-panel-close fa fa-times"></i>
	</div>
	<div class="fl-builder-panel-content-wrap fl-nanoscroller">
		<div class="fl-builder-panel-content fl-nanoscroller-content">
			<div class="fl-builder-blocks">

				<!-- Search Section -->
				<div id="fl-builder-blocks-rows" class="fl-builder-blocks-section">
					<input type="text" id="section_search" placeholder="Search Section..." style="width: 100%;">
					<div class="filter-count"></div>
				</div><!-- Search Section -->

				<?php
				
				/**
				 * categorized rows - templates.dat
				 */
				?>
				<?php if ( ! $is_row_template && ! $is_module_template && $has_editing_cap ) { ?>

						<?php if ( count( $row_templates['categorized'] ) > 0 ) : ?>
							<?php foreach ( $row_templates['categorized'] as $cat ) : ?>

							<?php // Excluded 'Uncategorized' category which is showing in - [Saved Rows]
							if( trim($cat['name']) != 'Uncategorized' ) { ?>
								<div class="fl-builder-blocks-section">
									<span class="fl-builder-blocks-section-title">
										<?php echo __( $cat['name'] , 'uabb' ); ?>
										<i class="fa fa-chevron-down"></i>
									</span>
									<div class="fl-builder-blocks-section-content fl-builder-row-templates">
										<?php foreach ( $cat['templates'] as $template ) : ?>
										<span class="fl-builder-block fl-builder-block-template fl-builder-block-row-template" data-id="<?php echo $template['id']; ?>" data-type="<?php echo $template['type']; ?>">
											<?php if ( ! stristr( $template['image'], 'blank.jpg' ) ) : ?>
											<img class="fl-builder-block-template-image" src="<?php echo $template['image']; ?>" />
											<?php endif; ?>
											<span class="fl-builder-block-title"><?php echo $template['name']; ?></span>
										</span>
										<?php endforeach; ?>
									</div>
								</div>
							<?php } ?>
							<?php endforeach; ?>
						<?php endif; ?>

				<?php } ?>

				<div class="fl-builder-blocks-separator"></div>

				<?php
				/**
				 *	uncategorized rows - templates.dat
				 */
				$row_templates_uncategorized = 0;
				if( is_array( $row_templates ) ) {
					if( array_key_exists('uncategorized', $row_templates) ) {
						$row_templates_uncategorized = count( $row_templates['uncategorized'] );
					}
				}

				if( $is_row_template &&
					$has_editing_cap &&
					$row_templates_uncategorized > 0
				) { ?>

					<div class="fl-builder-blocks-section">
						<span class="fl-builder-blocks-section-title">
							<?php echo __( 'Saved Rows', 'uabb' ); ?>
							<i class="fa fa-chevron-down"></i>
						</span>

						<div class="fl-builder-blocks-section-content fl-builder-row-templates">
							<?php

							/**
							 *	4. Getting Uncategorized rows from - templates.dat file
							 */
							if ( ! $is_row_template && $has_editing_cap ) {
								if ( count( $row_templates['uncategorized'] ) > 0 ) :
									foreach ( $row_templates['uncategorized'] as $cat ) :
										// Excluded 'Uncategorized' category which is showing in - [Saved Rows]
										if( trim($cat['name']) == 'Uncategorized' ) {
											foreach ( $cat['templates'] as $template ) : ?>
												<span class="fl-builder-block fl-builder-block-template fl-builder-block-row-template" data-id="<?php echo $template['id']; ?>" data-type="<?php echo $template['type']; ?>">
													<?php if ( ! stristr( $template['image'], 'blank.jpg' ) ) : ?>
													<img class="fl-builder-block-template-image" src="<?php echo $template['image']; ?>" />
													<?php endif; ?>
													<span class="fl-builder-block-title"><?php echo $template['name']; ?></span>
												</span>
											<?php endforeach;
										}
									endforeach;
								endif;
							} ?>
						</div><!-- .fl-builder-row-templates -->
					</div>

					<?php do_action( 'uabb_fl_builder_ui_panel_after_rows' ); ?>

				<?php } ?>

				<?php

				/**
				 * categorized modules - templates.dat
				 */
				?>
				<?php if ( ! $is_module_template && $has_editing_cap ) { ?>				
					<?php if ( count( $module_templates['categorized'] ) > 0 ) : ?>
						<?php foreach ( $module_templates['categorized'] as $cat ) : ?>

						<?php // Excluded 'Uncategorized' category which is showing in - [Saved Modules]
							if( trim($cat['name']) != 'Uncategorized' ) { ?>
								<div class="fl-builder-blocks-section">
									<span class="fl-builder-blocks-section-title">
										<?php echo __( $cat['name'], 'uabb' ); ?>
										<i class="fa fa-chevron-down"></i>
									</span>
									<div class="fl-builder-blocks-section-content fl-builder-module-templates">
										<?php foreach ( $cat['templates'] as $template ) : ?>
										<span class="fl-builder-block fl-builder-block-template fl-builder-block-module-template" data-id="<?php echo $template['id']; ?>" data-type="<?php echo $template['type']; ?>">
											<?php if ( ! stristr( $template['image'], 'blank.jpg' ) ) : ?>
											<img class="fl-builder-block-template-image" src="<?php echo $template['image']; ?>" />
											<?php endif; ?>
											<span class="fl-builder-block-title"><?php echo $template['name']; ?></span>
										</span>
										<?php endforeach; ?>
									</div>
								</div>
							<?php } ?>

						<?php endforeach; ?>
					<?php endif; ?>
				<?php } ?>

				<?php do_action( 'uabb_fl_builder_ui_panel_after_modules' ); ?>

				<?php
				/**
				 * uncategorized modules - templates.dat
				 */
				$module_templates_uncategorized = 0;
				if( is_array( $module_templates ) ) {
					if( array_key_exists('uncategorized', $module_templates) ) {
						$module_templates_uncategorized = count( $module_templates['uncategorized'] );
					}
				}

				if( $is_module_template &&
					$has_editing_cap &&
					$module_templates_uncategorized > 0
				) { ?>

					<div class="fl-builder-blocks-section">
						<span class="fl-builder-blocks-section-title">
							<?php echo __( 'Saved Modules', 'uabb' ); ?>
							<i class="fa fa-chevron-down"></i>
						</span>

						<div class="fl-builder-blocks-section-content fl-builder-module-templates">
							<?php

							/**
							 *	Uncategorized templates.dat file
							 */
							if ( ! $is_module_template && $has_editing_cap ) {
								if ( count( $module_templates['uncategorized'] ) > 0 ) :
									foreach ( $module_templates['uncategorized'] as $cat ) :
										// Excluded 'Uncategorized' category which is showing in - [Saved Modules]
										if( trim($cat['name']) == 'Uncategorized' ) {
											foreach ( $cat['templates'] as $template ) : ?>
												<span class="fl-builder-block fl-builder-block-template fl-builder-block-module-template" data-id="<?php echo $template['id']; ?>" data-type="<?php echo $template['type']; ?>">
													<?php if ( ! stristr( $template['image'], 'blank.jpg' ) ) : ?>
													<img class="fl-builder-block-template-image" src="<?php echo $template['image']; ?>" />
													<?php endif; ?>
													<span class="fl-builder-block-title"><?php echo $template['name']; ?></span>
												</span>
											<?php endforeach;
										}
									endforeach;
								endif;
							} ?>
						</div><!-- .fl-builder-module-templates -->
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>
