<div id="fl-uabb-form" class="fl-settings-form uabb-fl-settings-form">

	<h3 class="fl-settings-form-header"><?php _e("UABB - General Settings", 'fl-builder'); ?></h3>

	<form id="uabb-form" action="<?php FLBuilderAdminSettings::render_form_action( 'uabb' ); ?>" method="post">

		<?php if ( FLBuilderAdminSettings::multisite_support() && ! is_network_admin() ) : ?>
		<label>
			<input class="fl-override-ms-cb" type="checkbox" name="fl-override-ms" value="1" <?php if(get_option('_fl_builder_uabb')) echo 'checked="checked"'; ?> />
			<?php _e('Override network settings?', 'fl-builder'); ?>
		</label>
		<?php endif; ?>

		<div class="fl-settings-form-content">

			<?php

				$uabb = BB_Ultimate_Addon::get_builder_uabb();

				$is_load_templates = $is_load_panels = $uabb_google_map_api = $uabb_colorpicker = '';
				if( is_array($uabb) ) {
					$is_load_templates 			= ( array_key_exists( 'load_templates', $uabb ) && $uabb['load_templates'] == 1 )  ? ' checked' : '';
					$is_load_panels 			= ( array_key_exists( 'load_panels', $uabb ) && $uabb['load_panels'] == 1 )  ? ' checked' : '';
					$uabb_google_map_api 		= ( array_key_exists( 'uabb-google-map-api', $uabb ) )  ? $uabb['uabb-google-map-api'] : '';
					$uabb_colorpicker		= ( array_key_exists( 'uabb-colorpicker', $uabb ) && $uabb['uabb-colorpicker'] == 1 )  ? ' checked' : '';
				} ?>

				<!-- Load Templates -->
				<p>
					<label>
						<input type="checkbox" class="uabb-load-templates" name="uabb-load-templates" value="" <?php echo $is_load_templates; ?> >
						<?php echo __( "Enable UABB Addon's Templates", 'fl-builder' ); ?>
					</label>
				</p>

				<!-- Load Panels -->
				<p>
					<label>
						<input type="checkbox" class="uabb-enabled-panels" name="uabb-enabled-panels" value="" <?php echo $is_load_panels; ?> >
						<?php echo __( "Enable UABB Addon's UI Design", 'fl-builder' ); ?>
					</label>
				</p>

				<!-- Color Picker -->
				<p>
					<label>
						<input type="checkbox" class="uabb-colorpicker" name="uabb-colorpicker" value="" <?php echo $uabb_colorpicker; ?> >
						<?php echo __( "Enable UABB Colorpicker", 'fl-builder' ); ?>
					</label>
				</p>

				<br/><hr/>

				<!-- Google Map API Key -->
				<p>
					<h4><?php _e( 'Google Map API Key', 'fl-builder' ); ?></h4>
					<input type="text" class="uabb-google-map-api" name="uabb-google-map-api" value="<?php echo $uabb_google_map_api; ?>" class="uabb-wp-text uabb-google-map-api" />
					<br><?php _e('Need help to get Google map API key? Read ', 'fl-builder'); ?> <a target="_blank" href="https://docs.brainstormforce.com/how-to-create-google-api-key-in-uabb-google-map-element/"><?php _e('this article', 'fl-builder'); ?></a>.
				</p>


		</div>

		<p class="submit">
			<input type="submit" name="fl-save-uabb" class="button-primary" value="<?php esc_attr_e( 'Save Settings', 'fl-builder' ); ?>" />
		</p>

		<?php wp_nonce_field('uabb', 'fl-uabb-nonce'); ?>

	</form>
</div>
