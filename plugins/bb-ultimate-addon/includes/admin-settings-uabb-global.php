<div id="fl-uabb-global-form" class="fl-settings-form uabb-global-fl-settings-form">

	<h3 class="fl-settings-form-header"><?php _e("UABB - Global Styling", 'fl-builder'); ?></h3>

	<form id="uabb-global-form" action="<?php FLBuilderAdminSettings::render_form_action( 'uabb-global' ); ?>" method="post">

		<?php if ( FLBuilderAdminSettings::multisite_support() && ! is_network_admin() ) : ?>
		<label>
			<input class="fl-override-ms-cb" type="checkbox" name="fl-override-ms" value="1" <?php if(get_option('_fl_builder_uabb_global')) echo 'checked="checked"'; ?> />
			<?php _e('Override network settings?', 'fl-builder'); ?>
		</label>
		<?php endif; ?>
		
		<div class="fl-settings-form-content">

			<?php
				// FLBuilderModel::update_admin_settings_option( '_fl_builder_uabb_global', '' );
				$uabb = BB_Ultimate_Addon::get_builder_uabb_global();
				$checked = '';

				$is_uabb_global = $uabb_theme_color = $uabb_text_color = $uabb_link_color = $uabb_link_hover_color = $uabb_button_color	= $uabb_button_hover_color = $uabb_button_bg_color = $uabb_button_bg_hover_color = $uabb_button_font_size = $uabb_button_line_height = $uabb_button_letter_spacing = $uabb_button_text_transform = $uabb_button_border_radius = $uabb_button_v_padding = $uabb_button_h_padding	= '';
				if( is_array($uabb) ) {

					//	Check Neble Disable Global
					$is_uabb_global 			= ( array_key_exists( 'uabb-global-enable', $uabb ) && $uabb['uabb-global-enable'] == 1 )  ? ' checked' : '';
					$button_text_transform 		= $uabb['uabb-button-text-transform'] != ''  ? $uabb['uabb-button-text-transform'] : 'none';
					$uabb_theme_color 			= ( array_key_exists( 'uabb-theme-color', $uabb ) ) ? $uabb['uabb-theme-color'] : '';
					$uabb_text_color 			= ( array_key_exists( 'uabb-text-color' , $uabb ) ) ? $uabb['uabb-text-color' ] : '';
	 				$uabb_link_color 			= ( array_key_exists( 'uabb-link-color' , $uabb ) ) ? $uabb['uabb-link-color' ] : '';
			 		$uabb_link_hover_color 		= ( array_key_exists( 'uabb-link-hover-color' , $uabb ) ) ? $uabb['uabb-link-hover-color' ] : '';
					$uabb_button_color 			= ( array_key_exists( 'uabb-button-color' , $uabb ) ) ? $uabb['uabb-button-color' ] : '';
					$uabb_button_hover_color 	= ( array_key_exists( 'uabb-button-hover-color' , $uabb ) ) ? $uabb['uabb-button-hover-color' ] : '';
					$uabb_button_bg_color 		= ( array_key_exists( 'uabb-button-bg-color' , $uabb ) ) ? $uabb['uabb-button-bg-color' ] : '';
					$uabb_button_bg_hover_color = ( array_key_exists( 'uabb-button-bg-hover-color' , $uabb ) ) ? $uabb['uabb-button-bg-hover-color' ] : '';
					$uabb_button_font_size 		= ( array_key_exists( 'uabb-button-font-size' , $uabb ) ) ? $uabb['uabb-button-font-size' ] : '';
					$uabb_button_line_height 	= ( array_key_exists( 'uabb-button-line-height' , $uabb ) ) ? $uabb['uabb-button-line-height' ] : '';
					$uabb_button_letter_spacing = ( array_key_exists( 'uabb-button-letter-spacing' , $uabb ) ) ? $uabb['uabb-button-letter-spacing' ] : '';
					$uabb_button_text_transform = ( array_key_exists( 'uabb-button-text-transform' , $uabb ) ) ? $uabb['uabb-button-text-transform' ] : '';
					$uabb_button_border_radius 	= ( array_key_exists( 'uabb-button-border-radius' , $uabb ) ) ? $uabb['uabb-button-border-radius' ] : '';
					$uabb_button_v_padding 		= ( array_key_exists( 'uabb-button-v-padding' , $uabb ) ) ? $uabb['uabb-button-v-padding' ] : '';
					$uabb_button_h_padding 		= ( array_key_exists( 'uabb-button-h-padding' , $uabb ) ) ? $uabb['uabb-button-h-padding' ] : '';
					
				} ?>

			<p>
				<label>
					<input type="checkbox" class="uabb-global-enable" name="uabb-global-enable" value="" <?php echo $is_uabb_global; ?> >
					<?php echo __( "Enable UABB Global Styling", 'fl-builder' ); ?>
				</label>
			</p>

			<?php /* Theme Color theme-color */ ?> 
			<div class="uabb-admin-fields" style="margin-top: 30px;">
			<h4 class="field-title"><?php _e( 'Theme Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-theme-color" value="<?php echo $uabb_theme_color; ?>" class="uabb-wp-colopicker uabb-theme-color" />
			</div>

		
			<?php /* Text Color text-color */ ?> 
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Text Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-text-color" value="<?php echo $uabb_text_color; ?>" class="uabb-wp-colopicker uabb-text-color" />
			</div>
			
			<?php /* Link Color link-color */ ?> 
			<?php /*<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Link Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-link-color" value="<?php echo $uabb_link_color; ?>" class="uabb-wp-colopicker uabb-link-color" />
			</div> */?>

			<?php /* link-hover-color */ ?>
			<?php /*<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Link Hover Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-link-hover-color" value="<?php echo $uabb_link_hover_color; ?>" class="uabb-wp-colopicker uabb-link-hover-color" />
			</div> */ ?>

			<?php /* Text button-color*/ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Text Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-button-color" value="<?php echo $uabb_button_color; ?>" class="uabb-wp-colopicker uabb-button-color" />
			</div>

			<?php /* Text button-hover-color*/ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Text Hover Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-button-hover-color" value="<?php echo $uabb_button_hover_color; ?>" class="uabb-wp-colopicker uabb-button-hover-color" />
			</div> 

			<?php /* button-bg-color */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Background Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-button-bg-color" value="<?php echo $uabb_button_bg_color; ?>" class="uabb-wp-colopicker uabb-button-bg-color" />
			</div>

			<?php /* button-bg-hover-color*/ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Background Hover Color', 'fl-builder' ); ?></h4>
			<input type="text" name="uabb-button-bg-hover-color" value="<?php echo $uabb_button_bg_hover_color; ?>" class="uabb-wp-colopicker uabb-button-bg-hover-color" />
			</div>
			
			<?php /* button-font-size */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Font Size', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-font-size" value="<?php echo $uabb_button_font_size; ?>" class="uabb-wp-number uabb-button-font-size" /><span class="uabb-admin-field-desc"> px</span>
			</div>

			<?php /* button-line-height */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Line Height', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-line-height" value="<?php echo $uabb_button_line_height; ?>" class="uabb-wp-number uabb-button-line-height" /><span class="uabb-admin-field-desc"> px</span>
			</div>

			<?php /* button-letter-spacing */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Letter Spacing', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-letter-spacing" value="<?php echo $uabb_button_letter_spacing; ?>" class="uabb-wp-number uabb-button-letter-spacing" /><span class="uabb-admin-field-desc"> px</span>
			</div>

			<?php /* button-text-transform */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Text Transform', 'fl-builder' ); ?></h4>
			<select name="uabb-button-text-transform">
				<option value="none" 		<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'none' ); } ?>><?php _e( 'None', 'fl-builder' ); ?></option>
				<option value="capitalize" 	<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'capitalize' ); } ?>><?php _e( 'Capitalize', 'fl-builder' ); ?></option>
				<option value="uppercase" 	<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'uppercase' ); } ?>><?php _e( 'Uppercase', 'fl-builder' ); ?></option>
				<option value="lowercase" 	<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'lowercase' ); } ?>><?php _e( 'Lowercase', 'fl-builder' ); ?></option>
				<option value="initial" 	<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'initial' ); } ?>><?php _e( 'Initial', 'fl-builder' ); ?></option>
				<option value="inherit" 	<?php if( !empty( $button_text_transform ) ) { selected( $button_text_transform, 'inherit' ); } ?>><?php _e( 'Inherit', 'fl-builder' ); ?></option>
			</select>
			</div>
			
			<?php /* button-border-radius */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Border Radius', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-border-radius" value="<?php echo $uabb_button_border_radius; ?>" class="uabb-wp-number uabb-button-border-radius" /><span class="uabb-admin-field-desc"> px</span>
			</div>

			<?php /* button-v-padding */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Vertical Padding', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-v-padding" value="<?php echo $uabb_button_v_padding; ?>" class="uabb-wp-number uabb-button-v-padding" /><span class="uabb-admin-field-desc"> px</span>
			</div>

			<?php /* button-h-padding */ ?>
			<div class="uabb-admin-fields">
			<h4 class="field-title"><?php _e( 'Button Horizontal Padding', 'fl-builder' ); ?></h4>
			<input type="number" name="uabb-button-h-padding" value="<?php echo $uabb_button_h_padding; ?>" class="uabb-wp-number uabb-button-h-padding" /><span class="uabb-admin-field-desc"> px</span>
			</div>

		</div>

		<p class="submit">
			<input type="submit" name="fl-save-uabb-global" class="button-primary" value="<?php esc_attr_e( 'Save Settings', 'fl-builder' ); ?>" />
			
			<?php wp_nonce_field('uabb-global', 'fl-uabb-global-nonce'); ?>
		</p>
	</form>
</div>
