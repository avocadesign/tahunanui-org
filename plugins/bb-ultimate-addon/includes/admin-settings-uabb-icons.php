<div id="fl-uabb-icons-form" class="fl-settings-form uabb-fl-settings-form">

	<h3 class="fl-settings-form-header"><?php _e('Reload Ultimate Icons', 'fl-builder'); ?></h3>
	
	<form id="uabb-icons-form" action="<?php FLBuilderAdminSettings::render_form_action( 'uabb-icons' ); ?>" method="post">
		
		<div class="fl-settings-form-content">

			<p><?php echo __( 'Clicking the button below will reinstall Ultimate icons on your website. If you are facing issues to load Ultimate icons then you are at right place to troubleshoot it.', 'uabb' ); ?></p>
			<span class="button uabb-reload-icons">
				<i class="dashicons dashicons-update" style="padding: 3px;"></i>
				<?php esc_attr_e( 'Reload Ultimate Icons', 'fl-builder' ); ?>
			</span>

		</div>
	</form>
</div>
