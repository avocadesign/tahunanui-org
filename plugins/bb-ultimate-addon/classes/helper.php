<?php

/**
 * 		UABB Helper
 *
 * 	Helper functions, actions & filter hooks etc.
 */

if( !class_exists( 'UABB_Helper' ) ) {

	class UABB_Helper
	{
		public static $uabb_options;
		public static $enable_colorpicker;

		static public function set_colorpicker_options(){
			self::$uabb_options = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb', true );
			self::$enable_colorpicker = true;

			if ( !empty( self::$uabb_options ) ) {
					self::$enable_colorpicker = true;
					if ( array_key_exists( 'uabb-colorpicker', self::$uabb_options ) ) {
						if ( self::$uabb_options['uabb-colorpicker'] == 1 ) {
							self::$enable_colorpicker = true;
						}else{
							self::$enable_colorpicker = false;
						}
						
					}
				}
			/*if ( !empty( self::$uabb_options ) ) {
				self::$enable_colorpicker = ( array_key_exists( 'uabb-colorpicker', self::$uabb_options ) && self::$uabb_options['uabb-colorpicker'] == 1 )  ? true : false;
			}*/
		}
		/**
		 * Helper function to render css styles for a selected font.
		 *
		 * @since  1.0
		 * @param  array $font An array with font-family and weight.
		 * @return void
		 */
		static public function uabb_font_css( $font ){
			$css = '';
			
			if( array_key_exists( $font['family'], FLBuilderFontFamilies::$system ) ){
				$css .= 'font-family: '. $font['family'] .','. FLBuilderFontFamilies::$system[ $font['family'] ]['fallback'] .';';
			} else {
				$css .= 'font-family: '. $font['family'] .';';
			}

			if( $font['weight'] == 'regular' ) {
				$css .= 'font-weight: normal;';
			}else {
				$css .= 'font-weight: '. $font['weight'] .';';
			}	
			
			echo $css;
		}

		/**
		 *  Get - Color
		 *
		 * Get HEX color and return RGBA. Default return HEX color.
		 *
		 * @param   $hex        HEX color code
		 * @param   $opacity    Opacity of HEX color
		 * @return  $rgba       Return RGBA if opacity is set. Default return HEX.
		 * @since 1.0
		 */
		static public function uabb_get_color( $hex, $opacity )
		{
		    $rgba = $hex;
		    if( $opacity != '' ) {
		        if(strlen( $hex ) == 3) {
		            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
		        } else {
		            $r = hexdec(substr($hex,0,2));
		            $g = hexdec(substr($hex,2,2));
		            $b = hexdec(substr($hex,4,2));
		        }
		        return 'rgba( ' . $r . ', ' . $g . ', ' . $b . ', ' . $opacity . ' )';
		    } else {
		        return '#' . $hex;
		    }
		}

		/**
		 *	Get - RGBA Color
		 *
		 *  Get HEX color and return RGBA. Default return RGB color.
		 *
		 * @param   $hex        HEX color code
		 * @param   $opacity    Opacity of HEX color
		 * @return  $rgba       Return RGBA if opacity is set. Default return RGB.
		 * @since 	1.0
		 */
		static public function uabb_hex2rgba($color, $opacity = false, $is_array = false ) {
 
			$default = $color;
		 
			//Return default if no color provided
			if(empty($color))
		          return $default; 
		 
			//Sanitize $color if "#" is provided 
	        if ($color[0] == '#' ) {
	        	$color = substr( $color, 1 );
	        }
	 
	        //Check if color has 6 or 3 characters and get values
	        if (strlen($color) == 6) {
	                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	        } elseif ( strlen( $color ) == 3 ) {
	                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	        } else {
	                return $default;
	        }
	 
	        //Convert hexadec to rgb
	        $rgb =  array_map('hexdec', $hex);
	 
	        //Check if opacity is set(rgba or rgb)
	       if($opacity){
	        	if(abs($opacity) > 1) {
	        		//$opacity = 1.0;
	        		$opacity = $opacity / 100;
	        	}
	        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
	        } else {
	        	$output = 'rgb('.implode(",",$rgb).')';
	        }
	 
	 		if ( $is_array ) {
	        	return $rgb;
	 		}else{
	        	//Return rgb(a) color string
	        	return $output;
	 		}
		}

		/**
		 *	Get - Colorpicker Value based on colorpicker
		 *
		 * @param   $hex        HEX color code
		 * @param   $opacity    Opacity of HEX color
		 * @return  $rgba       Return RGBA if opacity is set. Default return RGB.
		 * @since 	1.0
		 */
		static public function uabb_colorpicker( $settings, $name = '', $opc = false ) {
			
			$hex_color = $opacity = '';
			$hex_color = $settings->$name;
			
			if ( apply_filters( 'uabb_colorpicker', true ) == true && self::$enable_colorpicker ) {
				if ( $hex_color != '' && $hex_color[0] != 'r' && $hex_color[0] != '#' ) {
			      
					return '#'.$hex_color;
				}
				return $hex_color;
			}else{
				
				if ( $hex_color != '' ) {
					$hex_color = $hex_color;
					
			        //var_dump( $name );
			        //echo "From Function\n";
					//var_dump( $hex_color );
					//var_dump( $settings->{ $name.'_opc' } );
			        //echo "\nFrom Function End\n";
					
					if ( $opc == true && $settings->{ $name.'_opc' }  != '' ) {
						$opacity 	= $settings->{ $name.'_opc' };
						$rgba 		= self::uabb_hex2rgba( $hex_color, $opacity  );
						return $rgba;
					}
				}


				if ( $hex_color != '' && $hex_color[0] != '#' ) {
					
			        //$hex_color = substr( $hex_color, 1 );
			       
					return '#'.$hex_color;
				}
				
				return $hex_color;
			}
		}
	}

	UABB_Helper::set_colorpicker_options();
}
