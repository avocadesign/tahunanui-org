<?php
/*
 *	Switch Param
 */

if(!class_exists('UABB_Switch_Param'))
{
	class UABB_Switch_Param
	{
		function __construct()
		{	
			add_action( 'fl_builder_control_uabb-switch', array($this, 'uabb_switch_field'), 1, 4 );
			add_action( 'wp_enqueue_scripts', array( $this, 'switch_scripts' ) );
		}

		function switch_scripts() {
		    if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
				wp_enqueue_style( 'switch-styles', plugins_url( 'css/uabb-switch.css', __FILE__ ) );
				wp_enqueue_script( 'switch-scripts', plugins_url( 'js/uabb-switch.js', __FILE__ ), array('jquery'), '', true );
		    }
		}
		
		function uabb_switch_field($name, $value, $field, $settings) {

			// Options Set by User or Default Options
			$options = isset( $field['options'] ) ? $field['options'] : array( 'on' => 'ON', 'off' => 'OFF');
			
			// Check minimum 2 options Provided 
			if( count($options) >= 2 ) {

				// Set Default value
				$settings->{$name} = ( isset( $settings->{$name} ) ) ? $settings->{$name} : '';
				
				$checked = '';

				$toggle = isset( $field['toggle'] ) ? json_encode($field['toggle']) : json_encode(array());
				
				$keys = array_keys($options);
				
				if(!isset($settings->{$name}) || !empty($settings->{$name})) {
					if( isset($field->default) && !empty($settings->{$name})) {
						$settings->{$name} = $field->default;
					} else {
						$settings->{$name} = $keys[0];
					}
				}

				if( $keys[0] == $settings->{$name} ) {
					$checked = 'checked="checked"';
				}

				echo '<div class="fl-fields uabb_switch_wrap">';
				echo '<label class="uabb-switch uabb-switch-left-right">
						<input class="uabb_switch_input" type="checkbox" '. $checked .' data-first="'.$keys[0].'" data-second="'.$keys[1].'"/>
						<span class="uabb_switch-label" data-on="'. $options[$keys[0]] .'" data-off="'. $options[$keys[1]] .'"></span> 
						<span class="uabb-switch-handle"></span> 
					</label>';

				echo '<select class="uabb_switch_hidden uabb_switch_' . $name . '" name="' . $name . '" data-toggle='. $toggle .'>';
				
				foreach ($options as $key => $value) {
					
					$selected = '';
					if( $settings->{$name} == $key ) {
						$selected = ' selected="selected"';
					}
					echo '<option value="'. $key .'" '. $selected .'>'. $value .'</option>';	
				}
				echo '</select>';

				echo '</div>';
			}
		}

		static function get_border_frontend( $settings, $name ) {
			
		}
		
	}

	$UABB_Switch_Param = new UABB_Switch_Param();
}
?>