(function($){

	jQuery(document).ready(function() {

		UABBFLBuilder = {

			_timeout            : null,

			_xhr				: null,

			_init : function() {

				UABBFLBuilder._bindEvents();
			},

			_initSwitchFields: function()
			{
				$('.fl-builder-settings:visible').find('.fl-builder-settings-fields input.uabb_switch_input').trigger('change');
			},

			_bindEvents: function() {
				
				/* Select Fields */
				$('body').delegate('.fl-builder-settings-fields input.uabb_switch_input', 'change', UABBFLBuilder._settingsSwitchChanged);
			},

			_settingsSwitchChanged: function() {
				
				var switch_wrap = jQuery(this).closest('.uabb_switch_wrap'),
				switch_hidden = switch_wrap.find('.uabb_switch_hidden'),
				toggle = switch_hidden.data('toggle');

				if( jQuery(this).attr('checked') == "checked" ) {
					checkbox_value = jQuery(this).data('first');
				} else {
					checkbox_value = jQuery(this).data('second');
				}

				switch_hidden.find('option').removeAttr('selected');
				switch_hidden.find('option[value="'+checkbox_value+'"]').attr('selected','selected');


				var current = switch_hidden.val();

				// TOGGLE sections, fields or tabs.
				if(typeof toggle !== 'undefined') {
				
					for(i in toggle) {
						UABBFLBuilder._settingsSwitchToggle(toggle[i].fields, 'hide', '#fl-field-');
						UABBFLBuilder._settingsSwitchToggle(toggle[i].sections, 'hide', '#fl-builder-settings-section-');
						UABBFLBuilder._settingsSwitchToggle(toggle[i].tabs, 'hide', 'a[href*=fl-builder-settings-tab-', ']');
					}
					
					if(typeof toggle[current] !== 'undefined') {
						UABBFLBuilder._settingsSwitchToggle(toggle[current].fields, 'show', '#fl-field-');
						UABBFLBuilder._settingsSwitchToggle(toggle[current].sections, 'show', '#fl-builder-settings-section-');
						UABBFLBuilder._settingsSwitchToggle(toggle[current].tabs, 'show', 'a[href*=fl-builder-settings-tab-', ']');
					}
				}

				UABBFLBuilder.delayPreview(this);
			},

			_settingsSwitchToggle: function (inputArray, func, prefix, suffix) {

				var i = 0;
				
				suffix = 'undefined' == typeof suffix ? '' : suffix;
				
				if(typeof inputArray !== 'undefined') {
					for( ; i < inputArray.length; i++) {
						$(prefix + inputArray[i] + suffix)[func]();
					}
				}
			},

			delay: function(length, callback)
			{
				UABBFLBuilder._cancelDelay();
				UABBFLBuilder._timeout = setTimeout(callback, length);
			},

			_cancelDelay: function()
			{
				if(UABBFLBuilder._timeout !== null) {
					clearTimeout(UABBFLBuilder._timeout);
				}
			},

			delayPreview: function(e)
			{
				var heading         = typeof e == 'undefined' ? [] : jQuery(e).closest('tr').find('th'),
					widgetHeading   = jQuery('.fl-builder-widget-settings .fl-builder-settings-title'),
					lightboxHeading = jQuery('.fl-builder-settings .fl-lightbox-header'),
					loaderSrc       = FLBuilderLayoutConfig.paths.pluginUrl + 'img/ajax-loader-small.gif',
					loader          = jQuery('<img class="fl-builder-preview-loader" src="' + loaderSrc + '" />');
				
				jQuery('.fl-builder-preview-loader').remove();

				if(heading.length > 0) {
					heading.append(loader);
				}
				else if(widgetHeading.length > 0) {
					widgetHeading.append(loader);
				}
				else if(lightboxHeading.length > 0) {
					lightboxHeading.append(loader);
				}
				UABBFLBuilder.delay(1000, jQuery.proxy(UABBFLBuilder.preview, UABBFLBuilder));
			},

			preview: function() 
			{
				var form     = jQuery('.fl-builder-settings-lightbox .fl-builder-settings'),
					nodeId   = form.attr('data-node'),
					settings = FLBuilder._getSettings(form);
				
				// Abort an existing preview request. 
				UABBFLBuilder._cancelPreview();

				// Make a new preview request.
				UABBFLBuilder._xhr = FLBuilder.ajax({
					action          : 'render_layout',
					node_id         : nodeId,
					node_preview    : settings
				
				}, function(response) {
					
					UABBFLBuilder._xhr = null;
					
					FLBuilder._renderLayout(response, function() {
						
						// Remove the loading graphic.
						jQuery('.fl-builder-preview-loader').remove();
					   
						// Fire the preview rendered event. 
						jQuery( FLBuilder._contentClass ).trigger( 'fl-builder.preview-rendered' );
					});
				
				});
			},

			_cancelPreview: function() 
			{
				if(UABBFLBuilder._xhr) {
					UABBFLBuilder._xhr.abort();
					UABBFLBuilder._xhr = null;
				}
			},

		};

		UABBFLBuilder._init();
		
	});
})(jQuery);