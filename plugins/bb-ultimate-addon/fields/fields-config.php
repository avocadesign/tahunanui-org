<?php
/*
 * Custom Fields Config File
 * Description: This is custom fields config file. Require your custom field's "main" file here.
 *
*/


require_once 'uabb-simplify/uabb-simplify.php';
require_once 'uabb-spacing/uabb-spacing.php';
require_once 'uabb-switch/uabb-switch.php';
require_once 'uabb-color/uabb-color.php';
require_once 'uabb-toggle-switch/uabb-toggle-switch.php';
require_once 'uabb-blank-spacer/uabb-blank-spacer.php';
require_once 'uabb-hide-field/uabb-hide-field.php';