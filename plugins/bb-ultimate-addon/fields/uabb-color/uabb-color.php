<?php
/*
 *	Color Picker Param
 */

if( !class_exists( 'UABB_ColorPicker_Param' ) ) {

	class UABB_ColorPicker_Param {


		function __construct()
		{	
			add_action('fl_builder_control_uabb-color', array( $this, 'uabb_color_picker' ), 1, 4);
			add_action( 'wp_enqueue_scripts', array( $this, 'uabb_color_assets' ), 100 );

			//add_action( 'fl_builder_control_uabb-color', array($this, 'uabb_switch_field'), 1, 4 );
			//add_action( 'wp_enqueue_scripts', array( $this, 'switch_scripts' ) );
		}

		function switch_scripts() {
		    if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
				wp_enqueue_style( 'switch-styles', plugins_url( 'css/uabb-switch.css', __FILE__ ) );
				wp_enqueue_script( 'switch-scripts', plugins_url( 'js/uabb-switch.js', __FILE__ ), array('jquery'), '', true );
		    }
		}


		/**
		 * Custom fields
		 */
		function uabb_color_picker( $name, $value, $field, $settings ) {

			$default = '';
			$hidden_val = '';

			if( isset( $field['default'] ) ) {
				if( $field['default'] != '' ) {
					$default = $field['default'];
				}
			}

			/*if( isset( $value ) ) {
				if( $value != '' ) {
					if ( strpos( $value, 'rgba' ) !== false ) {
						$value = $value;
					}
				}
			}*/
			//var_dump($value);

			//if( $value == '' ) {
			//	$hidden_val = $default;
			//}

			$preview = isset( $field['preview'] ) ? json_encode( $field['preview'] ) : json_encode( array( 'type' => 'refresh' ) );

		    //echo '<input type="text" class="cs-wp-color-picker fl-field" data-default-color="' . $default . '" value="' . $value . '" /><input type="hidden" name="' . $name . '" class="fl-color-picker-value" value="' . $new_value . '" data-type="color" data-preview=\'' . $preview . '\' />';

		    //echo '<input type="text" class="cs-wp-color-picker" data-default-color="' . $default . '" value="' . $value . '" /><input type="hidden" name="' . $name . '" class="fl-color-picker-value fl-field" value="' . $value . '" data-type="text" data-preview=\'' . $preview . '\' />';
			echo '<div class="color-all-wrap-ajax fl-field" data-type="color" data-preview=\'' . $preview . '\'>';
			echo 	'<input type="text" class="cs-wp-color-picker" data-default-color="' . $default . '" value="' . $value . '" />
		    		<input type="hidden" name="' . $name . '" class="fl-color-picker-value" value="' . $value . '"/>';
		    /*echo 	'<input type="text" class="cs-wp-color-picker" data-default-color="' . $default . '" value="' . $value . '" />
		    		<input type="hidden" name="' . $name . '" class="fl-color-picker-value fl-field" value="' . $value . '" data-type="text" data-preview=\'' . $preview . '\' />';*/
		    echo "</div>";
		}


		/**
		 * Custom field styles and scripts
		 */
		function uabb_color_assets() {
		    if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
				wp_enqueue_script(
				    'iris',
				    admin_url( 'js/iris.min.js' ),
				    array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
				    false,
				    1
				);
				wp_enqueue_script(
				    'wp-color-picker-new',
				    admin_url( 'js/color-picker.min.js' ),
				    array( 'iris' ),
				    false,
				    1
				);
				wp_enqueue_style( 'wp-color-picker' );
				wp_enqueue_style( 'cs-wp-color-picker', BB_ULTIMATE_ADDON_URL . 'fields/uabb-color/css/cs-wp-color-picker.css', array( 'wp-color-picker' ), '1.0.0', 'all' );
				wp_enqueue_script( 'cs-wp-color-picker', BB_ULTIMATE_ADDON_URL . 'fields/uabb-color/js/cs-wp-color-picker.js', array( 'wp-color-picker-new' ), '1.0.0', true );

				$colorpicker_l10n = array(
				    'clear' => __( 'Clear' ),
				    'colorPresets' => $this->uabb_color_presets(),
				    'defaultString' => __( 'Default' ),
				    'pick' => __( 'Select Color' ),
				    'current' => __( 'Current Color' ),
				);
				wp_localize_script( 'wp-color-picker-new', 'wpColorPickerL10n', $colorpicker_l10n );
		    }
		}

		function uabb_color_presets() {
			$colors = array( '#8224e3', '#1e73be', '#81d742', '#eeee22', '#dd9933', '#dd3333', '#ffffff', '#000000' );
			$colors_bb = FLBuilderModel::get_color_presets();

			if( is_array($colors_bb) && count($colors_bb) > 0 ) {
				foreach($colors_bb as &$value) { $value = '#'.$value; }

				//	If less than 8 then merge default
				if( count( $colors_bb ) < 8 ) {
					$colors = array_slice($colors, count( $colors_bb ) );
					$colors = array_merge( $colors_bb, $colors );
				} else {
					$colors = $colors_bb;
				}
			}

			return $colors;
		}

	}

	//Instantiation
	new UABB_ColorPicker_Param();
}
?>
