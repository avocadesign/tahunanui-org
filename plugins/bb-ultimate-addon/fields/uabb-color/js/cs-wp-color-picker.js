/**
 *
 * Codestar WP Color Picker v1.1.0
 * This is plugin for WordPress Color Picker Alpha Channel
 *
 * Copyright 2015 Codestar <info@codestarlive.com>
 * GNU GENERAL PUBLIC LICENSE (http://www.gnu.org/licenses/gpl-2.0.txt)
 *
 */
;(function ( $, window, document, undefined ) {
  'use strict';

  // adding alpha support for Automattic Color.js toString function.
  if( typeof Color.fn.toString !== undefined ) {

    Color.fn.toString = function () {

      // check for alpha
      if ( this._alpha < 1 ) {
        return this.toCSS('rgba', this._alpha).replace(/\s+/g, '');
      }

      var hex = parseInt( this._color, 10 ).toString( 16 );

      if ( this.error ) { return ''; }

      // maybe left pad it
      if ( hex.length < 6 ) {
        for (var i = 6 - hex.length - 1; i >= 0; i--) {
          hex = '0' + hex;
        }
      }

      return '#' + hex;

    };

  }

  $.cs_ParseColorValue = function( val ) {

    var value = val.replace(/\s+/g, ''),
        alpha = ( value.indexOf('rgba') !== -1 ) ? parseFloat( value.replace(/^.*,(.+)\)/, '$1') * 100 ) : 100,
        rgba  = ( alpha < 100 ) ? true : false;

    return { value: value, alpha: alpha, rgba: rgba };

  };

  $.fn.cs_wpColorPicker = function() {

    return this.each(function() {

      var $this = $(this);

      // check for rgba enabled/disable
      if( $this.data('rgba') !== false ) {

        // parse value
        var picker = $.cs_ParseColorValue( $this.val() );

        // wpColorPicker core
        $this.wpColorPicker({
          palettes: wpColorPickerL10n.colorPresets,

          // wpColorPicker: clear
          clear: function() {
            $this.trigger('keyup');
            //console.log(this);
            var field       = $(this).closest('.color-all-wrap-ajax'),
                color_change = field.find('.fl-color-picker-value'),
                preview     = field.data('preview');
                
            if ( 'css' == preview.type ) {  
              var uabb_node = FLBuilder._contentClass+' .fl-node-' + field.closest('.fl-builder-settings').data('node'),
                  selector = FLBuilder.preview._getPreviewSelector( uabb_node, preview.selector ),
                  property = preview.property,
                  color_value    = 'inherit';
                  FLBuilder.preview.updateCSSRule(selector, property, color_value);
            }else{
                color_change.trigger( 'change' );
            }
              
          },

          // wpColorPicker: change
          change: function( event, ui ) {
            //console.log('Its change now');
            var ui_color_value = ui.color.toString();
            $this.closest('.wp-picker-container').find('.cs-alpha-slider-offset').css('background-color', ui_color_value);
            $this.closest('.wp-picker-container').parent().find('.fl-color-picker-value').val(ui_color_value);
            $this.val(ui_color_value).trigger('change');
            
            var input   = jQuery( this ),
                preview = input.closest( '.fl-field' ).data( 'preview' ),
                i       = null;
            //console.log(preview);
            if ( 'css' == preview.type ) {  
                if ( 'undefined' != typeof preview.rules ) {
                    for ( i in preview.rules ) {
                        FLBuilder.preview._previewCSS( preview.rules[ i ], event );
                    }
                }
                else {
                    FLBuilder.preview._previewCSS( preview, event );
                }
            }else{
              var field       = $(this).closest('.color-all-wrap-ajax'),
                color_change = field.find('.fl-color-picker-value'),
                preview     = field.data('preview');
                color_change.trigger( 'change' );
          }
            
          },

          // wpColorPicker: create
          create: function() {

            // set variables for alpha slider
            var a8cIris       = $this.data('a8cIris'),
                $container    = $this.closest('.wp-picker-container'),

                // appending alpha wrapper
                $alpha_wrap   = $('<div class="cs-alpha-wrap">' +
                                  '<div class="cs-alpha-slider"></div>' +
                                  '<div class="cs-alpha-slider-offset"></div>' +
                                  '<div class="cs-alpha-text"></div>' +
                                  '</div>').appendTo( $container.find('.wp-picker-holder') ),

                $alpha_slider = $alpha_wrap.find('.cs-alpha-slider'),
                $alpha_text   = $alpha_wrap.find('.cs-alpha-text'),
                $alpha_offset = $alpha_wrap.find('.cs-alpha-slider-offset');

            // alpha slider
            $alpha_slider.slider({

              // slider: slide
              slide: function( event, ui ) {

                var slide_value = parseFloat( ui.value / 100 );

                // update iris data alpha && wpColorPicker color option && alpha text
                a8cIris._color._alpha = slide_value;
                $this.wpColorPicker( 'color', a8cIris._color.toString() );
                $alpha_text.text( ( slide_value < 1 ? slide_value : '' ) );

              },

              // slider: create
              create: function() {

                var slide_value = parseFloat( picker.alpha / 100 ),
                    alpha_text_value = slide_value < 1 ? slide_value : '';

                // update alpha text && checkerboard background color
                $alpha_text.text(alpha_text_value);
                $alpha_offset.css('background-color', picker.value);

                // wpColorPicker clear for update iris data alpha && alpha text && slider color option
                $container.on('click', '.wp-picker-clear', function() {

                  a8cIris._color._alpha = 1;
                  $alpha_text.text('');
                  $alpha_slider.slider('option', 'value', 100).trigger('slide');
                  $this.closest('.wp-picker-container').parent().find('.fl-color-picker-value').val('');   
                  /*var input   = jQuery( this ),
                      preview = input.closest( '.fl-field' ).data( 'preview' ),
                      i       = null;
                  //console.log(preview);
                  if ( 'css' == preview.type ) {  
                      if ( 'undefined' != typeof preview.rules ) {
                          for ( i in preview.rules ) {
                              FLBuilder.preview._previewCSS( preview.rules[ i ], event );
                          }
                      }
                      else {
                          FLBuilder.preview._previewCSS( preview, event );
                      }
                  }*/

                });

                // wpColorPicker default button for update iris data alpha && alpha text && slider color option
                $container.on('click', '.wp-picker-default', function() {

                  var default_picker = $.cs_ParseColorValue( $this.data('default-color') ),
                      default_value  = parseFloat( default_picker.alpha / 100 ),
                      default_text   = default_value < 1 ? default_value : '';

                  a8cIris._color._alpha = default_value;
                  $alpha_text.text(default_text);
                  $alpha_slider.slider('option', 'value', default_picker.alpha).trigger('slide');

                });

                // show alpha wrapper on click color picker button
                $container.on('click', '.wp-color-result', function() {
                  $alpha_wrap.toggle();
                });

                // hide alpha wrapper on click body
                $('body').on( 'click.wpcolorpicker', function() {
                  $alpha_wrap.hide();
                });

              },

              // slider: options
              value: picker.alpha,
              step: 1,
              min: 1,
              max: 100

            });
          }

        });

      } else {

        // wpColorPicker default picker
        $this.wpColorPicker({
          clear: function() {
            $this.trigger('keyup');
            //console.log('hii');
          },
          change: function( event, ui ) {
            $this.val(ui.color.toString()).trigger('change');
            $this.closest('.wp-picker-container').parent().find('.fl-color-picker-value').val(ui.color.toString());
            var input   = jQuery( this ),
                preview = input.closest( '.fl-field' ).data( 'preview' ),
                i       = null;
            //console.log('change');
            if ( 'css' == preview.type ) {  
                if ( 'undefined' != typeof preview.rules ) {
                    for ( i in preview.rules ) {
                        FLBuilder.preview._previewCSS( preview.rules[ i ], event );
                    }
                }
                else {
                    FLBuilder.preview._previewCSS( preview, event );
                }
            }
          }
        });

      }

    });

  };

  // Color Picker JS - On Page Load
  $(document).ready( function(){
    $('.cs-wp-color-picker').cs_wpColorPicker();
  });

  // Color Picker JS - On Form Load
  var initSettingsForms = FLBuilder._initSettingsForms;
  FLBuilder._initSettingsForms = function() {
    $('.cs-wp-color-picker').cs_wpColorPicker();
    initSettingsForms();
  };

  //Color Picker JS - On Ajax Load
  $( '.fl-builder-content' ).on( 'fl-builder.layout-rendered', function() {
    $('.cs-wp-color-picker').cs_wpColorPicker();
  });

})( jQuery, window, document );
