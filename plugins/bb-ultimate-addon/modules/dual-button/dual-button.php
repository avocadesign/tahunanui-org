<?php

class UABBDualButtonModule extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Dual Button', 'fl-builder' ),
            'description'     => __( 'A totally awesome module!', 'fl-builder' ),
            'category'        => __('Ultimate Addons', 'fl-builder'),
            'dir'             => BB_ULTIMATE_ADDON_DIR . 'modules/dual-button/',
            'url'             => BB_ULTIMATE_ADDON_URL . 'modules/dual-button/',
        ));
    }

    function render_own_imgicon( $image_icon_arr ) {
        $image_icon_arr = (object) $image_icon_arr;
        $output = '';        
        if ( $image_icon_arr->image_type != 'none' ) {
            $output = '<div class="uabb-imgicon-wrap">';
            if( $image_icon_arr->image_type == 'icon' ) {
                $output .= '<span class="uabb-icon-wrap">';
                $output .= '<span class="uabb-icon">';
                $output .= '<i class="'.$image_icon_arr->icon.'"></i>'; 
                $output .= '</span>';
                $output .= '</span>';
            } // Icon Html End

            if( $image_icon_arr->image_type == 'photo' ) { // Photo Html
                $src = isset($image_icon_arr->photo_src) ? $image_icon_arr->photo_src : '';
                $output .= '<div class="uabb-image-simple">';
                $output .= '<div class="uabb-image-content">';
                $output .= '<img class="uabb-img-src" src="'.$src.'"/>';
                $output .= '</div>';
                $output .= '</div>';

            } // Photo Html End
            $output .= '</div>'; /* End Module Wrap */ 
            echo $output;
        }
    }
    function render_image_icon( $image_icon_arr ) {
        $image_icon_arr = (object) $image_icon_arr;
        $imageicon_array = array(
     
            /* General Section */
            'image_type' => $image_icon_arr->image_type,
         
            /* Icon Basics */
            'icon' => $image_icon_arr->icon,
            'icon_size' => "30",
            'icon_align' => "center",
         
            /* Image Basics */
            'photo_source' => $image_icon_arr->photo_source,
            'photo' => $image_icon_arr->photo,
            'photo_url' => $image_icon_arr->photo_url,
            'img_size' => "30",
            'img_align' => "center",
            'photo_src' => ( isset( $image_icon_arr->photo_src ) ) ? $image_icon_arr->photo_src : '' ,
         
            /* Icon Style */
            'icon_style' => "",
            'icon_bg_size' => "",
            'icon_border_style' => "",
            'icon_border_width' => "",
            'icon_bg_border_radius' => "0",
         
            /* Image Style */
            'image_style' => "",
            'img_bg_size' => "",
            'img_border_style' => "",
            'img_border_width' => "",
            'img_bg_border_radius' => "0",
        ); 
        /* Render HTML Function */
        FLBuilder::render_module_html( 'image-icon', $imageicon_array );
    }

    function render_image_icon_css( $id, $image_icon_arr ) {
        $image_icon_arr = (object) $image_icon_arr;
        $imageicon_array = array(
     
            /* General Section */
            'image_type' => $image_icon_arr->image_type,
         
            /* Icon Basics */
            'icon' => $image_icon_arr->icon,
            'icon_size' => $image_icon_arr->icon_size,
            'icon_align' => "center",
         
            /* Image Basics */
            'photo_source' => $image_icon_arr->photo_source,
            'photo' => $image_icon_arr->photo,
            'photo_url' => $image_icon_arr->photo_url,
            'img_size' => $image_icon_arr->img_size,
            'img_align' => "center",
            'photo_src' => ( isset( $image_icon_arr->photo_src ) ) ? $image_icon_arr->photo_src : '' ,
         
            /* Icon Style */
            'icon_style' => "",
            'icon_bg_size' => "",
            'icon_border_style' => "",
            'icon_border_width' => "",
            'icon_bg_border_radius' => "0",
         
            /* Image Style */
            'image_style' => "",
            'img_bg_size' => "",
            'img_border_style' => "",
            'img_border_width' => "",
            'img_bg_border_radius' => "0",

            /* Icon Colors */ 
            'icon_color' => $image_icon_arr->icon_color,
            'icon_hover_color' => $image_icon_arr->icon_hover_color,
            'icon_bg_color' => "rgba(255,255,255,0)",
            'icon_bg_hover_color' => "",
            'icon_border_color' => "",
            'icon_border_hover_color' => "",
            'icon_three_d' => "",
         
            /* Image Colors */
            'img_bg_color' => "rgba(255,255,255,0)",
            'img_bg_hover_color' => "",
            'img_border_color' => "",
            'img_border_hover_color' => "",
        ); 
        /* CSS Render Function */ 
        FLBuilder::render_module_css( 'image-icon', $id, $imageicon_array );
    }
}

FLBuilder::register_module('UABBDualButtonModule', array(
    'dual_button'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_button'       => array( // Section
                'title'         => __('Button Settings', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'dual_button_type'   => array(
                        'type'          => 'select',
                        'label'         => __('Button Type', 'fl-builder'),
                        'default'       => 'horizontal',
                        'options'       => array(
                            'horizontal'      => __('Horizontal', 'fl-builder'),
                            'vertical'      => __('Vertical', 'fl-builder'),
                        ),
                        'toggle'    => array(
                            'horizontal'        => array(
                                'fields'    => array( 'responive_dual_button' )
                            )
                        )
                    ),
                    
                    'dual_button_width_type'   => array(
                        'type'          => 'select',
                        'label'         => __('Button Width', 'fl-builder'),
                        'default'       => 'auto',
                        'options'       => array(
                            'auto'      => __('Auto', 'fl-builder'),
                            'full'      => __('Full width', 'fl-builder'),
                            'custom'      => __('Custom', 'fl-builder'),
                        ),
                        'toggle'          => array(
                            'auto' => array(
                                'fields'    => array( 'dual_button_align' ),
                            ),
                            'custom'   => array(
                                'fields'    => array( 'dual_button_align', 'dual_button_width','dual_button_height','dual_button_pad_top_bot','dual_button_pad_lef_rig' ),
                            ),
                        )
                    ),
                    'dual_button_width' => array(
                        'type'          => 'text',
                        'label'         => __('Custom Width', 'fl-builder'),
                        'size'          => '6',
                        'default'       => '100',
                        'help'          => __('Custom Width of Single Button.'),
                        'description'   => 'px'
                    ),
                    'dual_button_height' => array(
                        'type'          => 'text',
                        'label'         => __('Custom Height', 'fl-builder'),
                        'size'          => '6',
                        'default'       => '45',
                        'help'          => __('Custom Height of Single Button.'),
                        'description'   => 'px'
                    ),
                    'dual_button_pad_top_bot' => array(
                        'type'          => 'text',
                        'label'         => __('Padding Top/Bottom', 'fl-builder'),
                        'size'          => '6',
                        'default'       => '',
                        'description'   => 'px'
                    ),
                    'dual_button_pad_lef_rig' => array(
                        'type'          => 'text',
                        'label'         => __('Padding Left/Right', 'fl-builder'),
                        'size'          => '6',
                        'default'       => '',
                        'description'   => 'px'
                    ),
                    'dual_button_align'   => array(
                        'type'          => 'select',
                        'label'         => __('Alignment', 'fl-builder'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Left', 'fl-builder'),
                            'right'      => __('Right', 'fl-builder'),
                            'center'      => __('Center', 'fl-builder'),
                        )
                    ),
                    'dual_button_radius'   => array(
                        'type'          => 'text',
                        'label'         => __('Border Radius', 'fl-builder'),
                        'size'          => '6',
                        'description'   => 'px'
                    ),
                    'responive_dual_button'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Enable Responsive Mode', 'fl-builder' ),
                        'default'       => 'no',
                        'options'       => array(
                            'no'       => 'No',
                            'yes'      => 'Yes',
                        ),
                        'help'          => __('Convert Horizontal style to Vertical style below 992px', 'uabb')
                    ),

                )
            ),
            'dual_button_styles'       => array( // Section
                'title'         => __('Styles', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'dual_button_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Button Style', 'fl-builder'),
                        'default'       => 'flat',
                        'options'       => array(
                            'flat'      => __('Flat', 'fl-builder'),
                            'gradient'      => __('Gradient', 'fl-builder'),
                            'transparent'      => __('Transparent', 'fl-builder'),
                        ),
                        'toggle'        => array(
                            'transparent'   => array(
                                'sections'  => array( 'dual_border_section' ),
                                'fields'    => array( 'transparent_button_options' ),
                            ),
                            'flat'   => array(
                                'fields'    => array( 'flat_button_options', '_btn_one_back_color', '_btn_one_back_color_opc', '_btn_two_back_color', '_btn_two_back_color_opc' )
                            ),
                            'gradient'   => array(
                                'fields'    => array( '_btn_one_back_color', '_btn_one_back_color_opc', '_btn_two_back_color', '_btn_two_back_color_opc' )
                            )
                        )
                    ),
                    'transparent_button_options'         => array(
                        'type'          => 'select',
                        'label'         => __('Hover Styles', 'uabb'),
                        'default'       => 'transparent-fade',
                        'options'       => array(
                            'transparent-fade'          => __('Fade Background', 'uabb'),
                            'transparent-fill-top'      => __('Fill Background From Top', 'uabb'),
                            'transparent-fill-bottom'      => __('Fill Background From Bottom', 'uabb'),
                            'transparent-fill-left'     => __('Fill Background From Left', 'uabb'),
                            'transparent-fill-right'     => __('Fill Background From Right', 'uabb'),
                            'transparent-fill-center'       => __('Fill Background Vertical', 'uabb'),
                            'transparent-fill-diagonal'     => __('Fill Background Diagonal', 'uabb'),
                            'transparent-fill-horizontal'  => __('Fill Background Horizontal', 'uabb'),
                        ),
                    ),
                    'flat_button_options'         => array(
                        'type'          => 'select',
                        'label'         => __('Hover Styles', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => __('None', 'uabb'),
                            'animate_to_right'          => __('Appear Icon/Image From Left', 'uabb'),
                            'animate_to_left'      => __('Appear Icon/Image From Right', 'uabb'),
                            'animate_from_top'      => __('Appear Icon/Image From Top', 'uabb'),
                            'animate_from_bottom'     => __('Appear Icon/Image From Bottom', 'uabb'),
                        ),
                    ),
                )
            ),
            'dual_border_section'       => array( // Section
                'title'         => __('Border Styles', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'dual_button_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'fl-builder'),
                        'default'       => 'solid',
                        'options'       => array(
                            'none'      => __('None', 'fl-builder'),
                            'solid'      => __('Solid', 'fl-builder'),
                            'dashed'      => __('Dashed', 'fl-builder'),
                            'dotted'      => __('Dotted', 'fl-builder'),
                            'double'      => __('Double', 'fl-builder'),
                            'inset'      => __('Inset', 'fl-builder'),
                            'outset'      => __('Outset', 'fl-builder'),
                        ),
                        'toggle'        => array(
                            'solid'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                            'dashed'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                            'dotted'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                            'double'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                            'inset'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                            'outset'     => array(
                                'fields' => array( 'button_border_color', 'button_border_width' )
                            ),
                        )
                    ),
                    'button_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'fl-builder'),  
                        )
                    ),
                    'button_border_width' => array(
                        'type'          => 'text',
                        'label'         => __('Border Width', 'fl-builder'),
                        'size'          => '6',
                        'default'       => '2',
                        'description'   => 'px'
                    ),
                    
                )
            )
            
        )
    ),
    'dual_button_one'       => array( // Tab
        'title'         => __('Button 1', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_button_one'       => array( // Section
                'title'         => __('Button 1 Options', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'button_one_title'   => array(
                        'type'          => 'text',
                        'label'         => __('Button Text', 'fl-builder'),
                        'placeholder'   => "Button One",
                        'default'       => 'Check Demo'
                    ),
                    'button_one_link'   => array(
                        'type'          => 'link',
                        'label'         => __('Button Link', 'fl-builder'),
                    ),
                    'button_one_link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'uabb'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'uabb'),
                            '_blank'        => __('New Window', 'uabb')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    '_btn_one_back_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                        )
                    ),
                    '_btn_one_back_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    '_btn_one_back_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Hover Color', 'uabb'),
                            'preview'       => array(
                                    'type'      => 'none',
                            )
                        )
                    ),
                    '_btn_one_back_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),

            /* Icon Image Param Code Starts */
            'type_general'      => array( // Section
                'title'         => 'Image / Icon', // Section Title
                'fields'        => array( // Section Fields
                    'image_type_btn_one'    => array(
                        'type'          => 'select',
                        'label'         => __('Image Type', 'uabb'),
                        'default'       => '',
                        'options'       => array(
                            'none'          => __( 'None', 'Image type.', 'uabb' ),
                            'icon'          => __('Icon', 'uabb'),
                            'photo'         => __('Photo', 'uabb'),
                        ),
                        'toggle'        => array(
                            'icon'          => array(
                                'fields'   => array( 'icon_btn_one', 'icon_position_btn_one', 'img_icon_width_btn_one' ),
                            ),
                            'photo'         => array(
                                'fields'   => array( 'photo_btn_one', 'icon_position_btn_one', 'img_icon_width_btn_one' ),
                            )
                        ),
                    ),
                    'icon_btn_one'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'uabb')
                    ),
                    /*'photo_source_btn_one'  => array(
                        'type'          => 'select',
                        'label'         => __('Photo Source', 'uabb'),
                        'default'       => 'library',
                        'options'       => array(
                            'library'       => __('Media Library', 'uabb'),
                            'url'           => __('URL', 'uabb')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('photo_btn_one')
                            ),
                            'url'           => array(
                                'fields'        => array('photo_url_btn_one' )
                            )
                        )
                    ),*/
                    'photo_btn_one'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'uabb')
                    ),
                    /*'photo_url_btn_one'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'uabb'),
                        'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                    ),*/
                    'icon_position_btn_one' => array(
                        'type'          => 'select',
                        'label'         => __('Photo/Icon Position', 'uabb'),
                        'default'       => 'before',
                        'options'       => array(
                            'before'       => __('Before Text', 'uabb'),
                            'after'           => __('After Text', 'uabb')
                        ),
                    ),
                    'img_icon_width_btn_one' => array(
                        'type'          => 'text',
                        'label'         => __('Photo/Icon Width', 'uabb'),
                        'default'       => '30',
                        'description'   => 'px',
                        'size'          => '8'
                    )
                )
            ),
            /* Icon Image Param Code Ends */
        )
    ),
    'dual_button_two'       => array( // Tab
        'title'         => __('Button 2', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_button_two'       => array( // Section
                'title'         => __('Button 2 Options', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'button_two_title'   => array(
                        'type'          => 'text',
                        'label'         => __('Button Text', 'fl-builder'),
                        'placeholder'   => "Button two",
                        'default'       => 'Learn More'
                    ),
                    'button_two_link'   => array(
                        'type'          => 'link',
                        'label'         => __('Button Link', 'fl-builder'),
                    ),
                    'button_two_link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'uabb'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'uabb'),
                            '_blank'        => __('New Window', 'uabb')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    '_btn_two_back_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                        )
                    ),
                    '_btn_two_back_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    '_btn_two_back_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Hover Color', 'uabb'),
                            'preview'       => array(
                                    'type'      => 'none',
                            )
                        )
                    ),
                    '_btn_two_back_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),

            /* Icon Image Param Code Starts */
            'type_general_btn_two'      => array( // Section
                'title'         => 'Image / Icon', // Section Title
                'fields'        => array( // Section Fields
                    'image_type_btn_two'    => array(
                        'type'          => 'select',
                        'label'         => __('Image Type', 'uabb'),
                        'default'       => '',
                        'options'       => array(
                            'none'          => __( 'None', 'uabb' ),
                            'icon'          => __('Icon', 'uabb'),
                            'photo'         => __('Photo', 'uabb'),
                        ),
                        'toggle'        => array(
                            'icon'          => array(
                                'fields'   => array( 'icon_btn_two', 'icon_position_btn_two', 'img_icon_width_btn_two' ),
                            ),
                            'photo'         => array(
                                'fields'   => array( 'photo_btn_two', 'icon_position_btn_two', 'img_icon_width_btn_two' ),
                            )
                        ),
                    ),
                    'icon_btn_two'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'uabb')
                    ),
                    /*'photo_source_btn_two'  => array(
                        'type'          => 'select',
                        'label'         => __('Photo Source', 'uabb'),
                        'default'       => 'library',
                        'options'       => array(
                            'library'       => __('Media Library', 'uabb'),
                            'url'           => __('URL', 'uabb')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('photo_btn_two')
                            ),
                            'url'           => array(
                                'fields'        => array('photo_url_btn_two' )
                            )
                        )
                    ),*/
                    'photo_btn_two'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'uabb')
                    ),
                    /*'photo_url_btn_two'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'uabb'),
                        'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                    ),*/
                    'icon_position_btn_two' => array(
                        'type'          => 'select',
                        'label'         => __('Photo/Icon Position', 'uabb'),
                        'default'       => 'before',
                        'options'       => array(
                            'before'       => __('Before Text', 'uabb'),
                            'after'           => __('After Text', 'uabb')
                        ),
                    ),
                    'img_icon_width_btn_two' => array(
                        'type'          => 'text',
                        'label'         => __('Photo/Icon Width', 'uabb'),
                        'default'       => '30',
                        'description'   => 'px',
                        'size'          => '8'
                    )

                )
            ),
            /* Icon Image Param Code Ends */
        )
    ),
    'dual_button_divider'       => array( // Tab
        'title'         => __('Divider', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_button_divider'       => array( // Section
                'title'         => __('Divider Settings', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'divider_options'   => array(
                        'type'          => 'select',
                        'label'         => __('Select Divider', 'fl-builder'),
                        'default'       => 'text',
                        'options'       => array(
                            'none'      => __('None', 'fl-builder' ),
                            'text'      => __('Text', 'fl-builder'),
                            'icon'      => __('Icon', 'fl-builder'),
                            'photo'     => __('Image', 'fl-builder'),
                        ),
                        'toggle'        => array(
                            'text'   => array(
                                'sections'  => array( 'dual_btn_divider_color', 'dual_btn_divider_border', 'divider_text' ),
                                'fields'    => array( 'divider_text', 'divider_color' ),
                            ),
                            'icon'   => array(
                                'sections'  => array( 'dual_btn_divider_color', 'dual_btn_divider_border' ),
                                'fields'    => array( 'divider_icon', 'divider_color' )
                            ),
                            'photo'   => array(
                                'sections'  => array( 'dual_btn_divider_border' ),
                                'fields'    => array( 'divider_photo' ),
                            )
                        )
                    ),
                    'divider_text'   => array(
                        'type'          => 'text',
                        'label'         => __('Text', 'fl-builder'),
                        'default'       => 'or'
                    ),
                    'divider_icon'   => array(
                        'type'          => 'icon',
                        'label'         => __('Select Icon', 'fl-builder'),
                    ),
                    /*'divider_photo_source'  => array(
                        'type'          => 'select',
                        'label'         => __('Photo Source', 'uabb'),
                        'default'       => 'library',
                        'options'       => array(
                            'library'       => __('Media Library', 'uabb'),
                            'url'           => __('URL', 'uabb')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('divider_photo')
                            ),
                            'url'           => array(
                                'fields'        => array('divider_photo_url' )
                            )
                        )
                    ),*/
                    'divider_photo'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'uabb')
                    ),
                    /*'divider_photo_url'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'uabb'),
                        'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                    ),*/
                )
            ),
            'dual_btn_divider_color'       => array( // Section
                'title'         => __('Divider Colors', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'divider_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Text / Icon Color', 'fl-builder'),
                        )
                    ),
                    'divider_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => '#ffffff'
                        )
                    ),
                    'divider_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),
            'dual_btn_divider_border'       => array( // Section
                'title'         => __('Divider Border', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'divider_border_radius'   => array(
                        'type'          => 'text',
                        'label'         => __('Border Radius', 'fl-builder'),
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'default'       => '50',
                        'size'          => '5',
                    ),
                    'divider_border'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'fl-builder'),
                        'default'       => '',
                        'options'       => array(
                            ""     => __( 'None', 'fl-builder'),    
                            "solid"     => __( 'Solid', 'fl-builder'),    
                            "dashed"    => __( 'Dashed', 'fl-builder'),
                            "dotted"    => __( 'Dotted', 'fl-builder'),
                            "double"    => __( 'Double', 'fl-builder'),
                            "inset"     => __( 'Inset', 'fl-builder'),
                            "outset"    => __( 'Outset', 'fl-builder'),
                        ),
                        'toggle'        => array(
                            'solid'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                            'dashed'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                            'dotted'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                            'double'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                            'inset'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                            'outset'     => array(
                                'fields'    => array( 'divider_border_color', 'divider_border_width' )
                            ),
                        )
                    ),
                    'divider_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'fl-builder'),
                        )
                    ),
                    'divider_border_width'   => array(
                        'type'          => 'text',
                        'label'         => __('Border Width', 'fl-builder'),
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'size'          => '5',
                        'default'       => '1'
                    ),
                    
                )
            ),

        )
    ),
    'dual_button_typography'       => array( // Tab
        'title'         => __('Typography', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'typography_btn_one'    =>  BB_Ultimate_Addon::uabb_section_get( 
                                'typography', 
                                array( 
                                    'title'     => __('Button 1', 'uabb' ),
                                    'fields'   => array(
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-btn-one-text'
                                            )
                                        ),
                                        'font_size' => array(
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-btn.uabb-btn-one',
                                                'property'        => 'font-size',
                                                'unit'            => 'px'
                                            )
                                        ),
                                        'line_height' => array(
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-btn.uabb-btn-one',
                                                'property'        => 'line-height',
                                                'unit'            => 'px'
                                            )
                                        ),
                                        'text_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                                            array(
                                                'label'         => __('Title Color', 'fl-builder'),  
                                            )
                                        ),
                                        'text_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                                            array(
                                                'label'         => __('Title Hover Color', 'fl-builder'),  
                                            )
                                        ),
                                    ),
                                ),
                                
                                array('tag_selection','color'),
                                '_btn_one'
            ),
            'typography_btn_two'    =>  BB_Ultimate_Addon::uabb_section_get( 
                                'typography', 
                                array( 
                                    'title'     => __('Button 2', 'uabb' ),
                                    'fields'   => array(
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-btn-two-text'
                                            )
                                        ),
                                        'font_size' => array(
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-btn.uabb-btn-two',
                                                'property'        => 'font-size',
                                                'unit'            => 'px'
                                            )
                                        ),
                                        'line_height' => array(
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-btn.uabb-btn-two',
                                                'property'        => 'line-height',
                                                'unit'            => 'px'
                                            )
                                        ),
                                        'text_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                                            array(
                                                'label'         => __('Title Color', 'fl-builder'),  
                                            )
                                        ),
                                        'text_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                                            array(
                                                'label'         => __('Title Hover Color', 'fl-builder'),  
                                            )
                                        ),
                                    ),
                                ), 
                                array('tag_selection','color'),
                                '_btn_two'
            ),
            'divider_text'    =>  BB_Ultimate_Addon::uabb_section_get( 
                                'typography', 
                                array( 
                                    'title'     => __('Divider Text', 'uabb' ),
                                    'fields'   => array(
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-middle-text'
                                            )
                                        ),
                                        'font_size' => array(
                                            'help'  => __( 'Divider width and height will adjust according to font size' ),
                                        ),
                                    ),
                                ), 
                                array('tag_selection', 'line_height', 'color'),
                                '_divider'
            ),
        )
    )
));

?>