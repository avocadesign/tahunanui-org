(function($){
	FLBuilder.registerModuleHelper('dual-button', {
		rules: {
			button_one_link: {
				required: true
			},
			button_two_link: {
				required: true
			},
		},

		init: function()
		{
			var form        = $('.fl-builder-settings'),
				image_type   = form.find('select[name=image_type_btn_one]'),
				photo_soruce_type   = form.find('select[name=photo_source_btn_one]'),
				image_type_btn_two   = form.find('select[name=image_type_btn_two]'),
				photo_soruce_type_btn_two   = form.find('select[name=photo_source_btn_two]'),
				divider_image_option   = form.find('select[name=divider_options]'),
				button_style   = form.find('select[name=dual_button_style]'),
				button_sub_style   = form.find('select[name=flat_button_options]');
				
				

			if ( image_type.val() == "none" ) {
				jQuery("#fl-field-photo_url_btn_one").hide();
				jQuery("#fl-field-photo_btn_one").hide();
			}
			if ( image_type_btn_two.val() == "none" ) {
				jQuery("#fl-field-photo_url_btn_two").hide();
				jQuery("#fl-field-photo_btn_two").hide();
			}
			if ( divider_image_option.val() != "photo" ) {
				jQuery("#fl-field-divider_photo").hide();
				jQuery("#fl-field-divider_photo_url").hide();	
			}


			// Validation events.
			image_type.on('change', this.photo_type_changed);
			image_type_btn_two.on('change', this.photo_type_changed_btn_two);
			divider_image_option.on('change', this.divider_photo_type_changed);
			button_style.on('change', this.button_style_changed);
			button_sub_style.on('change', this.button_style_changed);
			
			
		},
		photo_type_changed: function()
		{

			var form        = $('.fl-builder-settings'),
				image_type   = form.find('select[name=image_type_btn_one]').val(),
				photo_soruce_type   = form.find('select[name=photo_source_btn_one]').val();
	
	
			if( photo_soruce_type == 'library' ) {
				setTimeout(function(){
					jQuery("#fl-field-photo_url_btn_one").hide();
				},100);
				jQuery("#fl-field-photo_btn_one").show();
			}
			else if( photo_soruce_type == 'url' ) {
				jQuery("#fl-field-photo_url_btn_one").show();
				setTimeout(function(){
					jQuery("#fl-field-photo_btn_one").hide();
				},100);
			}
		},
		photo_type_changed_btn_two: function()
		{

			var form        = $('.fl-builder-settings'),
				image_type_btn_two   = form.find('select[name=image_type_btn_two]').val(),
				photo_soruce_type_btn_two   = form.find('select[name=photo_source_btn_two]').val();
	
	
			if( photo_soruce_type_btn_two == 'library' ) {
				setTimeout(function(){
					jQuery("#fl-field-photo_url_btn_two").hide();
				},100);
				jQuery("#fl-field-photo_btn_two").show();
			}
			else if( photo_soruce_type_btn_two == 'url' ) {
				jQuery("#fl-field-photo_url_btn_two").show();
				setTimeout(function(){
					jQuery("#fl-field-photo_btn_two").hide();
				},100);
			}
		},
		divider_photo_type_changed: function()
		{
			var form        = $('.fl-builder-settings'),
				divider_image_option   = form.find('select[name=divider_options]').val(),
				divider_photo_source   = form.find('select[name=divider_photo_source]').val();
	
	
			if( divider_photo_source == 'library' ) {
				setTimeout(function(){
					jQuery("#fl-field-divider_photo_url").hide();
				},100);
				jQuery("#fl-field-divider_photo").show();
			}
			else if( divider_photo_source == 'url' ) {
				jQuery("#fl-field-divider_photo_url").show();
				setTimeout(function(){
					jQuery("#fl-field-divider_photo").hide();
				},100);
			}
		},
		button_style_changed: function()
		{
			var form        = $('.fl-builder-settings'),
			button_style   = form.find('select[name=dual_button_style]').val(),
			button_sub_style   = form.find('select[name=flat_button_options]').val(),
			button_one_img_type   = form.find('select[name=image_type_btn_one]'),
			button_two_img_type   = form.find('select[name=image_type_btn_two]');

			///button_one_icon   = form.find('select[name=icon_btn_one]'),
			//button_two_icon   = form.find('select[name=icon_btn_two]');

			//button_one_photo   = form.find('select[name=photo_btn_one]'),
			//button_two_photo   = form.find('select[name=photo_btn_two]'),

			//button_one_photo_url   = form.find('select[name=photo_url_btn_one]'),
			//button_two_photo_url   = form.find('select[name=photo_url_btn_two]');

			
				
			button_one_img_type.rules('remove');
			button_two_img_type.rules('remove');

			//button_one_icon.rules('remove');
			//button_two_icon.rules('remove');

			//button_one_photo.rules('remove');
			//button_two_photo.rules('remove');

			//button_one_photo_url.rules('remove');
			//button_two_photo_url.rules('remove');

			if(button_style == 'flat' && button_sub_style != 'none' ) {
				button_one_img_type.rules('add', { required: true });
				button_two_img_type.rules('add', { required: true });

				//button_one_icon.rules('add', { required: true });
				//button_two_icon.rules('add', { required: true });

				//button_one_photo.rules('add', { required: true });
				//button_two_photo.rules('add', { required: true });

				//button_one_photo_url.rules('add', { required: true });
				//button_two_photo_url.rules('add', { required: true });
			}
		},
	});

})(jQuery);