<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class CreativeLink
 */
class CreativeLink extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Creative Menu', 'uabb'),
            'description'   => __('Creative Menu', 'uabb'),
            'category'		=> __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/creative-link/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/creative-link/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }

    public function render_text( $title ) {
        switch ( $this->settings->link_style ) {
            case 'simple':
            case 'style1':
            case 'style3':
            case 'style4':
            case 'style6':
            case 'style7':
            case 'style8':
            case 'style10':
            case 'style12':
            case 'style13':
            case 'style14':
            case 'style15':
            case 'style16':
            case 'style20':
                echo trim( $title , '' );
                break;
            
            case 'style2':
            case 'style5':
            case 'style17':
            case 'style18':
            case 'style19':
                echo '<span data-hover="' . trim( $title , '' ) . '">' . trim( $title , '' ) . '</span>';
                break;

            case 'style9':
            case 'style11':
                echo '<span>' . trim( $title , '' ) . '</span>';
                break;
        }
    }

}



/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('CreativeLink', array(
    'general'       => array( // Tab
        'title'         => __('Title', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Title', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'screens'     => array(
                        'type'         => 'form',
                        'label'        => __('Title', 'uabb'),
                        'form'         => 'screens_form',
                        'preview_text' => 'title',
                        'multiple'     => true
                    ),
                )
            ),
        )
    ),
    'style'       => array( // Tab
        'title'         => __('Style', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Style', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'link_style'    => array(
                        'type'          => 'select',
                        'label'         => __('Link Style', 'uabb'),
                        'default'       => 'style1',
                        'options'       => array(
                            'style1'         => __('Style 1', 'uabb'),
                            'style2'         => __('Style 2', 'uabb'),
                            'style3'         => __('Style 3', 'uabb'),
                            'style4'         => __('Style 4', 'uabb'),
                            'style5'         => __('Style 5', 'uabb'),
                            'style6'         => __('Style 6', 'uabb'),
                            'style7'         => __('Style 7', 'uabb'),
                            'style8'         => __('Style 8', 'uabb'),
                            'style9'         => __('Style 9', 'uabb'),
                            /*'style10'         => __('Style 10', 'uabb'),*/
                            'style11'         => __('Style 10', 'uabb'),
                            'simple'         => __('Style 11', 'uabb'),
                            /*'style12'         => __('Style 12', 'uabb'),
                            'style13'         => __('Style 13', 'uabb'),*/
                            'style14'         => __('Style 12', 'uabb'),
                            'style15'         => __('Style 13', 'uabb'),
                            'style16'         => __('Style 14', 'uabb'),
                            'style17'         => __('Style 15', 'uabb'),
                            'style18'         => __('Style 16', 'uabb'),
                            'style19'         => __('Style 17', 'uabb'),
                            'style20'         => __('Style 18', 'uabb'),
                        ),
                        'toggle' => array(
                            'simple' => array(
                                'fields' => array()
                            ),
                            'style1' => array(
                                'fields' => array()
                            ),
                            'style2' => array(
                                'fields' => array( 'background_color', 'background_color_opc', 'background_hover_color', 'background_hover_color_opc' ),
                            ),
                            'style3' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style4' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style5' => array(
                                'fields' => array(),
                            ),
                            'style6' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style7' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style8' => array(
                                'fields' => array( 'border_size', 'border_color', 'border_style', 'border_hover_color'  ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style9' => array(
                                'fields' => array( 'background_color', 'background_color_opc', 'background_hover_color', 'background_hover_color_opc'  ),
                            ),
                            'style10' => array(
                                'fields' => array( 'border_size', 'border_color', 'border_style', 'border_hover_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style11' => array(
                                'fields' => array( 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style12' => array(
                                'fields' => array(),
                            ),
                            'style13' => array(
                                'fields' => array('border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style14' => array(
                                'fields' => array(),
                            ),
                            'style15' => array(
                                'fields' => array(),
                            ),
                            'style16' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style17' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                            'style18' => array(
                                'fields' => array( 'background_color', 'background_color_opc', 'background_hover_color', 'background_hover_color_opc', 'box_width' ),
                            ),
                            'style19' => array(
                                'fields' => array( 'background_color', 'background_color_opc', 'background_hover_color', 'background_hover_color_opc' ),
                            ),
                            'style20' => array(
                                'fields' => array( 'border_size', 'border_color' ),
                                'sections' => array( 'border_settings' )
                            ),
                        )
                    ),
                    'alignment'    => array(
                        'type'          => 'select',
                        'label'         => __('Overall Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'left'         => __('Left', 'uabb'),
                            'right'          => __('Right', 'uabb'),
                            'center'          => __( 'Center', 'uabb' ),
                        ),
                    ),
                    /*'column'    => array(
                        'type'          => 'select',
                        'label'         => __('Column', 'uabb'),
                        'default'       => 'auto',
                        'options'       => array(
                            'auto'         => __('Auto', 'uabb'),
                            'dropdown'          => __('Custom Grid Size', 'uabb'),
                        ),
                        'toggle'    => array(
                            'dropdown' => array(
                                'fields' => array( 'custom_grid' )
                            ),
                            'auto' => array(
                                'fields' => array( 'alignment' )
                            )
                        )
                    ),
                    'custom_grid'    => array(
                        'type'          => 'select',
                        'label'         => __('Custom Grid', 'uabb'),
                        'default'       => '2',
                        'options'       => array(
                            '1'         => __('1 Column', 'uabb'),
                            '2'          => __('2 Column', 'uabb'),
                            '3'          => __('3 Column', 'uabb'),
                            '4'          => __('4 Column', 'uabb'),
                            '5'          => __('5 Column', 'uabb'),
                            '6'          => __('6 Column', 'uabb'),
                        ),
                    ),*/
                    'spacing'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Space Between Links', 'uabb' ),
                        'default'       => '10',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'box_width' => array(
                        'type'          => 'text',
                        'label'         => __( 'Box Width', 'uabb' ),
                        'default'       => '200',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    )
                )
            ),
            'color_settings'       => array( // Section
                'title'         => __('Color Settings', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'link_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Link Color', 'uabb'),
                        )
                    ),
                    'link_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Link Hover Color', 'uabb'),
                        )
                    ),
                    /*'link_active_color' => array(
                        'type'          => 'uabb-color',
                        'label'         => __('Link Active Color', 'uabb'),
                        'show_reset'    => true,
                    ),*/
                    'background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                        )
                    ),
                    'background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                    'background_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Hover Color', 'uabb'),
                        )
                    ),
                    'background_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),
            'border_settings'       => array( // Section
                'title'         => __('Border Settings', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'border_style' => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'uabb'),
                        'default'       => 'solid',
                        'options'       => array(
                            'solid'        => __('Solid', 'uabb'),
                            'dashed'       => __('Dashed', 'uabb'),
                            'double'       => __('Double', 'uabb'),
                            'dotted'       => __('Dotted', 'uabb'),
                        ),
                    ),
                    'border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Size', 'uabb' ),
                        'default'       => '1',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                        )
                    ),
                    'border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Hover Color', 'uabb'),
                            'preview'       => array(
                                    'type'      => 'none',
                            )
                        )
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'link_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Text Typography', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-creative-link a, .uabb-creative-link a span'
                            )
                        ),
                        'tag_selection' => array(
                            'default' => 'h4'
                        )
                    ), 
                ),
                array( 'color' ),
                'link_typography'
            ),
        )
    ),
));

FLBuilder::register_settings_form('screens_form', array(
    'title' => __( 'Title', 'uabb' ),
    'tabs'  => array(
        'general'      => array(
            'title'         => __('General', 'uabb'),
            'sections'      => array(
                'features'       => array(
                    'title'         => __( 'Titles', 'uabb' ),
                    'fields'        => array(
                        'title'         => array(
                            'type'          => 'text',
                            'label'         => __('Title', 'uabb'),
                        ),
                        'link'          => array(
                            'type'          => 'link',
                            'label'         => __('Link', 'uabb'),
                            'placeholder'   => __( 'http://www.example.com', 'uabb' ),
                        ),
                        'target' => array(
                            'type'          => 'select',
                            'label'         => __('Target', 'uabb'),
                            'default'       => '',
                            'options'       => array(
                                '_blank'        => __('New Page', 'uabb'),
                                ''              => __('Same Page', 'uabb'),
                            ),
                        )
                    )
                ),
            )
        ),
    )
));