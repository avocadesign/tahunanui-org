<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */
//echo '<pre>'; print_r($module); echo '</pre>';
//echo '<pre>'; print_r( $settings ); echo '</pre>';
?>
<div class="uabb-cl-wrap">
	<ul class="uabb-cl-ul">
	<?php
	if( count( $settings->screens ) > 0 ) {
		foreach( $settings->screens as $screen ) {
	?>
		<li class="uabb-creative-link uabb-cl-<?php echo $settings->link_style; ?>">
			<<?php echo $settings->link_typography_tag_selection; ?> class="uabb-cl-heading">
				<a href="<?php echo $screen->link; ?>" target="<?php echo $screen->target; ?>" data-hover="<?php echo $screen->title; ?>"><?php $module->render_text( $screen->title ); ?></a>
			</<?php echo $settings->link_typography_tag_selection; ?>>
		</li>
	<?php
		}
	}
	?>
	</ul>
</div>