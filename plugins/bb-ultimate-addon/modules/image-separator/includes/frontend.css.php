<?php
$settings->img_bg_color = UABB_Helper::uabb_colorpicker( $settings, 'img_bg_color', true );
$settings->img_bg_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'img_bg_hover_color', true );

$settings->img_border_color = UABB_Helper::uabb_colorpicker( $settings, 'img_border_color' );
$settings->img_border_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'img_border_hover_color' );

?>

<?php 
    $settings->gutter =  ( $settings->gutter != '' ) ? $settings->gutter : '50';
?>

<?php if ( $settings->image_position == 'bottom' ) { ?>
    .fl-node-<?php echo $id; ?> {
        bottom: 0;
        top: auto;
        transform: translate(-50%, <?php echo $settings->gutter; ?>%);
    }
<?php } ?>

<?php if ( $settings->image_position == 'top' ) { ?>
    .fl-node-<?php echo $id; ?> {
        top: 0;
        bottom: auto;
        transform: translate(-50%, -<?php echo $settings->gutter; ?>%);
    }
<?php } ?>


/*.fl-node-<?php echo $id; ?>,*/
.fl-node-<?php echo $id; ?> .uabb-image .uabb-photo-img {
    <?php if( !empty( $settings->img_size ) ) : ?>
        width: <?php echo $settings->img_size; ?>px;
    <?php endif; ?>

    <?php /* Border Style */?>
    <?php if($settings->image_style == 'custom') : ?>

        <?php if( $settings->img_border_style != 'none' ) : ?>
            border-style: <?php echo $settings->img_border_style;?>;
        <?php endif; ?>

        background: <?php echo uabb_theme_base_color( $settings->img_bg_color );?>;

        <?php if( !empty( $settings->img_bg_size ) ): ?>
            padding: <?php echo $settings->img_bg_size; ?>px;
        <?php endif; ?>

        <?php if( !empty( $settings->img_border_width ) ): ?>
            border-width: <?php echo $settings->img_border_width; ?>px;
        <?php endif; ?>

        <?php if( !empty( $settings->img_border_color ) ): ?>
            border-color: <?php echo $settings->img_border_color; ?>;
        <?php endif; ?>

        border-radius: <?php echo ( empty( $settings->img_bg_border_radius ) ) ? '0' : $settings->img_bg_border_radius; ?>px;
    <?php endif; ?>
}

/* Responsive Photo Size */
/*<?php if( isset( $settings->responsive_img_size ) && !empty( $settings->responsive_img_size )  && $global_settings->responsive_enabled ) { // Global Setting If started ?> 
        @media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
            .fl-node-<?php echo $id; ?> .uabb-image .uabb-photo-img{
            <?php if( is_numeric( $settings->responsive_img_size ) ) : ?>
                width: <?php echo $settings->responsive_img_size; ?>px;
            <?php endif; ?>
            }
        }
<?php } ?>*/

/* Animation CSS */
<?php if ( $settings->img_animation_repeat != '0' && $settings->img_animation_repeat != '1'  ) { ?>
.fl-node-<?php echo $id; ?> .animated {
    -webkit-animation-iteration-count: <?php echo $settings->img_animation_repeat; ?>;
            animation-iteration-count: <?php echo $settings->img_animation_repeat; ?>;
}
<?php } ?>

.fl-node-<?php echo $id; ?> .imgseparator-link:hover ~ .uabb-image .uabb-photo-img {

    <?php if($settings->image_style == 'custom') : ?>
        <?php if( !empty( $settings->img_bg_hover_color ) ): ?>
            background: <?php echo $settings->img_bg_hover_color;?>;
        <?php endif; ?>
        
        <?php if( !empty( $settings->img_border_hover_color ) ): ?>
            border-color: <?php echo $settings->img_border_hover_color; ?>;
        <?php endif; ?>
    <?php endif; ?>
    
}



