<?php

class UABBDualColorModule extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Dual Color Heading', 'fl-builder' ),
            'description'     => __( 'A totally awesome module!', 'fl-builder' ),
            'category'        => __('Ultimate Addons', 'fl-builder'),
            'dir'             => BB_ULTIMATE_ADDON_DIR . 'modules/dual-color-heading/',
            'url'             => BB_ULTIMATE_ADDON_URL . 'modules/dual-color-heading/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module('UABBDualColorModule', array(
    'dual_color'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_color_heading'       => array( // Section
                'title'         => __('Heading Text', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'first_heading_text'     => array(
                        'type'          => 'text',
                        'label'         => __('First Heading', 'fl-builder'),
                        'default'       => 'First Heading',
                        'placeholder'   => '',
                        'class'         => 'uabb-first-heading',
                        'description'   => '',
                        'preview'         => array(
                            'type'            => 'text',
                            'selector'        => '.uabb-first-heading-text'
                        ),
                        'help'          => __( 'Enter first part of heading.' )
                    ),
                    'second_heading_text'     => array(
                        'type'          => 'text',
                        'label'         => __('Second Heading', 'fl-builder'),
                        'default'       => 'Second Heading',
                        'placeholder'   => '',
                        'class'         => 'uabb-second-heading',
                        'description'   => '',
                        'preview'         => array(
                            'type'            => 'text',
                            'selector'        => '.uabb-second-heading-text'
                        ),
                        'help'          => __( 'Enter second part of heading.' )
                    ),
                )
            ),
            'dual_color'       => array( // Section
                'title'         => __('Style', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'add_spacing_option'   => array(
                        'type'          => 'select',
                        'label'         => __('Spacing Between Headings', 'fl-builder'),
                        'default'       => 'no',
                        'class'         => 'dual-color-spacing-option',
                        'options'       => array(
                            'yes'      => __('Yes', 'fl-builder'),
                            'no'      => __('No', 'fl-builder')
                        ),
                        'toggle' => array(
                            'yes' => array(
                                'fields' => array('heading_margin')
                            ),
                            'no' => array(
                                'fields' => array()
                            )
                        ),
                        'help'          => __( 'Adjust spacing between first & second part of heading.' )
                    ),
                    'heading_margin'     => array(
                        'type'          => 'text',
                        'label'         => __('Spacing', 'fl-builder'),
                        'default'       => '',
                        'placeholder'   => '10',
                        'size'          => '8',
                        'class'         => 'uabb-add-spacing',
                        'description'   => 'px',
                        'help'          => __( 'Enter value for the spacing between first & second heading.' )
                    ),
                    'first_heading_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('First Heading Color', 'fl-builder'),
                            'default'       => '',
                            'show_reset'    => true,
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-first-heading-text',
                                'property'        => 'color'
                            ),
                            'help'          => __( 'Select color for first part of heading.' )
                        )
                    ),
                    'second_heading_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Second Heading Color', 'fl-builder'),
                            'default'       => '',
                            'show_reset'    => true,
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-second-heading-text',
                                'property'        => 'color'
                            ),
                            'help'          => __( 'Select color for second part of heading.' )
                        )
                    ),
                    'dual_color_alignment'   => array(
                        'type'          => 'select',
                        'label'         => __('Alignment', 'fl-builder'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Left', 'fl-builder'),
                            'right'      => __('Right', 'fl-builder'),
                            'center'      => __('Center', 'fl-builder')
                        ),
                        'help'          => __( 'Select alignment for complete element.' )
                    ),
                )
            )
        )
    ),

    'dual_typography'       => array( // Tab
        'title'         => __('Typography', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'dual_typography'    =>  BB_Ultimate_Addon::uabb_section_get(
                                'typography',
                                array(
                                    //'title'     => __('Heading Settings', 'uabb' ) ,
                                    'fields'   => array(
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-dual-color-heading *'
                                            )
                                        ),
                                        'color'   => array(
                                            'label' => __('Choose Color', 'uabb'),
                                        ),
                                        'tag_selection'   => array(
                                            'label'     => __('Select Tag', 'uabb'),
                                            'default'   => 'h3'
                                        )
                                    )
                                ),
                                array('color'),
                                'dual'
            ),
        )
    )
));
