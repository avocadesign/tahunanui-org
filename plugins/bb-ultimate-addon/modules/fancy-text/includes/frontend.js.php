<?php if ( $settings->effect_type == 'type' ) { ?>
 jQuery(".fl-node-<?php echo $id; ?> .uabb-typed-main").typed({
            strings: <?php 
                  // Order of replacement
                  $order   = array("\r\n", "\n", "\r", "<br/>", "<br>");
                  $replace = '|';

                  $str = str_replace($order, $replace, $settings->fancy_text);
                  $lines = explode("|", $str);
                  $count_lines = count($lines);

                  $strings = '['; 
                        foreach($lines as $key => $line)  
                        { 
                              $strings .= '"'.__(trim(htmlspecialchars_decode(strip_tags($line))),'js_composer').'"';
                              if($key != ($count_lines-1))
                                    $strings .= ','; 
                        } 
                  $strings .= ']';
                  echo $strings;
            ?>,
            typeSpeed: <?php echo ( !empty($settings->typing_speed) ) ? $settings->typing_speed : 35; ?>,
            startDelay: <?php echo ( !empty($settings->start_delay) ) ? $settings->start_delay : 200; ?> ,
            backSpeed: <?php echo ( !empty($settings->back_speed) ) ? $settings->back_speed : 0; ?>,
            backDelay: <?php echo ( !empty($settings->back_delay) ) ? $settings->back_delay : 1500; ?>,
            loop: <?php echo ( $settings->enable_loop == 'no' ) ? 'false' : 'true'; ?>,
            loopCount: false,
            <?php if( $settings->show_cursor == 'yes' ) {
                        echo "showCursor: true,\n";
                        echo ( !empty($settings->cursor_text) ) ? 'cursorChar: "'.$settings->cursor_text.'",' : 'cursorChar: "|",';
                  }else{
                        echo 'showCursor: false,';
                  }
            ?>
      });
<?php }elseif ( $settings->effect_type == 'slide_up' ) { ?>
      jQuery('.fl-node-<?php echo $id; ?> .uabb-slide-main')
            .vTicker('init', {
                  speed: <?php echo ( !empty($settings->animation_speed) ) ? $settings->animation_speed : 200; ?>, 
                  pause: <?php echo ( !empty($settings->pause_time) ) ? $settings->pause_time : 3000; ?>,
                  mousePause: <?php echo ( $settings->pause_hover == 'yes') ? 'true' : 'false'; ?>,
                  /*showItems: <?php echo ( !empty($settings->show_items) ) ? $settings->show_items : 1; ?>,*/
            });

      jQuery( window ).resize(function() {
        jQuery('.fl-node-<?php echo $id; ?> .uabb-slide-main')
        .vTicker('init', {
              speed: <?php echo ( !empty($settings->animation_speed) ) ? $settings->animation_speed : 200; ?>, 
              pause: <?php echo ( !empty($settings->pause_time) ) ? $settings->pause_time : 3000; ?>,
              mousePause: <?php echo ( $settings->pause_hover == 'yes') ? 'true' : 'false'; ?>,
        });
      });

<?php } ?>