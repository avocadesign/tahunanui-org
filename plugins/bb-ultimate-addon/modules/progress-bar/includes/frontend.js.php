jQuery(document).ready( function() {

	function progress( percent, $element ) {
	    var progressBarWidth = percent + '%';
	    $element.find('.uabb-progress-bar').animate({ width: progressBarWidth }, 500);
	    if( percent > 50 ) {
		    <?php
		    if( $settings->horizontal_style != 'style4' ) {
		    ?>
		    $element.find('.uabb-progress-info').animate({ width: progressBarWidth }, 500);
		    <?php
		    }
		    ?>
	    }
	}

	function progressHeight( percent, $element ) {
	    var progressBarWidth = percent + '%';
	    $element.find('.uabb-progress-bar').animate({ height: progressBarWidth }, 500);
	}

	function commaSeparateNumber(val){
	    while (/(\d+)(\d{3})/.test(val.toString())){
			val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}
		if( isNaN( val ) ) return 0;
	    return val;
	}

	<?php
	$layout = ( $settings->layout == 'circular' ) ? 'circular' : 'horizontal';
	if( count( $settings->$layout ) > 0 ) {
		for( $i = 0; $i < count( $settings->$layout ); $i++ ) {
			$tmp = $settings->$layout;
			if( is_object( $tmp[$i] ) ) {

				if( $settings->layout == 'vertical' ) {
?>
	progressHeight( <?php echo $tmp[$i]->horizontal_number; ?>, jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-vertical.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-box') );

	jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-vertical.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-value').animate( { progressBarValue: <?php echo $tmp[$i]->horizontal_number; ?> }, {
		duration: 2000,
		easing:'swing', // can be anything
		step: function() { // called on every step
			// Update the element's text with rounded-up value:
			jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-vertical.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-value').html(commaSeparateNumber(Math.round(this.progressBarValue)) + '%');
		}
	});

	<?php
				} else if( $settings->layout == 'horizontal' ) {
	?>

	jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-horizontal.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-value').animate( { progressBarValue: <?php echo $tmp[$i]->horizontal_number; ?> }, {
		duration: 2000,
		easing:'swing', // can be anything
		step: function() { // called on every step
			// Update the element's text with rounded-up value:
			jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-horizontal.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-value').html(commaSeparateNumber(Math.round(this.progressBarValue)) + '%');
		}
	});


	progress( <?php echo $tmp[$i]->horizontal_number; ?>, jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-horizontal.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-progress-box') );
	//console.log(jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-vertical' ).find('.uabb-progress-value'));

	<?php
				}
			}
		}
	}
	?>

} );

<?php
	$layout = ( $settings->layout == 'circular' ) ? 'circular' : 'horizontal';
	if( count( $settings->$layout ) > 0 ) {
		for( $i = 0; $i < count( $settings->$layout ); $i++ ) {
			$tmp = $settings->$layout;
			if( is_object( $tmp[$i] ) ) {
				if( $settings->layout == 'circular' ) {
?>
jQuery(document).ready( function() {
		var circleval_<?php echo $i; ?> = jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-circular.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-circular-bg');

		var circle = circleval_<?php echo $i; ?>;

	    var percent_counter = jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-layout-circular.uabb-progress-bar-<?php echo $i; ?>' ).find('.uabb-percent-counter');

	    var interval = 5;
	    var angle = 0;
	    var angle_increment = 1;

	    <?php
	    $stroke_thickness = ( $settings->stroke_thickness != '' ) ? $settings->stroke_thickness : '10';
	    $width = !empty( $settings->circular_thickness ) ? $settings->circular_thickness : 300;
	    $pos = ( $width / 2 );
	    $radius = $pos - ( 2 * $stroke_thickness );
	    ?>

	    var r = <?php echo $radius; ?>;

	    var timer<?php echo $i; ?> = window.setInterval(function () {
	        circle.attr( "stroke-dasharray", ( 3.14*2*r*(angle/360) ) + ", 20000" );
	        
	        percent_counter.html( parseInt(angle/360*100) + '%' );
	        
	        if ( angle >= ( 3.6 * <?php echo $tmp[$i]->circular_number; ?> ) ) {
	            window.clearInterval(timer<?php echo $i; ?>);
	        }
	        
	        angle += angle_increment;
	    }.bind(this), interval);

	    jQuery(this).unbind();
} );

<?php
				}
			}
		}
	}
	?>
