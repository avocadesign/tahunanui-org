<?php
$settings->background_color = UABB_Helper::uabb_colorpicker( $settings, 'background_color', true );
$settings->gradient_color = UABB_Helper::uabb_colorpicker( $settings, 'gradient_color', true );

?>

<?php
if( $settings->layout == 'vertical' || $settings->layout == 'circular' ) {
	$layout = ( $settings->layout == 'circular' ) ? 'circular' : 'horizontal';
?>

.fl-node-<?php echo $id; ?> .uabb-pb-list{
    text-align: <?php echo $settings->overall_alignment; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-pb-list li {
	display: inline-block;
	margin: 0 <?php echo ( $settings->spacing != '' ) ? $settings->spacing : '10'; ?>px 30px 0;
	<?php
	if( $settings->layout == 'circular' ) {
	?>
	width: <?php echo !empty( $settings->circular_thickness ) ? $settings->circular_thickness : '300'; ?>px;
	height: <?php echo !empty( $settings->circular_thickness ) ? $settings->circular_thickness : '300'; ?>px;
	max-width: 100%;
	<?php
	} else {
	?>
	width: <?php echo !empty( $settings->vertical_width ) ? $settings->vertical_width : '300'; ?>px;
	max-width: 100%;
	<?php
	}
	?>
}
.fl-node-<?php echo $id; ?> .uabb-pb-list li:last-of-type {
    margin-right: 0;
}
<?php
} else if( $settings->layout == 'horizontal' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-pb-list li {
	display: block;
	margin: 0 0 <?php echo ( $settings->spacing != '' ) ? $settings->spacing : '10'; ?>px 0;
}
.fl-node-<?php echo $id; ?> .uabb-pb-list li:last-of-type {
    margin-bottom: 0;
}
<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-progress-wrap {
	<?php
	if( $settings->border_style != 'none' ) {
		$border_size = ( $settings->border_size != '' ) ? $settings->border_size : '1';
		echo 'border: ' . $border_size . 'px ' . $settings->border_style . ' ' . $settings->border_color . ';';
	} else {
		echo ( $settings->border_radius != '' ) ? 'border-radius: ' . $settings->border_radius . 'px;' : '';
	}
	
	?>
	overflow: hidden;
}

.fl-node-<?php echo $id; ?> .uabb-progress-bar {
<?php
	if( $settings->border_style == 'none' ) {
		echo ( $settings->border_radius != '' ) ? 'border-radius: ' . $settings->border_radius . 'px;' : '';
	}
?>
}

.fl-node-<?php echo $id; ?> .uabb-progress-title {
	color: <?php echo uabb_theme_text_color( $settings->text_color ); ?>;
	<?php
	if( $settings->text_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->text_font_family );
	}

	echo ( $settings->text_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->text_font_size['desktop'] . 'px;' : '';

	echo ( $settings->text_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->text_line_height['desktop'] . 'px;' : '';


	if( $settings->layout == 'horizontal' ) {

		echo ( $settings->horizontal_style == 'style1' || ( $settings->horizontal_style == 'style4' && $settings->text_position == 'above' ) ) ? 'padding-bottom: 10px;' : ( ( $settings->horizontal_style == 'style2' || ( $settings->horizontal_style == 'style4' && $settings->text_position == 'below' ) ) ? 'padding-top: 10px;' : 'padding: 10px;' );

	} else if( $settings->layout == 'vertical' ) {
		echo ( $settings->vertical_style == 'style1' ) ? 'padding-bottom: 10px;' : ( ( $settings->vertical_style == 'style2' ) ? 'padding-top: 10px;' : 'padding: 10px;' );
	}
	?>
}

.fl-node-<?php echo $id; ?> .uabb-ba-text {
	color: <?php echo uabb_theme_text_color( $settings->before_after_color ); ?>;
	<?php
	if( $settings->before_after_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->before_after_font_family );
	}

	echo ( $settings->before_after_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->before_after_font_size['desktop'] . 'px;' : '';

	echo ( $settings->before_after_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->before_after_line_height['desktop'] . 'px;' : '';

	?>
}

.fl-node-<?php echo $id; ?> .uabb-percent-counter {
	color: <?php echo uabb_theme_text_color( $settings->number_color ); ?>;
	<?php
	if( $settings->before_after_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->before_after_font_family );
	}

	echo ( $settings->before_after_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->before_after_font_size['desktop'] . 'px;' : '';

	echo ( $settings->before_after_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->before_after_line_height['desktop'] . 'px;' : '';

	?>
}

.fl-node-<?php echo $id; ?> .uabb-progress-value,
.fl-node-<?php echo $id; ?> .uabb-percent-counter {
	color: <?php echo uabb_theme_text_color( $settings->number_color ); ?>;
	<?php
	if( $settings->number_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->number_font_family );
	}

	echo ( $settings->number_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->number_font_size['desktop'] . 'px;' : '';

	echo ( $settings->number_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->number_line_height['desktop'] . 'px;' : '';


	if( $settings->layout == 'horizontal' ) {

		echo ( $settings->horizontal_style == 'style1' ) ? 'padding-bottom: 10px;' : ( ( $settings->horizontal_style == 'style2' ) ? 'padding-top: 10px;' : 'padding: 10px;' );

	} else if( $settings->layout == 'vertical' ) {
		echo ( $settings->vertical_style == 'style1' ) ? 'padding-bottom: 10px;' : ( ( $settings->vertical_style == 'style2' ) ? 'padding-top: 10px;' : 'padding: 10px;' );
	}
	?>
}

<?php
$layout = ( $settings->layout == 'circular' ) ? 'circular' : 'horizontal';
if( count( $settings->$layout ) > 0 ) {
	for( $i = 0; $i < count( $settings->$layout ); $i++ ) {
		$tmp = $settings->$layout;
		if( is_object( $tmp[$i] ) ) {
?>
.fl-node-<?php echo $id; ?> .uabb-progress-bar-<?php echo $i; ?> .uabb-progress-wrap {
	background: <?php echo $tmp[$i]->background_color; ?>;
}

<?php
			if( $settings->layout == 'horizontal' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-layout-horizontal.uabb-progress-bar-<?php echo $i; ?> .uabb-progress-bar {
	<?php
	if( $settings->stripped == 'yes' ) {
	?>
	background-color: <?php echo uabb_theme_base_color( $tmp[$i]->gradient_color ); ?>;
	background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	-webkit-background-size: 40px 40px;

	background-size: 40px 40px;
	<?php
	} else {
?>
	background: <?php echo uabb_theme_base_color( $tmp[$i]->gradient_color ); ?>;
<?php
	}
	?>
}

.fl-node-<?php echo $id; ?> .uabb-layout-horizontal.uabb-progress-bar-style-style3.uabb-progress-bar-<?php echo $i; ?> .uabb-progress-box .uabb-progress-info {
    <?php
    if( $tmp[$i]->horizontal_number > 50 ) {
    ?>
    width: 50%;
    <?php
    } else {
    ?>
    width: 100%;
    <?php
    }
    ?>
    
}
<?php
			} else if( $settings->layout == 'vertical' ) {
?>

.fl-node-<?php echo $id; ?> .uabb-layout-vertical.uabb-progress-bar-<?php echo $i; ?> .uabb-progress-bar {
	width: 100%;
	<?php
	if( $settings->stripped == 'yes' ) {
	?>
	background-color: <?php echo uabb_theme_base_color( $tmp[$i]->gradient_color ); ?>;
	background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);

	-webkit-background-size: 40px 40px;

	background-size: 40px 40px;
	<?php
	} else {
?>
	background: <?php echo uabb_theme_base_color( $tmp[$i]->gradient_color ); ?>;
<?php
	}
	?>
}

<?php
			}
		}
	}
}
?>
.fl-node-<?php echo $id; ?> .uabb-progress-bar-wrapper.uabb-layout-circular {
	max-width: <?php echo !empty( $settings->circular_thickness ) ? $settings->circular_thickness : '300'; ?>px;
    max-height: <?php echo !empty( $settings->circular_thickness ) ? $settings->circular_thickness : '300'; ?>px;
}

.fl-node-<?php echo $id; ?> .uabb-layout-vertical.uabb-progress-bar-style-style3 .uabb-progress-title {
	text-align: <?php echo $settings->title_alignment ?>;
}

.fl-node-<?php echo $id; ?> .uabb-layout-vertical .uabb-progress-wrap {
	height: <?php echo ( $settings->vertical_thickness != '' ) ? $settings->vertical_thickness : '200'; ?>px;
}

.fl-node-<?php echo $id; ?> .uabb-layout-horizontal.uabb-progress-bar-style-style2 .uabb-progress-box,
.fl-node-<?php echo $id; ?> .uabb-layout-horizontal.uabb-progress-bar-style-style1 .uabb-progress-box {
	height: <?php echo ( $settings->horizontal_thickness != '' ) ? $settings->horizontal_thickness : '20'; ?>px;
}

.fl-node-<?php echo $id; ?> .uabb-layout-horizontal.uabb-progress-bar-style-style4 .uabb-progress-box .uabb-progress-info {
	width: 100%;
}

<?php
if( $global_settings->responsive_enabled ) { // Global Setting If started
?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {
    	.fl-node-<?php echo $id; ?> .uabb-progress-title {
			<?php
			echo ( $settings->text_font_size['medium'] != '' ) ? 'font-size: ' . $settings->text_font_size['medium'] . 'px;' : '';

			echo ( $settings->text_line_height['medium'] != '' ) ? 'line-height: ' . $settings->text_line_height['medium'] . 'px;' : '';
			?>
		}

		.fl-node-<?php echo $id; ?> .uabb-progress-value,
		.fl-node-<?php echo $id; ?> .uabb-percent-counter {
			<?php
			echo ( $settings->number_font_size['medium'] != '' ) ? 'font-size: ' . $settings->number_font_size['medium'] . 'px;' : '';

			echo ( $settings->number_line_height['medium'] != '' ) ? 'line-height: ' . $settings->number_line_height['medium'] . 'px;' : '';
			?>
		}

		.fl-node-<?php echo $id; ?> .uabb-ba-text {
			<?php
			echo ( $settings->before_after_font_size['medium'] != '' ) ? 'font-size: ' . $settings->before_after_font_size['medium'] . 'px;' : '';

			echo ( $settings->before_after_line_height['medium'] != '' ) ? 'line-height: ' . $settings->before_after_line_height['medium'] . 'px;' : ''; ?>
		}
    }
 
     @media ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {
     	.fl-node-<?php echo $id; ?> .uabb-progress-title {
			<?php
			echo ( $settings->text_font_size['small'] != '' ) ? 'font-size: ' . $settings->text_font_size['small'] . 'px;' : '';

			echo ( $settings->text_line_height['small'] != '' ) ? 'line-height: ' . $settings->text_line_height['small'] . 'px;' : '';
			?>
		}

		.fl-node-<?php echo $id; ?> .uabb-progress-value,
		.fl-node-<?php echo $id; ?> .uabb-percent-counter {
			<?php
			echo ( $settings->number_font_size['small'] != '' ) ? 'font-size: ' . $settings->number_font_size['small'] . 'px;' : '';

			echo ( $settings->number_line_height['small'] != '' ) ? 'line-height: ' . $settings->number_line_height['small'] . 'px;' : '';
			?>
		}

		.fl-node-<?php echo $id; ?> .uabb-ba-text {
			<?php
			echo ( $settings->before_after_font_size['small'] != '' ) ? 'font-size: ' . $settings->before_after_font_size['small'] . 'px;' : '';

			echo ( $settings->before_after_line_height['small'] != '' ) ? 'line-height: ' . $settings->before_after_line_height['small'] . 'px;' : ''; ?>
		}
    }
<?php
}
?>