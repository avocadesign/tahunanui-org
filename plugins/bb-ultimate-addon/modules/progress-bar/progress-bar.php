<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class ProgressBarModule
 */
class ProgressBarModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Progress Bar', 'uabb'),
            'description'   => __('Progress Bar', 'uabb'),
            'category'		=> __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/progress-bar/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/progress-bar/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
        $this->add_js('jquery-waypoints');
    }

    public function render_horizontal_content( $obj, $style = '', $position = '' ) {

        if( $this->settings->horizontal_style == $style ) {
            if( $style == 'style4' ) {
                if( $this->settings->text_position == $position ) {

                    echo '<div class="uabb-progress-info">
                        <' . $this->settings->text_tag_selection . ' class="uabb-progress-title">' . $obj->horizontal_before_number . '</' . $this->settings->text_tag_selection . '>
                    </div>';
                }

            } else if( $style != 'style3' ) {

                echo '<div class="uabb-progress-info">
                        <' . $this->settings->text_tag_selection . ' class="uabb-progress-title">' . $obj->horizontal_before_number . '</' . $this->settings->text_tag_selection . '>
                        <div class="uabb-progress-value">' . $obj->horizontal_number . '%</div>
                    </div>';
            }
        }
    }

    public function render_horizontal_progress_bar( $obj ) {
        if( $this->settings->horizontal_style == 'style3' ) {
            echo '<div class="uabb-progress-wrap">
                    <div class="uabb-progress-box">
                        <div class="uabb-progress-bar"></div>
                        <div class="uabb-progress-info">
                            <' . $this->settings->text_tag_selection . ' class="uabb-progress-title">' . $obj->horizontal_before_number . '</' . $this->settings->text_tag_selection . '>
                            <div class="uabb-progress-value">' . $obj->horizontal_number . '%</div>
                        </div>
                    </div>
                </div>';
        } else if( $this->settings->horizontal_style == 'style4' ) {
            echo '<div class="uabb-progress-wrap">
                    <div class="uabb-progress-box">
                        <div class="uabb-progress-bar"></div>
                        <div class="uabb-progress-info">
                            <div class="uabb-progress-value">' . $obj->horizontal_number . '%</div>
                        </div>
                    </div>
                </div>';
        } else {
            echo '<div class="uabb-progress-wrap">
                    <div class="uabb-progress-box">
                        <div class="uabb-progress-bar"></div>
                    </div>
                </div>';
        }
    }


    public function render_vertical_content( $obj, $style = '' ) {
        
        if( $this->settings->vertical_style == $style ) {
            if( $style != 'style3' ) {
                echo '<div class="uabb-progress-info">
                        <' . $this->settings->text_tag_selection . ' class="uabb-progress-title">' . $obj->horizontal_before_number . '</' . $this->settings->text_tag_selection . '>
                        <div class="uabb-progress-value">' . $obj->horizontal_number . '%</div>
                    </div>';
            } else {
                echo '<div class="uabb-progress-info">
                        <' . $this->settings->text_tag_selection . ' class="uabb-progress-title">' . $obj->horizontal_before_number . '</' . $this->settings->text_tag_selection . '>
                    </div>';
            }
        }
    }

    public function render_vertical_progress_bar( $obj ) {
        if( $this->settings->vertical_style == 'style3' ) {
            echo '<div class="uabb-progress-wrap">
                    <div class="uabb-progress-box">
                        <div class="uabb-progress-bar"></div>
                        <div class="uabb-progress-info">
                            <div class="uabb-progress-value">' . $obj->horizontal_number . '%</div>
                        </div>
                    </div>
                </div>';
        } else {
            echo '<div class="uabb-progress-wrap">
                    <div class="uabb-progress-box">
                        <div class="uabb-progress-bar"></div>
                    </div>
                </div>';
        }
    }


    public function render_circle_progress_bar( $obj ) {
               
        $obj->background_color = UABB_Helper::uabb_colorpicker( $obj, 'background_color', true );
        $obj->gradient_color   = UABB_Helper::uabb_colorpicker( $obj, 'gradient_color', true );

        $stroke_thickness = ( $this->settings->stroke_thickness != '' ) ? $this->settings->stroke_thickness : '10';
        $width = !empty( $this->settings->circular_thickness ) ? $this->settings->circular_thickness : 300;
        $pos = ( $width / 2 );
        $radius = $pos - ( 2 * $stroke_thickness );
        $dash = number_format( ( ( M_PI * 2 ) * $radius ), 2, '.', '');
        

        $txt = '<svg viewBox="0 0 '. $width .' '. $width .'" preserveAspectRatio="none">
            <circle cx="' . $pos . '" cy="' . $pos . '" r="' . $radius . '" class="uabb-circular-bg"  fill="' . $obj->background_color . '" stroke="' . uabb_theme_base_color( $obj->gradient_color ) . '" stroke-width="' . $stroke_thickness . '" stroke-dasharray="0,20000" transform="rotate(-90,' . $pos . ',' . $pos . ')" />
        </svg>';

        echo $txt;
    }

}



/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('ProgressBarModule', array(
    'elements'       => array( // Tab
        'title'         => __('Style', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'items' => array(
                'title' => __('', 'uabb'), // Section Title
                'fields' => array(
                    'layout'    => array(
                        'type'          => 'select',
                        'label'         => __('Layout', 'uabb'),
                        'default'       => 'horizontal',
                        'options'       => array(
                            'horizontal'         => __('Horizontal', 'uabb'),
                            'vertical'          => __('Vertical', 'uabb'),
                            /*'circular'          => __( 'Circular', 'uabb' ),*/
                        ),
                        'toggle'        => array(
                            'horizontal'         => array(
                                'sections'      => array( 'horizontal', 'horizontal_layout', 'text_typography', 'border' ),
                                'fields' => array( 'stripped' )
                            ),
                            'vertical'          => array(
                                'sections'      => array( 'vertical', 'horizontal_layout', 'text_typography', 'border' ),
                                'fields' => array( 'stripped' )
                            ),
                            'circular'  => array(
                                'sections' => array( 'circular', 'circular_layout', 'before_after_typography' ),
                            )
                        )
                    ),
                )
            ),
            'horizontal_layout' => array(
                'title' => __('Progress Bar Items', 'uabb'),
                'fields' => array(
                    'horizontal'   => array(
                        'type'         => 'form',
                        'label'        => __('Progress Bar Item', 'uabb'),
                        'form'         => 'progress_bar_horizontal_item_form',
                        'preview_text' => 'horizontal_number',
                        'multiple'     => true
                    ),
                )
            ),
            /*'vertical_layout' => array(
                'title' => __('Progress Bar Items', 'uabb'),
                'fields' => array(
                    'vertical'   => array(
                        'type'         => 'form',
                        'label'        => __('Progress Bar Item', 'uabb'),
                        'form'         => 'progress_bar_horizontal_item_form',
                        'preview_text' => 'horizontal_number',
                        'multiple'     => true
                    ),
                )
            ),*/
            'circular_layout' => array(
                'title' => __('Progress Bar Items', 'uabb'),
                'fields' => array(
                    'circular'   => array(
                        'type'         => 'form',
                        'label'        => __('Progress Bar Item', 'uabb'),
                        'form'         => 'progress_bar_circular_item_form',
                        'preview_text' => 'circular_number',
                        'multiple'     => true
                    ),
                )
            ),
        )
    ),
    'general'       => array( // Tab
        'title'         => __('Layout', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'spacing_options'       => array(
                'title'         => '',
                'fields'        => array(
                    'overall_alignment'          => array(
                        'type'          => 'select',
                        'label'         => __('Overall Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'center'         => __('Center', 'uabb'),
                            'left'         => __('Left', 'uabb'),
                            'right'         => __('Right', 'uabb'),
                        ),
                    ),
                    'spacing'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Spacing', 'uabb' ),
                        'placeholder'       => '10',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'stripped'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Striped Selector', 'uabb' ),
                        'default'       => 'no',
                        'help'          => __( 'Enable to display stripes on progress', 'uabb' ),
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                    ),
                )
            ),
            'horizontal'          => array(
                'title'         => __('Horizontal', 'uabb'),
                'fields'        => array(
                    'horizontal_style'          => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'uabb'),
                        'default'       => 'style1',
                        'options'       => array(
                            'style1'         => __('Number and Text Above the Progress Bar', 'uabb'),
                            'style2'         => __('Number and Text Below the Progress Bar', 'uabb'),
                            'style3'         => __('Number and Text Inside the Progress Bar', 'uabb'),
                            'style4'        => __( 'Number Inside and Text Outside the Progress Bar', 'uabb' ),
                        ),
                        'toggle' => array(
                            'style1' => array(
                                'fields' => array( 'horizontal_thickness' )
                            ),
                            'style2' => array(
                                'fields' => array( 'horizontal_thickness' )
                            ),
                            'style4' => array(
                                'fields' => array( 'text_position' )
                            )
                        )
                    ),
                    'text_position'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Text Position', 'uabb' ),
                        'default'       => 'above',
                        'options'       => array(
                            'above'      => __( 'Above', 'uabb' ),
                            'below'        => __( 'Below', 'uabb' ),
                        )
                    ),
                    'horizontal_thickness'     => array(
                        'type'          => 'text',
                        'label'         => __('Thickness', 'uabb'),
                        'size'          => '8',
                        'placeholder'       => '20',
                        'description'   => 'px',
                        'help'          => __( 'This is basically the height', 'uabb'),
                    ),
                )
            ),
            'vertical'          => array(
                'title'         => __('Vertical', 'uabb'),
                'fields'        => array(
                    'vertical_style'          => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'uabb'),
                        'default'       => 'style1',
                        'options'       => array(
                            'style1'         => __('Number and Text Above the Progress Bar', 'uabb'),
                            'style2'         => __('Number and Text Below the Progress Bar', 'uabb'),
                            'style3'         => __('Number inside and Text below the Progress Bar', 'uabb'),
                        ),
                        'toggle' => array(
                            'style3' => array(
                                'fields' => array( 'title_alignment' )
                            )
                        )
                    ),
                    'title_alignment'          => array(
                        'type'          => 'select',
                        'label'         => __('Title Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'center'         => __('Center', 'uabb'),
                            'left'         => __('Left', 'uabb'),
                            'right'         => __('Right', 'uabb'),
                        ),
                    ),
                    'vertical_thickness'     => array(
                        'type'          => 'text',
                        'label'         => __('Vertical Height', 'uabb'),
                        'size'          => '8',
                        'placeholder'   => '200',
                        'description'   => 'px',
                    ),
                    'vertical_width'     => array(
                        'type'          => 'text',
                        'label'         => __('Vertical Width', 'uabb'),
                        'size'          => '8',
                        'placeholder'   => '300',
                        'description'   => 'px',
                    ),
                )
            ),
            'circular'          => array(
                'title'         => __('Circular', 'uabb'),
                'fields'        => array(
                    'circular_thickness'     => array(
                        'type'          => 'text',
                        'label'         => __('Circle Width', 'uabb'),
                        'size'          => '8',
                        'placeholder'       => '300',
                        'description'   => 'px',
                    ),
                    'stroke_thickness'     => array(
                        'type'          => 'text',
                        'label'         => __('Stroke Thickness', 'uabb'),
                        'size'          => '8',
                        'placeholder'       => '10',
                        'description'   => 'px',
                        'help'          => __( 'This is the thickness of stroke.', 'uabb'),
                    ),
                )
            ),
            'border'       => array( // Section
                'title'         => __('Border', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'uabb'),
                        'default'       => 'solid',
                        'options'       => array(
                            'none'      => __('None', 'uabb'),
                            'solid'      => __('Solid', 'uabb'),
                            'dashed'      => __('Dashed', 'uabb'),
                            'dotted'      => __('Dotted', 'uabb'),
                            'double'      => __('Double', 'uabb'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array( 'border_radius' )
                            ),
                            'solid' => array(
                                'fields' => array( 'border_size', 'border_color' )
                            ),
                            'dashed' => array(
                                'fields' => array( 'border_size', 'border_color' )
                            ),
                            'dotted' => array(
                                'fields' => array( 'border_size', 'border_color' )
                            ),
                            'double' => array(
                                'fields' => array( 'border_size', 'border_color' )
                            ),
                        ),
                    ),
                    'border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Size', 'uabb' ),
                        'placeholder'   => '1',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                    'border_radius'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Radius', 'uabb' ),
                        'default'       => '',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'text_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-progress-title'
                            )
                        ),
                        'color'   => array(
                            'label' => 'Text Color',
                        ),
                        'tag_selection' => array(
                            'default' => 'h4'
                        )
                    ), 
                ),
                array(),
                'text'
            ),
            'before_after_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Before/After Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-ba-text'
                            )
                        ),
                        'color'   => array(
                            'label' => 'Color',
                        ),
                    ), 
                ),
                array( 'tag_selection' ),
                'before_after'
            ),
            'number_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Number', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-progress-value, .uabb-percent-counter'
                            )
                        ),
                        'color'   => array(
                            'label' => 'Number Color',
                        ),
                    ), 
                ),
                array( 'tag_selection' ),
                'number'
            ),
        )
    ),
));


/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('progress_bar_horizontal_item_form', array(
    'title' => __('Add Progress Bar Item', 'uabb'),
    'tabs'  => array(
        'general'       => array( // Tab
            'title'         => __('Layout', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'horizontal'          => array(
                    'title'         => __('Number', 'uabb'),
                    'fields'        => array(
                        'horizontal_number'     => array(
                            'type'          => 'text',
                            'label'         => __('Number', 'uabb'),
                            'default'       => '100',
                            'size'          => '8',
                            'description'   => '%'
                        ),
                        'horizontal_before_number'     => array(
                            'type'          => 'text',
                            'label'         => __('Title', 'uabb'),
                            'default'       => __('Progress Bar', 'uabb'),
                        ),
                    )
                ),
                'general'       => array( // Section
                    'title'         => __('Style', 'uabb'), // Section Title
                    'fields'        => array( // Section Fields
                        'background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Background Color', 'uabb'),
                                'default'       => '#e5e5e5',
                            )
                        ),
                        'background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                        
                        'gradient_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Progress Color', 'uabb'),
                            )
                        ),
                        'gradient_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                        'gradient_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Progress Color', 'uabb'),
                            )
                        ),
                        'gradient_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                        
                    )
                ),
            )
        ),
    )
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('progress_bar_circular_item_form', array(
    'title' => __('Add Progress Bar Item', 'uabb'),
    'tabs'  => array(
        'general'       => array( // Tab
            'title'         => __('Layout', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'circular'          => array(
                    'title'         => __('Number', 'uabb'),
                    'fields'        => array(
                        'circular_number'     => array(
                            'type'          => 'text',
                            'label'         => __('Number', 'uabb'),
                            'default'       => '100',
                            'size'          => '8',
                            'description'   => '%'
                        ),
                        'circular_before_number'     => array(
                            'type'          => 'text',
                            'label'         => __('Text Before Number', 'uabb'),
                            'default'       => __('Before Text', 'uabb'),
                        ),
                        'circular_after_number'     => array(
                            'type'          => 'text',
                            'label'         => __('Text After Number', 'uabb'),
                            'default'       => __('After Text', 'uabb'),
                        ),
                    )
                ),
                'general'       => array( // Section
                    'title'         => __('Style', 'uabb'), // Section Title
                    'fields'        => array( // Section Fields
                        'background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Background Color', 'uabb'),
                                'default'       => '#e5e5e5',
                            )
                        ),
                        'background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                        
                        'gradient_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Progress Color', 'uabb'),
                            )
                        ),
                        'gradient_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    )
                ),
            )
        ),
    )
));

