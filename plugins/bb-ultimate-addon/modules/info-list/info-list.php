<?php

class UABBInfoList extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Info List', 'fl-builder' ),
            'description'     => __( 'A totally awesome module!', 'fl-builder' ),
            'category'      => __('Ultimate Addons', 'fl-builder'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/info-list/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/info-list/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
        $this->add_css('info-list', $this->url . 'css/info-list.css');
    }


    /**
     * @method render_image
     */
    public function render_image( $item, $settings )
    {
        if ( $settings->list_icon_style == 'circle' ) {
            $infolist_icon_size = $settings->icon_image_size / 2;
        }else if ( $settings->list_icon_style == 'square' ) {
            $infolist_icon_size = $settings->icon_image_size / 2;
        }else if ( $settings->list_icon_style == 'custom' ) {
            $infolist_icon_size = $settings->icon_image_size;
        }else {
            $infolist_icon_size = $settings->icon_image_size;
        }
        $imageicon_array = array(
 
            /* General Section */
            'image_type' => $item->image_type,
         
            /* Icon Basics */
            'icon' => $item->icon,
            'icon_size' => $infolist_icon_size,
            'icon_align' => "center",
         
            /* Image Basics */
            'photo_source' => $item->photo_source,
            'photo' => $item->photo,
            'photo_url' => $item->photo_url,
            'img_size' => $settings->icon_image_size,
            'img_align' => "center",
            'photo_src' => ( isset( $item->photo_src ) ) ? $item->photo_src : '' ,
         
            /* Icon Style */
            'icon_style' => $settings->list_icon_style,
            'icon_bg_size' => $settings->list_icon_bg_padding,
            'icon_border_style' => "",
            'icon_border_width' => "",
            'icon_bg_border_radius' => $settings->list_icon_bg_border_radius,
         
            /* Image Style */
            'image_style' => $settings->list_icon_style,
            'img_bg_size' => $settings->list_icon_bg_padding,
            'img_border_style' => "",
            'img_border_width' => "",
            'img_bg_border_radius' => $settings->list_icon_bg_border_radius,
        ); 
        /* Render HTML Function */
        FLBuilder::render_module_html( 'image-icon', $imageicon_array );
    }
    /**
     * @method render_text
     */
    public function render_each_item( $item, $list_item_counter )
    {
        echo '<li class="uabb-info-list-item info-list-item-dynamic'.$list_item_counter.'">';
        echo '<div class="uabb-info-list-content-wrapper '.$this->settings->icon_position.'">';

        if ( !empty( $item->list_item_link ) && $item->list_item_link === "complete" && !empty($item->list_item_url) ) {

            echo '<a href="'.$item->list_item_url.'" class="uabb-info-list-link" target="'.$item->list_item_link_target.'"></a>';
        }

        echo '<div class="uabb-info-list-icon info-list-icon-dynamic'. $list_item_counter.'">';

        if ( !empty( $item->list_item_link ) && $item->list_item_link == "icon") {
            echo '<a href="'. $item->list_item_url .'" class="uabb-info-list-link" target="'. $item->list_item_link_target .'"></a>';
        }
            $this->render_image( $item, $this->settings );
      
        echo '</div>';

        echo '<div class="uabb-info-list-content '. $this->settings->icon_position.' info-list-content-dynamic'. $list_item_counter.'">';
        
        echo '<'. $this->settings->heading_tag_selection . ' class="uabb-info-list-title">';
        if ( !empty( $item->list_item_link ) && $item->list_item_link === "list-title" && !empty($item->list_item_url) ) {

            echo '<a href="'. $item->list_item_url .'" target="'.$item->list_item_link_target.'">';

        }
        echo $item->list_item_title;
        if ( !empty( $item->list_item_link ) && $item->list_item_link === "list-title" && !empty($item->list_item_url) ) {

            echo '</a>';

        }
        echo '</'. $this->settings->heading_tag_selection . ' >';
        
        
        echo '<div class="uabb-info-list-description uabb-text-editor info-list-description-dynamic'. $list_item_counter.'">';
        if ( strpos( $item->list_item_description, "</p>" ) > 0 ) {
            echo $item->list_item_description;
        }else{
            echo "<p>".$item->list_item_description."</p>";
        }

        echo '</div>';

        echo '</div>';
                    
        $animation_classes = "";
        if ( $this->settings->list_connector_animation == "yes" && ( $this->settings->icon_position == "left" || $this->settings->icon_position == "right" ) ) {
            $animation_classes = "fadeInUp animated";
        }else if($this->settings->list_connector_animation == "yes" && $this->settings->icon_position == "top" ){
            $animation_classes = "fadeInLeft animated";
        }
        else{
            $animation_classes = "";
        }

        
        $list_item_counter = $list_item_counter + 1;
        echo '</div>';
        //if( $this->settings->list_icon_style != "simple" ) {
            echo '<div class="uabb-info-list-connector '. $this->settings->icon_position.'  '.$animation_classes.'"></div>';
        //}
        
        echo '</li>';
    }
    /**
     * @method render_text
     */
    public function render_list()
    {
        $info_list_html= "";
        $list_item_counter = 0;
        foreach( $this->settings->add_list_item as $item ){
            $this->render_each_item( $item, $list_item_counter );
            $list_item_counter = $list_item_counter + 1;
        }
    }
}

FLBuilder::register_module('UABBInfoList', array(
    'info_list_item'       => array( // Tab
        'title'         => __('List Item', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'info_list_general'       => array( // Section
                'title'         => __('List Item Settings', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'add_list_item'     => array(
                        'type'         => 'form',
                        'label'        => __('Add List Item', 'fl-builder'),
                        'form'         => 'info_list_item_form',
                        'preview_text' => 'list_item_title',
                        'multiple'     => true
                    ),
                )
            )
        )
    ),

    'info_list_general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'info_list_general'       => array( // Section
                'title'         => __('List Settings', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'icon_position'   => array(
                        'type'          => 'select',
                        'label'         => __('Icon / Image Position', 'fl-builder'),
                        'description'   => '',
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Icon to the left', 'fl-builder'),
                            'right'     => __('Icon to the right', 'fl-builder'),
                            'top'       => __('Icon at top', 'fl-builder')
                        ),
                        'toggle' => array(
                            'left' => array(
                                'fields' => array( 'align_items', 'mobile_view' )
                            ),
                            'right' => array(
                                'fields' => array( 'align_items', 'mobile_view' )
                            ),
                        )
                    ),
                    'align_items' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Icon Vertical Alignment', 'uabb'),
                        'default'       => 'top',
                        'options'       => array(
                            'center'        => __('Center', 'uabb'),
                            'top'          => __('Top', 'uabb'),
                        ),
                    ),
                    'mobile_view' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Mobile Structure', 'uabb'),
                        'default'       => '',
                        'options'       => array(
                            ''        => __('Inline', 'uabb'),
                            'stack'   => __('Stack', 'uabb'),
                        ),
                        'preview'       => array(
                            'type'    => 'none'
                        )
                    ),
                    'icon_image_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Icon / Image Size', 'fl-builder'),
                        'description'   => 'px',
                        'size'          => '8',
                        'placeholder'   => '75',
                        'default'       => ''
                    ),
                    'space_between_elements'          => array(
                        'type'          => 'text',
                        'label'         => __('Space Between Two Elements', 'fl-builder'),
                        'description'   => 'px',
                        'size'          => '8',
                        'placeholder'   => '',
                        'default'       => ''
                    ),
                    'list_icon_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Icon / Image Style', 'fl-builder'),
                        'default'       => 'simple',
                        'description'   => '',
                        'options'       => array(
                            'simple'         => __('Simple', 'fl-builder'),
                            'square'         => __('Square', 'fl-builder'),
                            'circle'          => __('Circle', 'fl-builder'),
                            'custom'         => __('Design your own', 'fl-builder'),
                        ),
                        'toggle' => array(
                            'circle' => array(
                                'fields' => array( 'list_icon_bg_color', 'list_icon_bg_color_opc' )
                            ),
                            'square' => array(
                                'fields' => array( 'list_icon_bg_color', 'list_icon_bg_color_opc' )
                            ),
                            'custom' => array(
                                'fields' => array( 'list_icon_bg_color', 'list_icon_bg_color_opc', 'list_icon_bg_size', 'list_icon_bg_border_radius', 'list_icon_bg_padding', 'list_icon_border_style' )
                            )
                        )
                    ),
                    'list_icon_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Color Option for Background', 'fl-builder'),
                        )
                    ),
                    'list_icon_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'list_icon_bg_border_radius'          => array(
                        'type'          => 'text',
                        'label'         => __('Border Radius ( For Background )', 'fl-builder'),
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),

                    'list_icon_bg_padding'          => array(
                        'type'          => 'text',
                        'label'         => __('Padding ( For Background )', 'fl-builder'),
                        'default'       => '',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),
                    'list_icon_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'uabb'),
                        'default'       => 'none',
                        'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
                        'options'       => array(
                            'none'  => __( 'None', 'Border type.', 'uabb' ),
                            'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
                            'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
                            'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
                            'double' => __( 'Double', 'Border type.', 'uabb' )
                        ),
                    ),
                    'list_icon_border_width'    => array(
                        'type'          => 'text',
                        'label'         => __('Border Width', 'uabb'),
                        'default'       => '1',
                        'description'   => 'px',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'placeholder'   => '0',
                    ),
                    'list_icon_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                        )
                    ),
                )
            ),
            'info_list_connector'       => array( // Section
                'title'         => __('List Connector', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'list_connector_option'   => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Show Connector', 'fl-builder'),
                        'description'   => '',
                        'help'          => __( 'Select whether you would like to show connector on list items.', 'fl-builder' ),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                        'toggle'       => array(
                            'yes' => array(
                                'fields' => array( 'list_connector_color', 'list_connector_style', 'list_connector_animation' )
                            )
                        )

                    ),
                    'list_connector_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Connector Line Color', 'fl-builder'),
                        )
                    ),
                    'list_connector_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Connector Line Style', 'fl-builder'),
                        'description'   => '',
                        'default'       => 'solid',
                        'options'       => array(
                            'solid'         => __('Solid', 'fl-builder'),
                            'dashed'        => __('Dashed', 'fl-builder'),
                            'dotted'        => __('Dotted', 'fl-builder')
                        ),
                    ),
                    'list_connector_animation' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Connector Line Animation', 'fl-builder'),
                        'description'   => '',
                        'help'          => __( 'Select whether you want to animate connector line or not', 'fl-builder' ),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'         => __('Yes', 'fl-builder'),
                            'no'        => __('No', 'fl-builder')
                        ),
                    ),
                )
            )
        )
    ),

    'info_list_style'       => array( // Tab
        'title'         => __('Typography', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'heading_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                                'typography', 
                                array( 
                                    'title'     => __('Heading', 'uabb' ) ,
                                    'fields'   => array(
                                        'color'   => array(
                                            'label' => __('Choose Color', 'uabb'),
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-info-list-title',
                                                'property'        => 'color'
                                            )
                                        ),
                                        'tag_selection'   => array(
                                            'label' => __('Select Tag', 'uabb'),
                                            'default'   => 'h3',
                                        ),
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-info-list-title'
                                            )
                                        ),
                                        'margin_top' => array(
                                            'label' => __('Margin Top', 'uabb'),
                                            'type'  => 'text',
                                            'size'  => '8',
                                            'description'   => 'px',
                                            'max_length'    => '3',
                                        ),
                                        'margin_bottom' => array(
                                            'label' => __('Margin Bottom', 'uabb'),
                                            'type'  => 'text',
                                            'size'  => '8',
                                            'description'   => 'px',
                                            'max_length'    => '3',
                                        ),
                                    )
                                ), 
                                array(),
                                'heading' 
            ),
            'description_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                                'typography', 
                                array( 
                                    'title'     => __('Description', 'uabb' ),
                                    'fields'   => array(
                                        'color'   => array(
                                            'label' => __('Choose Color', 'uabb'),
                                            'preview'         => array(
                                                'type'            => 'css',
                                                'selector'        => '.uabb-info-list-content .uabb-info-list-description *',
                                                'property'        => 'color'
                                            )
                                        ),
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-info-list-description *',
                                                
                                            )
                                        ),
                                    )
                                ), 
                                array( 'tag_selection' ),
                                'description' 
            ),
        )
    )
));


//Add List Items
FLBuilder::register_settings_form('info_list_item_form', array(
    'title' => __( 'Add List Item', 'fl-builder' ),
    'tabs'  => array(
        'list_item_general'      => array(
            'title'         => __('General', 'fl-builder'),
            'sections'      => array(
                'title'       => array(
                    'title'         => __( 'General Settings', 'fl-builder' ),
                    'fields'        => array(
                        'list_item_title'          => array(
                            'type'          => 'text',
                            'label'         => __('Title', 'fl-builder'),
                            'description'   => '',
                            'default'       => 'Name of the element',
                            'help'          => __( 'Provide a title for this icon list item.', 'fl-builder' ),
                            'placeholder'         => 'Title',
                            'class'         => 'uabb-list-item-title'
                        ),
                        'list_item_url'          => array(
                            'type'          => 'link',
                            'label'         => __('Link', 'fl-builder')
                        ),
                        'list_item_link'          => array(
                            'type'          => 'select',
                            'label'         => __('Apply Link To', 'fl-builder'),
                            'default'       => 'no',
                            'options'       => array(
                                'no'        => __('No Link', 'fl-builder'),
                                'complete'  => __('Complete Box', 'fl-builder'),
                                'list-title' => __('List Title', 'fl-builder'),
                                'icon'      => __('Icon', 'fl-builder')
                            ),
                            'preview'       => 'none'
                        ),
                        'list_item_link_target'   => array(
                            'type'          => 'select',
                            'label'         => __('Link Target', 'fl-builder'),
                            'default'       => '_self',
                            'options'       => array(
                                '_self'         => __('Same Window', 'fl-builder'),
                                '_blank'        => __('New Window', 'fl-builder')
                            ),
                        ),
                        'list_item_description'          => array(
                            'type'          => 'editor',
                            'default'       => 'Enter description text here.',
                            'label'         => '',
                            'rows'          => 13
                        )
                    ),
                ),
            )
        ),

        'list_item_image'      => array(
            'title'         => __('Icon / Image', 'fl-builder'),
            'sections'      => array(
                'title'       => array(
                    'title'         => __( 'Icon / Image', 'fl-builder' ),
                    'fields'        => array(
                        'image_type'    => array(
                            'type'          => 'select',
                            'label'         => __('Image Type', 'uabb'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'          => __( 'None', 'Image type.', 'uabb' ),
                                'icon'          => __('Icon', 'uabb'),
                                'photo'         => __('Photo', 'uabb'),
                            ),
                            'toggle'        => array(
                                'icon'          => array(
                                    'sections'   => array( 'icon_basic',  'icon_style', 'icon_colors' ),
                                ),
                                'photo'         => array(
                                    'sections'   => array( 'img_basic', 'img_style' ),
                                )
                            ),
                        ),
                    ),
                ),
                /* Icon Basic Setting */
                'icon_basic'        => array( // Section
                    'title'         => 'Icon', // Section Title
                    'fields'        => array( // Section Fields
                        'icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb')
                        ),
                        'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'     => __('Icon Color', 'uabb'),
                            )
                        ),
                    )
                ),
                /* Image Basic Setting */
                'img_basic'     => array( // Section
                    'title'         => 'Image', // Section Title
                    'fields'        => array( // Section Fields
                        'photo_source'  => array(
                            'type'          => 'select',
                            'label'         => __('Photo Source', 'uabb'),
                            'default'       => 'library',
                            'options'       => array(
                                'library'       => __('Media Library', 'uabb'),
                                'url'           => __('URL', 'uabb')
                            ),
                            'toggle'        => array(
                                'library'       => array(
                                    'fields'        => array('photo')
                                ),
                                'url'           => array(
                                    'fields'        => array('photo_url' )
                                )
                            )
                        ),
                        'photo'         => array(
                            'type'          => 'photo',
                            'label'         => __('Photo', 'uabb')
                        ),
                        'photo_url'     => array(
                            'type'          => 'text',
                            'label'         => __('Photo URL', 'uabb'),
                            'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                        ),
                    )
                ),
            ),
        )
    )
));
