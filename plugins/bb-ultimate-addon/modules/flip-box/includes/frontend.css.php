<?php

$settings->front_background_color = UABB_Helper::uabb_colorpicker( $settings, 'front_background_color', true );
$settings->front_border_color = UABB_Helper::uabb_colorpicker( $settings, 'front_border_color' );

$settings->back_background_color = UABB_Helper::uabb_colorpicker( $settings, 'back_background_color', true );
$settings->back_border_color = UABB_Helper::uabb_colorpicker( $settings, 'back_border_color' );

$settings->front_title_typography_color = UABB_Helper::uabb_colorpicker( $settings, 'front_title_typography_color' );
$settings->front_desc_typography_color = UABB_Helper::uabb_colorpicker( $settings, 'front_desc_typography_color' );
$settings->back_title_typography_color = UABB_Helper::uabb_colorpicker( $settings, 'back_title_typography_color' );
$settings->back_desc_typography_color = UABB_Helper::uabb_colorpicker( $settings, 'back_desc_typography_color' );

?>
<?php
if( $settings->show_button == 'yes' ) {
    ( $settings->button != '' ) ? FLBuilder::render_module_css( 'uabb-button', $id, $settings->button ) : '';
}
if( $settings->smile_icon != '' ) {
    /* Render CSS */

    /* CSS "$settings" Array */
    //$settings->smile_icon['image_type'] = 'icon';
    $settings->smile_icon->image_type = 'icon';

    /* CSS Render Function */ 
    FLBuilder::render_module_css( 'image-icon', $id, $settings->smile_icon );
}
?>

<?php
if( $settings->flip_box_min_height_options == 'uabb-custom-height' && $settings->flip_box_min_height != '' ) {
?>
    .fl-node-<?php echo $id; ?> .uabb-flip-box {
        height: <?php echo $settings->flip_box_min_height; ?>px;
    }
<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-front {
    <?php
    echo ( $settings->front_border_color != '' ) ? 'border-color: ' . $settings->front_border_color . ';' : '';
    echo ( $settings->front_background_color != '' ) ? 'background-color: ' . $settings->front_background_color . ';' : '';
    echo ( $settings->front_border_size != '' ) ? 'border-width: ' . $settings->front_border_size . 'px;' : '';
    echo ( $settings->front_box_border_style != '' ) ? 'border-style: ' . $settings->front_box_border_style . ';' : '';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-back {
    <?php
    echo ( $settings->back_border_color != '' ) ? 'border-color: ' . $settings->back_border_color . ';' : '';
    echo ( $settings->back_background_color != '' ) ? 'background-color: ' . $settings->back_background_color . ';' : '';
    echo ( $settings->back_border_size != '' ) ? 'border-width: ' . $settings->back_border_size . 'px;' : '';
    echo ( $settings->back_box_border_style != '' ) ? 'border-style: ' . $settings->back_box_border_style . ';' : '';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-front .uabb-flip-box-section-content {
    color : <?php echo uabb_theme_text_color( $settings->front_desc_typography_color ); ?>;
    <?php
    echo ( $settings->front_desc_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->front_desc_typography_font_size['desktop'] . 'px;' : '';
    echo ( $settings->front_desc_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->front_desc_typography_line_height['desktop'] . 'px;' : '';
    if( $settings->front_desc_typography_font_family['family'] != 'Default' ) {
        UABB_Helper::uabb_font_css( $settings->front_desc_typography_font_family );
    }

    echo ( $settings->front_desc_typography_margin_top != '' ) ? 'margin-top: ' . $settings->front_desc_typography_margin_top . 'px;' : 'margin-top: 0;';

    echo ( $settings->front_desc_typography_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->front_desc_typography_margin_bottom . 'px;' : 'margin-bottom: 0;';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-front .uabb-flip-box-section-content * {
    font-family: inherit;
    font-weight: inherit; 
    font-size: inherit;
    font-style: inherit; 
    color: inherit; 
    line-height: inherit;
}

.fl-node-<?php echo $id; ?> .uabb-front .uabb-face-text-title {
    <?php
    echo ( $settings->front_title_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->front_title_typography_font_size['desktop'] . 'px;' : '';
    echo ( $settings->front_title_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->front_title_typography_line_height['desktop'] . 'px;' : '';
    if( $settings->front_title_typography_font_family['family'] != 'Default' ) {
        UABB_Helper::uabb_font_css( $settings->front_title_typography_font_family );
    }
    echo ( $settings->front_title_typography_color != '' ) ? 'color: ' . $settings->front_title_typography_color . ';' : '';

    echo ( $settings->front_title_typography_margin_top != '' ) ? 'margin-top: ' . $settings->front_title_typography_margin_top . 'px;' : 'margin-top: 0;';

    echo ( $settings->front_title_typography_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->front_title_typography_margin_bottom . 'px;' : 'margin-bottom: 12px;';
    ?>
}


.fl-node-<?php echo $id; ?> .uabb-back .uabb-back-flip-box-section-content {
    color : <?php echo uabb_theme_text_color( $settings->back_desc_typography_color ); ?>;
    <?php
    echo ( $settings->back_desc_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->back_desc_typography_font_size['desktop'] . 'px;' : '';
    echo ( $settings->back_desc_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->back_desc_typography_line_height['desktop'] . 'px;' : '';
    if( $settings->back_desc_typography_font_family['family'] != 'Default' ) {
        UABB_Helper::uabb_font_css( $settings->back_desc_typography_font_family );
    }

    echo ( $settings->back_desc_typography_margin_top != '' ) ? 'margin-top: ' . $settings->back_desc_typography_margin_top . 'px;' : 'margin-top: 0;';

    echo ( $settings->back_desc_typography_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->back_desc_typography_margin_bottom . 'px;' : 'margin-bottom: 0;';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-button-wrap {
    <?php
    echo ( $settings->button_margin_top != '' ) ? 'margin-top: ' . $settings->button_margin_top . 'px;' : 'margin-top: 15px;';
    echo ( $settings->button_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->button_margin_bottom . 'px;' : 'margin-bottom: 0;';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-imgicon-wrap {
    <?php
    echo ( $settings->icon_margin_top != '' ) ? 'margin-top: ' . $settings->icon_margin_top . 'px;' : 'margin-top: 0;';
    echo ( $settings->icon_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->icon_margin_bottom . 'px;' : 'margin-bottom: 15px;';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-back .uabb-back-flip-box-section-content * {
    font-family: inherit;
    font-weight: inherit; 
    font-size: inherit;
    font-style: inherit; 
    color: inherit; 
    line-height: inherit;
}

.fl-node-<?php echo $id; ?> .uabb-back .uabb-back-text-title {
    <?php
    echo ( $settings->back_title_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->back_title_typography_font_size['desktop'] . 'px;' : '';
    echo ( $settings->back_title_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->back_title_typography_line_height['desktop'] . 'px;' : '';
    if( $settings->back_title_typography_font_family['family'] != 'Default' ) {
        UABB_Helper::uabb_font_css( $settings->back_title_typography_font_family );
    }
    echo ( $settings->back_title_typography_color != '' ) ? 'color: ' . $settings->back_title_typography_color . ';' : '';

    echo ( $settings->back_title_typography_margin_top != '' ) ? 'margin-top: ' . $settings->back_title_typography_margin_top . 'px;' : 'margin-top: 0;';

    echo ( $settings->back_title_typography_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->back_title_typography_margin_bottom . 'px;' : 'margin-bottom: 12px;';
    ?>
}



<?php
if( $global_settings->responsive_enabled ) { // Global Setting If started
?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {

        .fl-node-<?php echo $id; ?> .uabb-front .uabb-flip-box-section-content {
            <?php
            echo ( $settings->front_desc_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->front_desc_typography_font_size['medium'] . 'px;' : '';
            echo ( $settings->front_desc_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->front_desc_typography_line_height['medium'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-front .uabb-face-text-title {
            <?php
            echo ( $settings->front_title_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->front_title_typography_font_size['medium'] . 'px;' : '';
            echo ( $settings->front_title_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->front_title_typography_line_height['medium'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-back .uabb-back-flip-box-section-content {
            <?php
            echo ( $settings->back_desc_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->back_desc_typography_font_size['medium'] . 'px;' : '';
            echo ( $settings->back_desc_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->back_desc_typography_line_height['medium'] . 'px;' : '';
            ?>
        }


        .fl-node-<?php echo $id; ?> .uabb-back .uabb-back-text-title {
            <?php
            echo ( $settings->back_title_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->back_title_typography_font_size['medium'] . 'px;' : '';
            echo ( $settings->back_title_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->back_title_typography_line_height['medium'] . 'px;' : '';
            ?>
        }
    }
 
     @media ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {

        .fl-node-<?php echo $id; ?> .uabb-front .uabb-flip-box-section-content {
            <?php
            echo ( $settings->front_desc_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->front_desc_typography_font_size['small'] . 'px;' : '';
            echo ( $settings->front_desc_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->front_desc_typography_line_height['small'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-front .uabb-face-text-title {
            <?php
            echo ( $settings->front_title_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->front_title_typography_font_size['small'] . 'px;' : '';
            echo ( $settings->front_title_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->front_title_typography_line_height['small'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-back .uabb-back-flip-box-section-content {
            <?php
            echo ( $settings->back_desc_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->back_desc_typography_font_size['small'] . 'px;' : '';
            echo ( $settings->back_desc_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->back_desc_typography_line_height['small'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-back .uabb-back-text-title {
            <?php
            echo ( $settings->back_title_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->back_title_typography_font_size['small'] . 'px;' : '';
            echo ( $settings->back_title_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->back_title_typography_line_height['small'] . 'px;' : '';
            ?>
        }
    }
<?php
}
?>