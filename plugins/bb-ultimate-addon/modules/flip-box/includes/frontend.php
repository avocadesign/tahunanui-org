<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */
//echo '<pre>'; print_r($module); echo '</pre>';
//echo '<pre>'; print_r($settings); echo '</pre>';

?>
<div class="uabb-flip-box-wrap">
	<div class="uabb-flip-box  <?php echo $settings->flip_type; ?> <?php echo $settings->flip_box_min_height_options; ?>" data-min-height="<?php echo ( $settings->flip_type == 'uabb-custom-height' ) ? $settings->flip_box_min_height : ''; ?>">
		<div class="uabb-flip-box">
			<div class="uabb-face uabb-front ">
				<div class="uabb-flip-box-section <?php echo ( $settings->display_vertically_center != 'no' ) ? 'uabb-flip-box-section-vertical-middle' : ''; ?>">
					<?php $module->render_icon(); ?>
					<?php
					if( $settings->title_front != '' ) {
					?>
					<<?php echo $settings->front_title_typography_tag_selection; ?> class="uabb-face-text-title"><?php echo $settings->title_front; ?></<?php echo $settings->front_title_typography_tag_selection; ?>>
					<?php
					}
					if( $settings->desc_front != '' ) {
					?>
					<div class="uabb-flip-box-section-content uabb-text-editor" >
						<?php echo $settings->desc_front; ?>
					</div>
					<?php
					}
					?>
				</div>
			</div><!-- END .front -->
			<div class="uabb-face uabb-back">
				<div class="uabb-flip-box-section <?php echo ( $settings->display_vertically_center != 'no' ) ? 'uabb-flip-box-section-vertical-middle' : ''; ?>">
					<?php
					if( $settings->title_back != '' ) {
					?>
					<<?php echo $settings->back_title_typography_tag_selection; ?> class="uabb-back-text-title"><?php echo $settings->title_back; ?></<?php echo $settings->back_title_typography_tag_selection; ?>>
					<?php
					}
					if( $settings->desc_front != '' ) {
					?>
					<div class="uabb-back-flip-box-section-content uabb-text-editor">
						<?php echo $settings->desc_back; ?>
					</div>
					<?php
					}
					$module->render_button();
					?>
				</div>
			</div><!-- END .back -->
		</div> <!-- ifb-flip-box -->
	</div> <!-- flip-box -->
</div>