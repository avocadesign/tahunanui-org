/**
 * This file should contain frontend JavaScript that 
 * will be applied to individual module instances.
 *
 * You have access to three variables in this file: 
 * 
 * $module An instance of your module class.
 * $id The module's ID.
 * $settings The module's settings.
 *
 * Example: 
 */
if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
	var is_touch_device = false;
else
	var is_touch_device = true;

jQuery('#page').click(function(){
	jQuery('.uabb-hover').removeClass('uabb-hover');
});
if(!is_touch_device){
	jQuery('.uabb-flip-box').hover(function(event){
		event.stopPropagation();
		jQuery(this).addClass('uabb-hover');
	},function(event){
		event.stopPropagation();
		jQuery(this).removeClass('uabb-hover');
	});
}
jQuery('.uabb-flip-box').each(function(index, element) {
	if(jQuery(this).parent().hasClass('style_9')) {
		jQuery(this).hover(function(){
				jQuery(this).addClass('uabb-door-hover');
			},
			function(){
				jQuery(this).removeClass('uabb-door-hover');
			})
		jQuery(this).on('click',function(){
				jQuery(this).toggleClass('uabb-door-right-open');
				jQuery(this).removeClass('uabb-door-hover');
			});
	}
});
jQuery('.uabb-flip-box').click(function(event){
	event.stopPropagation();
	jQuery(document).trigger('ultFlipBoxClicked', jQuery(this));
	if(jQuery(this).hasClass('uabb-hover')){
		jQuery(this).removeClass('uabb-hover');
	}
	else{
		jQuery('.uabb-hover').removeClass('uabb-hover');
		jQuery(this).addClass('uabb-hover');
	}
});

//Flipbox
	//Vertical Door Flip
	jQuery('.vertical_door_flip .uabb-front').each(function() {
		jQuery(this).wrap('<div class="v_door uabb-multiple-front uabb-front-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-front-1').addClass('uabb-front-2').insertAfter(jQuery(this).parent());
	});
	//Reverse Vertical Door Flip
	jQuery('.reverse_vertical_door_flip .uabb-back').each(function() {
		jQuery(this).wrap('<div class="rv_door uabb-multiple-back uabb-back-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-back-1').addClass('uabb-back-2').insertAfter(jQuery(this).parent());
	});
	//Horizontal Door Flip
	jQuery('.horizontal_door_flip .uabb-front').each(function() {
		jQuery(this).wrap('<div class="h_door uabb-multiple-front uabb-front-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-front-1').addClass('uabb-front-2').insertAfter(jQuery(this).parent());
	});
	//Reverse Horizontal Door Flip
	jQuery('.reverse_horizontal_door_flip .uabb-back').each(function() {
		jQuery(this).wrap('<div class="rh_door uabb-multiple-back uabb-back-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-back-1').addClass('uabb-back-2').insertAfter(jQuery(this).parent());
	});
	//Stlye 9 front
	jQuery('.style_9 .uabb-front').each(function() {
		jQuery(this).wrap('<div class="new_style_9 uabb-multiple-front uabb-front-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-front-1').addClass('uabb-front-2').insertAfter(jQuery(this).parent());
	});
	//Style 9 back
	jQuery('.style_9 .uabb-back').each(function() {
		jQuery(this).wrap('<div class="new_style_9 uabb-multiple-back uabb-back-1"></div>');
		jQuery(this).parent().clone().removeClass('uabb-back-1').addClass('uabb-back-2').insertAfter(jQuery(this).parent());
	});
	var is_safari = /^((?!chrome).)*safari/i.test(navigator.userAgent);
	if( is_safari ){
		jQuery('.vertical_door_flip').each(function(index, element) {
            var safari_link = jQuery(this).find('.flip_link').outerHeight();
			jQuery(this).find('.flip_link').css('top', - safari_link/2 +'px');
            jQuery(this).find('.uabb-multiple-front').css('width', '50.2%');
        });
		jQuery('.horizontal_door_flip').each(function(index, element) {
            var safari_link = jQuery(this).find('.flip_link').outerHeight();
			jQuery(this).find('.flip_link').css('top', - safari_link/2 +'px');
            jQuery(this).find('.uabb-multiple-front').css('height','50.2%');
        });
		jQuery('.reverse_vertical_door_flip').each(function(index, element) {
            var safari_link = jQuery(this).find('.flip_link').outerHeight();
			jQuery(this).find('.flip_link').css('top', - safari_link/2 +'px');
        });
		jQuery('.reverse_horizontal_door_flip').each(function(index, element) {
            var safari_link = jQuery(this).find('.flip_link').outerHeight();
			jQuery(this).find('.flip_link').css('top', - safari_link/2 +'px');
			jQuery(this).find('.uabb-back').css('position', 'inherit');
        });
	}

<?php //$module->example_method(); ?>