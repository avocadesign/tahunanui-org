
(function ( $, window, undefined ) {
	$(window).on( 'load', function(a){
		var delay = 700;
		flip_box_set_auto_height();
		setTimeout(function() {
		  $('.uabb-face').css('opacity', '1');
		}, delay);
	});

	
	function flip_box_set_auto_height() {
		$('.uabb-flip-box').each(function(index, value) {
			var bodywidth=$(document).width();
			var WW =window.innerWidth;
			
			console.log(WW);
			if( WW != '' ) {
				if( WW >= 768 ) {
					var h = $(this).attr('data-min-height') || '';

					if( h != '' ) {
						if( $( this ).hasClass( "uabb-custom-height" ) ) {
							$( this ).css( 'height', h );
							if( $(this).find( ".uabb-back" ).find( ".uabb-flip-box-section" ).hasClass( "uabb-flip-box-section-vertical-middle") ) {
									
									var flag = $(this).find(".uabb-back").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle");

									var back=$(this).find(".uabb-back").outerHeight();

									back = parseInt(h);

									var backsection = $(this).find(".uabb-back").find(".uabb-flip-box-section").outerHeight();

									backsection=parseInt(backsection);

									if( backsection >= back ) {
										$(this).find(".uabb-back").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
									}
									
							}
							if( $(this).find(".uabb-front").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle") ) {

								var front=$(this).find(".uabb-front").outerHeight();

								front=parseInt(h);

								var frontsection=$(this).find(".uabb-front").find(".uabb-flip-box-section").outerHeight();

								frontsection=parseInt(frontsection);
								
								if( frontsection >= front ) {
									$(this).find(".uabb-front").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
								}
							}
						}
					} else {
						if( $(this).hasClass("uabb-jq-height") ) {
							
							var ht1 = $(this).find(".uabb-back").find(".uabb-flip-box-section").outerHeight();

							ht1 = parseInt(ht1);
						
							var ht2=$(this).find(".uabb-front").find(".uabb-flip-box-section").outerHeight();

							ht2 = parseInt(ht2);

							if( ht1 >= ht2 ) {
								$(this).find(".uabb-face").css('height', ht1);
							} else {
								$(this).find(".uabb-face").css('height', ht2);
							}
						}					
					}
				} else {

					var h = $(this).attr('data-min-height') || '';
								

				    //	for style - 9
					if( $(this).hasClass('style_9') ) {

						$(this).css('height', 'initial');
						var f1 = $(this).find('.uabb-front-1 .uabb-front').outerHeight();
						var f2 = $(this).find('.uabb-back-1 .uabb-back').outerHeight();
						//	set largest height - of either front or back
						if( f1 > f2 ) {
							$(this).css('height', f1);
						} else {
							$(this).css('height', f2);
						}
						
					} else {
						if( $(this).hasClass("uabb-jq-height") ) {
							var ht1 = $(this).find(".uabb-back").find(".uabb-flip-box-section").outerHeight();

							ht1 = parseInt(ht1);
						
							var ht2 = $(this).find(".uabb-front").find(".uabb-flip-box-section").outerHeight();

							ht2 = parseInt(ht2);

							if( ht1 >= ht2 ) {
								$(this).find(".uabb-face").css('height', ht1);
							} else {
								$(this).find(".uabb-face").css('height', ht2);
							}
						} else if( $(this).hasClass("uabb-auto-height") ) {
							if( $(this).find(".uabb-back").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle") ) {
									
								var flag = $(this).find(".uabb-back").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle");
								
								var back = $(this).find(".uabb-back").outerHeight();

								back = parseInt(back);

								var backsection = $(this).find(".uabb-back").find(".uabb-flip-box-section").outerHeight();

								backsection = parseInt(backsection);

								if( backsection > back ) {
									$(this).find(".uabb-back").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
								}	
							}
						} else if( $(this).hasClass("uabb-custom-height") ) {
							if( h!='' ) {
						 		$(this).css('height', h);

						 		if( $(this).find(".uabb-back").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle") ) {
						 			var flag = $(this).find(".uabb-back").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle");

									var back = $(this).find(".uabb-back").outerHeight();

									back = parseInt(back);

									var backsection = $(this).find(".uabb-back").find(".uabb-flip-box-section").outerHeight();

									backsection = parseInt(backsection);

									if( backsection >= back ) {
										$(this).find(".uabb-back").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
									}
								}

								if( $(this).find(".uabb-front").find(".uabb-flip-box-section").hasClass("uabb-flip-box-section-vertical-middle") ) {
									
									var front=$(this).find(".uabb-front").outerHeight();
									front=parseInt(front);
									var frontsection=$(this).find(".uabb-front").find(".uabb-flip-box-section").outerHeight();
									frontsection=parseInt(frontsection);
									
									if( frontsection >= front ) {
										$(this).find(".uabb-front").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
									} else {
										$(this).find(".uabb-front").find(".uabb-flip-box-section").addClass("uabb_disable_middle");
									}
								}
							}
						} else {
							$(this).css('height', 'initial');
						}
					}
				}
			}
		});
	}

}(jQuery, window));