<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class FlipBoxModule
 */
class FlipBoxModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Flip Box', 'uabb'),
            'description'   => __('An basic example for coding new modules.', 'uabb'),
            'category'      => __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/flip-box/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/flip-box/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
        
        $this->add_css( 'font-awesome' );
    }

    public function enqueue_scripts() {
           
        $upload_dir = FLBuilderModel::get_upload_dir();
        $this->add_css( 'ultimate-icons', $upload_dir['url'] . 'icons/ultimate-icons/style.css');
    }

    /**
     * @method render_button
     */
    public function render_button() {
        if( $this->settings->show_button == 'yes' ) {
            if( $this->settings->button != '' ) {
                FLBuilder::render_module_html( 'uabb-button', $this->settings->button );
            }
        }
    }

    /**
     * @method render_icon
     */
    public function render_icon() {
        if( $this->settings->smile_icon != '' ) {
            $this->settings->smile_icon->image_type = 'icon';
            FLBuilder::render_module_html( 'image-icon', $this->settings->smile_icon );
        }
    }
}

FLBuilder::register_settings_form('flip_box_icon_form_field', array(
    'title' => __('Icon', 'uabb'),
    'tabs'  => array(
        array(
            'title'         => __('Image / Icon', 'uabb'),
            'sections'      => array(
                'icon_basic'    => array(
                    'title'         => 'Icon Basics', // Section Title
                    'fields'        => array( // Section Fields
                        'icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb')
                        ),
                        'icon_size'     => array(
                            'type'          => 'text',
                            'label'         => __('Size', 'uabb'),
                            'default'       => '30',
                            'maxlength'     => '5',
                            'size'          => '6',
                            'description'   => 'px',
                        ),
                    )
                ),
                'icon_style'    => BB_Ultimate_Addon::uabb_section_get( 'icon_style' ),
                'icon_colors'   => BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
            )
        )
    )
));

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FlipBoxModule', array(
    'flip_front'       => array( // Tab
        'title'         => __('Flip Box Front', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('Front', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'smile_icon' => array(
                        'type'          => 'form',
                        'label'         => __('Icon Settings', 'uabb'),
                        'form'          => 'flip_box_icon_form_field', // ID of a registered form.
                        'preview_text'  => 'icon', // ID of a field to use for the preview text.
                    ),
                    'title_front'     => array(
                        'type'          => 'text',
                        'label'         => __('Title on Front', 'uabb'),
                        'default'       => 'Flip Box Front',
                        'help'          => 'Perhaps, this is the most highlighted text.',
                    ),
                    'desc_front'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                    ),
                )
            ),            
        )
    ),
    'flip_back'       => array( // Tab
        'title'         => __('Flip Box Back', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('Back', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'title_back'     => array(
                        'type'          => 'text',
                        'label'         => __('Title on Back', 'uabb'),
                        'default'       => 'Flip Box Back',
                        'help'          => 'Perhaps, this is the most highlighted text.',
                    ),
                    'desc_back'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                    ),                 
                )
            ),
            'button'       => array( // Section
                'title'         => __('Button', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'show_button'    => array(
                        'type'          => 'select',
                        'label'         => __('Show button', 'uabb'),
                        'default'       => 'no',
                        'options'       => array(
                            'no'      => __('No', 'uabb'),
                            'yes'      => __('Yes', 'uabb'),
                        ),
                        'toggle' => array(
                            'no' => array(
                                'fields' => array()
                            ),
                            'yes' => array(
                                'fields' => array( 'button', 'button_margin_top', 'button_margin_bottom' )
                            )
                        ),
                    ),
                    'button' => array(
                        'type'          => 'form',
                        'label'         => __('Button Settings', 'uabb'),
                        'form'          => 'button_form_field', // ID of a registered form.
                        'preview_text'  => 'text', // ID of a field to use for the preview text.
                    ),
                )
            ),
        )
    ),
    'style'       => array( // Tab
        'title'         => __('Styles', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Flipbox Styles', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'flip_type'   => array(
                        'type'          => 'select',
                        'label'         => __('Flip Type', 'uabb'),
                        'default'       => 'horizontal_flip_left',
                        'help'          => 'Select Flip type for this flip box.',
                        'options'       => array(
                            'horizontal_flip_left'      => __('Flip Horizontally From Left', 'uabb'),
                            'horizontal_flip_right'      => __('Flip Horizontally From Right', 'uabb'),
                            'vertical_flip_top'      => __('Flip Vertically From Top', 'uabb'),
                            'vertical_flip_bottom'      => __('Flip Vertically From Bottom', 'uabb'),
                        )
                    ),
                    'flip_box_min_height_options'    => array(
                        'type'          => 'select',
                        'label'         => __('Flip Type', 'uabb'),
                        'default'       => 'uabb-jq-height',
                        'options'       => array(
                            'uabb-jq-height'      => __('Display full the content and adjust height of box accordingly', 'uabb'),
                            'uabb-custom-height'      => __('Give a custom height of your choice to the box', 'uabb'),
                        ),
                        'toggle' => array(
                            'uabb-jq-height' => array(
                                'fields' => array()
                            ),
                            'uabb-custom-height' => array(
                                'fields' => array( 'flip_box_min_height' )
                            )
                        ),
                    ),
                    'flip_box_min_height'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Set Flip Box Min Height', 'uabb' ),
                        'default'       => '300',
                        'description' => __( 'px', 'uabb' ),
                        'size'          => '8',
                        'help'          => __( 'Select minimum height option for this box.', 'uabb' ),
                    ),
                    'display_vertically_center' => array(
                        'type'          => 'select',
                        'label'         => __('Display Vertically Center', 'uabb'),
                        'default'       => 'vertical-middle',
                        'options'       => array(
                            'vertical-middle'      => __('Yes', 'uabb'),
                            'no'      => __('No', 'uabb'),
                        )
                    ),
                )
            ),
            'front_styles'       => array( // Section
                'title'         => __('Front Styles', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'front_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Front Background Color', 'uabb'),
                        )
                    ),
                    'front_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'front_box_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Front Box Border Style', 'uabb'),
                        'default'       => 'solid',
                        'help'          => 'Select the border style for box.',
                        'options'       => array(
                            'none'      => __('None', 'uabb'),
                            'solid'      => __('Solid', 'uabb'),
                            'dashed'      => __('Dashed', 'uabb'),
                            'dotted'      => __('Dotted', 'uabb'),
                            'double'      => __('Double', 'uabb'),
                            'inset'      => __('Inset', 'uabb'),
                            'outset'      => __('Outset', 'uabb')
                        ),
                        /*'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '.uabb-front',
                            'property'        => 'border-style'
                        ),*/
                        'toggle' => array(
                            'none' => array(
                                'fields' => array()
                            ),
                            'solid' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                            'dashed' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                            'dotted' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                            'double' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                            'inset' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                            'outset' => array(
                                'fields' => array( 'front_border_size', 'front_border_color' )
                            ),
                        ),
                    ),
                    'front_border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Front Border Size', 'uabb' ),
                        'default'       => '1',
                        'size'          => '8',
                        'placeholder'   => __( 'Size of front border', 'uabb' ),
                        'class'         => '',
                        'description'   => __( 'px', 'uabb' ),
                        'help'          => __( 'Enter value in pixels.', 'uabb' ),
                        'preview'         => array(
                            'type'             => 'css',
                            'selector'         => '.uabb-front',
                            'property'         => 'border-width',
                            'unit'             => 'px'
                        )
                    ),
                    'front_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Front Border Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                )
            ),
            'back_styles'       => array( // Section
                'title'         => __('Back Styles', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'back_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Back Background Color', 'uabb'),
                        )
                    ),
                    'back_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'back_box_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Back Box Border Style', 'uabb'),
                        'default'       => 'solid',
                        'help'          => 'Select the border style for box.',
                        'options'       => array(
                            'none'      => __('None', 'uabb'),
                            'solid'      => __('Solid', 'uabb'),
                            'dashed'      => __('Dashed', 'uabb'),
                            'dotted'      => __('Dotted', 'uabb'),
                            'double'      => __('Double', 'uabb'),
                            'inset'      => __('Inset', 'uabb'),
                            'outset'      => __('Outset', 'uabb')
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array()
                            ),
                            'solid' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                            'dashed' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                            'dotted' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                            'double' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                            'inset' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                            'outset' => array(
                                'fields' => array( 'back_border_size', 'back_border_color' )
                            ),
                        ),

                    ),
                    'back_border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Back Border Size', 'uabb' ),
                        'default'       => '1',
                        'size'          => '8',
                        'placeholder'   => __( 'Size of back border', 'uabb' ),
                        'class'         => '',
                        'description'   => __( 'px', 'uabb' ),
                        'help'          => __( 'Enter value in pixels.', 'uabb' ),
                        'preview'         => array(
                            'type'             => 'css',
                            'selector'         => '.uabb-back',
                            'property'         => 'border-width',
                            'unit'             => 'px'
                        )
                    ),
                    'back_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Back Border Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'front_title_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Front Title', 'uabb' ),
                    'fields' => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-face-text-title'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Front Title Color', 'uabb'),
                        ),
                        'margin_top'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Top', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                        'margin_bottom'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Bottom', 'uabb' ),
                            'placeholder'   => '12',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                    )
                ),
                array(),
                'front_title_typography'
            ),
            'front_desc_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __( 'Front Description', 'uabb' ),
                    'fields' => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-flip-box-section-content'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Front Description Color', 'uabb'),
                        ),
                        'margin_top'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Top', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                        'margin_bottom'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Bottom', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                    )
                ), 
                array( 'tag_selection' ),
                'front_desc_typography'
            ),
            'back_title_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Back Title', 'uabb' ),
                    'fields' => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-back-text-title'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Back Title Color', 'uabb'),
                        ),
                        'margin_top'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Top', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                        'margin_bottom'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Bottom', 'uabb' ),
                            'placeholder'   => '12',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                    )
                ),
                array(),
                'back_title_typography'
            ),
            'back_desc_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __( 'Back Description', 'uabb' ),
                    'fields' => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-back-flip-box-section-content'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Back Description Color', 'uabb'),
                        ),
                        'margin_top'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Top', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                        'margin_bottom'    => array(
                            'type'          => 'text',
                            'label'         => __( 'Margin Bottom', 'uabb' ),
                            'placeholder'   => '0',
                            'description'   => __( 'px', 'uabb' ),
                            'size'          => '8',
                        ),
                    )
                ), 
                array( 'tag_selection' ),
                'back_desc_typography'
            ),
            'margin_options'       => array( // Section
                'title'         => __('Margin', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'icon_margin_top'    => array(
                        'type'          => 'text',
                        'label'         => __('Icon Margin Top', 'uabb'),
                        'placeholder'   => '0',
                        'description'   => __( 'px', 'uabb' ),
                        'size'          => '8',
                    ),
                    'icon_margin_bottom'    => array(
                        'type'          => 'text',
                        'label'         => __('Icon Margin Bottom', 'uabb'),
                        'placeholder'   => '15',
                        'description'   => __( 'px', 'uabb' ),
                        'size'          => '8',
                    ),
                    'button_margin_top'    => array(
                        'type'          => 'text',
                        'label'         => __('Button Margin Top', 'uabb'),
                        'placeholder'   => '15',
                        'description'   => __( 'px', 'uabb' ),
                        'size'          => '8',
                    ),
                    'button_margin_bottom'    => array(
                        'type'          => 'text',
                        'label'         => __('Button Margin Bottom', 'uabb'),
                        'placeholder'   => '0',
                        'description'   => __( 'px', 'uabb' ),
                        'size'          => '8',
                    ),
                )
            ),
        )
    ),
));

