jQuery(document).on( 'click', function(e){
	//e.stopImmediatePropagation();
    var target = e.target,
    	slidebox = target.closest('.fl-module-slide-box');

        if( !jQuery('html').hasClass('fl-builder-edit') ) {
            _hideAll_SlideBox(slidebox);
        }
});

function _hideAll_SlideBox( slidebox ) {
    jQuery(document).find('.fl-module-slide-box').each(function(i,e) {

            var thisNode = jQuery(this).data('node'),
                slideboxNode = jQuery(slidebox).data('node');

                if( thisNode != slideboxNode ) { 
                    var style2 = jQuery(this).find('.uabb-style2');
                    dropdown_icon = style2.find('.uabb-slide-dropdown .fa'),
                    slidebox_wrap = style2.closest('.uabb-slide-box-wrap');

                    if( style2.hasClass('open-slidedown') ) {
                        style2.removeClass('open-slidedown');
                        slidebox_wrap.removeClass('set-z-index');
                        dropdown_icon.removeClass('fa-angle-up');
                        dropdown_icon.addClass('fa-angle-down');
                    }

                    var style3 = jQuery(this).find('.uabb-style3');
                    dropdown_icon = style3.find('.uabb-slide-dropdown .fa'),
                    slidebox_wrap = style3.closest('.uabb-slide-box-wrap');

                    if( style3.hasClass('open-slidedown') ) {
                        style3.removeClass('open-slidedown');
                        slidebox_wrap.removeClass('set-z-index');
                        dropdown_icon.removeClass('fa-minus');
                        dropdown_icon.addClass('fa-plus');
                    }
                }
        });
}