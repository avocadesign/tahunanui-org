jQuery(document).on( 'click' , '.fl-builder-settings-tabs a', function(){
    var node = jQuery(this).closest( 'form' ).attr( 'data-node' );
    var slidebox_wrap = jQuery('.fl-node-' + node + ' .uabb-slide-box-wrap');
    
    if( jQuery(this).attr( 'href' ) == '#fl-builder-settings-tab-slide_down' || jQuery(this).attr( 'href' ) == '#fl-builder-settings-tab-style' || jQuery(this).attr( 'href' ) == '#fl-builder-settings-tab-typography' ) {
        previewSlideDown( slidebox_wrap );
    } else {
        previewSlideUp( slidebox_wrap );
    }
});

jQuery(document).on( 'click' , '.fl-lightbox-footer span', function(){
    var node = jQuery(this).closest( 'form' ).attr( 'data-node' );
    
    if( jQuery(this).hasClass('fl-builder-settings-cancel') ){
        
        var slidebox_wrap = jQuery('.fl-node-'+ node +' .uabb-slide-box-wrap');
        previewSlideUp( slidebox_wrap );
    }
});

jQuery(document).load(function(){
	jQuery(".cs-wp-color-picker").cs_wpColorPicker();
});

/* Call previewSlideDown when on Editor Slide Down tab is active */
jQuery( '.fl-builder-content' ).on( 'fl-builder.layout-rendered', function() {
    
    var active_tab = jQuery('.fl-builder-settings-tabs a.fl-active');

    if(active_tab.attr('href') == '#fl-builder-settings-tab-slide_down' || active_tab.attr('href') == '#fl-builder-settings-tab-style' || active_tab.attr( 'href' ) == '#fl-builder-settings-tab-typography' ) {
        var node = jQuery(active_tab).closest( 'form' ).attr( 'data-node' ),
            slidebox_wrap = jQuery('.fl-node-'+ node +' .uabb-slide-box-wrap');
        
        previewSlideDown( slidebox_wrap );
    }
});

/* Preview Slide Down When user Editing */
function previewSlideDown( slidebox_wrap ) {
    
    var style1 = slidebox_wrap.find( '.uabb-style1' ).first(),
        style2 = slidebox_wrap.find( '.uabb-style2' ).first(),
        style3 = slidebox_wrap.find( '.uabb-style3' ).first();

    var setMinHeight = jQuery('select[name="set_min_height"]');
    if( setMinHeight.find('option:selected').val() == 'custom' ) {
        var min_height = jQuery('input[name="slide_down_min_height"]').val();
    }
    
    min_height = (min_height != "") ? min_height : '40';

    slidebox_wrap.addClass('set-z-index');
    slidebox_wrap.find('.uabb-slide-down').css({'min-height': min_height+'px'});

    if( style1.length > 0 ) {
        style1.addClass('open-slidedown');
        
    } else if( style2.length > 0 ) {
        var dropdown_icon = style2.find('.uabb-slide-dropdown .fa');
        style2.addClass('open-slidedown');
        dropdown_icon.removeClass('fa-angle-down');
        dropdown_icon.addClass('fa-angle-up');
    
    } else if( style3.length > 0 ) {
        var dropdown_icon = style3.find('.uabb-slide-dropdown .fa');
        style3.addClass('open-slidedown');
        dropdown_icon.removeClass('fa-plus');
        dropdown_icon.addClass('fa-minus');
    }
}

/* Preview Slide Up */
function previewSlideUp( slidebox_wrap ) {

    if(slidebox_wrap.hasClass('set-z-index')) {
        var style1 = slidebox_wrap.find( '.uabb-style1' ).first(),
            style2 = slidebox_wrap.find( '.uabb-style2' ).first(),
            style3 = slidebox_wrap.find( '.uabb-style3' ).first();

        slidebox_wrap.find('.uabb-slide-down').css({'min-height': '40px'});
        if( style1.length > 0 ) {
            style1.removeClass('open-slidedown');
            
        } else if( style2.length > 0 ) {
            var dropdown_icon = style2.find('.uabb-slide-dropdown .fa');
            style2.removeClass('open-slidedown');
            dropdown_icon.addClass('fa-angle-down');
            dropdown_icon.removeClass('fa-angle-up');
        
        } else if( style3.length > 0 ) {
            var dropdown_icon = style3.find('.uabb-slide-dropdown .fa');
            style3.removeClass('open-slidedown');
            dropdown_icon.addClass('fa-plus');
            dropdown_icon.removeClass('fa-minus');
        }
        
        window.setTimeout( function() {
            slidebox_wrap.removeClass('set-z-index');
        },200);
    }
}

FLBuilder.registerModuleHelper('slide-box', {
   rules: {
        icon_size: {
            required: true
        },
        img_size: {
            required: true
        }
    },
   init: function()
    {   
        var form        = jQuery('.fl-builder-settings'),
            image_type   = form.find('select[name=image_type]'),        
            icon_style  = form.find('select[name=icon_style]'),
            image_style = form.find('select[name=image_style]'),
            photoSource     = form.find('select[name=photo_source]'),
            librarySource   = form.find('select[name=photo_src]'),
            urlSource       = form.find('input[name=photo_url]');
        
        //console.log( this );
        
        this._imageTypeChanged();
        this._toggleImageIcon();

            // Validation events
        //imageType.on('change', this._imageTypeChanged);
        photoSource.on('change', jQuery.proxy( this._photoSourceChanged, this ) );
        image_type.on('change', jQuery.proxy( this._toggleImageIcon, this ) );
        icon_style.on('change', jQuery.proxy( this._toggleBorderOptions, this ) ) ;
        image_style.on('change', jQuery.proxy( this._toggleBorderOptions, this ) ) ;
        //image_type.trigger('change');
    },
    
    _imageTypeChanged: function()
    {
        var form        = jQuery('.fl-builder-settings'),
            imageType   = form.find('select[name=image_type]').val(),
            photo       = form.find('input[name=photo]'),
            icon       = form.find('input[name=icon]');
            
        photo.rules('remove');
        icon.rules('remove');
        
        if(imageType == 'photo') {
            photo.rules('add', { required: true });
        }
        else if(imageType == 'icon') {
            icon.rules('add', { required: true });
        }
    },
    _toggleBorderOptions: function() {
        var form        = jQuery('.fl-builder-settings'),
            show_border = false
            image_type  = form.find('select[name=image_type]').val(),
            icon_style  = form.find('select[name=icon_style]').val(),
            border_style    = form.find('select[name=icon_border_style]').val(),
            image_style     = form.find('select[name=image_style]').val(),
            img_border_style    = form.find('select[name=img_border_style]').val();

        
        if( image_type == 'icon' ){
            if( icon_style == 'custom'  ){
                show_border = true;
            }

            if( show_border == false ){
                form.find('#fl-field-icon_border_width').hide();
                form.find('#fl-field-icon_border_color').hide();
                form.find('#fl-field-icon_border_hover_color').hide();
            }else if( show_border ){
                if ( border_style != 'none' ) {
                    form.find('#fl-field-icon_border_width').show();
                    form.find('#fl-field-icon_border_color').show();
                    form.find('#fl-field-icon_border_hover_color').show();
                }
            }
        }else if( image_type == 'photo' ){
            if( image_style == 'custom' ){
                show_border = true;
            }

            if( show_border == false ){
                form.find('#fl-field-img_border_width').hide();
                form.find('#fl-field-img_border_color').hide();
                form.find('#fl-field-img_border_hover_color').hide();
            }else if( show_border ){
                if ( img_border_style != 'none' ) {
                    form.find('#fl-field-img_border_width').show();
                    form.find('#fl-field-img_border_color').show();
                    form.find('#fl-field-img_border_hover_color').show();
                }
            }
        }
    },
    _toggleImageIcon: function() {
        var form        = jQuery('.fl-builder-settings'),
            show_color  = false,
            image_type  = form.find('select[name=image_type]').val(),
            image_style = form.find('select[name=image_style]').val();
        
        // console.log( this );
        //console.log( image_style );
        if( image_type == 'photo' && image_style == 'custom' ){
            show_color = true;
        }

        if( show_color == false ){
            form.find('#fl-builder-settings-section-img_colors').hide();
        }else if( show_color ){
            form.find('#fl-builder-settings-section-img_colors').show();
        }
        this._toggleBorderOptions();
        this._photoSourceChanged();
    },
    _photoSourceChanged: function()
    {
        var form            = jQuery('.fl-builder-settings'),
            image_type      = form.find('select[name=image_type]').val(),
            photo           = form.find('input[name=photo]'),
            iconSize        = form.find('input[name=icon_size]');


        photo.rules('remove');
        iconSize.rules('remove');

        if ( image_type == 'photo' ) {
                photo.rules('add', { required: true });
        }else if ( image_type == 'icon' ) {
            iconSize.rules('add', { required: true });
        }
    }

});
