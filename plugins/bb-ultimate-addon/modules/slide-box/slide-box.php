<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SlideBoxModule
 */
class SlideBoxModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Slide Box', 'uabb'),
            'description'   => __('An basic example for coding new modules.', 'uabb'),
            'category'		=> __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/slide-box/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/slide-box/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }

    public function render_link()
    {
        if($this->settings->cta_type == 'link') {
            echo '<a href="' . $this->settings->link . '" target="' . $this->settings->link_target . '" class="uabb-callout-cta-link">' . $this->settings->cta_text . '</a>';
        }
    }

    /**
     * @method render_button
     */
    public function render_button() {
        if( $this->settings->cta_type == 'button' ) {
            if( $this->settings->button != '' ) {
                //echo '<div class="uabb-ib1-button-outter">';
                FLBuilder::render_module_html( 'uabb-button', $this->settings->button );
                //echo '</div>';
            }
        }
    }

    /**
     * @method render_image
     */
    public function render_image( $pos ) {
        if( $this->settings->front_img_icon_position == $pos ){
            $imageicon_array = array(

                /* General Section */
                'image_type' => $this->settings->image_type,

                /* Icon Basics */
                'icon' => $this->settings->icon,
                'icon_size' => $this->settings->icon_size,
                'icon_align' => '',

                /* Image Basics */
                'photo_source' => 'library',
                'photo' => $this->settings->photo,
                'photo_url' => '',
                'img_size' => $this->settings->img_size,
                'img_align' => 'inherit',
                'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,

                /* Icon Style */
                'icon_style' => 'simple',
                'icon_bg_size' => '',
                'icon_border_style' => '',
                'icon_border_width' => '',
                'icon_bg_border_radius' => '',

                /* Image Style */
                'image_style' => $this->settings->image_style,
                'img_bg_size' => '',
                'img_border_style' => '',
                'img_border_width' => '',
                'img_bg_border_radius' => '',

                /* Preset Color variable new */
                'icon_color_preset' => 'simple', 

                /* Icon Colors */
                'icon_color' => $this->settings->icon_color,
                'icon_hover_color' => '',
                'icon_bg_color' => '',
                'icon_bg_hover_color' => '',
                'icon_border_color' => '',
                'icon_border_hover_color' => '',
                'icon_three_d' => '',

                /* Image Colors */
                'img_bg_color' => '',
                'img_bg_hover_color' => '',
                'img_border_color' => '',
                'img_border_hover_color' => '',
            );
            
            /* Render HTML Function */
            FLBuilder::render_module_html( 'image-icon', $imageicon_array );
        }
    }

    public function render_overlay_icon()
    {
        if( $this->settings->slide_type == "style1" && $this->settings->overlay == "yes" ) { 
            /* Render HTML Function */
            echo '<div class="uabb-slide-box-overlay">';
            echo    '<span class="uabb-icon-wrap">
                        <span class="uabb-icon">
                            <i class="' . $this->settings->overlay_icon . '"></i> 
                        </span>
                    </span>';
            echo '</div>';
        }
    }

    public function render_dropdown_icon() {

        if( $this->settings->slide_type == "style2" ) {

            $icon_settings = array(
                'bg_color'       => $this->settings->dropdown_icon_bg_color,
                'color'          => $this->settings->dropdown_icon_color,
                'icon'           => 'fa fa-angle-down',
                'size'           => $this->settings->dropdown_icon_size,
                'text'           => '',
            );

            //echo '<div class="uabb-slide-dropdown">';
            //FLBuilder::render_module_html('uabb-icon', $icon_settings);
            //echo '</div>';

            echo '<div class="uabb-slide-dropdown">';
            echo    '<span class="uabb-icon-wrap">
                        <span class="uabb-icon">
                            <i class="fa fa-angle-down"></i> 
                        </span>
                    </span>';
            echo '</div>';
        } 

        if( $this->settings->slide_type == "style3" ) {

            $icon_settings = array(
                'color'          => $this->settings->dropdown_icon_color,
                'icon'           => 'fa fa-plus',
                'size'           => $this->settings->dropdown_icon_size,
                'text'           => '',
            );

            //echo '<div class="uabb-slide-dropdown">';
            //FLBuilder::render_module_html('uabb-icon', $icon_settings);
            //echo '</div>';

            echo '<div class="uabb-slide-dropdown">';
            echo    '<span class="uabb-icon-wrap">
                        <span class="uabb-icon">
                            <i class="fa fa-plus"></i> 
                        </span>
                    </span>';
            echo '</div>';
        }   
    }
}



/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SlideBoxModule', array(
    'slide_front'       => array( // Tab
        'title'         => __('Slide Box Front', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
             'title'       => array( // Section
                'title'         =>__('Slide Box Front', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'title_front'     => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'uabb'),
                        'default'       => 'Slide Box Front',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-slide-face-text-title',
                        )
                    ),
                    'desc_front'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                    ),
                )
            ),
            'general'       => array( // Section
                'title'         => __('Image / Icon', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'image_type'    => array(
                        'type'          => 'select',
                        'label'         => __('Image Type', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => _x('None', 'Image type.', 'uabb' ),
                            'photo'         => __('Photo', 'uabb'),
                            'icon'          => __('Icon', 'uabb')
                        ),
                        'toggle'        => array(
                            'none'          => array(),
                            'photo'         => array(
                                'sections'      => array( 'photo', 'img_icon_styles' )
                            ),
                            'icon'          => array(
                                'sections'      => array( 'icon', 'img_icon_styles' )
                            )
                        )
                    )
                )
            ),
            'icon'          => array(
                'title'         => __('Icon', 'uabb'),
                'fields'        => array(
                    'icon'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'uabb')
                    ),
                    'icon_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'     => __('Icon Color', 'uabb'),
                        )
                    ),
                    'icon_hover_color'         => array(
                        'type'          => 'uabb-color',
                        'label'         => __('Icon Hover/Focus Color', 'uabb'),
                    ),
                    'icon_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Size', 'uabb'),
                        'default'       => '30',
                        'maxlength'     => '5',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                )
            ),
            'photo'          => array(
                'title'         => __('Photo', 'uabb'),
                'fields'        => array(
                    'photo'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'uabb')
                    ),
                    'image_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Image Style', 'uabb'),
                        'default'       => 'simple',
                        'help'          => __('Circle and Square style will crop your image in 1:1 ratio','uabb'),
                        'options'       => array(
                            'simple'        => __('Simple', 'uabb'),
                            'circle'        => __('Circle', 'uabb'),
                            'square'        => __('Square', 'uabb'),
                            'custom'        => __('Design your own', 'uabb'),
                        ),
                        'class' => 'uabb-image-icon-style',
                        'toggle' => array(
                            'simple' => array(
                                'fields' => array()
                            ),
                            'circle' => array(
                                'fields' => array( ),
                            ),
                            'square' => array(
                                'fields' => array( ),
                            ),
                            'custom' => array(
                                'sections'  => array( 'img_colors' ),
                                'fields'    => array( 'img_bg_size', 'img_bg_color', 'img_bg_color_opc' ) 
                            )
                        ),
                        'trigger'       => array(
                            'custom'           => array(
                                'fields'        => array('img_border_style')
                            ),
                            
                        )
                    ),
                    'img_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Size', 'uabb'),
                        'default'       => '60',
                        'maxlength'     => '5',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                    'img_bg_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'uabb'),
                        'default'       => '',
                        'help'          => 'Spacing between Image edge & Background edge',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                    'img_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Color', 'uabb'),
                        )
                    ),
                    'img_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                )
            ),
           
            'img_icon_styles'       => array( // Section
                'title'         => __('Image / Icon Position', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'front_img_icon_position' => array(
                        'type'          => 'select',
                        'label'         => __('Position', 'uabb'),
                        'default'       => 'above-title',
                        'help'  => __( 'Image / Icon position', 'uabb' ),
                        'options'       => array(
                            'above-title'   => __('Above Heading', 'uabb'),
                            'left-title'    => __( 'Left of Heading', 'uabb' ),
                            'right-title'   => __( 'Right of Heading', 'uabb' ),
                            'left'          => __('Left of Text and Heading', 'uabb'),
                            'right'         => __('Right of Text and Heading', 'uabb')
                        ),
                        'toggle'            => array(
                            'above-title'   => array(
                                'fields'    => array( 'front_alignment' ),
                            ),
                            'left'          => array(
                                'fields'    => array( 'front_align_items', 'front_icon_border' )
                            ),
                            'right'          => array(
                                'fields'    => array( 'front_align_items', 'front_icon_border' )
                            ),
                        )
                    ),
                    'front_alignment'   => array(
                        'type'          => 'select',
                        'label'         => __('Overall Alignment', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'center'           => __('Center', 'uabb'),
                            'left'            => __('Left', 'uabb'),
                            'right'            => __('Right', 'uabb')
                        ),
                    ),
                    'front_align_items' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Icon Vertical Alignment', 'uabb'),
                        'default'       => 'top',
                        'options'       => array(
                            'top'          => __('Top', 'uabb'),
                            'middle'        => __('Center', 'uabb'),
                        ),
                    ),
                    'front_icon_border' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Border in Icon and Text', 'uabb'),
                        'default'       => '',
                        'options'       => array(
                            'yes'          => __('Yes', 'uabb'),
                            ''             => __('No', 'uabb'),
                        ),
                        'toggle'        => array(
                            'yes'   => array(
                                'fields'    => array('front_icon_border_size', 'front_icon_border_color', 'front_icon_border_color_opc', 'front_icon_border_hover_color', 'front_icon_border_hover_color_opc')    
                            )
                        )
                    ),
                    'front_icon_border_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Border Thickness', 'uabb'),
                        'default'       => '',
                        'placeholder'   => '1',
                        'maxlength'     => '5',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                    'front_icon_border_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Border Color', 'uabb'),
                        )
                    ),
                    'front_icon_border_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'front_icon_border_hover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Border Hover / Focus Color', 'uabb'),
                        )
                    ),
                    'front_icon_border_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                )
            ),
        )
    ),
    'slide_down'       => array( // Tab
        'title'         => __('Slide Box Back', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('Slide Box Back', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'title_back'     => array(
                        'type'          => 'text',
                        'label'         => __('Title on Back', 'uabb'),
                        'default'       => 'Slide Box Back',
                        'help'          => 'Perhaps, this is the most highlighted text.',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-slide-back-text-title'
                        )
                    ),
                )
            ),
            'description'       => array( // Section
                'title'         => __('', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'desc_back'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                    ),
                )
            ),
            'cta'           => array(
                'title'         => __('Call to Action', 'uabb'),
                'fields'        => array(
                    'cta_type'      => array(
                        'type'          => 'select',
                        'label'         => __('Type', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => _x( 'None', 'Call to action.', 'uabb' ),
                            'link'          => __('Text', 'uabb'),
                            'button'        => __('Button', 'uabb')
                        ),
                        'toggle'        => array(
                            'none'          => array(),
                            'link'          => array(
                                'sections'      => array( 'cta_type_text', 'link_typography' )
                            ),
                            'button'        => array(
                                'sections'      => array( 'cta_type_button' )
                            )
                        )
                    ),
                )
            ),
            'cta_type_text'       => array( // Section
                'title'         => __('Text', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'link'          => array(
                        'type'          => 'link',
                        'label'         => __('Link', 'uabb'),
                        'help'          => __('The link applies to the entire module. If choosing a call to action type below, this link will also be used for the text or button.', 'uabb'),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'uabb'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'uabb'),
                            '_blank'        => __('New Window', 'uabb')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'cta_text'      => array(
                        'type'          => 'text',
                        'label'         => __('Text', 'uabb'),
                        'default'       => __('Read More', 'uabb'),
                    ),
                )
            ),
            'cta_type_button'       => array( // Section
                'title'         => __('Button', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'button' => array(
                        'type'          => 'form',
                        'label'         => __('Button Settings', 'uabb'),
                        'form'          => 'button_form_field', // ID of a registered form.
                        'preview_text'  => 'text', // ID of a field to use for the preview text.
                    ),
                )
            ),
        )
    ),
    'style'       => array( // Tab
        'title'         => __('Styles', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'slide_type'   => array(
                        'type'          => 'select',
                        'label'         => __('Select Style', 'uabb'),
                        'default'       => 'style1',
                        'help'          => 'Select Slide style for this slide box.',
                        'options'       => array(
                            'style1'      => __('Style 1', 'uabb'),
                            'style2'      => __('Style 2', 'uabb'),
                            'style3'      => __('Style 3', 'uabb'),
                        ),
                        'toggle' => array(
                            'style1' => array(
                                'sections' => array( 'overlay' )
                            ),
                            'style2' => array(
                                'fields'    => array( 'focused_front_title_color', 'focused_front_desc_color','dropdown_icon_bg_color', 'dropdown_icon_bg_color_opc', 'dropdown_icon_color' ),
                                'sections'  => array( 'dropdown_icon' )
                            ),
                            'style3' => array(
                                'fields'    => array( 'focused_front_title_color', 'focused_front_desc_color', 'dropdown_plus_icon_color' ),
                                'sections'  => array( 'dropdown_icon' )
                            )
                        )
                    ),
                )
            ),
            'front_styles'       => array( // Section
                'title'         => __('Slide Box Front Style', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'front_background_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => '#efefef',
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-slide-front',
                                'property'        => 'background'
                            )
                        )
                    ),
                    'front_background_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'focused_front_background_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Hover/Focus Color', 'uabb'),
                        )
                    ),
                    'focused_front_background_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                )
            ),
            'back_styles'       => array( // Section
                'title'         => __('Slide Box Back Style', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'back_alignment'   => array(
                        'type'          => 'select',
                        'label'         => __('Overall Alignment', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'center'           => __('Center', 'uabb'),
                            'left'            => __('Left', 'uabb'),
                            'right'            => __('Right', 'uabb')
                        ),
                        'preview'         => array(
                            'type'            => 'css',
                            'selector'        => '.uabb-slide-down',
                            'property'        => 'text-align'
                        )
                    ),
                    'back_background_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Hover/Focus Color', 'uabb'),
                            'default'       => '#dfdfdf',
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-slide-down',
                                'property'        => 'background'
                            )
                        )
                    ),
                    'back_background_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                )
            ),
            'overlay'       => array( // Section
                'title'         => __('Overlay', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'overlay'   => array(
                        'type'          => 'select',
                        'label'         => __('Enable Overlay', 'uabb'),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'           => __('Yes', 'uabb'),
                            'no'            => __('No', 'uabb')
                        ),
                        'toggle' => array(
                            'no' => array(
                                'fields' => array()
                            ),
                            'yes' => array(
                                'fields' => array( 'overlay_color', 'overlay_color_opc', 'overlay_opacity', 'overlay_icon', 'overlay_icon_color', 'overlay_icon_bg_color', 'overlay_icon_bg_color_opc', 'overlay_icon_size' )
                            )
                        )
                    ),
                    'overlay_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Overlay Color', 'uabb'),
                            'default'       => 'rgba( 0, 0, 0, 0.50 )',
                        )
                    ),
                    'overlay_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'overlay_icon'          => array(
                        'type'          => 'icon',
                        'default'       => 'fa fa-plus-square-o',
                        'label'         => __('Icon', 'uabb')
                    ),
                    'overlay_icon_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Icon Color', 'uabb'),
                        )
                    ),
                    
                    'overlay_icon_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Icon Background Color', 'uabb'),
                        )
                    ),
                    'overlay_icon_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'overlay_icon_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Icon Size', 'uabb'),
                        'default'       => '30',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    )
                )
            ),
            'dropdown_icon'       => array( // Section
                'title'         => __('Drop Down Icon', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'dropdown_icon_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Icon Color', 'uabb'),
                            'default'       => '#ffffff',
                        )
                    ),
                    'dropdown_plus_icon_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Icon Color', 'uabb'),
                        )
                    ),
                    'dropdown_icon_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Icon Background Color', 'uabb'),
                        )
                    ),
                    'dropdown_icon_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'dropdown_icon_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Icon Size', 'uabb'),
                        'default'       => '20',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'dropdown_icon_align'   => array(
                        'type'          => 'select',
                        'label'         => __('Icon Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'center'           => __('Center', 'uabb'),
                            'left'            => __('Left', 'uabb'),
                            'right'            => __('Right', 'uabb')
                        )
                    ),
                )
            ),
            'slide_box_min_height'       => array( // Section
                'title'         => __('Slide Box Min Height', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'set_min_height'   => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Minimum Height', 'uabb'),
                        'default'       => 'default',
                        'options'       => array(
                            'default'      => __('No', 'uabb'),
                            'custom'      => __('Yes', 'uabb'),
                        ),
                        'toggle' => array(
                            'default' => array(
                                'fields' => array()
                            ),
                            'custom' => array(
                                'fields' => array( 'slide_min_height', 'slide_vertical_align' )
                            )
                        )
                    ),
                    'slide_min_height'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Enter Height', 'uabb' ),
                        'default'       => '',
                        'description'   => __( 'px', 'uabb' ),
                        'size'          => '8',
                        'help'          => __( 'Apply minimum height to complete SlideBox. It is useful when multiple SlideBox are in same row.', 'uabb' )
                    ),
                    'slide_vertical_align'         => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Overall Vertical Alignment', 'uabb'),
                        'default'       => 'center',
                        'help'          => __('If enabled, the Content would align vertically center', 'uabb'),
                        'options'       => array(
                            'center'        => __('Center', 'uabb'),
                            'inherit'       => __('Top', 'uabb')
                        )
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'front_title_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Front Title', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-slide-face-text-title'
                            )
                        ),
                        'tag_selection'   => array(
                            'default'       => 'h3',
                        ),
                        'focused_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                                'label'         => __('Hover/Focus Color', 'uabb'),
                            )
                        ),
                        
                        'margin_top'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                        'margin_bottom'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                    ), 
                ),
                array(),
                'front_title'
            ),
            'front_desc_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Front Description', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-slide-box-section-content'
                            )
                        ),
                        'color' => array(
                            'label'         => __('Description Color', 'uabb'),
                        ),
                        'focused_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                                'label'         => __('Hover/Focus Color', 'uabb'),
                            )
                        ),
                        'margin_top'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                        'margin_bottom'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                    ),
                ), 
                array('tag_selection'),
                'front_desc'
            ),
            'back_title_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Back Title', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-slide-back-text-title'
                            )
                        ),
                        'tag_selection'   => array(
                            'default'       => 'h3',
                        ),
                        'color' => array(
                            'preview' => array(
                                'type' => 'css',
                                'property' => 'color',
                                'selector' => '.uabb-slide-back-text-title'
                            )
                        ),
                        'margin_top'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                        'margin_bottom'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                    ),
                ), 
                array(),
                'back_title'
            ),
            'back_desc_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Back Description', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-slide-down-box-section-content'
                            )
                        ),
                        'color' => array(
                            'label'         => __('Description Color', 'uabb'),
                            'preview' => array(
                                'type' => 'css',
                                'property' => 'color',
                                'selector' => '.uabb-slide-down-box-section-content'
                            )
                        ),
                        'margin_top'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'size'          => '8',
                            'description'   => __('px', 'uabb'),
                        ),
                        'margin_bottom'     => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'size'          => '8',
                            'placeholder'       => '10',
                            'description'   => __('px', 'uabb'),
                        ),
                    ),
                ), 
                array('tag_selection'),
                'back_desc'
            ),
            'link_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __( 'Link Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-callout-cta-link'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Color', 'uabb'),
                        )
                    ),
                 ), 
                array( 'tag_selection' ),
                'link'
            ),
        )
    ),
));

