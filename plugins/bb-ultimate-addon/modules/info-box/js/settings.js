(function($){

	FLBuilder.registerModuleHelper('info-box', {

		init: function()
		{
			var form    	= $('.fl-builder-settings'),
				image_type 	= form.find('select[name=image_type]'),
				icon_style	= form.find('select[name=icon_style]'),
				image_style	= form.find('select[name=image_style]'),
				photoSource     = form.find('select[name=photo_source]'),
				librarySource   = form.find('select[name=photo_src]'),
				urlSource       = form.find('input[name=photo_url]'),
				ctaType     = form.find('select[name=cta_type]'),
				align       = form.find('select[name=align]'),
				mobile_align       = form.find('select[name=mobile_align]'),
				icon_position = form.find('select[name=img_icon_position]');

			// Init validation events.
			/*this._imageTypeChanged();*/
			this._ctaTypeChanged();
			this._toggleImageIcon();

			
			// Validation events.
			/*imageType.on('change', this._imageTypeChanged);*/
			ctaType.on('change', this._ctaTypeChanged);
			photoSource.on('change', $.proxy( this._photoSourceChanged, this ) );
			image_type.on('change', $.proxy( this._toggleImageIcon, this ) );
			icon_position.on('change', $.proxy( this._toggleImageIcon, this ) );
			icon_style.on('change', $.proxy( this._toggleBorderOptions, this ) ) ;
			image_style.on('change', $.proxy( this._toggleBorderOptions, this ) ) ;
			
			// Preview events.
			align.on('change', this._previewAlign);
			
		},
		
		_toggleBorderOptions: function() {
			var form		= $('.fl-builder-settings'),
				show_border = false
				image_type 	= form.find('select[name=image_type]').val(),
				icon_style 	= form.find('select[name=icon_style]').val(),
				border_style 	= form.find('select[name=icon_border_style]').val(),
				image_style 	= form.find('select[name=image_style]').val(),
				img_border_style 	= form.find('select[name=img_border_style]').val();

			
			if( image_type == 'icon' ){
				if( icon_style == 'custom'  ){
					show_border = true;
				}

				if( show_border == false ){
					form.find('#fl-field-icon_border_width').hide();
					form.find('#fl-field-icon_border_color').hide();
					form.find('#fl-field-icon_border_hover_color').hide();
				}else if( show_border ){
					if ( border_style != 'none' ) {
						form.find('#fl-field-icon_border_width').show();
						form.find('#fl-field-icon_border_color').show();
						form.find('#fl-field-icon_border_hover_color').show();
					}
				}
			}else if( image_type == 'photo' ){
				if( image_style == 'custom' ){
					show_border = true;
				}

				if( show_border == false ){
					form.find('#fl-field-img_border_width').hide();
					form.find('#fl-field-img_border_color').hide();
					form.find('#fl-field-img_border_hover_color').hide();
				}else if( show_border ){
					if ( img_border_style != 'none' ) {
						form.find('#fl-field-img_border_width').show();
						form.find('#fl-field-img_border_color').show();
						form.find('#fl-field-img_border_hover_color').show();
					}
				}
			}
		},
		_toggleImageIcon: function() {
			var form        = $('.fl-builder-settings'),
				show_color 	= false,
				image_type 	= form.find('select[name=image_type]').val(),
				image_style = form.find('select[name=image_style]').val();

			form.find('#fl-builder-settings-section-icon_margins').hide();
			
			//console.log( this );
			if( image_type == 'photo' && image_style == 'custom' ){
				show_color = true;
			}

			if( show_color == false ){
				form.find('#fl-builder-settings-section-img_colors').hide();
			}else if( show_color ){
				form.find('#fl-builder-settings-section-img_colors').show();
			}
			if( image_type == 'icon' ) {
				form.find('#fl-builder-settings-section-icon_basic').show();
				form.find('#fl-builder-settings-section-icon_style').show();
				form.find('#fl-builder-settings-section-icon_colors').show();
				form.find('#fl-builder-settings-section-icon_margins').show();
				
				form.find('#fl-field-img_icon_position').show();
				form.find('#fl-field-align_items').show();
				form.find('#fl-field-mobile_view').show();

				var position = form.find('#fl-field-img_icon_position').find('select[name=img_icon_position]').val();
				
				//console.log( position );
				if( position == 'above-title' || position == 'below-title' || position == 'left' || position == 'right' ) {
					form.find('#fl-builder-settings-section-icon_margins').show();
				}else{
					form.find('#fl-builder-settings-section-icon_margins').hide();
				}

				if( position == 'above-title' || position == 'below-title' ) {
					form.find('#fl-field-align').show();
					form.find('#fl-field-mobile_align').show();
				}else {
					form.find('#fl-field-align').hide();
					form.find('#fl-field-mobile_align').hide();
				}
				if( position == 'left' || position == 'right' ) {
					form.find('#fl-field-align_items').show();
					form.find('#fl-field-mobile_view').show();
				} else {
					form.find('#fl-field-align_items').hide();
					form.find('#fl-field-mobile_view').hide();
				}

			} else if( image_type == 'photo' ) {

				form.find('#fl-builder-settings-section-img_basic').show();
				form.find('#fl-builder-settings-section-img_style').show();

				form.find('#fl-builder-settings-section-icon_basic').hide();
				form.find('#fl-builder-settings-section-icon_style').hide();
				form.find('#fl-builder-settings-section-icon_colors').hide();

				form.find('#fl-field-img_icon_position').show();
				form.find('#fl-field-align_items').show();
				form.find('#fl-field-mobile_view').show();


				var position = form.find('#fl-field-img_icon_position').find('select[name=img_icon_position]').val();
				if( position == 'above-title' || position == 'below-title' ) {
					form.find('#fl-field-align').show();
					form.find('#fl-field-mobile_align').show();
				} else {
					form.find('#fl-field-align').hide();
					form.find('#fl-field-mobile_align').hide();
				}
				if( position == 'left' || position == 'right' ) {
					form.find('#fl-field-align_items').show();
					form.find('#fl-field-mobile_view').show();
				} else {
					form.find('#fl-field-align_items').hide();
					form.find('#fl-field-mobile_view').hide();
				}
			} else {

				form.find('#fl-builder-settings-section-icon_basic').hide();
				form.find('#fl-builder-settings-section-icon_style').hide();
				form.find('#fl-builder-settings-section-icon_colors').hide();
				form.find('#fl-builder-settings-section-img_basic').hide();
				form.find('#fl-builder-settings-section-img_style').hide();
				form.find('#fl-field-img_icon_position').hide();
				form.find('#fl-field-align_items').hide();
				form.find('#fl-field-mobile_view').hide();
				form.find('#fl-field-align').show();
				form.find('#fl-field-mobile_align').show();
			}

			this._toggleBorderOptions();
			this._photoSourceChanged();
		},
		_photoSourceChanged: function()
		{
			var form            = $('.fl-builder-settings'),
				image_type	 	= form.find('select[name=image_type]').val(),
				photoSource     = form.find('select[name=photo_source]').val(),
				photo           = form.find('input[name=photo]'),
				photoUrl        = form.find('input[name=photo_url]'),
				iconSize        = form.find('input[name=icon_size]'),
				imgSize        	= form.find('input[name=img_size]');


			photo.rules('remove');
			photoUrl.rules('remove');
			iconSize.rules('remove');
			imgSize.rules('remove');
			if ( image_type == 'photo' ) {
				if(photoSource == 'library') {
					photo.rules('add', { required: true });
				}
				else {
					photoUrl.rules('add', { required: true });
				}
				imgSize.rules('add', { required: true, number:true });
			}else if ( image_type == 'icon' ) {
				iconSize.rules('add', { required: true, number:true });
			}
		},
		/*_imageTypeChanged: function()
		{
			var form        = $('.fl-builder-settings'),
				imageType   = form.find('select[name=image_type]').val(),
				photo       = form.find('input[name=photo]'),
				icon       = form.find('input[name=icon]');
				
			photo.rules('remove');
			icon.rules('remove');
			
			if(imageType == 'photo') {
				photo.rules('add', { required: true });
			}
			else if(imageType == 'icon') {
				icon.rules('add', { required: true });
			}
		},*/
		
		_ctaTypeChanged: function()
		{
			var form    = $('.fl-builder-settings'),
				ctaType = form.find('select[name=cta_type]').val(),
				ctaText = form.find('input[name=cta_text]');
				
			ctaText.rules('remove');
			
			if(ctaType != 'none') {
				ctaText.rules('add', {
					required: true
				});
			}
		},
		
		_previewAlign: function()
		{
			var form   = $('.fl-builder-settings'),
				align  = form.find('select[name=align]').val(),
				wrap   = FLBuilder.preview.elements.node.find('.infobox');
				
			wrap.removeClass('infobox-left');
			wrap.removeClass('infobox-center');
			wrap.removeClass('infobox-right');
			wrap.addClass('infobox-' + align);
		}
	});

})(jQuery);