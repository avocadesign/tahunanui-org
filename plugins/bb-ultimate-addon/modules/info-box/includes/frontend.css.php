<?php
$settings->prefix_color = UABB_Helper::uabb_colorpicker( $settings, 'prefix_color' );
$settings->title_color = UABB_Helper::uabb_colorpicker( $settings, 'title_color' );
$settings->subhead_color = UABB_Helper::uabb_colorpicker( $settings, 'subhead_color' );
$settings->link_color = UABB_Helper::uabb_colorpicker( $settings, 'link_color' );

$settings->bg_color = UABB_Helper::uabb_colorpicker( $settings, 'bg_color', true );
?>

.fl-node-<?php echo $id; ?> {
	width: 100%;
}
<?php

/* Render Butto */
if($settings->cta_type == 'button') {
	FLBuilder::render_module_css('uabb-button', $id, array(
		/* General Section */
        'text'              => $settings->btn_text,
        
        /* Link Section */
        'link'              => $settings->btn_link,
        'link_target'       => $settings->btn_link_target,
        
        /* Style Section */
        'style'             => $settings->btn_style,
        'border_size'       => $settings->btn_border_size,
        'transparent_button_options' => $settings->btn_transparent_button_options,
        'threed_button_options'      => $settings->btn_threed_button_options,
        'flat_button_options'        => $settings->btn_flat_button_options,

        /* Colors */
        'bg_color'          => $settings->btn_bg_color,
        'bg_hover_color'    => $settings->btn_bg_hover_color,
        'text_color'        => $settings->btn_text_color,
        'text_hover_color'  => $settings->btn_text_hover_color,

        /* Icon */
        'icon'              => $settings->btn_icon,
        'icon_position'     => $settings->btn_icon_position,
        
        /* Structure */
        'width'              => $settings->btn_width,
        'custom_width'       => $settings->btn_custom_width,
        'custom_height'      => $settings->btn_custom_height,
        'padding_top_bottom' => $settings->btn_padding_top_bottom,
        'padding_left_right' => $settings->btn_padding_left_right,
        'border_radius'      => $settings->btn_border_radius,
        'align'              => $settings->btn_banner_alignemnt,
        'mob_align'          => '',

        /* Typography */
        'font_size'         => $settings->btn_font_size,
        'line_height'       => $settings->btn_line_height,
        'font_family'       => $settings->btn_font_family,
	));
}
/* Render Separator */
if( $settings->enable_separator == 'block' ) {
	FLBuilder::render_module_css('uabb-separator', $id, array(
		'color'			=> $settings->separator_color,
		'height'		=> $settings->separator_height,
		'width'			=> $settings->separator_width,
		'alignment'		=> $settings->separator_alignment,
		'style'			=> $settings->separator_style
	));
?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-separator {
		margin-top: <?php echo $settings->separator_margin_top; ?>px;
		margin-bottom: <?php echo $settings->separator_margin_bottom; ?>px;
	}
<?php }
/* Render Image Icon CSS */
/* CSS "$settings" Array */
 
 /* Icon Image Render */
if( $settings->image_type != 'none' ) {

	/* CSS "$settings" Array */
 	$imageicon_array = array(
      
      	/* General Section */
      	'image_type' => $settings->image_type,
 
      	/* Icon Basics */
      	'icon' => $settings->icon,
      	'icon_size' => $settings->icon_size,
      	'icon_align' => '',
 
      	/* Image Basics */
      	'photo_source' => $settings->photo_source,
      	'photo' => $settings->photo,
      	'photo_url' => $settings->photo_url,
      	'img_size' => $settings->img_size,
      	'responsive_img_size' => $settings->responsive_img_size,
      	'img_align' => '',
      	'photo_src' => ( isset( $settings->photo_src ) ) ? $settings->photo_src : '' ,
 
      	/* Icon Style */
      	'icon_style' => $settings->icon_style,
      	'icon_bg_size' => $settings->icon_bg_size,
      	'icon_border_style' => $settings->icon_border_style,
      	'icon_border_width' => $settings->icon_border_width,
      	'icon_bg_border_radius' => $settings->icon_bg_border_radius,
 
      	/* Image Style */
      	'image_style' => $settings->image_style,
      	'img_bg_size' => $settings->img_bg_size,
      	'img_border_style' => $settings->img_border_style,
      	'img_border_width' => $settings->img_border_width,
      	'img_bg_border_radius' => $settings->img_bg_border_radius,
 		
 		/* Preset Color variable new */
      	'icon_color_preset' => $settings->icon_color_preset,

      	/* Icon Colors */ 
      	'icon_color' => $settings->icon_color,
      	'icon_hover_color' => $settings->icon_hover_color,
      	'icon_bg_color' => $settings->icon_bg_color,
      	'icon_bg_hover_color' => $settings->icon_bg_hover_color,
      	'icon_border_color' => $settings->icon_border_color,
      	'icon_border_hover_color' => $settings->icon_border_hover_color,
      	'icon_three_d' => $settings->icon_three_d,
 
      	/* Image Colors */
      	'img_bg_color' => $settings->img_bg_color,
      	'img_bg_hover_color' => $settings->img_bg_hover_color,
      	'img_border_color' => $settings->img_border_color,
      	'img_border_hover_color' => $settings->img_border_hover_color,
 	);
 
 	/* CSS Render Function */ 
 	FLBuilder::render_module_css( 'image-icon', $id, $imageicon_array );
}
?>

<?php if ( $settings->image_type == 'icon' && $settings->icon_style == 'simple' ) { ?>
/* Icon */

.fl-node-<?php echo $id; ?> .infobox .uabb-icon-wrap .uabb-icon i, 
.fl-node-<?php echo $id; ?> .infobox .uabb-icon-wrap .uabb-icon i:before {
    width: 1.3em;
    height: 1.3em;
    line-height: 1.3em;
}
<?php } ?>


<?php
if( $settings->image_type != 'none' ) {
	if( $settings->img_icon_position == 'left' || $settings->img_icon_position == 'right' ) {
		if( $settings->align_items == 'center' ) {
?>
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-imgicon-wrap {
	vertical-align: middle;
}
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-content {
	vertical-align: middle;
}
<?php
		}
	}
}
?>

/* Image icon Margin 0 */
<?php if ( $settings->title == '' && $settings->text == '' ) { ?>
	.fl-node-<?php echo $id; ?> .uabb-imgicon-wrap {
		margin: 0;
	}
<?php } ?>

/* Border Properties */
<?php if($settings->uabb_border_type != 'none') : ?>
		.fl-builder-content .fl-node-<?php echo $id; ?> .infobox {
			border-style: <?php echo $settings->uabb_border_type; ?>;
			
			<?php if( $settings->uabb_border_color != "" ) : ?>
				border-color: <?php echo $settings->uabb_border_color; ?>;
			<?php endif; ?>
			
			border-top-width: <?php echo  $settings->uabb_border_top ? $settings->uabb_border_top : '0'; ?>px;
			border-bottom-width: <?php echo $settings->uabb_border_bottom ? $settings->uabb_border_bottom : '0'; ?>px;
			border-left-width: <?php echo $settings->uabb_border_left ? $settings->uabb_border_left : '0'; ?>px;
			border-right-width: <?php echo $settings->uabb_border_right ? $settings->uabb_border_right : '0'; ?>px;
			
			<?php if( $settings->uabb_border_top > 0 || $settings->uabb_border_bottom > 0 || $settings->uabb_border_left > 0 || $settings->uabb_border_right > 0 ) : ?>
				padding: 20px;
			<?php endif; ?>
		}
	
<?php endif; ?>

/* Background Property */
<?php if( !empty($settings->bg_color) ) : ?>
	.fl-node-<?php echo $id; ?> .infobox { 
		background: <?php echo $settings->bg_color; ?>;
		padding: 20px;
	}
<?php endif; ?>

/* Align */
.fl-node-<?php echo $id; ?> .infobox-<?php echo $settings->align; ?> {
	text-align: <?php echo $settings->align; ?>;
}

/* Minimum Height and Vertical Alignment */	
<?php if( $settings->min_height_switch == 'custom' && $settings->min_height != "" ) : ?>
.fl-node-<?php echo $id; ?> .infobox {
	
	min-height: <?php echo $settings->min_height; ?>px;
	display: flex;
	align-items: <?php echo $settings->vertical_align; ?>;
}

.fl-node-<?php echo $id; ?> .infobox-<?php echo $settings->align; ?> {
	justify-content: <?php echo ( $settings->align == 'center' ) ? 'center' : ( ( $settings->align == 'left' ) ? 'flex-start' : 'flex-end' )  ?>;
}

<?php endif; ?>

/* Heading Margin Properties */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-title {
	margin-top: <?php echo $settings->heading_margin_top; ?>px;
	margin-bottom: <?php echo ( $settings->heading_margin_bottom != '' ) ? $settings->heading_margin_bottom : '10' ; ?>px;
}

/* Prefix Margin Properties */
<?php if ( $settings->prefix_margin_top ) { ?>
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-title-prefix {
	margin-top: <?php echo $settings->prefix_margin_top; ?>px;
}
<?php } ?>

/* Heading Color */
<?php if( !empty($settings->title_color) ) : ?> 
.fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title, 
.fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title span a,
.fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title * {
	color: <?php echo $settings->title_color; ?>
}
<?php endif; ?>

.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-text {
	margin-top: <?php echo $settings->content_margin_top; ?>px;
	margin-bottom: <?php echo $settings->content_margin_bottom; ?>px;
}

/* Description Color */
 
.fl-node-<?php echo $id; ?> .uabb-infobox-text,
.fl-node-<?php echo $id; ?> .uabb-infobox-text * {
	color: <?php echo uabb_theme_text_color( $settings->subhead_color ); ?>;
}

/* Icon Margin */
<?php if ( $settings->image_type == 'icon' ) { ?>
	<?php $pos = $settings->img_icon_position;
	if ( $pos == 'above-title' || $pos == 'below-title' || $pos == 'left' || $pos == 'right' ) { ?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-imgicon-wrap {
		margin-top: <?php echo ( $settings->icon_margin_top != '' ) ? $settings->icon_margin_top : '5'; ?>px;
		margin-bottom: <?php echo ( $settings->icon_margin_bottom != '' ) ? $settings->icon_margin_bottom : '0'; ?>px;
	}
	<?php } ?>
<?php } ?>

<?php if( $settings->cta_type == 'button' ) { ?>
/* Button Margin */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-button {
	margin-top: <?php echo ( $settings->btn_margin_top != '' ) ? $settings->btn_margin_top : '10'; ?>px;
	margin-bottom: <?php echo ( $settings->btn_margin_bottom != '' ) ? $settings->btn_margin_bottom : ''; ?>px;
}
<?php } ?>

<?php if( $settings->cta_type == 'link' ) { ?>
/* Link Text Margin */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-cta-link {
	margin-top: <?php echo ( $settings->link_margin_top != '' ) ? $settings->link_margin_top : ''; ?>px;
	margin-bottom: <?php echo ( $settings->link_margin_bottom != '' ) ? $settings->link_margin_bottom : ''; ?>px;

	<?php if ( $settings->link_margin_top != '' || $settings->link_margin_bottom != '' ) { ?>
	display:block;
	<?php } ?>
}
<?php } ?>

/* Link Color */
<?php if( !empty($settings->link_color) ) : ?> 
.fl-builder-content .fl-node-<?php echo $id; ?> a,
.fl-builder-content .fl-node-<?php echo $id; ?> a *,
.fl-builder-content .fl-node-<?php echo $id; ?> a:visited {
	color: <?php echo uabb_theme_text_color( $settings->link_color ); ?>;
}
<?php endif; ?>

/* Typography Options for Title */
.fl-builder-content .fl-node-<?php echo $id; ?> <?php /*echo $settings->title_tag_selection;*/ ?>.uabb-infobox-title,
.fl-builder-content .fl-node-<?php echo $id; ?> <?php /*echo $settings->title_tag_selection;*/ ?>.uabb-infobox-title a {
	
	<?php if( $settings->title_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->title_font_family ); ?>
	<?php endif; ?>

	<?php if( $settings->title_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->title_font_size['desktop']; ?>px;
	line-height: <?php echo $settings->title_font_size['desktop'] + 2; ?>px;
	<?php endif; ?>

	<?php if( $settings->title_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->title_line_height['desktop']; ?>px;
	<?php endif; ?>
}

/* Typography Options for Description */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-text {
	
	<?php if( $settings->subhead_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->subhead_font_family ); ?>
	<?php endif; ?>

	<?php if( $settings->subhead_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->subhead_font_size['desktop']; ?>px;
	line-height: <?php echo $settings->subhead_font_size['desktop'] + 2; ?>px;
	<?php endif; ?>

	<?php if( $settings->subhead_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->subhead_line_height['desktop']; ?>px;
	<?php endif; ?>
}

/* Typography Options for Prefix */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-title-prefix {
	
	<?php if( $settings->prefix_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->prefix_font_family ); ?>
	<?php endif; ?>

	<?php if( $settings->prefix_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->prefix_font_size['desktop']; ?>px;
	line-height: <?php echo $settings->prefix_font_size['desktop'] + 2; ?>px;
	<?php endif; ?>

	<?php if( $settings->prefix_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->prefix_line_height['desktop']; ?>px;
	<?php endif; ?>

	<?php if( $settings->prefix_color != '' ) : ?>
	color: <?php echo $settings->prefix_color; ?>;
	<?php endif; ?>
}

/* Typography Options for Link Text */
.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-cta-link {
	<?php if( $settings->link_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->link_font_family ); ?>
	<?php endif; ?>

	<?php if( $settings->link_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->link_font_size['desktop']; ?>px;
	<?php endif; ?>

	<?php if( $settings->link_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->link_line_height['desktop']; ?>px;
	<?php endif; ?>
}

/* Module Link */
<?php  if( $settings->cta_type == 'module' && !empty($settings->link) ) { ?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .infobox {
		position: relative; 
	}
	.fl-node-<?php echo $id; ?> .uabb-infobox-module-link:hover ~ .uabb-infobox-content .uabb-imgicon-wrap i,
	.fl-node-<?php echo $id; ?> .uabb-infobox-module-link:hover ~ .uabb-infobox-content .uabb-imgicon-wrap i:before {
		color : <?php echo $settings->icon_hover_color; ?>;
		background-color: <?php echo $settings->icon_bg_hover_color; ?>;
	}

	.fl-node-<?php echo $id; ?> .uabb-infobox-module-link:hover ~ .uabb-infobox-content .uabb-imgicon-wrap img,
	.fl-node-<?php echo $id; ?> .uabb-infobox-module-link:hover ~ .uabb-infobox-content .uabb-imgicon-wrap img:before {
		background-color: <?php echo $settings->img_bg_hover_color; ?>;
	}
<?php } ?>


/* Calculation Width */
<?php 	$class 		= '';
		$pos 		= '';
		$cal_width 	= '';
if( $settings->image_type == 'icon' ) { 
	/*$class = 'uabb-number-'.$settings->image_type.'-'.$settings->icon_position;*/
	$class = 'infobox-icon-' . $settings->img_icon_position;
	$pos = $settings->img_icon_position;
	if ( $pos == 'left' || $pos == 'right' || $pos == 'left-title' || $pos == 'right-title') {
		$cal_width = $settings->icon_size;
		if ( $pos == 'left' || $pos == 'right' || $pos == 'left-title' || $pos == 'right-title' ) {
            $cal_width = $settings->icon_size * 1.3;
        }
		if ( $settings->icon_style != 'simple' ) {
			$cal_width = $settings->icon_size * 2;
			if ( $settings->icon_style == 'custom' ) {
				$cal_width = $settings->icon_size + intval($settings->icon_bg_size);
				if ( $settings->icon_border_style != 'none' ) {
					$cal_width = $cal_width + ( intval($settings->icon_border_width) * 2 );
				}
			}
		}
		$cal_width = $cal_width + 25;
	}

}elseif ( $settings->image_type == 'photo' ) {
	/*$class = 'uabb-number-'.$settings->image_type.'-'.$settings->image_position;*/
	$class = 'infobox-photo-' . $settings->img_icon_position;
	$pos = $settings->img_icon_position;
	if ( $pos == 'left' || $pos == 'right' || $pos == 'left-title' || $pos == 'right-title' ) {
		$cal_width = $settings->img_size;
		if ( $settings->image_style == 'custom' ) {
			$cal_width = $cal_width + intval($settings->img_bg_size) * 2;
			if ( $settings->img_border_style != 'none' ) {
				$cal_width = $cal_width + ( intval($settings->img_border_width) * 2 );
			}
		}
		$cal_width = $cal_width + 25;
	}
}
?>

/* Left Right Title Image */
<?php if ( $pos == 'left' || $pos == 'right' ) { ?>
	.fl-node-<?php echo $id; ?> .infobox {
		text-align: <?php echo $pos; ?>;
	}
	.fl-builder-content .fl-node-<?php echo $id; ?> .<?php echo $class; ?> .uabb-infobox-content{
		width: calc(100% - <?php echo $cal_width; ?>px);
	}
<?php }elseif( $pos == 'left-title' || $pos == 'right-title' ) { ?>
	.fl-node-<?php echo $id; ?> .infobox {
		text-align: <?php echo ( $pos == 'left-title' ) ? 'left' : 'right'; ?>;
	}
	.fl-builder-content .fl-node-<?php echo $id; ?> .<?php echo $class; ?> .uabb-infobox-title-wrap {
		width: calc(100% - <?php echo $cal_width; ?>px);
		display: inline-block;
	}
<?php } ?>

/* Responsive CSS */
<?php if($global_settings->responsive_enabled) { ?>

<?php if( $settings->medium_border == 'yes' ) : ?>
<?php echo '@media (min-width: '. $global_settings->responsive_breakpoint .'px) and (max-width: '. $global_settings->medium_breakpoint .'px) { '; ?>

	.fl-builder-content .fl-node-<?php echo $id; ?> .infobox {
		border: none;
		padding: 0;
	}
}	
<?php endif; ?>

	

<?php echo '@media (max-width: '. $global_settings->medium_breakpoint .'px) { '; ?>
	
	.fl-builder-content .fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title,
	.fl-builder-content .fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title a {
		
		<?php if( $settings->title_font_size['medium'] != '' ) : ?>
		font-size: <?php echo $settings->title_font_size['medium']; ?>px;
		line-height: <?php echo $settings->title_font_size['medium'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->title_line_height['medium'] != '' ) : ?>
		line-height: <?php echo $settings->title_line_height['medium']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-text {
		
		<?php if( $settings->subhead_font_size['medium'] != '' ) : ?>
		font-size: <?php echo $settings->subhead_font_size['medium']; ?>px;
		line-height: <?php echo $settings->subhead_font_size['medium'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->subhead_line_height['medium'] != '' ) : ?>
		line-height: <?php echo $settings->subhead_line_height['medium']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-title-prefix {
		
		<?php if( $settings->prefix_font_size['medium'] != '' ) : ?>
		font-size: <?php echo $settings->prefix_font_size['medium']; ?>px;
		line-height: <?php echo $settings->prefix_font_size['medium'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->prefix_line_height['medium'] != '' ) : ?>
		line-height: <?php echo $settings->prefix_line_height['medium']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-cta-link {

		<?php if( $settings->link_font_size['medium'] != '' ) : ?>
		font-size: <?php echo $settings->link_font_size['medium']; ?>px;
		<?php endif; ?>

		<?php if( $settings->link_line_height['medium'] != '' ) : ?>
		line-height: <?php echo $settings->link_line_height['medium']; ?>px;
		<?php endif; ?>
	}
}

<?php echo '@media (max-width: '. $global_settings->responsive_breakpoint .'px) { '; ?>
	
	<?php if( $settings->responsive_border == 'yes' ) : ?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .infobox {
		border: none;
		padding: 0;
	}	
	<?php endif; ?>
	
	<?php if( $settings->mobile_view == 'stack' ) : ?>
	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-left-right-wrap .uabb-imgicon-wrap {
		padding: 0;
		margin-bottom: 20px;
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .infobox .uabb-infobox-left-right-wrap .uabb-infobox-content,
	.fl-builder-content .fl-node-<?php echo $id; ?> .infobox .uabb-infobox-left-right-wrap .uabb-imgicon-wrap {
		display: block;
		width: 100%;
		text-align: center;
	}
	<?php endif; ?>


	.fl-node-<?php echo $id; ?> .infobox-responsive-<?php echo $settings->mobile_align; ?> {
        text-align: <?php echo $settings->mobile_align; ?>;
    }
    

	.fl-builder-content .fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title,
	.fl-builder-content .fl-node-<?php echo $id; ?> <?php echo $settings->title_tag_selection; ?>.uabb-infobox-title a {
		
		<?php if( $settings->title_font_size['small'] != '' ) : ?>
		font-size: <?php echo $settings->title_font_size['small']; ?>px;
		line-height: <?php echo $settings->title_font_size['small'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->title_line_height['small'] != '' ) : ?>
		line-height: <?php echo $settings->title_line_height['small']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-text {
		
		<?php if( $settings->subhead_font_size['small'] != '' ) : ?>
		font-size: <?php echo $settings->subhead_font_size['small']; ?>px;
		line-height: <?php echo $settings->subhead_font_size['small'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->subhead_line_height['small'] != '' ) : ?>
		line-height: <?php echo $settings->subhead_line_height['small']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?> .uabb-infobox-title-prefix {
		
		<?php if( $settings->prefix_font_size['small'] != '' ) : ?>
		font-size: <?php echo $settings->prefix_font_size['small']; ?>px;
		line-height: <?php echo $settings->prefix_font_size['small'] + 2; ?>px;
		<?php endif; ?>

		<?php if( $settings->prefix_line_height['small'] != '' ) : ?>
		line-height: <?php echo $settings->prefix_line_height['small']; ?>px;
		<?php endif; ?>
	}

	.fl-builder-content .fl-node-<?php echo $id; ?>	.uabb-infobox-cta-link {

		<?php if( $settings->link_font_size['small'] != '' ) : ?>
		font-size: <?php echo $settings->link_font_size['small']; ?>px;
		<?php endif; ?>

		<?php if( $settings->link_line_height['small'] != '' ) : ?>
		line-height: <?php echo $settings->link_line_height['small']; ?>px;
		<?php endif; ?>
	}

	<?php if ( $settings->image_type == 'photo' && !empty( $settings->responsive_img_size ) ) {
		$class = 'infobox-photo-' . $settings->img_icon_position;
		$pos = $settings->img_icon_position;
		if ( $pos == 'left' || $pos == 'right' || $pos == 'left-title' || $pos == 'right-title' ) {
			$cal_width = $settings->responsive_img_size;
			if ( $settings->image_style == 'custom' ) {
				$cal_width = $cal_width + intval($settings->img_bg_size) * 2;
				if ( $settings->img_border_style != 'none' ) {
					$cal_width = $cal_width + ( intval($settings->img_border_width) * 2 );
				}
			}
			$cal_width = $cal_width + 25;
		}
	}
	?>

	/* Left Right Title Image */
	<?php if ( $pos == 'left' || $pos == 'right' ) { ?>
		.fl-node-<?php echo $id; ?> .infobox {
			text-align: <?php echo $pos; ?>;
		}
		.fl-builder-content .fl-node-<?php echo $id; ?> .<?php echo $class; ?> .uabb-infobox-content{
			width: calc(100% - <?php echo $cal_width; ?>px);
		}
	<?php }elseif( $pos == 'left-title' || $pos == 'right-title' ) { ?>
		.fl-node-<?php echo $id; ?> .infobox {
			text-align: <?php echo ( $pos == 'left-title' ) ? 'left' : 'right'; ?>;
		}
		.fl-builder-content .fl-node-<?php echo $id; ?> .<?php echo $class; ?> .uabb-infobox-title-wrap {
			width: calc(100% - <?php echo $cal_width; ?>px);
			display: inline-block;
		}
	<?php } ?>
}
<?php } ?>