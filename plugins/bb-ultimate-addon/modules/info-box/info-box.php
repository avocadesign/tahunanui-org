<?php

/**
 * @class UABBInfoBoxModule
 */
class UABBInfoBoxModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Info Box', 'uabb'),
			'description'   	=> __('A heading and snippet of text with an optional link, icon and image.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/info-box/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/info-box/',
            'partial_refresh'	=> true
		));
	}


	/**
	 * @method get_classname
	 */
	public function get_classname()
	{
		$classname = '';
		if( $this->settings->image_type == 'photo' ) {
			if ( $this->settings->img_icon_position == 'above-title' || $this->settings->img_icon_position == 'below-title' ) {
				$classname = 'infobox infobox-' . $this->settings->align;
				if( $this->settings->mobile_align != '' ) {
					$classname .= ' infobox-responsive-'. $this->settings->mobile_align;
				}
			}
			
			if ( $this->settings->img_icon_position == 'left-title' || $this->settings->img_icon_position == 'left' ) {
				$classname = 'infobox infobox-left';
			}
			if ( $this->settings->img_icon_position == 'right-title' || $this->settings->img_icon_position == 'right' ) {
				$classname = 'infobox infobox-right';
			}
			$classname .= ' infobox-has-photo infobox-photo-' . $this->settings->img_icon_position;
		} else if( $this->settings->image_type == 'icon' ) {
			if ( $this->settings->img_icon_position == 'above-title' || $this->settings->img_icon_position == 'below-title' ) {
				$classname = 'infobox infobox-' . $this->settings->align;
				if( $this->settings->mobile_align != '' ) {
					$classname .= ' infobox-responsive-'. $this->settings->mobile_align;
				}
			}
			
			if ( $this->settings->img_icon_position == 'left-title' || $this->settings->img_icon_position == 'left' ) {
				$classname = 'infobox infobox-left';
			}
			if ( $this->settings->img_icon_position == 'right-title' || $this->settings->img_icon_position == 'right' ) {
				$classname = 'infobox infobox-right';
			}
			$classname .= ' infobox-has-icon infobox-icon-' . $this->settings->img_icon_position;
		} else {
			$classname = 'infobox infobox-' . $this->settings->align;
			if( $this->settings->mobile_align != '' ) {
				$classname .= ' infobox-responsive-'. $this->settings->mobile_align;
			}
		}


		return $classname;
	}

	/**
	 * @method render_title
	 */
	public function render_title()
	{
		$flag = false;
		if ( ($this->settings->image_type == 'photo' && $this->settings->img_icon_position == 'left-title') || ( $this->settings->image_type == 'icon' && $this->settings->img_icon_position == 'left-title' ) ) {
			echo '<div class="left-title-image">';
			$flag = true;
		}
		if ( ($this->settings->image_type == 'photo' && $this->settings->img_icon_position == 'right-title') || ( $this->settings->image_type == 'icon' && $this->settings->img_icon_position == 'right-title' ) ) {
			echo '<div class="right-title-image">';
			$flag = true; 
		}
		$this->render_image('left-title');
		echo "<div class='uabb-infobox-title-wrap'>";
		if ( $this->settings->heading_prefix != "" ) {
			echo '<' . $this->settings->prefix_tag_selection . ' class="uabb-infobox-title-prefix">'. $this->settings->heading_prefix.'</' . $this->settings->prefix_tag_selection . '>';
		}
		
		echo '<' . $this->settings->title_tag_selection . ' class="uabb-infobox-title">';
		//echo '<span>';
		echo $this->settings->title;
		//echo '</span>';
		echo '</' . $this->settings->title_tag_selection . '> ';
		echo '</div>';
		$this->render_image('right-title');

		if ($flag) {
			echo '</div>';
		}

	}

	/**
	 * @method render_text
	 */
	public function render_text()
	{
		global $wp_embed;
	
		echo '<div class="uabb-infobox-text uabb-text-editor">' . wpautop( $wp_embed->autoembed( $this->settings->text ) ) . '</div>';
	}

	/**
	 * @method render_link
	 */
	public function render_link()
	{
		if($this->settings->cta_type == 'link') {
			echo '<a href="' . $this->settings->link . '" target="' . $this->settings->link_target . '" class="uabb-infobox-cta-link">' . $this->settings->cta_text . '</a>';
		}
	}

	/**
	 * @method render_button
	 */
	public function render_button()
	{

		if($this->settings->cta_type == 'button') {
			$btn_settings = array(

				/* General Section */
                'text'              => $this->settings->btn_text,
                
                /* Link Section */
                'link'              => $this->settings->btn_link,
                'link_target'       => $this->settings->btn_link_target,
                
                /* Style Section */
                'style'             => $this->settings->btn_style,
                'border_size'       => $this->settings->btn_border_size,
                'transparent_button_options' => $this->settings->btn_transparent_button_options,
                'threed_button_options'      => $this->settings->btn_threed_button_options,
                'flat_button_options'        => $this->settings->btn_flat_button_options,

                /* Colors */
                'bg_color'          => $this->settings->btn_bg_color,
                'bg_hover_color'    => $this->settings->btn_bg_hover_color,
                'text_color'        => $this->settings->btn_text_color,
                'text_hover_color'  => $this->settings->btn_text_hover_color,

                /* Icon */
                'icon'              => $this->settings->btn_icon,
                'icon_position'     => $this->settings->btn_icon_position,
                
                /* Structure */
                'width'              => $this->settings->btn_width,
                'custom_width'       => $this->settings->btn_custom_width,
                'custom_height'      => $this->settings->btn_custom_height,
                'padding_top_bottom' => $this->settings->btn_padding_top_bottom,
                'padding_left_right' => $this->settings->btn_padding_left_right,
                'border_radius'      => $this->settings->btn_border_radius,
                'align'              => '',
                'mob_align'          => '',

                /* Typography */
                'font_size'         => $this->settings->btn_font_size,
                'line_height'       => $this->settings->btn_line_height,
                'font_family'       => $this->settings->btn_font_family,
			);

			echo '<div class="uabb-infobox-button">';
			FLBuilder::render_module_html('uabb-button', $btn_settings);
			echo '</div>';
		}
	}

	/**
	 * @method render_image
	 */
	public function render_image($position)
	{
		$set_pos = '';
		if( $this->settings->image_type == 'icon' ){
			$set_pos 		= $this->settings->img_icon_position;
		}elseif( $this->settings->image_type == 'photo' ){
			$set_pos 		= $this->settings->img_icon_position;
		}

		/* Render Image / icon */
		if( $position == $set_pos ){
			$imageicon_array = array(
 
			    /* General Section */
			    'image_type' => $this->settings->image_type,
			 
			    /* Icon Basics */
			    'icon' => $this->settings->icon,
			    'icon_size' => $this->settings->icon_size,
			    'icon_align' => '',
			 
			    /* Image Basics */
			    'photo_source' => $this->settings->photo_source,
			    'photo' => $this->settings->photo,
			    'photo_url' => $this->settings->photo_url,
			    'img_size' => $this->settings->img_size,
			    'img_align' => '',
			    'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,
			 	
			 	/* Icon Style */
			    'icon_style' => $this->settings->icon_style,
			    'icon_bg_size' => $this->settings->icon_bg_size,
			    'icon_border_style' => $this->settings->icon_border_style,
			    'icon_border_width' => $this->settings->icon_border_width,
			    'icon_bg_border_radius' => $this->settings->icon_bg_border_radius,
			 	
			 	/* Image Style */
			    'image_style' => $this->settings->image_style,
			    'img_bg_size' => $this->settings->img_bg_size,
			    'img_border_style' => $this->settings->img_border_style,
			    'img_border_width' => $this->settings->img_border_width,
			    'img_bg_border_radius' => $this->settings->img_bg_border_radius,
			); 
			
			/* Render HTML Function */
			//echo '<div class="infobox-photo">';
				FLBuilder::render_module_html( 'image-icon', $imageicon_array );
			//echo '</div>';
		}
		
		
	}

	/**
	 * @method render_button
	 */
	public function render_separator() {

		if( $this->settings->enable_separator == 'block' ) {
			$separator_settings = array(
				'color'			=> $this->settings->separator_color,
				'height'		=> $this->settings->separator_height,
				'width'			=> $this->settings->separator_width,
				'alignment'		=> $this->settings->separator_alignment,
				'style'			=> $this->settings->separator_style
			);

			echo '<div class="uabb-infobox-separator">';
			FLBuilder::render_module_html('uabb-separator', $separator_settings);
			echo '</div>';
		}		
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBInfoBoxModule', array(
	'general'       => array(
		'title'         => __('General', 'uabb'),
		'sections'      => array(
			'title'         => array(
				'title'         => __('Heading', 'uabb'),
				'fields'        => array(
					'heading_prefix'          => array(
						'type'          => 'text',
						'label'         => 'Title Prefix',
						'help'			=> 'The small text appear above the title. You can leave it empty if not required.'
					),
					'title'         => array(
						'type'          => 'text',
						'label'         => __('Heading', 'uabb'),
						'default'		=> 'Info Box',
						'preview'       => array(
							'type'          => 'text',
							'selector'      => '.uabb-infobox-title'
						)
					),
				)
			),
			'text'          => array(
				'title'         => __('Description', 'uabb'),
				'fields'        => array(
					'text'          => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'rows'			=> 6,
						'default'		=> 'Enter description text here.',
					),
				)
			),
			'separator'       => array( // Section
				'title'         => __('Separator', 'uabb'), // Section Title
				'fields'        => array( // Section Fields
					'enable_separator'	=> array(
		                'type'			=> 'select',
		                'label'			=> __('Separator', 'uabb'),
		                'default'		=> '',
		                'options'		=> array(
		                    'none'			=> _x( 'No', 'Enable Separator', 'uabb' ),
		                    'block' 		=> _x( 'Yes', 'Enable Separator', 'uabb' )
		                ),
		                'toggle'		 => array(
		                    'none'				=> array(
		                        'fields'		=> array()
		                    ),
		                    'block'			=> array(
		                        'fields'		=> array( 'separator_color', 'separator_height', 'separator_style', 'separator_width', 'separator_alignment' ),
		                        'sections'		=> array( 'separator_margins')
		                    )
		                )
					),
					'separator_style'	=> array(
						'type'          => 'select',
						'label'         => __('Style', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'solid'         => _x( 'Solid', 'Border type.', 'uabb' ),
							'dashed'        => _x( 'Dashed', 'Border type.', 'uabb' ),
							'dotted'        => _x( 'Dotted', 'Border type.', 'uabb' ),
							'double'        => _x( 'Double', 'Border type.', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-style'
						),
						'help'          => __('The type of border to use. Double borders must have a height of at least 3px to render properly.', 'uabb'),
					),
					'separator_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Color', 'uabb'),
							'default'       => '#cccccc',
		                )
		            ),
					'separator_height'	=> array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'default'       => '1',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-width',
							'unit'          => 'px'
						),
						'help'			=> __('Adjust thickness of border.', 'uabb'),
					),
					'separator_width'        => array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%'
					),
					'separator_alignment'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'inherit',
						'options'       => array(
							'inherit'		=> _x( 'Default', 'uabb'),
							'center'		=> _x( 'Center', 'uabb' ),
							'left'			=> _x( 'Left', 'uabb' ),
							'right'			=> _x( 'Right', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator-parent',
							'property'      => 'text-align'
						),
					),
				)
			),
			'border'	=> BB_Ultimate_Addon::uabb_section_get('border',
				array( 'fields' => array(
						'uabb_border_type'	=> array(
		 					'toggle'        => array(
			                    'solid'         => array(
			                        'fields'        => array('medium_border')
			                    ),
			                    'dashed'        => array(
			                        'fields'        => array('medium_border')
			                    ),
			                    'dotted'        => array(
			                        'fields'        => array('medium_border')
			                    ),
			                    'double'        => array(
			                        'fields'        => array('medium_border')
			                    )
			                )
		 				),
						'medium_border'   => array(
			                'type'          => 'select',
			                'label'         => __('Hide on Medium Screen Devices', 'uabb'),
			                'default'       => 'no',
			                'options'       => array(
			                    'yes'   => _x( 'Yes', 'Border type.', 'uabb' ),
			                    'no'  	=> _x( 'No', 'Border type.', 'uabb' )
			                )
			            ),
		 			)
				)
			),
		)
	),
	'imageicon' => array(
		'title'         => __('Image / Icon', 'uabb'),
		'sections'      => array(
			'type_general' 		=> BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' ,
				array( 'fields' => array(
						'image_type'	=> array(
		 					'toggle'        => array(
								'icon'          => array(
									//'sections'	 => array( 'icon_margins' ),
									//'fields'	 => array( 'icon_margin_top', 'icon_margin_bottom' ),
								),
								/*'photo'         => array(
									'sections'	 => array( 'img_basic', 'img_style' ),
									'fields'	 => array( 'img_icon_position' ),	
								)*/
							),
		 				),
		 			)
				)
			),
			'icon_basic'		=> array( // Section
				'title'         => 'Icon Basics', // Section Title
				'fields'        => array( // Section Fields
					'icon'          => array(
						'type'          => 'icon',
						'label'         => __('Icon', 'uabb')
					),
					'icon_size'     => array(
						'type'          => 'text',
						'label'         => __('Size', 'uabb'),
						'default'       => '30',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
						'preview'	=> array(
                        	'type'		=> 'refresh',
                        ),
					),
				)
			),
			/* Image Basic Setting */
			'img_basic'		=> array( // Section
				'title'         => 'Image Basics', // Section Title
				'fields'        => array( // Section Fields
					'photo_source'  => array(
						'type'          => 'select',
						'label'         => __('Photo Source', 'uabb'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'uabb'),
							'url'           => __('URL', 'uabb')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo')
							),
							'url'           => array(
								'fields'        => array('photo_url' )
							)
						)
					),
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'uabb')
					),
					'photo_url'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'uabb'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
					),
					'img_size'     => array(
						'type'          => 'text',
						'label'         => __('Size', 'uabb'),
						'default'       => '150',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
					),
					'responsive_img_size'     => array(
						'type'          => 'text',
						'label'         => __('Responsive Size', 'uabb'),
						'default'       => '',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
						'help'			=> __( 'Image size below medium devices. Leve it blank if you want to keep same size', 'uabb' )
					),
				)
			),
			'icon_style'	=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_style',
				array( 'fields' => array(
						'icon_border_width'	=> array(
		 					'preview'	=> array(
                            	'type'		=> 'refresh',
                            ),
		 				),
		 			)
				)
			),
			'img_style'		=> 	BB_Ultimate_Addon::uabb_section_get( 'img_style',
				array(
					'fields' => array(
						'img_bg_size'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 				'img_border_width'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 			)
				)
			),
			'icon_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
		)
	),
	'style'         => array(
		'title'         => __('Style', 'uabb'),
		'sections'      => array(
			'overall_structure' => array(
				'title'         => __('Structure', 'uabb'),
				'fields'        => array(
					'img_icon_position' => array(
						'type'          => 'select',
						'label'         => __('Position', 'uabb'),
						'default'       => 'above-title',
						'help'	=> __( 'Image Icon position', 'uabb' ),
						'options'       => array(
							'above-title'   => __('Above Heading', 'uabb'),
							'below-title'   => __('Below Heading', 'uabb'),
							'left-title'    => __( 'Left of Heading', 'uabb' ),
							'right-title'   => __( 'Right of Heading', 'uabb' ),
							'left'          => __('Left of Text and Heading', 'uabb'),
							'right'         => __('Right of Text and Heading', 'uabb')
						),
						'toggle'			=> array(
							'above-title'	=> array(
								'fields'	=> array( 'align', 'mobile_align' ),
							),
							'below-title'	=> array(
								'fields'	=> array( 'align', 'mobile_align' ),
							),
							'left' => array(
								'fields' => array( 'align_items', 'mobile_view' )
							),
							'right' => array(
								'fields' => array( 'align_items', 'mobile_view' )
							)
						)
					),
					'align'         => array(
						'type'          => 'select',
						'label'         => __('Overall Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'left'          => __('Left', 'uabb'),
							'right'         => __('Right', 'uabb')
						),
						'help'          => __('The alignment that will apply to all elements within the infobox.', 'uabb'),
					),
					'mobile_align'         => array(
						'type'          => 'select',
						'label'         => __('Mobile Alignment', 'uabb'),
						'default'       => '',
						'options'       => array(
							''        		=> __('Default', 'uabb'),
							'center'        => __('Center', 'uabb'),
							'left'          => __('Left', 'uabb'),
							'right'         => __('Right', 'uabb')
						),
						'help'          => __('This alignment will apply on Mobile', 'uabb'),
					),
					'align_items' => array(
						'type'          => 'uabb-toggle-switch',
						'label'         => __('Icon Vertical Alignment', 'uabb'),
						'default'       => 'top',
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'top'          => __('Top', 'uabb'),
						),
					),
					'mobile_view' => array(
						'type'          => 'uabb-toggle-switch',
						'label'         => __('Mobile Structure', 'uabb'),
						'default'       => '',
						'options'       => array(
							''        => __('Inline', 'uabb'),
							'stack'	  => __('Stack', 'uabb'),
						),
						'preview'		=> array(
							'type'	  => 'none'
						)
					),
					'bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Color', 'uabb'),
		                )
		            ),
		            'bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
		            'min_height_switch'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Minimum Height', 'uabb' ),
                        'default'       => 'auto',
                        'options'       => array(
                          	'custom'		=> 'Yes',
                         	'auto'		=> 'No',
                        ),
                        'toggle'	=> array(
                        	'custom'	=> array(
                        		'fields'	=> array( 'min_height', 'vertical_align' ),
                        	)
                        ),
                    ),
					'min_height'          => array(
						'type'          => 'text',
		                'label'         => __('Enter Height', 'uabb'),
		                'description'   => 'px',
		                'maxlength'     => '4',
		                'size'          => '5',
		                'placeholder'   => 'auto',
		                'help'          => __('Apply minimum height to complete Callout. It is useful when multiple Callouts are in same row.', 'uabb'),
					),
					'vertical_align'         => array(
						'type'          => 'uabb-toggle-switch',
						'label'         => __('Overall Vertical Alignment', 'uabb'),
						'default'       => 'center',
						'help'			=> __('If enabled, the Content would align vertically center', 'uabb'),
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'inherit'       => __('Top', 'uabb')
						),
					),
				)
			),
			'heading_margins' => array(
				'title'         => __( 'Heading Margins', 'uabb' ),
				'fields'        => array(
					'heading_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-infobox-title',
							'property'      => 'margin-top',
							'unit'          => 'px'
						),
					),
					'heading_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '',
						'placeholder'		=> '10',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						/*'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-infobox-title',
							'property'      => 'margin-bottom',
							'unit'          => 'px'
						),*/
					),
					'prefix_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Prefix Top', 'uabb'),
						'default'           => '',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-infobox-title-prefix',
							'property'      => 'margin-top',
							'unit'          => 'px'
						),
					)
				)
			),
			'icon_margins' => array(
				'title'         => __( 'Icon Margins', 'uabb' ),
				'fields'        => array(
					'icon_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '',
						'placeholder'		=> '5',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					),
					'icon_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					)
				)
			),
			'content_margins' => array(
				'title'         => __( 'Description Margins', 'uabb' ),
				'fields'        => array(
					'content_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-infobox-text',
							'property'      => 'margin-top',
							'unit'          => 'px'
						),
					),
					'content_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-infobox-text',
							'property'      => 'margin-bottom',
							'unit'          => 'px'
						),
					)
				)
			),
			'separator_margins' => array(
				'title'         => __( 'Separator Margins', 'uabb' ),
				'fields'        => array(
					'separator_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '20',
						'placeholder'		=> '20',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'margin-top',
							'unit'          => 'px'
						),
					),
					'separator_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '20',
						'placeholder'		=> '20',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'margin-bottom',
							'unit'          => 'px'
						),
					)
				)
			),
		)
	),
	
	'cta'           => array(
		'title'         => __('Link', 'uabb'),
		'sections'      => array(
			'cta'           => array(
				'title'         => __('Call to Action', 'uabb'),
				'fields'        => array(
					'cta_type'      => array(
						'type'          => 'select',
						'label'         => __('Type', 'uabb'),
						'default'       => 'none',
						'options'       => array(
							'none'          => _x( 'None', 'Call to action.', 'uabb' ),
							'link'          => __('Text', 'uabb'),
							'button'        => __('Button', 'uabb'),
							'module'		=> __('Link To Module','uabb'),
						),
						'toggle'        => array(
							'none'          => array(),
							'link'          => array(
								'fields'        => array('cta_text'),
								'sections'		=> array('link', 'link_typography')
							),
							'button'        => array(
								'sections'      => array('btn-general', 'btn-link', 'btn-icon', 'btn-colors', 'btn-style', 'btn-structure', 'btn_typography')
							),
							'module'          => array(
								'sections'		=> array('link')
							),

						)
					),
					'cta_text'      => array(
						'type'          => 'text',
						'label'         => __('Text', 'uabb'),
						'default'		=> __('Read More', 'uabb'),
					),
				)
			),
			'btn-general'    => BB_Ultimate_Addon::uabb_section_get( 'btn-general' ),
            'btn-link'       => BB_Ultimate_Addon::uabb_section_get( 'btn-link' ),
            'btn-style'      => BB_Ultimate_Addon::uabb_section_get( 'btn-style' ),
            'btn-icon'       => BB_Ultimate_Addon::uabb_section_get( 'btn-icon' ),
            'btn-colors'     => BB_Ultimate_Addon::uabb_section_get( 'btn-colors' ),
            'btn-structure'  => BB_Ultimate_Addon::uabb_section_get( 'btn-structure', array(), array('btn_align', 'btn_mob_align') ),
			'link'          => array(
				'title'         => __('Link', 'uabb'),
				'fields'        => array(
					'link'          => array(
						'type'          => 'link',
						'label'         => __('Link', 'uabb'),
						'help'          => __('The link applies to the entire module. If choosing a call to action type below, this link will also be used for the text or button.', 'uabb'),
						'preview'       => array(
							'type'          => 'none'
						)
					),
					'link_target'   => array(
						'type'          => 'select',
						'label'         => __('Link Target', 'uabb'),
						'default'       => '_self',
						'options'       => array(
							'_self'         => __('Same Window', 'uabb'),
							'_blank'        => __('New Window', 'uabb')
						),
						'preview'       => array(
							'type'          => 'none'
						)
					)
				)
			),
		)
	),
	'typography'         => array(
		'title'         => __('Typography', 'uabb'),
		'sections'      => array(
			'prefix_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Title Prefix', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-infobox-title-prefix'
                        	),
            			),
            			'color'	=> array(
            				'label'         => __('Color', 'uabb'),
                        ),
                        'tag_selection' => array(
                        	'default' => 'h5'
                        )
            		),
				), 
				array(),
				'prefix' 
			),
			'title_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Title', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-infobox-title'
                        	),
            			),
                        'tag_selection' => array(
                        	'default' => 'h3'
                        )
            		),
				), 
				array(),
				'title' 
			),
			'subhead_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Description', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-infobox-text, .uabb-infobox-text * '
                        	),
            			),
            			'color'	=> array(
            				'label'         => __('Description Color', 'uabb'),
                        )
            		),
				), 
				array( 'tag_selection' ),
				'subhead' 
			),
			'btn_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __( 'Button Text', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> 'a.uabb-button'
                        	),
            			),
            			'margin_top' => array(
							'type'              => 'text',
							'label'             => __('Top', 'uabb'),
							'default'           => '',
							'placeholder'		=> '10',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
						'margin_bottom' => array(
							'type'              => 'text',
							'label'             => __('Bottom', 'uabb'),
							'default'           => '',
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
            		),
				), 
				array( 'color','tag_selection' ),
				'btn'
			),
			'link_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __( 'Link Text', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-infobox-cta-link'
                        	),
            			),
            			'color'	=> array(
            				'label'         => __('Link Color', 'uabb'),
                        ),
                        'margin_top' => array(
							'type'              => 'text',
							'label'             => __('Top', 'uabb'),
							'default'           => '',
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
						'margin_bottom' => array(
							'type'              => 'text',
							'label'             => __('Bottom', 'uabb'),
							'default'           => '',
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
            		),
				 ), 
				array( 'tag_selection' ),
				'link'
			),
		)
	)
));
