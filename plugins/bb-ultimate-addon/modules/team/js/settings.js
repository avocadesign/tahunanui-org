(function($){

	FLBuilder.registerModuleHelper('team', {

		init: function()
		{
			var form    	= $('.fl-builder-settings'),
				icon_style	= form.find('select[name=icon_style]'),
				image_style	= form.find('select[name=image_style]');
				

			// Init validation events.
			this._toggleBorderOptions();

			icon_style.on('change', $.proxy( this._toggleBorderOptions, this ) ) ;
			image_style.on('change', $.proxy( this._toggleBorderOptions, this ) ) ;
		},
		
		_toggleBorderOptions: function() {
			var form		= $('.fl-builder-settings'),
				show_border = false
				image_type 	= form.find('select[name=image_type]').val(),
				icon_style 	= form.find('select[name=icon_style]').val(),
				border_style 	= form.find('select[name=icon_border_style]').val(),
				image_style 	= form.find('select[name=image_style]').val(),
				img_border_style 	= form.find('select[name=img_border_style]').val();

			if( icon_style != 'custom'  ) {
				form.find('#fl-field-icon_border_width').hide();
				form.find('#fl-field-icon_border_color').hide();
				form.find('#fl-field-icon_border_hover_color').hide();
			}else{
				form.find('#fl-field-icon_border_width').hide();
				form.find('#fl-field-icon_border_color').hide();
				form.find('#fl-field-icon_border_hover_color').hide();
				if ( border_style != 'none' ) {
					form.find('#fl-field-icon_border_width').show();
					form.find('#fl-field-icon_border_color').show();
					form.find('#fl-field-icon_border_hover_color').show();
				}
			}

			if( image_style != 'custom' ) {
				form.find('#fl-builder-settings-section-img_colors').hide();
			}else{
				form.find('#fl-builder-settings-section-img_colors').hide();
				if ( img_border_style != 'none' ) {
					form.find('#fl-builder-settings-section-img_colors').show();
				}
			}
		},
	});

})(jQuery);