<?php

/**
 * @class UABBTeamModule
 */
class UABBTeamModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Team', 'uabb'),
			'description'   	=> __('A Team module to show team member.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/team/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/team/',
            'partial_refresh'	=> true
		));
	}
	

	/**
	 * @method render_image
	 */
	public function render_image()
	{
		/* Render Team Member Image */
		$imageicon_array = array(

		    /* General Section */
		    'image_type' => 'photo',
		 
		    /* Icon Basics */
		    'icon' => '',
		    'icon_size' => '',
		    'icon_align' => '',
		 
		    /* Image Basics */
		    'photo_source' => $this->settings->photo_source,
		    'photo' => $this->settings->photo,
		    'photo_url' => $this->settings->photo_url,
		    'img_size' => $this->settings->img_size,
		    'img_align' => '',
		    'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,
		 	
		 	/* Icon Style */
		    'icon_style' => '',
		    'icon_bg_size' => '',
		    'icon_border_style' => '',
		    'icon_border_width' => '',
		    'icon_bg_border_radius' => '',
		 	
		 	/* Image Style */
		    'image_style' => $this->settings->image_style,
		    'img_bg_size' => '',//$this->settings->img_bg_size,
		    'img_border_style' => $this->settings->img_border_style,
		    'img_border_width' => $this->settings->img_border_width,
		    'img_bg_border_radius' => $this->settings->img_bg_border_radius,
		); 
		
		/* Render HTML Function */
		//echo '<div class="infobox-photo">';
		FLBuilder::render_module_html( 'image-icon', $imageicon_array );
		//echo '</div>';
	}


	/**
	 * @method Render Name
	 */
	public function render_name()
	{
		if ( !empty( $this->settings->name ) ) {
			$output  = '<div class="uabb-team-name" >';
			$output .= '<'.$this->settings->tag_selection.' class="uabb-team-name-text">'.$this->settings->name.'</'.$this->settings->tag_selection.'>';
			$output .= '</div>';
			echo $output;
		}
	}
	
	/**
	 * @method Render Designation
	 */
	public function render_desgn()
	{
		if ( !empty( $this->settings->designation ) ) {
			$output  = '<div class="uabb-team-desgn">';
			$output .= '<span class="uabb-team-desgn-text">'.$this->settings->designation.'</span>';
			$output .= '</div>';
			echo $output;
		}
	}
	
	/**
	 * @method Render Desc
	 */
	public function render_desc()
	{
		if ( !empty( $this->settings->description ) ) {
			$output  = '<div class="uabb-team-desc">';
			$output .= '<span class="uabb-team-desc-text">'.$this->settings->description.'</span>';
			$output .= '</div>';
			echo $output;
		}
	}

	/**
	 * @method render_social_icons
	 */
	public function render_social_icons()
	{	
		if ( $this->settings->enable_social_icons == 'yes'  ) {
			$icon_count = 1;
			foreach( $this->settings->icons as $icon ) {

				if(!is_object($icon)) {
					continue;
				}

				echo '<a class="uabb-team-icon-link uabb-team-icon-'.$icon_count.'" href="'.$icon->link.'">';
				$imageicon_array = array(

				  /* General Section */
				  'image_type' 	=> 'icon',

				  /* Icon Basics */
				  'icon' 		=> $icon->icon,
				  'icon_size' 	=> $this->settings->icon_size,
				  'icon_align' 	=> 'center',

				  /* Image Basics */
				  'photo_source' 	=> '',
				  'photo' 			=> '',
				  'photo_url' 		=> '',
				  'img_size' 		=> '',
				  'img_align' 		=> '',
				  'photo_src' 		=> '' ,

				  /* Icon Style */
				  'icon_style' 				=> $this->settings->icon_style,
				  'icon_bg_size' 			=> $this->settings->icon_bg_size,
				  'icon_border_style' 		=> $this->settings->icon_border_style,
				  'icon_border_width' 		=> $this->settings->icon_border_width,
				  'icon_bg_border_radius' 	=> $this->settings->icon_bg_border_radius,

				  /* Image Style */
				  'image_style' 			=> '',
				  'img_bg_size' 			=> '',
				  'img_border_style' 		=> '',
				  'img_border_width' 		=> '',
				  'img_bg_border_radius' 	=> '',

				  /* Preset Color variable new */
				  'icon_color_preset' => $this->settings->icon_color_preset, 
				  
				  /* Icon Colors */
				  'icon_color' 				=> ( !empty($icon->icocolor) ) ? $icon->icocolor : $this->settings->icon_color,
				  'icon_hover_color' 		=> ( !empty($icon->icohover_color) ) ? $icon->icohover_color : $this->settings->icon_hover_color,
				  'icon_bg_color' 			=> ( !empty($icon->icobg_color) ) ? $icon->icobg_color : $this->settings->icon_bg_color,
				  'icon_bg_hover_color' 	=> ( !empty($icon->icobg_hover_color) ) ? $icon->icobg_hover_color : $this->settings->icon_bg_hover_color,
				  'icon_border_color' 		=> ( !empty($icon->icoborder_color) ) ? $icon->icoborder_color : $this->settings->icon_border_color,
				  'icon_border_hover_color' => ( !empty($icon->icoborder_hover_color) ) ? $icon->icoborder_hover_color : $this->settings->icon_border_hover_color,
				  'icon_three_d' 			=> $this->settings->icon_three_d,

				  /* Image Colors */
				  'img_bg_color' 			=> '',
				  'img_bg_hover_color' 		=> '',
				  'img_border_color' 		=> '',
				  'img_border_hover_color' 	=> '',
				);
				FLBuilder::render_module_html('image-icon', $imageicon_array);
				echo '</a>';
				$icon_count = $icon_count + 1 ;
			}
		}
	}

	/**
	 * @method render_button
	 */
	public function render_separator( $pos ) {

		if( $this->settings->enable_separator == 'block' && ($pos == $this->settings->separator_pos) )
		{
			$separator_settings = array(
				'color'			=> $this->settings->separator_color,
				'height'		=> $this->settings->separator_height,
				'width'			=> $this->settings->separator_width,
				'alignment'		=> $this->settings->separator_alignment,
				'style'			=> $this->settings->separator_style
			);

			echo '<div class="uabb-team-separator">';
			FLBuilder::render_module_html('uabb-separator', $separator_settings);
			echo '</div>';
		}		
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBTeamModule', array(
	'imageicon' => array(
		'title'         => __('Image', 'uabb'),
		'sections'      => array(
			 
			
			/* Image Basic Setting */
			'img_basic'	=> 	BB_Ultimate_Addon::uabb_section_get( 'img_basic', 
							array(
								'title'	 => '',
							), array('img_align') ),
			'img_style'		=> 	BB_Ultimate_Addon::uabb_section_get( 'img_style',
				array(
					'title'	 => __( 'Image Style', 'uabb' ),
					'fields' => array(
						'img_border_style'   => array(
			                'toggle'        => array(
			                    'solid'         => array(
			                    	'sections'	=> array('img_colors'),
			                    ),
			                    'dashed'        => array(
			                    	'sections'	=> array('img_colors'),
			                    ),
			                    'dotted'        => array(
			                    	'sections'	=> array('img_colors'),
			                    ),
			                    'double'        => array(
			                    	'sections'	=> array('img_colors'),
			                    )
			                ),
    					),
		 				'img_border_width'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 			)
				),
				array( 'img_bg_size' )
			),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors', 
								array(), 
								array( 'img_bg_color', 'img_bg_color_opc', 'img_bg_hover_color' )
								),
			'img_hover'		=> array(
				'title'		 => __('Style', 'uabb' ),
				'fields'	 => array(
					'img_spacing'		=> array(
						'type'          => 'uabb-spacing',
			            'label'         => __( 'Image Section Padding', 'uabb' ),
			            'mode'			=> 'padding',
			            'default'       => 'padding: 0px;' // Optional
					),
					'img_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Color', 'uabb'),
						)
                    ),
                    'img_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'photo_style' => array(
						'type'          => 'select',
		                'label'         => __( 'Photo Style', 'uabb' ),
		                'default'       => 'simple',
		                'options'       => array(
		                 	'simple'			=> 'Simple',
							'grayscale'			=> 'Grayscale',
		                ),
		                'toggle'		 => array(
		                    'simple'				=> array(
		                        'fields'		=> array( 'img_grayscale_simple' )
		                    ),
		                    'grayscale'			=> array(
		                        'fields'		=> array( 'img_grayscale_grayscale' ),
		                    )
		                )
					),
					'img_grayscale_simple' => array(
						'type'          => 'select',
		                'label'         => __( 'Hover Effect', 'uabb' ),
		                'default'       => 'no',
		                'options'       => array(
		                 	'yes'			=> 'Simple',
							'color_gray'	=> 'Grayscale on Hover',
		                ),
					),
					'img_grayscale_grayscale' => array(
						'type'          => 'select',
		                'label'         => __( 'Hover Effect', 'uabb' ),
		                'default'       => 'no',
		                'options'       => array(
		                 	'yes'			=> 'Simple',
							'gray_color'	=> 'Color on Hover',
		                ),
					),
					/*'img_grayscale' =>	array(
		                'type'          => 'select',
		                'label'         => __( 'Hover Effect', 'uabb' ),
		                'default'       => 'no',
		                'options'       => array(
		                 	'no'			=> 'No',
							'yes'			=> 'Effect 1',
							'gray_color'	=> 'Effect 2',
							'color_gray'	=> 'Effect 3'
		                ),
		                'help'			=> __( 'Effect 1 : Only Grayscale, Effect 2 : Grayscale to Color, Effect 3 : Color to Grayscale', 'uabb' )
		            ),*/
					/*'img_hover'	=> array(
						'type'          => 'select',
						'label'         => __('Image Hover Effect', 'uabb'),
						'default'       => 'no',
						'options'       => array(
							'no'   			=> __('No', 'uabb'),
							'zoom_out'   	=> __('Zoom Out', 'uabb'),
							'zoom_in'   	=> __('Zoom In', 'uabb'),
						)
					),*/
					/*'img_overlay_color'	=> array(
						'type'          => 'uabb-color',
						'label'         => __('Image Overlay Color', 'uabb'),
						'show_reset'    => true,
					),*/
				),
			)
		)
	),
	'team_text'         => array(
		'title'         => __('Information', 'uabb'),
		'sections'      => array(
			'member_info' => array(
				'title'         => __('Member Information', 'uabb'),
				'fields'        => array(
					'name'         => array(
						'type'          => 'text',
						'label'         => __('Name', 'uabb'),
						'default'		=> 'John Doe',
						/*'preview'       => array(
							'type'          => 'text',
							'selector'      => '.uabb-infobox-title'
						)*/
					),
					'designation'	=> array(
						'type'          => 'text',
						'label'         => __('Designation', 'uabb'),
					),
					'description'	=> array(
						'type'          => 'textarea',
						'label'         => __('Description', 'uabb'),
						'rows'          => '5',
					),
				)
			),
			'text_style' => array(
				'title'         => __('Content Style', 'uabb'),
				'fields'        => array(
					'text_spacing'		=> array(
						'type'          => 'uabb-spacing',
                        'label'         => __( 'Padding', 'fl-builder' ),
                        'mode'			=> 'padding',
                        'default'       => 'padding: 15px;' // Optional
					),
					'text_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Color', 'uabb'),
						)
                    ),
                    'text_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
				)
			),
			'separator'       => array( // Section
				'title'         => __('Separator', 'uabb'), // Section Title
				'fields'        => array( // Section Fields
					'enable_separator'	=> array(
		                'type'			=> 'select',
		                'label'			=> __('Separator', 'uabb'),
		                'default'		=> 'none',
		                'options'		=> array(
		                    'none'			=> __( 'No', 'Enable Separator', 'uabb' ),
		                    'block' 		=> __( 'Yes', 'Enable Separator', 'uabb' )
		                ),
		                'toggle'		 => array(
		                    'none'				=> array(
		                        'fields'		=> array()
		                    ),
		                    'block'			=> array(
		                        'fields'		=> array( 'separator_pos','separator_color', 'separator_height', 'separator_style', 'separator_width', 'separator_alignment','separator_margin_top', 'separator_margin_bottom' ),
		                        'sections'		=> array( 'separator_margins')
		                    )
		                )
					),
					'separator_pos'	=> array(
						'type'          => 'select',
						'label'         => __('Position', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'below_name'	=> __( 'Below Name', 'uabb' ),
							/*'below_image'	=> __( 'Below Image', 'uabb' ),*/
							'below_desg'    => __( 'Below Designation', 'uabb' ),
							'below_desc'   => __( 'Below Description', 'uabb' )
						),
					),
					'separator_style'	=> array(
						'type'          => 'select',
						'label'         => __('Style', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'solid'         => __( 'Solid', 'uabb' ),
							'dashed'        => __( 'Dashed', 'uabb' ),
							'dotted'        => __( 'Dotted', 'uabb' ),
							'double'        => __( 'Double', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-style'
						),
						'help'          => __('The type of border to use. Double borders must have a height of at least 3px to render properly.', 'uabb'),
					),
					'separator_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Color', 'uabb'),
                        	'default'       => '#cccccc',
						)
                    ),
					'separator_height'	=> array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'default'       => '1',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-width',
							'unit'          => 'px'
						),
						'help'			=> __('Adjust thickness of border.', 'uabb'),
					),
					'separator_width'	=> array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%'
					),
					'separator_alignment' => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'inherit',
						'options'       => array(
							'inherit'		=> __( 'Default', 'uabb'),
							'center'		=> __( 'Center', 'uabb' ),
							'left'			=> __( 'Left', 'uabb' ),
							'right'			=> __( 'Right', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator-parent',
							'property'      => 'text-align'
						),
					),
					'separator_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Margin Top', 'uabb'),
						'placeholder'		=> '10',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'margin-top',
							'unit'          => 'px'
						),
					),
					'separator_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Margin Bottom', 'uabb'),
						'placeholder'		=> '10',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'margin-bottom',
							'unit'          => 'px'
						),
					),
				)
			),
			/*'name_margin' => array(
				'title'         => __( 'Name Field Margins', 'uabb' ),
				'fields'        => array(
					'name_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '0',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					),
					'name_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '0',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					),
				)
			),*/
			/*'desc_margin' => array(
				'title'         => __( 'Description Margins', 'uabb' ),
				'fields'        => array(
					'desc_margin_top' => array(
						'type'              => 'text',
						'label'             => __('Top', 'uabb'),
						'default'           => '0',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					),
					'desc_margin_bottom' => array(
						'type'              => 'text',
						'label'             => __('Bottom', 'uabb'),
						'default'           => '15',
						'placeholder'		=> '0',
						'maxlength'         => '3',
						'size'              => '4',
						'description'       => 'px',
					)
				)
			),*/
		)
	),
	
	'social_links_section'           => array(
		'title'         => __('Social Links', 'uabb'),
		'sections'      => array(
			'social_icons_switch'     => array(
				'title'         => '',
				'fields'        => array(
					'enable_social_icons' =>	array(
		                'type'          => 'uabb-toggle-switch',
		                'label'         => __( 'Enable Social Icons', 'uabb' ),
		                'default'       => 'no',
		                'options'       => array(
		                 	'yes'		=> 'Yes',
		                  	'no'		=> 'No',
		                ),
		                'toggle'	=> array(
		                	'yes'	=> array(
		                		'sections'	=> array( 'social_links', 'icon_basic', 'icon_style', 'icon_colors' )
		                	)
		                )
		            )
	            )
            ),
			'social_links'           => array(
				'title'         => __( 'Social Icons', 'uabb'),
				'fields'        => array(
					'icons'         => array(
						'type'          => 'form',
						'label'         => __('Icon', 'uabb'),
						'form'          => 'uabb_social_icon_form', // ID from registered form below
						'preview_text'  => 'icon', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			),
			'icon_basic'		=>	BB_Ultimate_Addon::uabb_section_get( 'icon_basic', 
									array( 
										'fields'	=> array(
											'spacing'       => array(
												'type'          => 'text',
												'label'         => __('Spacing Between Icons', 'uabb'),
												'default'       => '10',
												'maxlength'     => '2',
												'size'          => '6',
												'description'   => 'px'
											),
										)
									),
									array( 'icon', 'icon_align') ),
			'icon_style'		=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_style' ),
			'icon_colors'		=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
		)
	),
	'typography'         => array(
		'title'         => __('Typography', 'uabb'),
		'sections'      => array(
			'name_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Name', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-team-name-text'
                        	),
            			),
                        'tag_selection' => array(
                        	'default' => 'h3'
                        ),
                        'name_margin_top' => array(
							'type'              => 'text',
							'label'             => __('Margin Top', 'uabb'),
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
						'name_margin_bottom' => array(
							'type'              => 'text',
							'label'             => __('Margin Bottom', 'uabb'),
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
            		),
				), 
				array()
			),
			'desg_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Designation', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-team-desgn-text'
                        	),
            			),
            			'color'	=> array(
            				'label'         => __('Color', 'uabb'),
                        ),
                        'margin_top' => array(
							'type'              => 'text',
							'label'             => __('Margin Top', 'uabb'),
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
						'margin_bottom' => array(
							'type'              => 'text',
							'label'             => __('Margin Bottom', 'uabb'),
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
            		),
				), 
				array( 'tag_selection' ),
				'desg' 
			),
			'desc_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Description', 'uabb' ),
					'fields'   => array(
						'font_family' => array(
                            'preview'	=> array(
                                'type'		=> 'font',
                                'selector'	=> '.uabb-team-desc-text'
                        	),
            			),
            			'color'	=> array(
            				'label'         => __('Color', 'uabb'),
                        ),
                        'margin_top' => array(
							'type'              => 'text',
							'label'             => __('Margin Top', 'uabb'),
							'placeholder'		=> '0',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
						'margin_bottom' => array(
							'type'              => 'text',
							'label'             => __('Margin Bottom', 'uabb'),
							'placeholder'       => '15',
							'maxlength'         => '3',
							'size'              => '4',
							'description'       => 'px',
						),
                    ),
				), 
				array( 'tag_selection' ),
				'desc' 
			),
			'structure'		=> array(
				'title'		 => __('Structure', 'uabb'),
				'fields'	 => array(
					'text_alignment' => array(
						'type'          => 'select',
						'label'         => __('Content Alignment', 'uabb'),
						'default'       => 'center',
						'help'			=> __('Overall Content Alignment', 'uabb'),
						'options'       => array(
							'center'		=> __( 'Center', 'uabb' ),
							'left'			=> __( 'Left', 'uabb' ),
							'right'			=> __( 'Right', 'uabb' )
						),
					),
					'module_border_radius'	=> array(
						'type'          => 'text',
						'label'         => __('Box Radius', 'uabb'),
						'default'       => '0',
						'maxlength'     => '3',
						'size'          => '6',
						'description'   => 'px'
					)
				)
			)
		)
	)
));


/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('uabb_social_icon_form', array(
	'title' => __('Add Icon', 'uabb'),
	'tabs'  => array(
		'general'       => array( // Tab
			'title'         => __('General', 'uabb'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array( // Section
					'title'         => '', // Section Title
					'fields'        => array( // Section Fields
						'icon'          => array(
							'type'          => 'icon',
							'label'         => __( 'Icon', 'uabb' )
						),
						'link'          => array(
							'type'          => 'link',
							'label'         => __( 'Link', 'uabb' )
						)
					)
				)
			)
		),
		'style'         => array( // Tab
			'title'         => __('Style', 'uabb'), // Tab title
			'sections'      => array( // Tab Sections
				'colors'        => array( // Section
					'title'         => __('Colors', 'uabb'), // Section Title
					'fields'        => array( // Section Fields
						'icocolor'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Color', 'uabb'),
							)
						),
						'icohover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Hover Color', 'uabb'),
							)
						),
						'icobg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Background Color', 'uabb'),
							)
						),
						'icobg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

						'icobg_hover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Background Hover Color', 'uabb'),
						    	'preview'       => array(
									'type'          => 'none'
								)
							)
						),
						'icobg_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity', array(
						    	'preview'       => array(
									'type'          => 'none'
								)
							) 
						),
	                    
						'icoborder_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Border Color', 'uabb'),
							)
						),
						'icoborder_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

						'icoborder_hover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
						    	'label'         => __('Border Hover Color', 'uabb'),
						    	'preview'       => array(
									'type'          => 'none'
								)
							)
						),
						'icoborder_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity', array(
						    	'preview'       => array(
									'type'          => 'none'
								)
							) 
						),
					)
				)
			)
		)
	)
));
