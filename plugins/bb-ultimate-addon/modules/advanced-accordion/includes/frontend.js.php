(function($) {

	$(function() {
	
		new UABBAdvAccordion({
			id: '<?php echo $id ?>',
			close_icon: '<?php $words = explode(' ', $settings->close_icon);
							$lastWord = end($words); 
							echo $lastWord ?>',
			open_icon: '<?php $words = explode(' ', $settings->open_icon);
							$lastWord = end($words); 
							echo $lastWord ?>',
			icon_animation: '<?php echo $settings->icon_animation; ?>',
			enable_first: '<?php echo $settings->enable_first; ?>',
		});
	});
	
})(jQuery);