<?php 
	$settings->title_color = UABB_Helper::uabb_colorpicker( $settings, 'title_color' );
	$settings->title_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'title_hover_color' );
	
	$settings->title_bg_color = UABB_Helper::uabb_colorpicker( $settings, 'title_bg_color', true );
	$settings->title_bg_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'title_bg_hover_color', true );

	$settings->title_border_color = UABB_Helper::uabb_colorpicker( $settings, 'title_border_color' );

	$settings->icon_color = UABB_Helper::uabb_colorpicker( $settings, 'icon_color' );
	$settings->icon_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'icon_hover_color' );

	$settings->content_color = UABB_Helper::uabb_colorpicker( $settings, 'content_color' );
	$settings->content_bg_color = UABB_Helper::uabb_colorpicker( $settings, 'content_bg_color', true );

	$settings->content_border_color = UABB_Helper::uabb_colorpicker( $settings, 'content_border_color' );
	
?>

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item {
	<?php if ( is_numeric( $settings->title_margin ) ) { ?>
		margin-bottom: <?php echo $settings->title_margin; ?>px;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button {
	<?php echo $settings->title_spacing; ?>;
	background: <?php echo $settings->title_bg_color; ?>;
<?php if ( $settings->title_border_type != 'none' ) {// var_dump( $settings->title_border_top); die(); ?>
	border: <?php echo $settings->title_border_type; ?> <?php echo $settings->title_border_color; ?>;
	border-top-width: <?php echo $settings->title_border_top; ?>px;
	border-bottom-width: <?php echo $settings->title_border_bottom; ?>px;
	border-left-width: <?php echo $settings->title_border_left; ?>px;
	border-right-width: <?php echo $settings->title_border_right; ?>px;
	<?php if( ( $settings->title_margin == 0 || $settings->title_margin == '' ) && ( $settings->title_border_top != 0 && $settings->title_border_top != '' ) ) { ?>
	border-bottom-width: 0;
	<?php } ?>
    -webkit-transition: all 100ms ease-in-out 100ms;
       -moz-transition: all 100ms ease-in-out 100ms;
         -o-transition: all 100ms ease-in-out 100ms;
			transition: all 100ms ease-in-out 100ms;
<?php } ?>
	<?php if( !empty( $settings->title_border_radius ) ){ ?>
	border-radius: <?php echo $settings->title_border_radius; ?>px;	
	<?php } ?>
}

<?php if( ( $settings->title_margin == 0 || $settings->title_margin == '' ) && ( $settings->title_border_top != 0 || $settings->title_border_top != '' ) ) : ?>

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item:last-child .uabb-adv-accordion-button,
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item-active .uabb-adv-accordion-button {
	<?php if ( $settings->title_border_type != 'none' ) { ?>
		border-bottom-width: <?php echo $settings->title_border_bottom; ?>px;
	<?php } ?>
}

<?php endif; ?>


/* Color */
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-label {
	color: <?php echo $settings->title_color; ?>;
	text-align: <?php echo $settings->title_align; ?>;
    -webkit-transition: all 100ms ease-in-out 100ms;
       -moz-transition: all 100ms ease-in-out 100ms;
         -o-transition: all 100ms ease-in-out 100ms;
			transition: all 100ms ease-in-out 100ms;
}

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-icon {
	color: <?php echo $settings->icon_color; ?>;
	transition: all 100ms ease-in-out 100ms;
    -moz-transition: all 100ms ease-in-out 100ms;
    -webkit-transition: all 100ms ease-in-out 100ms;
    -o-transition: all 100ms ease-in-out 100ms;
}



/* Content css */

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content {
	color: <?php echo $settings->content_color; ?>;
	background: <?php echo $settings->content_bg_color; ?>;
	<?php echo $settings->content_spacing; ?>;
	text-align: <?php echo $settings->content_align; ?>;
	<?php if ( $settings->content_border_type != 'none' ) { ?>
		border: <?php echo $settings->content_border_type ?> <?php echo $settings->content_border_color; ?>;
		border-top-width: <?php echo $settings->content_border_top; ?>px;
		border-bottom-width: <?php echo $settings->content_border_bottom; ?>px;
		border-left-width: <?php echo $settings->content_border_left; ?>px;
		border-right-width: <?php echo $settings->content_border_right; ?>px;
	<?php } ?>
	<?php if( !empty( $settings->content_border_radius ) ){ ?>
	border-radius: <?php echo $settings->content_border_radius; ?>px;	
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content * {
	color: inherit;
}

/* Hover State */
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button:hover .uabb-adv-accordion-button-label,
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item-active .uabb-adv-accordion-button-label {
	color: <?php echo $settings->title_hover_color; ?>;
}
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button:hover,
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item-active .uabb-adv-accordion-button {
	background: <?php echo $settings->title_bg_hover_color; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button:hover .uabb-adv-accordion-button-icon,
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-item-active .uabb-adv-accordion-button-icon {
	color: <?php echo $settings->icon_hover_color; ?>;
}


/* Typography */
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-label {
	<?php if( $settings->font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->font_family ); ?>
	<?php endif; ?>
	<?php if( $settings->font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->font_size['desktop']; ?>px;
	line-height: <?php echo $settings->font_size['desktop'] + 2; ?>px;
	<?php endif; ?>
	<?php if( $settings->line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->line_height['desktop']; ?>px;
	<?php endif; ?>
}

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-icon {
	<?php if( $settings->icon_size != '' ) : ?>
		font-size: <?php echo $settings->icon_size; ?>px;
		line-height: <?php echo $settings->icon_size + 2; ?>px;
		width: <?php echo $settings->icon_size + 2; ?>px;
	<?php endif; ?>
	
	/*<?php if( $settings->font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->font_size['desktop']; ?>px;
	line-height: <?php echo $settings->font_size['desktop'] + 2; ?>px;
	width: <?php echo $settings->font_size['desktop'] + 2; ?>px;
	<?php endif; ?>
	<?php if( $settings->line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->line_height['desktop']; ?>px;
	width: <?php echo $settings->line_height['desktop']; ?>px;
	<?php endif; ?>*/
}

.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content,
.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content * {
	<?php if( $settings->content_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->content_font_family ); ?>
	<?php endif; ?>
	<?php if( $settings->content_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->content_font_size['desktop']; ?>px;
	line-height: <?php echo $settings->content_font_size['desktop'] + 2; ?>px;
	<?php endif; ?>
	<?php if( $settings->content_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->content_line_height['desktop']; ?>px;
	<?php endif; ?>
}


<?php if($global_settings->responsive_enabled) { // Global Setting If started 
	if( $settings->font_size['medium'] != "" || $settings->line_height['medium'] != "" ) { ?>
		@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-label {
				<?php if( $settings->font_size['medium'] != '' ) : ?>
				font-size: <?php echo $settings->font_size['medium']; ?>px;
				line-height: <?php echo $settings->font_size['medium'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->line_height['medium'] != '' ) : ?>
				line-height: <?php echo $settings->line_height['medium']; ?>px;
				<?php endif; ?>
			}

			/*.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-icon {
				<?php if( $settings->font_size['medium'] != '' ) : ?>
				font-size: <?php echo $settings->font_size['medium']; ?>px;
				line-height: <?php echo $settings->font_size['medium'] + 2; ?>px;
				width: <?php echo $settings->font_size['medium'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->line_height['medium'] != '' ) : ?>
				line-height: <?php echo $settings->line_height['medium']; ?>px;
				width: <?php echo $settings->line_height['medium']; ?>px;
				<?php endif; ?>
			}*/
		}		
	<?php } ?>
	<?php if( $settings->font_size['small'] != "" || $settings->line_height['small'] != "" ) {
		/* Small Breakpoint media query */	
	?>
		@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-label {
				<?php if( $settings->font_size['small'] != '' ) : ?>
				font-size: <?php echo $settings->font_size['small']; ?>px;
				line-height: <?php echo $settings->font_size['small'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->line_height['small'] != '' ) : ?>
				line-height: <?php echo $settings->line_height['small']; ?>px;
				<?php endif; ?>
			}
			/*.fl-node-<?php echo $id; ?> .uabb-adv-accordion-button-icon {
				<?php if( $settings->font_size['small'] != '' ) : ?>
				font-size: <?php echo $settings->font_size['small']; ?>px;
				line-height: <?php echo $settings->font_size['small'] + 2; ?>px;
				width: <?php echo $settings->font_size['small'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->line_height['small'] != '' ) : ?>
				line-height: <?php echo $settings->line_height['small']; ?>px;
				width: <?php echo $settings->line_height['small']; ?>px;
				<?php endif; ?>
			}*/
		}		
	<?php
	}

	/* Content Responsive */
	if( $settings->content_font_size['medium'] != "" || $settings->content_line_height['medium'] != "" ) { ?>
		@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content,
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content * {
				<?php if( $settings->content_font_size['medium'] != '' ) : ?>
				font-size: <?php echo $settings->content_font_size['medium']; ?>px;
				line-height: <?php echo $settings->content_font_size['medium'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->content_line_height['medium'] != '' ) : ?>
				line-height: <?php echo $settings->content_line_height['medium']; ?>px;
				<?php endif; ?>
			}
		}		
	<?php } ?>
	<?php if( $settings->content_font_size['small'] != "" || $settings->content_line_height['small'] != "" ) {
		/* Small Breakpoint media query */	
	?>
		@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content,
			.fl-node-<?php echo $id; ?> .uabb-adv-accordion-content * {
				<?php if( $settings->content_font_size['small'] != '' ) : ?>
				font-size: <?php echo $settings->content_font_size['small']; ?>px;
				line-height: <?php echo $settings->content_font_size['small'] + 2; ?>px;
				<?php endif; ?>
				<?php if( $settings->content_line_height['small'] != '' ) : ?>
				line-height: <?php echo $settings->content_line_height['small']; ?>px;
				<?php endif; ?>
			}
		}		
	<?php
	}
} ?>