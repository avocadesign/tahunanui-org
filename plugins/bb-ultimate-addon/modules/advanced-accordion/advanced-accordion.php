<?php

/**
 * @class UABBAdvancedAccordionModule
 */
class UABBAdvancedAccordionModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Advanced Accordion', 'uabb'),
			'description'   	=> __('Display a collapsible accordion of items.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/advanced-accordion/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/advanced-accordion/',
			'partial_refresh'	=> true
		));

		$this->add_css('font-awesome');
	}

	function render_icon( $pos )
	{
		if ( $pos == $this->settings->icon_position ) {
			if ( $this->settings->icon_animation == 'none' ) {
				$output = '<div class="uabb-adv-accordion-icon-wrap">';
				$output .= '<i class="uabb-adv-accordion-button-icon '.$this->settings->close_icon.'"></i>';
				$output .= '</div>';
			}else {
				$output = '<div class="uabb-adv-accordion-icon-wrap uabb-adv-'.$this->settings->icon_animation.'">';
				$output .= '<div class="uabb-adv-accordion-icon-animation">';

				if ( $this->settings->icon_animation == 'push-out-top' || $this->settings->icon_animation == 'push-out-left' ) {
					$output .= '<i class="uabb-adv-accordion-button-icon uabb-adv-accordion-open-icon '.$this->settings->open_icon.'"></i>';
					$output .= '<i class="uabb-adv-accordion-button-icon uabb-adv-accordion-close-icon '.$this->settings->close_icon.'"></i>';
				}elseif ( $this->settings->icon_animation == 'push-out-bottom' || $this->settings->icon_animation == 'push-out-right' ) {
					$output .= '<i class="uabb-adv-accordion-button-icon uabb-adv-accordion-close-icon '.$this->settings->close_icon.'"></i>';
					$output .= '<i class="uabb-adv-accordion-button-icon uabb-adv-accordion-open-icon '.$this->settings->open_icon.'"></i>';
				}
				
				$output .= '</div>';
				$output .= '</div>';
			}
			return $output;
		}
		return '';
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBAdvancedAccordionModule', array(
	'items'         => array(
		'title'         => __('Items', 'uabb'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'acc_items'         => array(
						'type'          => 'form',
						'label'         => __('Item', 'uabb'),
						'form'          => 'uabb_advAccordion_items_form', // ID from registered form below
						'preview_text'  => 'acc_title', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			)
		)
	),
	'acc_setting'			=> array(
		'title'		 => __('Settings', 'uabb'),
		'sections'	 => array(
			'panel'		=> array(
				'title'	 	=> __( '', 'uabb' ),
				'fields'	=> array(
					'collapse'   => array(
						'type'          => 'select',
						'label'         => __('Inactive Other Items', 'fl-builder'),
						'default'       => 'yes',
						'options'       => array(
							'yes'             => __('Yes', 'fl-builder'),
							'no'             => __('No', 'fl-builder')
						),
						'help'          => __('Choosing yes will keep only one item open at a time. Choosing no will allow multiple items to be open at the same time.', 'fl-builder'),
						'preview'       => array(
							'type'          => 'none'
						)
					),
					'enable_first'       => array(
						'type'          => 'select',
						'label'         => __('Expand First Item', 'fl-builder'),
						'default'       => 'no',
						'options'       => array(
							'no'             => __('No', 'fl-builder'),
							'yes'             => __('Yes', 'fl-builder')
						),
						'help' 			=> __('Choosing yes will expand the first item by default.', 'fl-builder')
					)
				)
			)
		)
	),
	'acc_title_style'       => array(
		'title'         => __('Title', 'uabb'),
		'sections'      => array(
			'title_style'	=> array(
				'title'         => __('Title Style', 'uabb'),
				'fields'		=> array(
					'title_spacing'		=> array(
						'type'          => 'uabb-spacing',
                        'label'         => __( 'Padding', 'fl-builder' ),
                        'mode'			=> 'padding',
                        'default'       => 'padding: 15px;' // Optional
					),
					'title_margin'     => array(
						'type'          => 'text',
						'label'         => __('Spacing Between Titles', 'fl-builder'),
						'default'       => '',
						'maxlength'     => '2',
						'size'          => '6',
						'description'   => 'px',
					),
					'title_align'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'left'          => __('Left', 'uabb'),
							'right'         => __('Right', 'uabb')
						)
					),
					'title_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Title Color', 'uabb'),
                        )
                    ),
                    'title_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Title Hover Color', 'uabb'),
                        )
                    ),
					'title_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Title Background Color', 'uabb'),
                        )
                    ),
                    'title_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
					
					'title_bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Title Background Hover Color', 'uabb'),
                        )
                    ),
                    'title_bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
				)
			),
			'title_border'	=> array(
				'title'         => __('Title Border', 'uabb'),
				'fields'        => array(
					'title_border_type'   => array(
		                'type'          => 'select',
		                'label'         => __('Type', 'uabb'),
		                'default'       => 'solid',
		                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
		                'options'       => array(
		                    'none'   => _x( 'None', 'Border type.', 'uabb' ),
		                    'solid'  => _x( 'Solid', 'Border type.', 'uabb' ),
		                    'dashed' => _x( 'Dashed', 'Border type.', 'uabb' ),
		                    'dotted' => _x( 'Dotted', 'Border type.', 'uabb' ),
		                    'double' => _x( 'Double', 'Border type.', 'uabb' )
		                ),
		                'toggle'        => array(
		                    ''              => array(
		                        'fields'        => array()
		                    ),
		                    'solid'         => array(
		                        'fields'        => array('title_border_color', 'title_border_top', 'title_border_bottom', 'title_border_left', 'title_border_right' )
		                    ),
		                    'dashed'        => array(
		                        'fields'        => array('title_border_color', 'title_border_top', 'title_border_bottom', 'title_border_left', 'title_border_right' )
		                    ),
		                    'dotted'        => array(
		                        'fields'        => array('title_border_color', 'title_border_top', 'title_border_bottom', 'title_border_left', 'title_border_right' )
		                    ),
		                    'double'        => array(
		                        'fields'        => array('title_border_color', 'title_border_top', 'title_border_bottom', 'title_border_left', 'title_border_right' )
		                    )
		                )
		            ),
		            'title_border_top'    => array(
		                'type'          => 'text',
		                'label'         => __('Top Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'title_border_bottom' => array(
		                'type'          => 'text',
		                'label'         => __('Bottom Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'title_border_left'   => array(
		                'type'          => 'text',
		                'label'         => __('Left Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'title_border_right'  => array(
		                'type'          => 'text',
		                'label'         => __('Right Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'title_border_radius'   => array(
		                'type'          => 'text',
		                'label'         => __('Border Radius', 'uabb'),
		                'default'       => '',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'title_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Color', 'uabb'),
                        )
                    ),
		        )
			),
			'title_icon'       => array(
				'title'         => __('Title Icon', 'uabb'),
				'fields'        => array(
					'close_icon'          => array(
						'type'          => 'icon',
						'label'         => __('Close Icon', 'uabb'),
						'default'		=> 'fa fa-plus'
					),
					'open_icon'          => array(
						'type'          => 'icon',
						'label'         => __('Open Icon', 'uabb'),
						'default'		=> 'fa fa-minus'
					),
					 'icon_size'    => array(
		                'type'          => 'text',
		                'label'         => __('Icon Size', 'uabb'),
		                'default'       => '16',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		            ),
					'icon_position' => array(
						'type'          => 'select',
						'label'         => __('Icon Position', 'uabb'),
						'default'       => 'after',
						'options'       => array(
							'before'        => __('Before Text', 'uabb'),
							'after'         => __('After Text', 'uabb')
						)
					),
					'icon_animation' => array(
						'type'          => 'select',
						'label'         => __('Icon Animation', 'uabb'),
						'default'       => 'none',
						'options'       => array(
							'none'        => __('None', 'uabb'),
							'push-out-top'        => __('Push Out from Top', 'uabb'),
							'push-out-right'        => __('Push Out from Right', 'uabb'),
							'push-out-bottom'        => __('Push Out from Bottom', 'uabb'),
							'push-out-left'        => __('Push Out from Left', 'uabb'),
						)
					),
					'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Icon Color', 'uabb'),
                        )
                    ),
					'icon_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Icon Hover Color', 'uabb'),
                        )
                    ),
				)
			),
		)
	),
	'acc_content_style'     => array(
		'title'         => __('Content', 'uabb'),
		'sections'      => array(
			'content_style'	=> array(
				'title'         => __('Content Style', 'uabb'),
				'fields'	=> array(
					'content_spacing'		=> array(
						'type'          => 'uabb-spacing',
                        'label'         => __( 'Padding', 'fl-builder' ),
                        'mode'			=> 'padding',
                        'default'       => 'padding: 20px;' // Optional
					),
					'content_align'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'left'          => __('Left', 'uabb'),
							'right'         => __('Right', 'uabb')
						)
					),
					'content_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Content Color', 'uabb'),
                        )
                    ),
					'content_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Content Background Color', 'uabb'),
						)
                    ),
                    'content_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
				)
			),
			'content-section'       => array(
				'title'         => __('Content Border', 'uabb'),
				'fields'        => array(
					'content_border_type'   => array(
		                'type'          => 'select',
		                'label'         => __('Type', 'uabb'),
		                'default'       => 'none',
		                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
		                'options'       => array(
		                    'none'   => _x( 'None', 'Border type.', 'uabb' ),
		                    'solid'  => _x( 'Solid', 'Border type.', 'uabb' ),
		                    'dashed' => _x( 'Dashed', 'Border type.', 'uabb' ),
		                    'dotted' => _x( 'Dotted', 'Border type.', 'uabb' ),
		                    'double' => _x( 'Double', 'Border type.', 'uabb' )
		                ),
		                'toggle'        => array(
		                    ''              => array(
		                        'fields'        => array()
		                    ),
		                    'solid'         => array(
		                        'fields'        => array('content_border_color', 'content_border_top', 'content_border_bottom', 'content_border_left', 'content_border_right', 'responsive_border')
		                    ),
		                    'dashed'        => array(
		                        'fields'        => array('content_border_color', 'content_border_top', 'content_border_bottom', 'content_border_left', 'content_border_right', 'responsive_border')
		                    ),
		                    'dotted'        => array(
		                        'fields'        => array('content_border_color', 'content_border_top', 'content_border_bottom', 'content_border_left', 'content_border_right', 'responsive_border')
		                    ),
		                    'double'        => array(
		                        'fields'        => array('content_border_color', 'content_border_top', 'content_border_bottom', 'content_border_left', 'content_border_right', 'responsive_border')
		                    )
		                )
		            ),
		            'content_border_top'    => array(
		                'type'          => 'text',
		                'label'         => __('Top Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'content_border_bottom' => array(
		                'type'          => 'text',
		                'label'         => __('Bottom Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'content_border_left'   => array(
		                'type'          => 'text',
		                'label'         => __('Left Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'content_border_right'  => array(
		                'type'          => 'text',
		                'label'         => __('Right Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		             'content_border_radius'   => array(
		                'type'          => 'text',
		                'label'         => __('Border Radius', 'uabb'),
		                'default'       => '',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '5',
		                'placeholder'   => '0'
		            ),
		            'content_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
		                	'label'         => __('Color', 'uabb'),
			                'preview'       => array(
			                    'type'          => 'css',
			                    'selector'		=> 'uabb-acc-content',
			                    'property'      => 'border-color'
			                )
                        )
                    ),
				)
			)
		)
	),
	'acc_typography'		=> array(
		'title'		=> __('Typography', 'uabb'),
		'sections'	=> array(
			'title_typogrphy'	=> BB_Ultimate_Addon::uabb_section_get( 'typography',
                                array(
                                    'title'     => __('Title', 'uabb' ) ,
                                    'fields'   => array(
                                        /*'color'   => array(
                                            'label' => __('Choose Color', 'uabb'),
                                        ),*/
                                        'tag_selection'   => array(
                                            'default' => 'h4',
                                        ),
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-adv-accordion-button-label'
                                            )
                                        ),
                                    )
                                ),
                                array('color')
            ),
			'content_typogrphy'	=> BB_Ultimate_Addon::uabb_section_get( 'typography',
                                array(
                                    'title'     => __('Content', 'uabb' ) ,
                                    'fields'   => array(
                                        /*'color'   => array(
                                            'label' => __('Choose Color', 'uabb'),
                                        ),
                                        'tag_selection'   => array(
                                            'label' => __('Select Tag', 'uabb'),
                                        ),*/
                                        'font_family' => array(
                                            'preview'         => array(
                                                'type'            => 'font',
                                                'selector'        => '.uabb-adv-accordion-content, .uabb-adv-accordion-content *'
                                            )
                                        ),
                                    )
                                ),
                                array('color', 'tag_selection'),
                                'content'
            ),
		)
	)
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('uabb_advAccordion_items_form', array(
	'title' => __('Add Item', 'uabb'),
	'tabs'  => array(
		'general'      => array(
			'title'         => __('General', 'uabb'),
			'sections'      => array(
				'general'       => array(
					'title'         => '',
					'fields'        => array(
						'acc_title'         => array(
							'type'          => 'text',
							'label'         => __('Title', 'uabb')
						)
					)
				),
				'content'       => array(
					'title'         => __('Content', 'uabb'),
					'fields'        => array(
						'acc_content'       => array(
							'type'          => 'editor',
							'label'         => ''
						)
					)
				)
			)
		)
	)
));