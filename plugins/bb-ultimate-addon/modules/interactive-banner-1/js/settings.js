(function($){

	FLBuilder.registerModuleHelper('interactive-banner-1', {
		
		rules: {
			banner_image: {
				required: true
			}
		},
        init: function()
        {
            var a = $('.fl-builder-interactive-banner-1-settings').find('.fl-builder-settings-tabs a');
            a.on('click', this._toggleHoverAndTypographyTabs);
            $( '.fl-builder-content' ).on( 'fl-builder.layout-rendered', this._toggleAfterRender );
        },

        _toggleHoverAndTypographyTabs: function() {
            var anchorHref = $(this).attr('href');
            var node = jQuery(this).closest( 'form' ).attr( 'data-node' );
            if( anchorHref == '#fl-builder-settings-tab-hover' || anchorHref == '#fl-builder-settings-tab-typography' ){
                jQuery('.fl-node-' + node + ' .uabb-ib1-block').addClass('uabb-ib1-hover');
            } else {
                jQuery('.fl-node-' + node + ' .uabb-ib1-block').removeClass('uabb-ib1-hover');
            }
        },

        _toggleAfterRender: function() {
            
            var anchorHref = jQuery( '.fl-builder-settings-tabs' ).children('.fl-active').attr( 'href' );
            var node = jQuery( '.fl-builder-settings-tabs a' ).closest( 'form' ).attr( 'data-node' );
            if( anchorHref == '#fl-builder-settings-tab-hover' || anchorHref == '#fl-builder-settings-tab-typography' ){
                jQuery('.fl-node-' + node + ' .uabb-ib1-block').addClass('uabb-ib1-hover');
            } else {
                jQuery('.fl-node-' + node + ' .uabb-ib1-block').removeClass('uabb-ib1-hover');
            }
        },
	});

})(jQuery);