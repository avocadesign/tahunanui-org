jQuery(document).ready(function() {
	jQuery('.uabb-ib1-block').each(function(index, value){
		if( jQuery( this ).hasClass( "uabb-banner-block-custom-height" ) ) {
			var heading_ht = jQuery(this).find('.uabb-ib1-title').outerHeight();
			var custom_ht = jQuery(this).outerHeight();
			var ht = ( custom_ht - heading_ht );
			jQuery(this).find(".uabb-background").css('height', ht);
		}
	});
});