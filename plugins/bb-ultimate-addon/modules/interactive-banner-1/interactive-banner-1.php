<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class InteractiveBanner1Module
 */
class InteractiveBanner1Module extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Interactive Banner 1', 'uabb'),
            'description'   => __('An basic example for coding new modules.', 'uabb'),
            'category'		=> __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/interactive-banner-1/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/interactive-banner-1/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
        $this->add_css( 'font-awesome' );
    }
    

    /**
     * @method render_button
     */
    public function render_button() {
        if( $this->settings->show_button != 'no' ) {
            if( $this->settings->button != '' ) {
                echo '<div class="uabb-ib1-button-outter">';
                FLBuilder::render_module_html( 'uabb-button', $this->settings->button );
                echo '</div>';
            }
        }
    }

    /**
     * @method render_icon
     */
    public function render_icon() {
        if( $this->settings->icon != '' ) {
            $imageicon_array = array(
 
                /* General Section */
                'image_type' => 'icon',

                /* Icon Basics */
                'icon' => $this->settings->icon,
                'icon_size' => $this->settings->icon_size,
                'icon_align' => '',

                /* Image Basics */
                'photo_source' => '',
                'photo' => '',
                'photo_url' => '',
                'img_size' => '',
                'img_align' => '',
                'photo_src' => '' ,

                /* Icon Style */
                'icon_style' => '',
                'icon_bg_size' => '',
                'icon_border_style' => '',
                'icon_border_width' => '',
                'icon_bg_border_radius' => '',

                /* Image Style */
                'image_style' => '',
                'img_bg_size' => '',
                'img_border_style' => '',
                'img_border_width' => '',
                'img_bg_border_radius' => '',
            ); 
            /* Render HTML Function */
            FLBuilder::render_module_html( 'image-icon', $imageicon_array );
        }
    }
}



/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('InteractiveBanner1Module', array(
    'general'       => array( // Tab
        'title'         => __('General', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Title', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_title'     => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'uabb'),
                        'default'       => 'Interactive Banner',
                    ),
                    'banner_title_location'    => array(
                        'type'          => 'select',
                        'label'         => __('Title Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'left'      => __('Left', 'uabb'),
                            'right'      => __('Right', 'uabb'),
                            'center'      => __('Center', 'uabb'),
                        )
                    ),
                )
            ),
            'style'       => array( // Section
                'title'         => __('Style', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_style'     => array(
                        'type'          => 'select',
                        'label'         => __('Banner Style', 'uabb'),
                        'default'       => 'style1',
                        'help'          => 'Select appear effect for description text.',
                        'options'       => array(
                            'style01'     => __('Appear From Bottom', 'uabb'),
                            'style02'     => __('Appear From Top', 'uabb'),
                            'style03'     => __('Appear From Left', 'uabb'),
                            'style04'     => __('Appear From Right', 'uabb'),
                            'style11'     => __('Zoom In', 'uabb'),
                            'style12'     => __('Zoom Out', 'uabb'),
                            'style13'     => __('Zoom In-Out', 'uabb'),
                            'style21'     => __('Jump From Left', 'uabb'),
                            'style22'     => __('Jump From Right', 'uabb'),
                            'style31'     => __('Pull From Bottom', 'uabb'),
                            'style32'     => __('Pull From Top', 'uabb'),
                            'style33'     => __('Pull From Left', 'uabb'),
                            'style34'     => __('Pull From Right', 'uabb'),
                        ),
                    ),
                    'banner_image' => array(
                        'type'          => 'photo',
                        'label'         => __('Banner Image', 'uabb')
                    ),
                    'banner_height_options'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Banner Height', 'fl-builder' ),
                        'default'       => 'default',
                        'help'          => 'Control your banner height, by default - it depends on selected image size.',
                        'options'       => array(
                            'default'      => 'Default',
                            'custom'        => 'Custom',
                        ),
                        'toggle' => array(
                            'custom' => array(
                                'fields' => array( 'banner_height' ),
                            )
                        ),
                    ),
                    'banner_height'     => array(
                        'type'          => 'text',
                        'label'         => __('Custom Banner Height', 'uabb'),
                        'default'       => '',
                        'size'          => '8',
                        'description'   => 'px',
                    ),
                    'vertical_align'         => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Vertical Center', 'uabb'),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'        => __('Yes', 'uabb'),
                            'no'       => __('No', 'uabb')
                        )
                    ),
                )
            ),
        )
    ),
    'hover'       => array( // Tab
        'title'         => __('Hover', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'description'       => array( // Section
                'title'         => __('Description', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_desc'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-ib1-description',
                        )
                    ),
                    'overlay_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Overlay Color', 'uabb'),
                            'default'       => '#808080',
                            'show_reset'    => true,
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-background',
                                'property'        => 'background'
                            )
                        )
                    ),
                    'overlay_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),
            'icon'       => array( // Section
                'title'         => __('Icon Above Description', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'icon' => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'uabb'),
                        'show_remove'   => true,
                    ),
                    'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'     => __('Icon Color', 'uabb'),
                        )
                    ),
                    'icon_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Size', 'uabb'),
                        'default'       => '30',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px',
                    ),
                )
            ),
            'link'       => array( // Section
                'title'         => __('Call To Action Below Description', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'show_button'         => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __('Show Button', 'uabb'),
                        'default'       => 'no',
                        'options'       => array(
                            'no'          => __('No', 'uabb'),
                            'yes'         => __('Yes', 'uabb'),
                        ),
                        'toggle' => array(
                            'yes' => array(
                                'fields' => array( 'button' ),
                            )
                        ),
                    ),
                    'button' => array(
                        'type'          => 'form',
                        'label'         => __('Button Settings', 'uabb'),
                        'form'          => 'button_form_field', // ID of a registered form.
                        'preview_text'  => 'text', // ID of a field to use for the preview text.
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Title Typography', 'uabb' ),
                    'fields' => array(
                        'color' => array(
                            'preview' => array(
                                'type' => 'css',
                                'property' => 'color',
                                'selector' => '.uabb-ib1-title',
                            ),
                        ),
                        'font_size' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-ib1-title',
                                'property'        => 'font-size',
                                'unit'            => 'px'
                            )
                        ),
                        'line_height' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-ib1-title',
                                'property'        => 'line-height',
                                'unit'            => 'px'
                            )
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-ib1-title'
                            )
                        ),
                        'tag_selection'   => array(
                            'default'       => 'h3',
                        ),
                        'title_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Title Background Color', 'uabb'),
                            )
                        ),
                        'title_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    ),
                ),
                array(),
                'title_typography'
            ),
            'desc_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Description Typography', 'uabb' ),
                    'fields' => array(
                        'color' => array(
                            'label' => __('Description Text Color', 'uabb'),
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-ib1-description',
                                'property'        => 'color'
                            )
                        ),
                        'font_size' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-ib1-description',
                                'property'        => 'font-size',
                                'unit'            => 'px'
                            )
                        ),
                        'line_height' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-ib1-description',
                                'property'        => 'line-height',
                                'unit'            => 'px'
                            )
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-ib1-description'
                            )
                        ),
                    ),
                ),
                array( 'tag_selection' ),
                'desc_typography'
            ),
        )
    ),
));
