<?php

class UABBSpacerGap extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Spacer / Gap', 'fl-builder' ),
            'description'     => __( 'A totally awesome module!', 'fl-builder' ),
            'category'        => __('Ultimate Addons', 'fl-builder'),
            'dir'             => BB_ULTIMATE_ADDON_DIR . 'modules/spacer-gap/',
            'url'             => BB_ULTIMATE_ADDON_URL . 'modules/spacer-gap/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module('UABBSpacerGap', array(
    'spacer_gap_general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'spacer_gap_general'       => array( // Section
                'title'         => __('', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'desktop_space'   => array(
                        'type'          => 'text',
                        'label'         => __('Desktop', 'fl-builder'),
                        'default'       => '10',
                        'size'          => '8',
                        'placeholder'   => '10',
                        'class'         => 'uabb-spacer-gap-desktop',
                        'description'   => 'px',
                        'help'          => __( 'This value will work for all devices.' )
                    ),
                    'medium_device'   => array(
                        'type'          => 'text',
                        'label'         => __('Medium Device ( Tabs )', 'fl-builder'),
                        'default'       => '10',
                        'size'          => '8',
                        'placeholder'   => '10',
                        'class'         => 'uabb-spacer-gap-tab-landscape',
                        'description'   => 'px',
                        //'help'          => __( 'This value will work below 1200px screen width.' )
                    ),

                    'small_device'   => array(
                        'type'          => 'text',
                        'label'         => __('Small Device ( Mobile )', 'fl-builder'),
                        'default'       => '10',
                        'size'          => '8',
                        'placeholder'   => '10',
                        'class'         => 'uabb-spacer-gap-tab-landscape',
                        'description'   => 'px',
                        //'help'          => __( 'This value will work below 1200px screen width.' )
                    ),
                )
            )
        )
    )
));