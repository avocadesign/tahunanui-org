<?php

/**
 * @class AdvancedSeparatorModule
 */
class AdvancedSeparatorModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Advanced Separator', 'uabb'),
			'description'   	=> __('A divider line to separate content.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/advanced-separator/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/advanced-separator/',
            'editor_export' 	=> false,
			'partial_refresh'	=> true
		));
	}

	/**
	 * @method render_image
	 */
	public function render_image()
	{
		if( $this->settings->separator == 'line_image' || $this->settings->separator == 'line_icon' ) {
			$imageicon_array = array(
	 
				/* General Section */
				'image_type' => ( $this->settings->separator == 'line_image' ) ? 'photo' : ( ( $this->settings->separator == 'line_icon' ) ? 'icon' : '' ),

				/* Icon Basics */
				'icon' => $this->settings->icon,
				'icon_size' => $this->settings->icon_size,
				'icon_align' => 'center',//$this->settings->icon_align,

				/* Image Basics */
				'photo_source' => $this->settings->photo_source,
				'photo' => $this->settings->photo,
				'photo_url' => $this->settings->photo_url,
				'img_size' => $this->settings->img_size,
				'img_align' => 'center',//$this->settings->img_align,
				'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,

				/* Icon Style */
				'icon_style' => $this->settings->icon_style,
				'icon_bg_size' => $this->settings->icon_bg_size,
				'icon_border_style' => $this->settings->icon_border_style,
				'icon_border_width' => $this->settings->icon_border_width,
				'icon_bg_border_radius' => $this->settings->icon_bg_border_radius,

				/* Image Style */
				'image_style' => $this->settings->image_style,
				'img_bg_size' => $this->settings->img_bg_size,
				'img_border_style' => $this->settings->img_border_style,
				'img_border_width' => $this->settings->img_border_width,
				'img_bg_border_radius' => $this->settings->img_bg_border_radius,
			); 
			/* Render HTML Function */
			if( $this->settings->separator == 'line_image' ) {
				echo '<div class="uabb-image-outter-wrap">';
			}
			FLBuilder::render_module_html( 'image-icon', $imageicon_array );
			if( $this->settings->separator == 'line_image' ) {
				echo '</div>';
			}
		}
	}
}

	


/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('AdvancedSeparatorModule', array(
	'general'       => array( // Tab
		'title'         => __('General', 'uabb'), // Tab title
		'sections'      => array( // Tab Sections
			'separator'		=> array(
				'title'         => '', // Section Title
				'fields'		=>array(
					'separator'	=> array(
							'type'          => 'select',
							'label'         => __('Separator Style', 'uabb'),
							'default'       => 'line',
							'options'       => array(
								'line'      => _x( 'Line', 'uabb' ),
								'line_icon'        => _x( 'Line With Icon', 'uabb' ),
								'line_image'       => _x( 'Line With Image', 'uabb' ),
								'line_text'		   => _x( 'Line With Text', 'uabb' ),
							),
							'help'          => __('Choose Separator Style.', 'uabb'),
							'toggle'		=> array(
								'line'		=> array(
									'fields' => array(),
									'sections' => array( 'line_design' ),
									'tabs'		=> array(),
								),
						    	'line_icon'		=> array(
						    		'fields' => array( 'icon_photo_position', 'icon_spacing' ),
						    		'sections'		=> array( 'icon_basic',  'icon_style', 'icon_colors' ),
						    		'tabs'		=> array( 'design' )
						    	),
						    	'line_image'	=> array(
						    		'fields' => array( 'icon_photo_position', 'icon_spacing' ),
						    		'sections'		=> array( 'img_basic', 'img_style', 'img_colors' ),
						    		'tabs'		=> array( 'design' ),
						    	),
						    	'line_text'	=> array(
						    		'fields' => array( 'icon_photo_position', 'icon_spacing' ),
						    		'sections'		=> array( 'text', 'text_typography' ),
						    		'tabs'		=> array( 'design' ),
						    	),
						    ),
					),
				),
			),
			'line_design'	=>array(
				'title'         => __('Line Style', 'uabb'), // Section Title
				'fields'        => array( // Section Fields
					'line_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Color', 'uabb'),
							'default'       => '#dadada',
							'preview'       => array(
								'type'          => 'css',
								'selector'      => '.uabb-separator, .uabb-separator-line > span',
								'property'      => 'border-top-color'
							)
		                )
                    ),
					'line_height'        => array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'default'       => '1',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator, .uabb-separator-line > span',
							'property'      => 'border-top-width',
							'unit'          => 'px'
						),
						'help'			=> __( 'Thickness of Border', 'uabb' )
					),
					'line_width'        => array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%'
					),
					'line_alignment'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'center',
						'options'       => array(
							'center'      => _x( 'Center', 'uabb' ),
							'left'        => _x( 'Left', 'uabb' ),
							'right'       => _x( 'Right', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator-parent',
							'property'      => 'text-align'
						),
						'help'          => __('Align the border.', 'uabb'),
					),
					'line_style'         => array(
						'type'          => 'select',
						'label'         => __('Style', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'solid'         => _x( 'Solid', 'Border type.', 'uabb' ),
							'dashed'        => _x( 'Dashed', 'Border type.', 'uabb' ),
							'dotted'        => _x( 'Dotted', 'Border type.', 'uabb' ),
							'double'        => _x( 'Double', 'Border type.', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator, .uabb-separator-line > span',
							'property'      => 'border-top-style'
						),
						'help'          => __('The type of border to use. Double borders must have a height of at least 3px to render properly.', 'uabb'),
					)
				)
			),
			'icon_basic' 	=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_basic', array(), 
									array( 'icon_align' )
								),
			'img_basic' 	=> BB_Ultimate_Addon::uabb_section_get( 'img_basic',
				array(
					'fields' => array(
						'img_size' => array(
							'default' => '50'
						)
					)
				),
				array( 'img_align' )
			),
			'icon_style'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_style' ),
			'img_style'		=> BB_Ultimate_Addon::uabb_section_get( 'img_style' ),
			'icon_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
			'text'			=> array(
				'title'			=> __('Text', 'uabb'),
				'fields'		=> array(
					'text_inline'        => array(
						'type'          => 'text',
						'label'         => __('Text', 'uabb'),
						'default'       => 'Ultimate',
						'preview'       => array(
							'type'          => 'text',
							'selector'      => '.uabb-divider-text',
						)
					),
				)
			),
			'text_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __( 'Text Typography', 'uabb' ),
                    'fields' => array(
                    	'color' => array(
                    		'label' => 'Text Color'
                    	),
                    	'tag_selection' => array(
                    		'default' => 'h3'
                    	),
                    	'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-divider-text'
                            )
                        ),
                        'font_size' => array(
                            'preview'       => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-divider-text',
                                'property'	=> 'font-size'
                            )
                        ),
                    )
                ), 
                array(),
                'text'
            ),
		)
	),
	'design'		=> array(
		'title'		=> __('Style', 'uabb'), //tab title
		'sections'		=> array(
			'design'	=>array(
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'icon_photo_position'	=> array(
						'type'          => 'text',
						'label'         => __('Position', 'uabb'),
						'help'			=> __('Adjust the position of Icon / Image / Text.
0% for very left & 100% for very right.', 'uabb'),
						'default'       => '50',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%',
					),
					'icon_spacing'	=> array(
						'type'          => 'text',
						'label'         => __('Spacing', 'uabb'),
						'help'         => __('Adjust the spacing between separator line edges & your Icon / Image / Text.', 'uabb'),
						'default'       => '10',
						'maxlength'     => '2',
						'size'          => '5',
						'description'   => 'px',
					),
					'height'        => array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'default'       => '1',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator, .uabb-separator-line > span',
							'property'      => 'border-top-width',
							'unit'          => 'px'
						),
						'help'			=> __( 'Thickness of Border', 'uabb' )
					),
					'width'        	=> array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%'
					),
					'color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
							'label'         => __('Color', 'uabb'),
							'default'       => '#dadada',
							'preview'       => array(
								'type'          => 'css',
								'selector'      => '.uabb-separator, .uabb-separator-line > span',
								'property'      => 'border-top-color'
							)
		                )
                    ),
					'alignment'		=> array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'center',
						'options'       => array(
							'center'      => _x( 'Center', 'uabb' ),
							'left'        => _x( 'Left', 'uabb' ),
							'right'       => _x( 'Right', 'uabb' )
						),
						'help'          => __('Align the border.', 'uabb'),
					),
					'style'         => array(
						'type'          => 'select',
						'label'         => __('Seperator Style', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'solid'         => _x( 'Solid', 'Border type.', 'uabb' ),
							'dashed'        => _x( 'Dashed', 'Border type.', 'uabb' ),
							'dotted'        => _x( 'Dotted', 'Border type.', 'uabb' ),
							'double'        => _x( 'Double', 'Border type.', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator, .uabb-separator-line > span',
							'property'      => 'border-top-style'
						),
						'help'          => __('The type of border to use. Double borders must have a height of at least 3px to render properly.', 'uabb'),
					),
					
				)
			),
		)
	),
));
