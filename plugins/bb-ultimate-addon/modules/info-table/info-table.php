<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class UABBInfoTableModule
 */
class UABBInfoTableModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'              => __('Info Table', 'uabb'),
            'description'       => __('A basic info table.', 'uabb'),
            'category'          => __('Ultimate Addons', 'uabb'),
            'dir'               => BB_ULTIMATE_ADDON_DIR . 'modules/info-table/',
            'url'               => BB_ULTIMATE_ADDON_URL . 'modules/info-table/',
            'editor_export'     => true, // Defaults to true and can be omitted.
            'enabled'           => true, // Defaults to true and can be omitted.
            'partial_refresh'   => true, // Defaults to false and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBInfoTableModule', array(
    'general'       => array(
        'title'         => __( 'General', 'uabb' ),
        'sections'      => array(
            'title-section'  => array(
                'title'            => __( 'Info-Table', 'uabb' ),
                'fields'        => array(
                    'it_title'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Heading', 'uabb' ),
                        'default'       => 'Heading',
                        'help'          => 'Enter Info-Table Title',
                    ),
                    'sub_heading'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Sub Heading', 'uabb' ),
                        'default'       => 'Sub Heading',
                    ),
                    'it_long_desc'     => array(
                        'type'          => 'editor',
                        'label'         => __( '', 'uabb' ),
                        'default'       => 'Enter description text here.',
                    ),
                    'it_link_type'          => array(
                        'type'          => 'select',
                        'label'         => __( 'Add Link', 'uabb' ),  
                        'default'       => 'no',
                        'options'       => array(
                            'no'        => __( 'No Link', 'uabb' ),
                            'cta'       => __( 'Call to Action Button', 'uabb' ),
                            'complete_link'       => __( 'Link to Complete Box', 'uabb' ),
                        ),
                        'toggle'        => array(
                            'cta'       => array(
                                'sections'  => array( 'btn-style-section', 'btn_typography' ),
                                'fields'    => array( 'button_text', 'it_link' )
                            ),
                            'complete_link'       => array(
                                'fields'    => array( 'it_link' )
                            ),
                        ),
                    ),
                    'button_text'   => array(
                        'type'      => 'text',
                        'label'     => __( 'Call to action button text', 'uabb' ),
                    ),
                    'it_link'       => array(
                        'type'      => 'link',
                        'label'     => __( 'Select URL', 'uabb' )
                    ),
                )
            ),
        )
    ),
    'style'       => array(
        'title'         => __( 'Style', 'uabb' ),
        'sections'      => array(
            'style-section'  => array(
                'title'            => __( 'Styles', 'uabb' ),
                'fields'        => array(
                    'box_design'          => array(
                        'type'          => 'select',
                        'label'         => __( 'Select Design Style', 'uabb' ),  
                        'default'       => 'design01',
                        'options'       => array(
                            'design01'        => __( 'Design 01', 'uabb' ),
                            'design02'        => __( 'Design 02', 'uabb' ),
                            'design03'        => __( 'Design 03', 'uabb' ),
                            'design04'        => __( 'Design 04', 'uabb' ),
                            'design05'        => __( 'Design 05', 'uabb' ),
                            'design06'        => __( 'Design 06', 'uabb' ),
                        ),
                    ),
                    'color_scheme'          => array(
                        'type'          => 'select',
                        'label'         => __( 'Select Color Scheme', 'uabb' ),  
                        'default'       => 'black',
                        'options'       => array(
                            'black'        => __( 'Black', 'uabb' ),
                            'red'        => __( 'Red', 'uabb' ),
                            'blue'        => __( 'Blue', 'uabb' ),
                            'yellow'        => __( 'Yellow', 'uabb' ),
                            'green'        => __( 'Green', 'uabb' ),
                            'gray'        => __( 'Gray', 'uabb' ),
                            'custom'        => __( 'Design Your Own', 'uabb' ),
                        ),
                        'toggle'        => array(
                            'custom'       => array(
                                'fields'    => array( 'desc_back_color', 'desc_back_color_opc' )
                            ),
                        )
                    ),
                    'heading_back_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Main background Color', 'uabb'),
                        )
                    ),
                    'heading_back_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),

                    'desc_back_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Highlight background Color', 'uabb'),
                        )
                    ),
                    'desc_back_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                    'hover_effect'          => array(
                        'type'          => 'select',
                        'label'         => __( 'Hover Effect', 'uabb' ),  
                        'default'       => 'shadow',
                        'options'       => array(
                            'none'        => __( 'None', 'uabb' ),
                            'shadow'      => __( 'Box Shadow', 'uabb' ),
                        ),
                    ),
                    'min_height'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Min Height', 'uabb' ),  
                        'default'       => '',
                        'maxlength'     => '5',
                        'size'          => '6',
                        'description'   => 'px',
                    ),
                )
            ),
            'btn-style-section'  => array(
                'title'            => __( 'Button Style', 'uabb' ),
                'fields'        => array(

                    'btn_text_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        'label'         => __('Text Color', 'uabb'),
                        'default'       => '',
                        ) 
                    ),

                    'btn_text_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        'label'         => __('Text Hover Color', 'uabb'),
                        'default'       => '',
                        'preview'       => array(
                                'type'          => 'none'
                            )
                        ) 
                    ),
                    'btn_bg_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        'label'         => __('Background Color', 'uabb'),
                        'default'       => '',
                        )
                    ),
                    'btn_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

                    'btn_bg_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        'label'         => __('Background Hover Color', 'uabb'),
                        'default'       => '',
                        'preview'       => array(
                                'type'          => 'none'
                            )
                        ) 
                    ),
                    'btn_bg_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                )
            ),
        )
    ),

    'it_image_icon' => array(
        'title'         => __('Image / Icon', 'uabb'),
        'sections'      => array(
            'type_general'      => BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' , 
                                array(
                                    'fields'        => array( // Section Fields
                                        'image_type'    => array(
                                            'default'       => 'icon',
                                        ),
                                    )
                                )
            ),
            'icon_basic'    =>  BB_Ultimate_Addon::uabb_section_get( 'icon_basic',
                array( 'fields' => array(
                        'icon'          => array(
                            'default'       => 'fa fa-child'
                        ),
                        'icon_size'     => array(
                            'default'       => '75',
                            'preview'   => array(
                                'type'      => 'refresh',
                            ),
                        ),
                    )
                ),
                array( 'icon_align' ) 
            ),
            
            /* Image Basic Setting */
            'img_basic'     =>  BB_Ultimate_Addon::uabb_section_get( 'img_basic',
                array( 'fields' => array(
                        'img_size'  => array(
                            'default'       => '150',
                            'preview'   => array(
                                'type'      => 'refresh',
                            ),
                        ),
                        'img_align'         => array(
                            'default'       => 'inherit',
                            'options'       => array(
                                'inherit'       => __('Default', 'uabb'),
                            ),
                        ),
                    )
                ),
                array( 'img_align' ) 
            ),
            'icon_style'    =>  BB_Ultimate_Addon::uabb_section_get( 'icon_style',
                array( 'fields' => array(
                        'icon_border_width' => array(
                            'preview'   => array(
                                'type'      => 'refresh',
                            ),
                        ),
                    )
                )
            ),
            'img_style'     =>  BB_Ultimate_Addon::uabb_section_get( 'img_style',
                array(
                    'fields' => array(
                        'img_bg_size'   => array(
                            'preview'   => array(
                                'type'      => 'refresh',
                            ),
                        ),
                        'img_border_width'  => array(
                            'preview'   => array(
                                'type'      => 'refresh',
                            ),
                        ),
                    )
                )
            ),
            'icon_colors'   => BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
            'img_colors'    => BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
        )
    ),
    /*'it_image_icon' => BB_Ultimate_Addon::uabb_object_get( 'image-icon' ),*/
    'typography'         => array(
        'title'         => __('Typography', 'uabb'),
        'sections'      => array(
            'heading_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __('Heading', 'uabb' ),
                    'fields'   => array(
                        'tag_selection' => array(
                            'label'     => __( 'Select Tag', 'uabb' ),
                            'default'   => 'h3'
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.info-table-main-heading'
                            )
                        ),
                        'color' => array(
                            'label'     => __( 'Color', 'uabb' )
                        ),
                    )
                ), 
                array(),
                'heading'
            ),
            'sub_heading_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __('Sub Heading', 'uabb' ),
                    'fields'   => array(
                        'tag_selection' => array(
                            'label'     => __( 'Select Tag', 'uabb' ),
                            'default'   => 'h5'
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.info-table-sub-heading'
                            )
                        ),
                        'color' => array(
                            'label'     => __( 'Color', 'uabb' )
                        ),
                    )
                ), 
                array(),
                'sub_heading'
            ),
            'description_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __('Description', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.info-table-description *'
                            )
                        ),
                        'color' => array(
                            'label'     => __( 'Color', 'uabb' )
                        ),
                    )
                ),
                array( 'tag_selection' ),
                'description'
            ),
            'btn_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __('Button', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.info-table-button a'
                            )
                        ),
                        'color' => array(
                            'label'     => __( 'Color', 'uabb' )
                        ),
                    )
                ),
                array( 'color', 'tag_selection' ),
                'btn'
            ),
        )
    )
));
