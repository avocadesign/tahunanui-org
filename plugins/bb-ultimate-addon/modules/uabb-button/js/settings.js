(function($){
	FLBuilder.registerModuleHelper('uabb-button', {

		rules: {
			text: {
				required: true
			},
			link: {
				required: true
			},
			border_size: {
				required: true,
				number: true
			}
		},

		init: function()
		{
			var form        = $('.fl-builder-settings'),
				btn_style   = form.find('select[name=style]'),
				btn_style_opt   = form.find('select[name=flat_button_options]');

			
			// Init validation events.
			this._btn_styleChanged();
			
			// Validation events.
			btn_style.on('change', this._btn_styleChanged);
			btn_style_opt.on('change', this._btn_styleChanged);
		},

		_btn_styleChanged: function()
		{
			var form        = $('.fl-builder-settings'),
				btn_style   = form.find('select[name=style]').val(),
				btn_style_opt   = form.find('select[name=flat_button_options]').val(),
				icon       = form.find('input[name=icon]');
				
			icon.rules('remove');
			
			if(btn_style == 'flat' && btn_style_opt != 'none' ) {
				icon.rules('add', { required: true });
			}
		},
	});

})(jQuery);