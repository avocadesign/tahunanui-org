<?php

/**
 * @class UABBAdvancedIconModule
 */
class UABBAdvancedIconModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Advanced Icon', 'fl-builder'),
			'description'   	=> __('Display a group of Image / Icons.', 'fl-builder'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'editor_export' 	=> false,
			'partial_refresh'	=> true
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBAdvancedIconModule', array(
	'advimgicons'         => array(
		'title'         => __('Image / Icon', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'icons'         => array(
						'type'          => 'form',
						'label'         => __('Icon', 'fl-builder'),
						'form'          => 'icon_group_form', // ID from registered form below
						'preview_text'  => 'image_type', // Name of a field to use for the preview text
						'multiple'      => true
					)
				)
			)
		)
	),
	'style'         => array( // Tab
		'title'         => __('Style', 'fl-builder'), // Tab title
		'sections'      => array( // Tab Sections
			'structure'     => array( // Section
				'title'         => __('Structure', 'fl-builder'), // Section Title
				'fields'        => array( // Section Fields
					'icon_struc_align'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Icons Structure', 'fl-builder' ),
                        'default'       => 'horizontal',
                        'options'       => array(
                         	'horizontal'		=> 'Horizontal',
                          	'vertical'		=> 'Vertical',
                        ),
                        'width'			=> '70px',
                    ),
					'size'          => array(
						'type'          => 'text',
						'label'         => __('Size', 'fl-builder'),
						'default'       => '30',
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px'
					),
					'icoimage_style'         => array(
                    	'type'          => 'select',
                    	'label'         => __('Image / Icon Background Style', 'uabb'),
                    	'default'       => 'simple',
                    	'options'       => array(
                        	'simple'        => __('Simple', 'uabb'),
                        	'circle'          => __('Circle Background', 'uabb'),
                        	'square'         => __('Square Background', 'uabb'),
                        	'custom'         => __('Design your own', 'uabb'),
                    	),
                        'toggle' => array(
                        	'circle' => array(
                                'fields' => array( /*'icon_color_preset',*/ 'color_preset', 'bg_color', 'bg_color_opc', 'bg_hover_color', 'bg_hover_color_opc', 'three_d' ),
                            ),
                            'square' => array(
                                'fields' => array( 'color_preset', 'bg_color', 'bg_color_opc', 'bg_hover_color', 'bg_hover_color_opc', 'three_d' ),
                            ),
                            'custom' => array(
                                'fields' => array( 'color_preset', 'border_style', 'bg_color', 'bg_color_opc', 'bg_hover_color', 'bg_hover_color_opc', 'three_d', 'bg_size', 'bg_border_radius' ),
                            )
                        ),
                	),
                	'bg_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'uabb'),
                        'default'       => '',
                        'help'          => 'Spacing between Icon / Photo & Background edge',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'description'   => 'px'
                    ),
					'border_style'   => array(
		                'type'          => 'select',
		                'label'         => __('Border Style', 'uabb'),
		                'default'       => 'none',
		                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
		                'options'       => array(
		                    'none'   => __( 'None', 'Border type.', 'uabb' ),
		                    'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
		                    'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
		                    'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
		                    'double' => __( 'Double', 'Border type.', 'uabb' )
		                ),
		                'toggle'        => array(
		                    'solid'         => array(
		                        'fields'        => array('border_width', 'border_color','border_hover_color' )
		                    ),
		                    'dashed'        => array(
		                        'fields'        => array('border_width', 'border_color','border_hover_color' )
		                    ),
		                    'dotted'        => array(
		                        'fields'        => array('border_width', 'border_color','border_hover_color' )
		                    ),
		                    'double'        => array(
		                        'fields'        => array('border_width', 'border_color','border_hover_color' )
		                    )
		                ),
		            ),
		            'border_width'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',
		                'preview'		=> array(
                        		'type' 		=> 'css',
                        		'selector'	=> '.uabb-icon i',
                        		'property'	=> 'border-width',
                        		'unit'		=> 'px'
                        )
		            ),
		            'bg_border_radius'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Radius', 'uabb'),
		                'default'		=> '0',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',
		                'preview'		=> array(
                        		'type' 		=> 'css',
                        		'selector'	=> '.uabb-icon i',
                        		'property'	=> 'border-radius',
                        		'unit'		=> 'px'
                        )
		            ),
					'spacing'       => array(
						'type'          => 'text',
						'label'         => __('Spacing', 'fl-builder'),
						'default'       => '10',
						'maxlength'     => '2',
						'size'          => '4',
						'description'   => 'px'
					),
					'align'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'fl-builder'),
						'default'       => 'center',
						'options'       => array(
							'center'        => __('Center', 'fl-builder'),
							'left'          => __('Left', 'fl-builder'),
							'right'         => __('Right', 'fl-builder')
						)
					)
				)
			),
			'colors'        => array( // Section
				'title'         => __('Colors', 'fl-builder'), // Section Title
				'fields'        => array( // Section Fields
					 'color_preset'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Icon Color Presets', 'fl-builder' ),
                        'default'       => 'preset1',
                        'options'       => array(
                         	'preset1'		=> 'Preset 1',
                          	'preset2'		=> 'Preset 2',
                        ),
                        'help'			=> __('Preset 1 => Icon : White, Background : Theme </br>Preset 2 => Icon : Theme, Background : #f3f3f3', 'uabb')
                    ),
					 'color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'     => __('Color', 'uabb'),
		                )
		            ),
		            'hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Hover Color', 'uabb'),
		                    'preview'       => array(
		                            'type'      => 'none',
		                    )
		                )
		            ),
		            'bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Color', 'uabb'),
		                )
		            ),
		            'bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
		            'bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Hover Color', 'uabb'),
		                    'preview'       => array(
		                            'type'      => 'none',
		                    )
		                )
		            ),
		            'bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
		            
					'border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Color', 'uabb'),
		                )
		            ),
		            'border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Hover Color', 'uabb'),
		                )
		            ),
		            'three_d'       => array(
						'type'          => 'select',
						'label'         => __('Gradient', 'fl-builder'),
						'default'       => '0',
						'options'       => array(
							'0'             => __('No', 'fl-builder'),
							'1'             => __('Yes', 'fl-builder')
						)
					)
				)
			),
		)
	)
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('icon_group_form', array(
	'title' => __('Add Icon', 'fl-builder'),
	'tabs'  => array(
		'general'       => array( // Tab
			'title'         => __('General', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array( // Section
					'title'         => '', // Section Title
					'fields'        => array( // Section Fields
						'image_type'    => array(
							'type'          => 'select',
							'label'         => __('Image Type', 'uabb'),
							'default'       => 'icon',
							'options'       => array(
								'icon'          => __('Icon', 'uabb'),
								'photo'         => __('Photo', 'uabb'),
							),
							'toggle'        => array(
								'icon'          => array(
									'fields'	 => array( 'icon', 'icocolor', 'icohover_color' ),
								),
								'photo'         => array(
									'fields'	 => array( 'photo' ),
								)
							),
						),
						'icon'          => array(
							'type'          => 'icon',
							'label'         => __( 'Icon', 'fl-builder' )
						),
						'photo'			=> array(
							'type'			=> 'photo',
							'label'			=> __( 'Photo', 'fl-builder' )
						),
						'link'          => array(
							'type'          => 'link',
							'label'         => __( 'Link', 'fl-builder' )
						),
						'link_target'   => array(
							'type'          => 'select',
							'label'         => __('Link Target', 'uabb'),
							'default'       => '_self',
							'options'       => array(
								'_self'         => __('Same Window', 'uabb'),
								'_blank'        => __('New Window', 'uabb')
							),
							'preview'       => array(
								'type'          => 'none'
							)
						)
					)
				)
			)
		),
		'style'         => array( // Tab
			'title'         => __('Style', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
				'colors'        => array( // Section
					'title'         => __('Colors', 'fl-builder'), // Section Title
					'fields'        => array( // Section Fields
						
					 	'icocolor' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
			                array(
								'label'         => __('Color', 'fl-builder'),
			                )
			            ),
				        'icohover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
				            array(
								'label'         => __('Hover Color', 'fl-builder'),
				                'preview'       => array(
				                        'type'      => 'none',
				                )
				            )
				        ),

						'bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
			                array(
			                    'label'         => __('Background Color', 'uabb'),
			                )
			            ),
			            'bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
				        
			            'bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
			                array(
			                    'label'         => __('Background Hover Color', 'uabb'),
			                    'preview'       => array(
			                            'type'      => 'none',
			                    )
			                )
			            ),
			            'bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
			            
	                    /* Border Color Dependent on Border Style for ICon */
	                    'border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
			                array(
			                    'label'         => __('Border Color', 'uabb'),
			                )
			            ),
			            'border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
			                array(
			                    'label'         => __('Border Hover Color', 'uabb'),
								'preview'       => array(
									'type'          => 'none'
								)
			                )
			            ),
					)
				)
			)
		)
	)
));