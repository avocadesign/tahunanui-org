(function($){
	FLBuilder.registerModuleHelper('advanced-icon', {
		rules: {
			size: {
				required: true
			}
		},

		init: function()
		{	
			var form    	= $('.fl-builder-settings'),
				icoimage_style	= form.find('select[name=icoimage_style]');
							
			//console.log( this );
			// Init validation events.
			//this._photoSourceChanged();
			this._toggleBorderOptions();


				// Validation events
			icoimage_style.on('change', $.proxy( this._toggleBorderOptions, this ) ) ;
			
		},
		
		_toggleBorderOptions: function() {
			var form		= $('.fl-builder-settings'),
				icoimage_style 	= form.find('select[name=icoimage_style]').val(),
				border_style 	= form.find('select[name=border_style]').val();
				
			if( icoimage_style == 'custom' ){
				if ( border_style != 'none' ) {
					form.find('#fl-field-border_width').show();
					form.find('#fl-field-border_color').show();
					form.find('#fl-field-border_hover_color').show();
				}
			}else {
				//alert();
				form.find('#fl-field-border_width').hide();
				form.find('#fl-field-border_color').hide();
				form.find('#fl-field-border_hover_color').hide();
			}
		},
	});
})(jQuery);