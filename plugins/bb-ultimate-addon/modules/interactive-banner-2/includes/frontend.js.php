/**
 * This file should contain frontend JavaScript that 
 * will be applied to individual module instances.
 *
 * You have access to three variables in this file: 
 * 
 * $module An instance of your module class.
 * $id The module's ID.
 * $settings The module's settings.
 *
 * Example: 
 */
if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
	var is_touch_device = false;
else
	var is_touch_device = true;

jQuery('#page').click(function(){
	jQuery('.uabb-ib2-hover').removeClass('uabb-ib2-hover');
});
if(!is_touch_device){
	jQuery('.uabb-new-ib').hover(function(event){
		event.stopPropagation();
		jQuery(this).addClass('uabb-ib2-hover');
	},function(event){
		event.stopPropagation();
		jQuery(this).removeClass('uabb-ib2-hover');
	});
}
jQuery('.uabb-new-ib').click(function(event){
	event.stopPropagation();
	jQuery(document).trigger('ultFlipBoxClicked', jQuery(this));
	if(jQuery(this).hasClass('uabb-ib2-hover')){
		jQuery(this).removeClass('uabb-ib2-hover');
	}
	else{
		jQuery('.uabb-ib2-hover').removeClass('uabb-ib2-hover');
		jQuery(this).addClass('uabb-ib2-hover');
	}
});