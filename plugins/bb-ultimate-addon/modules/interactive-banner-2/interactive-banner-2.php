<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class InteractiveBanner2Module
 */
class InteractiveBanner2Module extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Interactive Banner 2', 'uabb'),
            'description'   => __('An basic example for coding new modules.', 'uabb'),
            'category'		=> __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/interactive-banner-2/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/interactive-banner-2/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}



/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('InteractiveBanner2Module', array(
    'style'       => array( // Tab
        'title'         => __('General', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Banner Styles', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_style'     => array(
                        'type'          => 'select',
                        'label'         => __('Banner Style', 'uabb'),
                        'default'       => 'style1',
                        'help'          => 'Select appear effect for description text.',
                        'options'       => array(
                            'style1'      => __('Style 1', 'uabb'),
                            'style2'      => __('Style 2', 'uabb'),
                            /*'style3'      => __('Style 3', 'uabb'),*/
                            'style4'      => __('Style 3', 'uabb'),
                            'style5'      => __('Style 4', 'uabb'),
                            /*'style6'      => __('Style 6', 'uabb'),*/
                            'style7'      => __('Style 5', 'uabb'),
                            'style8'      => __('Style 6', 'uabb'),
                            'style9'      => __('Style 7', 'uabb'),
                            'style10'     => __('Style 8', 'uabb'),
                            'style11'     => __('Style 9', 'uabb'),
                            /*'style12'     => __('Style 12', 'uabb'),*/
                            'style13'     => __('Style 10', 'uabb'),
                            'style14'     => __('Style 11', 'uabb'),
                            /*'style15'     => __('Style 14', 'uabb'),*/
                        ),
                        'toggle' => array(
                            'style5' => array(
                                'fields' => array( 'title_background_color', 'title_background_color_opc' )
                            )
                        )
                    ),

                    'title_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Title Background Color', 'uabb'),
                            'default'       => 'fafafa',
                        )
                    ),
                    'title_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'banner_image' => array(
                        'type'          => 'photo',
                        'label'         => __('Banner Image', 'uabb'),
                    ),
                    'banner_height'     => array(
                        'type'          => 'text',
                        'label'         => __('Custom Banner Height', 'uabb'),
                        'default'       => '',
                        'size'          => '3',
                        'description'   => 'px',
                        'help'          => 'How big would you like it?',
                    ),
                    /*'opacity'     => array(
                        'type'          => 'text',
                        'label'         => __('Image Opacity', 'uabb'),
                        'default'       => '100',
                        'size'          => '3',
                        'description'   => '%',
                        'help'          => 'Enter value between 0 to 100 (0% is maximum transparency, while 100% is lowest)',
                    ),
                    'hover_opacity'     => array(
                        'type'          => 'text',
                        'label'         => __('Image Hover Opacity', 'uabb'),
                        'default'       => '80',
                        'size'          => '3',
                        'description'   => '%',
                        'help'          => 'Enter value between 0 to 100 (0% is maximum transparency, while 100% is lowest)',
                    ),*/
                    'img_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Image Overlay Color', 'uabb'),
                            'default'       => '#cccccc',
                        )
                    ),
                    'img_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),
            'link'       => array( // Section
                'title'         => __('Link', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'link_url'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Link URL', 'uabb' ),
                        'placeholder'   => __( 'URL', 'uabb' ),
                    ),
                    'link_target'    => array(
                        'type'          => 'select',
                        'label'         => __('Link target', 'uabb'),
                        'default'       => '_blank',
                        'options'       => array(
                            '_blank'      => __('New Page', 'uabb'),
                            ''      => __('Same Page', 'uabb'),
                        ),
                    ),
                )
            ),
        )
    ),
    'title'       => array( // Tab
        'title'         => __('Title', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Title', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_title'     => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'uabb'),
                        'default'       => 'Interactive Banner 2',
                        'preview'       => array(
                            'type' => 'text',
                            'selector' => '.uabb-new-ib-title'
                        ),
                    ),
                )
            ),
            'title_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Title Typography', 'uabb' ),
                    'fields' => array(
                        'color' => array(
                            'preview' => array(
                                'type' => 'css',
                                'property' => 'color',
                                'selector' => '.uabb-new-ib-title',
                            ),
                        ),
                        'tag_selection'   => array(
                            'default'       => 'h3',
                        ),
                        'font_size' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-new-ib-title',
                                'property'        => 'font-size',
                                'unit'            => 'px'
                            )
                        ),
                        'line_height' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-new-ib-title',
                                'property'        => 'line-height',
                                'unit'            => 'px'
                            )
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-new-ib-title'
                            )
                        ),
                    ),
                ),
                array(),
                'title_typography'
            ),
        )
    ),
    'description'       => array( // Tab
        'title'         => __('Description', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'description'       => array( // Section
                'title'         => __('Description', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_desc'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => 'Enter description text here.',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-new-ib-content',
                        )
                    ),
                )
            ),
            'desc_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Description Typography', 'uabb' ),
                    'fields' => array(
                        'color' => array(
                            'label' => __('Description Text Color', 'uabb'),
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-new-ib-content',
                                'property'        => 'color'
                            )
                        ),
                        'font_size' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-new-ib-content',
                                'property'        => 'font-size',
                                'unit'            => 'px'
                            )
                        ),
                        'line_height' => array(
                            'preview'         => array(
                                'type'            => 'css',
                                'selector'        => '.uabb-new-ib-content',
                                'property'        => 'line-height',
                                'unit'            => 'px'
                            )
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-new-ib-content'
                            )
                        ),
                    ),
                ),
                array( 'tag_selection' ),
                'desc_typography'
            ),
        )
    ),
));
