<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class InfoBannerModule
 */
class InfoBannerModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Info Banner', 'uabb'),
            'description'   => __('Info Banner', 'uabb'),
            'category'      => __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/info-banner/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/info-banner/',
            'partial_refresh'   => true
        ));
        //$this->add_css('info-banner-css', $this->url . 'css/info-banner.css');
    }


    /**
     * @method render_link
     */
    public function render_link()
    {
        if($this->settings->cta_type == 'link') {
            echo '<a href="' . $this->settings->link . '" target="' . $this->settings->link_target . '" class="uabb-infobanner-cta-link">' . $this->settings->cta_text . '</a>';
        }
    }

    /**
     * @method render_button
     */
    public function render_button()
    {
        if($this->settings->cta_type == 'button') {
            $btn_settings = array(
                
                /* General Section */
                'text'              => $this->settings->btn_text,
                
                /* Link Section */
                'link'              => $this->settings->btn_link,
                'link_target'       => $this->settings->btn_link_target,
                
                /* Style Section */
                'style'             => $this->settings->btn_style,
                'border_size'       => $this->settings->btn_border_size,
                'transparent_button_options' => $this->settings->btn_transparent_button_options,
                'threed_button_options'      => $this->settings->btn_threed_button_options,
                'flat_button_options'        => $this->settings->btn_flat_button_options,

                /* Colors */
                'bg_color'          => $this->settings->btn_bg_color,
                'bg_hover_color'    => $this->settings->btn_bg_hover_color,
                'text_color'        => $this->settings->btn_text_color,
                'text_hover_color'  => $this->settings->btn_text_hover_color,

                /* Icon */
                'icon'              => $this->settings->btn_icon,
                'icon_position'     => $this->settings->btn_icon_position,
                
                /* Structure */
                'width'              => $this->settings->btn_width,
                'custom_width'       => $this->settings->btn_custom_width,
                'custom_height'      => $this->settings->btn_custom_height,
                'padding_top_bottom' => $this->settings->btn_padding_top_bottom,
                'padding_left_right' => $this->settings->btn_padding_left_right,
                'border_radius'      => $this->settings->btn_border_radius,
                'align'              => $this->settings->banner_alignemnt,
                'mob_align'          => '',

                /* Typography */
                'font_size'         => $this->settings->tbtn_font_size,
                'line_height'       => $this->settings->tbtn_line_height,
                'font_family'       => $this->settings->tbtn_font_family,
            );

            FLBuilder::render_module_html('uabb-button', $btn_settings);
        }
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('InfoBannerModule', array(
    'banner'       => array( // Tab
        'title'         => __('General', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Info Banner', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_title'     => array(
                        'type'          => 'text',
                        'label'         => __('Title', 'uabb'),
                        'default'       => 'Info Banner',
                        'help'          => 'Perhaps, this is the most highlighted text.',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-ultb3-title',
                        )
                    ),
                )
            ),
            'description'       => array( // Section
                'title'         => '', // Section Title
                'fields'        => array( // Section Fields
                    'banner_desc'     => array(
                        'type'          => 'editor',
                        'media_buttons' => false,
                        'rows'          => 10,
                        'label'         => __('', 'uabb'),
                        'default'       => '<p>Enter description text here.</p>',
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.uabb-ultb3-desc',
                        )
                    ),
                )
            ),
            'styles' => array( // Section
                'title'         => __('Style', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => '#f2f2f2',
                        )
                    ),
                    'background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'banner_alignemnt'     => array(
                        'type'          => 'select',
                        'label'         => __('Banner Alignment', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'uabb-ultb3-align-left' => __('Left', 'uabb'),
                            'uabb-ultb3-align-center' => __('Center', 'uabb'),
                            'uabb-ultb3-align-right' => __('Right', 'uabb'),
                        ),
                    ),
                    'min_height_switch'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Minimum Height', 'uabb' ),
                        'default'       => 'auto',
                        'options'       => array(
                            'auto'      => 'Default',
                            'custom'        => 'Custom',
                        ),
                        'toggle'    => array(
                            'custom'    => array(
                                'fields'    => array( 'min_height', 'vertical_align' ),
                            )
                        ),
                    ),
                    'vertical_align'         => array(
                        'type'          => 'select',
                        'label'         => __('Align Items', 'uabb'),
                        'default'       => 'middle',
                        'options'       => array(
                            'top' => __('Top', 'uabb'),
                            'middle' => __('Middle', 'uabb'),
                            'bottom' => __('Bottom', 'uabb'),
                        ),
                    ),
                    'min_height'          => array(
                        'type'          => 'text',
                        'label'         => __('Minimum Height', 'uabb'),
                        'description'   => 'px',
                        'maxlength'     => '4',
                        'size'          => '5',
                        'placeholder'   => 'auto',
                        'help'          => __('Apply minimum height to complete Callout. It will useful when multiple Callouts are in same row.', 'uabb'),
                    ),
                )
            ),
        )
    ),
    'image'       => array( // Tab
        'title'         => __('Image', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'   => array( // Section
                'title'         => __('Image', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_image' => array(
                        'type'          => 'photo',
                        'label'         => __('Banner Image', 'uabb'),
                        'show_remove'   => true,
                        'help'          => __('The image that would be appear as Background. Make sure overlay color you\'ve chosen must be transparent, to work this setting.', 'uabb')
                    ),
                    'banner_image_alignemnt'     => array(
                        'type'          => 'select',
                        'label'         => __('Image Alignment', 'uabb'),
                        'default'       => 'uabb-ultb3-img-center',
                        'options'       => array(
                            "uabb-ultb3-img-top-left" => __("Top Left","uabb"),
                            "uabb-ultb3-img-top-center" => __("Top Center","uabb"),
                            "uabb-ultb3-img-top-right" => __("Top Right","uabb"),
                            "uabb-ultb3-img-center-left" => __("Center Left","uabb"),
                            "uabb-ultb3-img-center" => __("Center","uabb"),
                            "uabb-ultb3-img-center-right" => __("Center Right","uabb"),
                            "uabb-ultb3-img-bottom-left" => __("Bottom Left","uabb"),
                            "uabb-ultb3-img-bottom-center" => __("Bottom Center","uabb"),
                            "uabb-ultb3-img-bottom-right" => __("Bottom Right","uabb"),
                        ),
                    ),
                    'overlay_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Overlay Color', 'uabb'),
                        )
                    ),
                    'overlay_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                )
            ),
            'animation' => array( // Section
                'title'         => __('Animation', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'banner_image_effect'     => array(
                        'type'          => 'select',
                        'label'         => __('Effect', 'uabb'),
                        'default'       => '',
                        'options'       => array(
                            "" => __("No Effect","uabb"),
                            "uabb-ib-slide-up"      => __("Slide Up","uabb"),
                            "uabb-ib-slide-down"    => __("Slide Down","uabb"),
                            "uabb-ib-slide-left"    => __("Slide Left","uabb"),
                            "uabb-ib-slide-right"   => __("Slide Right","uabb"),
                            "uabb-ib-zoom-in"       => __("Zoom In","uabb"),
                            "uabb-ib-zoom-out"      => __("Zoom Out","uabb"),
                            "uabb-ib-pan"           => __("Pan","uabb"),
                        ),
                    ),
                    'spacer'            => array(
                        'type'      => 'uabb-blank-spacer',
                        'height'    => '100px'
                    )
                )
            ),
        )
    ),
    'cta'           => array(
        'title'         => __('Link', 'uabb'),
        'sections'      => array(
            'cta'           => array(
                'title'         => __('Call to Action', 'uabb'),
                'fields'        => array(
                    'cta_type'      => array(
                        'type'          => 'select',
                        'label'         => __('Type', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => _x( 'None', 'Call to action.', 'uabb' ),
                            'link'          => __('Text', 'uabb'),
                            'button'        => __('Button', 'uabb'),
                            'module'        => __('Link To Module','uabb'),
                        ),
                        'toggle'        => array(
                            'none'          => array(),
                            'link'          => array(
                                'fields'        => array('cta_text'),
                                'sections'      => array('link', 'link_typography')
                            ),
                            'button'        => array(
                                'sections'      => array('btn-general', 'btn-icon', 'btn-link', 'btn-colors', 'btn-style', 'btn-structure', 'btn_typography')
                            ),
                            'module'          => array(
                                'sections'      => array('link')
                            ),

                        )
                    ),
                    'cta_text'      => array(
                        'type'          => 'text',
                        'label'         => __('Text', 'uabb'),
                        'default'       => __('Read More', 'uabb'),
                    ),
                )
            ),
            'btn-general'    => BB_Ultimate_Addon::uabb_section_get( 'btn-general' ),
            'btn-link'       => BB_Ultimate_Addon::uabb_section_get( 'btn-link' ),
            'btn-style'      => BB_Ultimate_Addon::uabb_section_get( 'btn-style' ),
            'btn-icon'       => BB_Ultimate_Addon::uabb_section_get( 'btn-icon' ),
            'btn-colors'     => BB_Ultimate_Addon::uabb_section_get( 'btn-colors' ),
            'btn-structure'  => BB_Ultimate_Addon::uabb_section_get( 'btn-structure', array(), array('btn_align', 'btn_mob_align') ),
            'link'          => array(
                'title'         => __('Link', 'uabb'),
                'fields'        => array(
                    'link'          => array(
                        'type'          => 'link',
                        'label'         => __('Link', 'uabb'),
                        'help'          => __('The link applies to the entire module. If choosing a call to action type below, this link will also be used for the text or button.', 'uabb'),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    ),
                    'link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'uabb'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'uabb'),
                            '_blank'        => __('New Window', 'uabb')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    )
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __('Title', 'uabb' ),
                    'fields'   => array(
                        'tag_selection' => array(
                            'default' => 'h3'
                        ),
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-ultb3-title'
                            ),
                        ),
                        'font_size' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-ultb3-title',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-ultb3-title',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        ),
                        'title_margin_top' => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'description'   => 'px',
                            'maxlength'     => '4',
                            'size'          => '8',
                            'default'       => '0',
                        ),
                        'title_margin_bottom' => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'description'   => 'px',
                            'maxlength'     => '4',
                            'size'          => '8',
                            'default'       => '0',
                        ),
                    ),
                ), 
                array() 
            ),
            'desc_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __('Description', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-ultb3-desc'
                            ),
                        ),
                        'color' => array(
                            'label'     => __('Description Color', 'uabb'),
                        ),
                        'font_size' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-ultb3-desc',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-ultb3-desc',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        ),
                        'margin_top' => array(
                            'type'          => 'text',
                            'label'         => __('Margin Top', 'uabb'),
                            'description'   => 'px',
                            'maxlength'     => '4',
                            'size'          => '8',
                            'default'       => '0',
                        ),
                        'margin_bottom' => array(
                            'type'          => 'text',
                            'label'         => __('Margin Bottom', 'uabb'),
                            'description'   => 'px',
                            'maxlength'     => '4',
                            'size'          => '8',
                            'default'       => '0',
                        ),
                    ),
                ), 
                array('tag_selection'),
                'desc' 
            ),
            'btn_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __('Button', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => 'a.uabb-button'
                            ),
                        ),
                        'font_size' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => 'a.uabb-button',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                            'preview'   => array(
                                'type'      => 'css',
                                'selector'  => 'a.uabb-button',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        )
                    )
                ), 
                array('color', 'tag_selection'),
                'tbtn' 
            ),
            'link_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __( 'Link Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-infobox-cta-link'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Link Color', 'uabb'),
                        )
                    ),
                 ), 
                array( 'tag_selection' ),
                'link'
            ),
        )
    ),
));
