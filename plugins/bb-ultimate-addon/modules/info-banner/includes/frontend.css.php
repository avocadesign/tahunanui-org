<?php
$settings->background_color = UABB_Helper::uabb_colorpicker( $settings, 'background_color', true );
$settings->overlay_color = UABB_Helper::uabb_colorpicker( $settings, 'overlay_color', true );

$settings->color = UABB_Helper::uabb_colorpicker( $settings, 'color' );
$settings->desc_color = UABB_Helper::uabb_colorpicker( $settings, 'desc_color' );
$settings->link_color = UABB_Helper::uabb_colorpicker( $settings, 'link_color' );

?>

 /* Min height */
<?php
if( $settings->min_height_switch == 'custom' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-ultb3-box {
	min-height: <?php echo $settings->min_height; ?>px;
	align-items: <?php echo ( $settings->vertical_align == 'middle' ) ? 'center' : ( ( $settings->vertical_align == 'bottom' ) ? 'flex-end' : 'flex-start' ) ; ?>;
}
<?php
}
if( $settings->background_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-ultb3-box {
	background: <?php echo $settings->background_color; ?>;
}
<?php
}
?>

/* Title Typography and Color */
.fl-node-<?php echo $id; ?> .uabb-ultb3-box .uabb-ultb3-title {
   	<?php if( $settings->font_family['family'] != "Default" ){ ?>
   		<?php UABB_Helper::uabb_font_css( $settings->font_family ); ?>
   	<?php } ?>
   	<?php if( $settings->font_size['desktop'] != '' ){ ?>
   		font-size: <?php echo $settings->font_size['desktop']; ?>px;
   		<?php /*line-height: <?php echo $settings->font_size['desktop'] + 2; ?>px;*/ ?>
   	<?php } ?>
   	<?php if( $settings->line_height['desktop'] != '' ){ ?>
   		line-height: <?php echo $settings->line_height['desktop']; ?>px;
   	<?php } ?>
   	<?php if( $settings->color != '' ){ ?>
   		color: <?php echo $settings->color; ?>;
   	<?php } ?>

   	<?php if( $settings->title_margin_top != '' ){ ?>
   		margin-top: <?php echo $settings->title_margin_top; ?>px;
   	<?php } ?>

   	<?php if( $settings->title_margin_bottom != '' ){ ?>
   		margin-bottom: <?php echo $settings->title_margin_bottom; ?>px;
   	<?php } ?>
}

/* Description Typography and Color */
.fl-node-<?php echo $id; ?> .uabb-ultb3-desc {
    <?php if( $settings->desc_font_family['family'] != "Default" ){ ?>
   		<?php UABB_Helper::uabb_font_css( $settings->desc_font_family ); ?>
   	<?php } ?>
   	<?php if( $settings->desc_font_size['desktop'] != '' ){ ?>
   		font-size: <?php echo $settings->desc_font_size['desktop']; ?>px;
   		/*line-height: <?php echo $settings->desc_font_size['desktop'] + 2; ?>px;*/
   	<?php } ?>
   	<?php if( $settings->desc_line_height['desktop'] != '' ){ ?>
   		line-height: <?php echo $settings->desc_line_height['desktop']; ?>px;
   	<?php } ?>
   	<?php if( $settings->desc_color != '' ){ ?>
   		color: <?php echo $settings->desc_color; ?>;
   	<?php } ?>

   	<?php if( $settings->desc_margin_top != '' ){ ?>
   		margin-top: <?php echo $settings->desc_margin_top; ?>px;
   	<?php } ?>

   	<?php if( $settings->desc_margin_bottom != '' ){ ?>
   		margin-bottom: <?php echo $settings->desc_margin_bottom; ?>px;
   	<?php } ?>
}

.fl-node-<?php echo $id; ?> .uabb-ultb3-desc * {
	font-family: inherit;
	font-weight: inherit; 
	font-size: inherit;
	font-style: inherit; 
	color: inherit; 
	line-height: inherit;
}

<?php
if($settings->cta_type == 'button') {
	/* Button Render Css */
	FLBuilder::render_module_css('uabb-button', $id, array(

    	/* General Section */
        'text'              => $settings->btn_text,
        
        /* Link Section */
        'link'              => $settings->btn_link,
        'link_target'       => $settings->btn_link_target,
        
        /* Style Section */
        'style'             => $settings->btn_style,
        'border_size'       => $settings->btn_border_size,
        'transparent_button_options' => $settings->btn_transparent_button_options,
        'threed_button_options'      => $settings->btn_threed_button_options,
        'flat_button_options'        => $settings->btn_flat_button_options,

        /* Colors */
        'bg_color'          => $settings->btn_bg_color,
        'bg_hover_color'    => $settings->btn_bg_hover_color,
        'text_color'        => $settings->btn_text_color,
        'text_hover_color'  => $settings->btn_text_hover_color,

        /* Icon */
        'icon'              => $settings->btn_icon,
        'icon_position'     => $settings->btn_icon_position,
        
        /* Structure */
        'width'              => $settings->btn_width,
        'custom_width'       => $settings->btn_custom_width,
        'custom_height'      => $settings->btn_custom_height,
        'padding_top_bottom' => $settings->btn_padding_top_bottom,
        'padding_left_right' => $settings->btn_padding_left_right,
        'border_radius'      => $settings->btn_border_radius,
        'align'              => $settings->banner_alignemnt,
        'mob_align'          => '',

        /* Typography */
        'font_size'         => $settings->tbtn_font_size,
        'line_height'       => $settings->tbtn_line_height,
        'font_family'       => $settings->tbtn_font_family,
	));
}
?>


.fl-node-<?php echo $id; ?> .uabb-ultb3-box-overlay {
    <?php if ( $settings->overlay_color != '' ) { ?>
    	background-color: <?php echo $settings->overlay_color; ?>;
    <?php } ?> 
}

/* Typography Options for Link Text */
.fl-builder-content .fl-node-<?php echo $id; ?>	.uabb-infobanner-cta-link {
	<?php if( $settings->link_font_family['family'] != "Default") : ?>
		<?php UABB_Helper::uabb_font_css( $settings->link_font_family ); ?>
	<?php endif; ?>

	<?php if( $settings->link_font_size['desktop'] != '' ) : ?>
	font-size: <?php echo $settings->link_font_size['desktop']; ?>px;
	<?php endif; ?>

	<?php if( $settings->link_line_height['desktop'] != '' ) : ?>
	line-height: <?php echo $settings->link_line_height['desktop']; ?>px;
	<?php endif; ?>
}

/* Link Color */
<?php if( !empty($settings->link_color) ) : ?> 
.fl-builder-content .fl-node-<?php echo $id; ?> a,
.fl-builder-content .fl-node-<?php echo $id; ?> a *,
.fl-builder-content .fl-node-<?php echo $id; ?> a:visited {
	color: <?php echo uabb_theme_text_color( $settings->link_color ); ?>;
}
<?php endif; ?>


<?php if($global_settings->responsive_enabled) { // Global Setting If started 
	if( $settings->desc_font_size['medium'] != "" || $settings->desc_line_height['medium'] != "" || $settings->font_size['medium'] != "" || $settings->line_height['medium'] != "" )
	{
	?>
		@media ( max-width: <?php echo $global_settings->medium_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-ultb3-box .uabb-ultb3-title {
			   	<?php if( $settings->font_size['medium'] != '' ){ ?>
			   		font-size: <?php echo $settings->font_size['medium']; ?>px;
			   		/*line-height: <?php echo $settings->font_size['medium'] + 2; ?>px;*/
			   	<?php } ?>
			   	<?php if( $settings->line_height['medium'] != '' ){ ?>
			   		line-height: <?php echo $settings->line_height['medium']; ?>px;
			   	<?php } ?>
			}

			.fl-node-<?php echo $id; ?> .uabb-ultb3-desc {
			   	<?php if( $settings->desc_font_size['medium'] != '' ){ ?>
			   		font-size: <?php echo $settings->desc_font_size['medium']; ?>px;
			   		<?php /*line-height: <?php echo $settings->desc_font_size['medium'] + 2; ?>px;*/ ?>
			   	<?php } ?>
			   	<?php if( $settings->desc_line_height['medium'] != '' ){ ?>
			   		line-height: <?php echo $settings->desc_line_height['medium']; ?>px;
			   	<?php } ?>
			}

			fl-builder-content .fl-node-<?php echo $id; ?>	.uabb-infobanner-cta-link {

				<?php if( $settings->link_font_size['medium'] != '' ) : ?>
				font-size: <?php echo $settings->link_font_size['medium']; ?>px;
				<?php endif; ?>

				<?php if( $settings->link_line_height['medium'] != '' ) : ?>
				line-height: <?php echo $settings->link_line_height['medium']; ?>px;
				<?php endif; ?>
			}
	    }
	<?php
	}
	if( $settings->desc_font_size['small'] != "" || $settings->desc_line_height['small'] != "" || $settings->font_size['small'] != "" || $settings->line_height['small'] != "" )
	{
	?>
		@media ( max-width: <?php echo $global_settings->responsive_breakpoint .'px'; ?> ) {
			.fl-node-<?php echo $id; ?> .uabb-ultb3-box .uabb-ultb3-title {
			   	<?php if( $settings->font_size['small'] != '' ){ ?>
			   		font-size: <?php echo $settings->font_size['small']; ?>px;
			   		<?php /*line-height: <?php echo $settings->font_size['small'] + 2; ?>px;*/ ?>
			   	<?php } ?>
			   	<?php if( $settings->line_height['small'] != '' ){ ?>
			   		line-height: <?php echo $settings->line_height['small']; ?>px;
			   	<?php } ?>
			}

			.fl-node-<?php echo $id; ?> .uabb-ultb3-desc {
			   	<?php if( $settings->desc_font_size['small'] != '' ){ ?>
			   		font-size: <?php echo $settings->desc_font_size['small']; ?>px;
			   		<?php /*line-height: <?php echo $settings->desc_font_size['small'] + 2; ?>px;*/ ?>
			   	<?php } ?>
			   	<?php if( $settings->desc_line_height['small'] != '' ){ ?>
			   		line-height: <?php echo $settings->desc_line_height['small']; ?>px;
			   	<?php } ?>
			}

			.fl-builder-content .fl-node-<?php echo $id; ?>	.uabb-infobanner-cta-link {

				<?php if( $settings->link_font_size['small'] != '' ) : ?>
				font-size: <?php echo $settings->link_font_size['small']; ?>px;
				<?php endif; ?>

				<?php if( $settings->link_line_height['small'] != '' ) : ?>
				line-height: <?php echo $settings->link_line_height['small']; ?>px;
				<?php endif; ?>
			}
	    }
	<?php
	}
}
?>