(function($){

	FLBuilder.registerModuleHelper('info-banner', {

		rules: {
			title: {
				required: true
			}
		},
		
		init: function()
		{
			var form    	= $('.fl-builder-info-banner-settings'),
				show_button = form.find('.uabb_toggle_switch_label');
			show_button.on('click', this._toggleAllButtonOptions);
			this._toggleAllButtonOptionsOnLoad();
		},
		
		_toggleAllButtonOptions: function() {
			if( $(this).html() == 'No' ) {
				$('#fl-builder-settings-section-btn-general').hide();
				$('#fl-builder-settings-section-btn-link').hide();
				$('#fl-builder-settings-section-btn-colors').hide();
				$('#fl-builder-settings-section-btn-style').hide();
				$('#fl-builder-settings-section-btn-structure').hide();
			} else {
				$('#fl-builder-settings-section-btn-general').show();
				$('#fl-builder-settings-section-btn-link').show();
				$('#fl-builder-settings-section-btn-colors').show();
				$('#fl-builder-settings-section-btn-style').show();
				$('#fl-builder-settings-section-btn-structure').show();
			}
		},

		_toggleAllButtonOptionsOnLoad: function() {
			var form = $('.fl-builder-info-banner-settings'),
			show_button = form.find('select[name=show_button]').val();
		
			if( show_button == 'no' ) {
				$('#fl-builder-settings-section-btn-general').hide();
				$('#fl-builder-settings-section-btn-link').hide();
				$('#fl-builder-settings-section-btn-colors').hide();
				$('#fl-builder-settings-section-btn-style').hide();
				$('#fl-builder-settings-section-btn-structure').hide();
			} else if ( show_button == 'yes' ) {
				$('#fl-builder-settings-section-btn-general').show();
				$('#fl-builder-settings-section-btn-link').show();
				$('#fl-builder-settings-section-btn-colors').show();
				$('#fl-builder-settings-section-btn-style').show();
				$('#fl-builder-settings-section-btn-structure').show();
			}
		},

	});

})(jQuery);