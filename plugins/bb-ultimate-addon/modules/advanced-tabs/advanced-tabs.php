<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class AdvancedTabsModule
 */
class AdvancedTabsModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Advanced Tabs', 'uabb'),
            'description'   => __('', 'uabb'),
            'category'      => __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/advanced-tabs/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/advanced-tabs/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));

        $this->add_css( 'font-awesome' );
    }
}

FLBuilder::register_module('AdvancedTabsModule', array(
    'items'         => array(
        'title'         => __('Items', 'uabb'),
        'sections'      => array(
            'general'       => array(
                'title'         => '',
                'fields'        => array(
                    'items'         => array(
                        'type'          => 'form',
                        'label'         => __('Item', 'uabb'),
                        'form'          => 'items_form', // ID from registered form below
                        'preview_text'  => 'label', // Name of a field to use for the preview text
                        'multiple'      => true
                    ),
                )
            )
        )
    ),
    'style'         => array(
        'title'         => __('Tab', 'uabb'),
        'sections'      => array(
            'general'       => array(
                'title'         => '',
                'fields'        => array(
                    'style'        => array(
                        'type'          => 'select',
                        'label'         => __('Style', 'uabb'),
                        'default'       => 'style1',
                        'options'       => array(
                            'simple'    => __('Simple', 'uabb'),
                            'bar'    => __('Bar', 'uabb'),
                            'iconfall'    => __('Icon fall', 'uabb'),
                            'underline'    => __('Underline', 'uabb'),
                            'topline'    => __('Topline', 'uabb'),
                            'linebox'    => __('Line box', 'uabb'),
                            /*'linetriangle'    => __('Line triangle', 'uabb'),
                            'iconbox'    => __('Icon box', 'uabb'),
                            'line'    => __('Line', 'uabb'),
                            'circle'    => __('Circle', 'uabb'),
                            'flip'    => __('Flip', 'uabb'),
                            'tzoid'    => __('T-Zoid', 'uabb'),
                            'fillup'    => __('Fill up', 'uabb'),*/
                        ),
                        'toggle' => array(
                            'simple' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                ),
                            ),
                            'bar' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_background_color',
                                    'title_background_color_opc',
                                    'title_background_hover_color',
                                    'title_background_hover_color_opc',
                                    'title_active_color',
                                    'title_active_background_color',
                                    'title_active_background_color_opc',
                                ),
                                'sections' => array( 'label_border' )
                            ),
                            'iconfall' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                ),
                                'sections' => array( 'underline_settings' )
                            ),
                            'underline' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_background_color',
                                    'title_background_color_opc',
                                    'title_background_hover_color',
                                    'title_background_hover_color_opc',
                                    'title_active_color',
                                    'underline_separation_color',
                                ),
                                'sections' => array( 'underline_settings' )
                            ),
                            'topline' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_background_color',
                                    'title_background_color_opc',
                                    'title_background_hover_color',
                                    'title_background_hover_color_opc',
                                    'title_active_color',
                                    'line_position',
                                ),
                                'sections' => array( 'underline_settings' )
                            ),
                            'linebox' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                    'title_background_hover_color',
                                    'title_background_hover_color_opc',
                                    'title_background_color',
                                    'title_background_color_opc',
                                )
                            ),
                            /*'iconbox' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_background_color',
                                    'title_background_color_opc',
                                    'title_background_hover_color',
                                    'title_background_hover_color_opc',
                                    'title_active_color',
                                )
                            ),                            
                            'line' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                )
                            ),
                            'circle' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                )
                            ),
                            'flip' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                    'content_background_color'
                                )
                            ),
                            'tzoid' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                    'content_background_color'
                                )
                            ),
                            'fillup' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                    'content_background_color'
                                )
                            ),
                            'linetriangle' => array(
                                'fields' => array(
                                    'title_color',
                                    'title_hover_color',
                                    'title_active_color',
                                )
                            ),*/
                        )
                    ),
                    'line_position' => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Line Position', 'uabb' ),
                        'default'       => 'top',
                        'options'       => array(
                            'top'       => 'Top',
                            'bottom'        => 'Bottom',
                        ),
                    ),
                    'tab_padding' => array(
                        'type'      => 'uabb-spacing',
                        'label'     => __( 'Tab Padding', 'uabb' ),
                        'mode'      => 'padding',
                        'default'   => 'padding: 15px;',
                    ),
                )
            ),
            'tab_style'       => array(
                'title'         => 'Tab Style',
                'fields'        => array(
                    'tab_style'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Style', 'uabb' ),
                        'default'       => 'full',
                        'options'       => array(
                            'full'       => 'Full',
                            'inline'     => 'Inline',
                        ),
                        'toggle' => array(
                            'inline' => array(
                                'fields' => array( 'tab_style_alignment' )
                            ),
                        )
                    ),
                    'tab_style_alignment' => array(
                        'type'          => 'select',
                        'label'         => __('Alignment', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Left', 'uabb'),
                            'right'     => __('Right', 'uabb'),
                            'center'    => __('Center', 'uabb'),
                        ),
                    ),
                    'title_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Color', 'uabb'),
                        )
                    ),
                    'title_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Hover Color', 'uabb'),
                            'preview'       => array(
                                    'type'      => 'none',
                            )
                        )
                    ),
                    'title_active_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Active Color', 'uabb'),
                        )
                    ),
                    'title_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => '#f7f7f7',
                        )
                    ),
                    'title_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                                        
                    'title_background_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Hover Color', 'uabb'),
                        )
                    ),
                    'title_background_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                    'title_active_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Active Background Color', 'uabb'),
                        )
                    ),
                    'title_active_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                )
            ),
            'underline_settings' => array(
                'title' => __( 'Tab Border', 'uabb' ),
                'fields' => array(
                    'underline_border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Thickness', 'uabb' ),
                        'placeholder'   => '6',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'underline_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                        )
                    ),
                    'underline_separation_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Separation Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                    
                )
            ),
            'label_border' => array(
                'title' => __( 'Tab Border', 'uabb' ),
                'fields' => array(
                    'tab_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'      => __('None', 'uabb'),
                            'solid'      => __('Solid', 'uabb'),
                            'dashed'      => __('Dashed', 'uabb'),
                            'dotted'      => __('Dotted', 'uabb'),
                            'double'      => __('Double', 'uabb'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array( 'tab_border_radius' )
                            ),
                            'solid' => array(
                                'fields' => array( 'tab_border_size', 'tab_border_color' )
                            ),
                            'dashed' => array(
                                'fields' => array( 'tab_border_size', 'tab_border_color' )
                            ),
                            'dotted' => array(
                                'fields' => array( 'tab_border_size', 'tab_border_color' )
                            ),
                            'double' => array(
                                'fields' => array( 'tab_border_size', 'tab_border_color' )
                            ),
                        ),
                    ),
                    'tab_border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Size', 'uabb' ),
                        'placeholder'   => '1',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'tab_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                    'tab_border_radius'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Radius', 'uabb' ),
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                )
            ),
            'icon_style'       => array(
                'title'         => 'Icon',
                'fields'        => array(
                    'show_icon'        => array(
                        'type'          => 'select',
                        'label'         => __('Show Icon', 'uabb'),
                        'default'       => 'yes',
                        'options'       => array(
                            'no'    => __('Disable', 'uabb'),
                            'yes'      => __('Enable', 'uabb'),
                        ),
                        /*'toggle' => array(
                            'no' => array(
                                'fields' => array()
                            ),
                            'yes' => array(
                                'fields' => array( 'icon_position', 'icon_color', 'icon_hover_color', 'icon_active_color' )
                            )
                        )*/
                    ),
                    'icon_position'        => array(
                        'type'          => 'select',
                        'label'         => __('Icon Position', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'    => __('Left', 'uabb'),
                            'right'      => __('Right', 'uabb'),
                            'top'      => __('Top', 'uabb'),
                        )
                    ),
                    'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'     => __('Icon Color', 'uabb'),
                        )
                    ),
                    'icon_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Icon Hover Color', 'uabb'),
                            'preview'       => array(
                                    'type'      => 'none',
                            )
                        )
                    ),
                    'icon_active_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Icon Active Color', 'uabb'),
                        )
                    ),
                )
            ),
        )
    ),
    'content' => array(
        'title' => __( 'Content', 'uabb' ),
        'sections' => array(
            'content_style'       => array(
                'title'         => 'Content Style',
                'fields'        => array(
                    'content_padding' => array(
                        'type'      => 'uabb-spacing',
                        'label'     => __( 'Padding', 'uabb' ),
                        'default'   => 'padding: 12px;',
                        'mode'      => 'padding',
                    ),
                    'content_alignment' => array(
                        'type'          => 'select',
                        'label'         => __('Alignment', 'uabb'),
                        'default'       => 'left',
                        'options'       => array(
                            'left'      => __('Left', 'uabb'),
                            'center'      => __('Center', 'uabb'),
                            'right'      => __('Right', 'uabb'),
                        ),
                    ),
                    'content_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Color', 'uabb'),
                        )
                    ),
                    'content_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'uabb'),
                        )
                    ),
                    'content_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'content_border_style'   => array(
                        'type'          => 'select',
                        'label'         => __('Border Style', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'      => __('None', 'uabb'),
                            'solid'      => __('Solid', 'uabb'),
                            'dashed'      => __('Dashed', 'uabb'),
                            'dotted'      => __('Dotted', 'uabb'),
                            'double'      => __('Double', 'uabb'),
                        ),
                        'toggle' => array(
                            'none' => array(
                                'fields' => array( 'content_border_radius' )
                            ),
                            'solid' => array(
                                'fields' => array( 'content_border_size', 'content_border_color' )
                            ),
                            'dashed' => array(
                                'fields' => array( 'content_border_size', 'content_border_color' )
                            ),
                            'dotted' => array(
                                'fields' => array( 'content_border_size', 'content_border_color' )
                            ),
                            'double' => array(
                                'fields' => array( 'content_border_size', 'content_border_color' )
                            ),
                        ),
                    ),
                    'content_border_size'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Size', 'uabb' ),
                        'placeholder'       => '1',
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                    'content_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Border Color', 'uabb'),
                            'default'       => '#dbdbdb',
                        )
                    ),
                    'content_border_radius'    => array(
                        'type'          => 'text',
                        'label'         => __( 'Border Radius', 'uabb' ),
                        'size'          => '8',
                        'description'   => __( 'px', 'uabb' ),
                    ),
                )
            ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title_typography' => BB_Ultimate_Addon::uabb_section_get(
                'typography',
                array(
                    'title' => __( 'Title', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-tabs ul li a *'
                            )
                        ),
                        /*'font_size' => array(
                            'default' => array(
                                'desktop' => '24'
                            )
                        ),*/
                    ), 
                ),
                array( 'tag_selection', 'color' ),
                'title'
            ),
            'content_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title' => __( 'Content', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-content-wrap .uabb-content, .uabb-content-wrap .uabb-content-current, .uabb-content-wrap .uabb-content p, .uabb-content-wrap .uabb-content-current p'
                            )
                        ),
                    ), 
                ), 
                array( 'tag_selection', 'color' ),
                'content'
            ),
        ),
    )
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('items_form', array(
    'title' => __('Add Item', 'uabb'),
    'tabs'  => array(
        'general'      => array(
            'title'         => __('General', 'uabb'),
            'sections'      => array(
                'general'       => array(
                    'title'         => '',
                    'fields'        => array(
                        'label'         => array(
                            'type'          => 'text',
                            'default'       => __('Tab 1', 'uabb'),
                            'label'         => __('Label', 'uabb')
                        ),
                        'tab_icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb'),
                            'show_remove'   => true
                        ),
                    )
                ),
                'content'       => array(
                    'title'         => __('Content', 'uabb'),
                    'fields'        => array(
                        'content'       => array(
                            'type'          => 'editor',
                            'default'       => 'Enter description text here.',
                            'label'         => ''
                        )
                    )
                )
            )
        )
    )
));
