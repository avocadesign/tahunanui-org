(function($){

	FLBuilder.registerModuleHelper('advanced-tabs', {

		init: function()
		{
			var form    	= $('.fl-builder-settings'),
				icon_style = form.find('select[name=show_icon]'),
				style 	= form.find('select[name=style]');

			this._styleChanged();
			style.on('change', this._styleChanged);

			this._iconStyleChanged();
			icon_style.on('change', this._iconStyleChanged);
		},
		
		_styleChanged: function() {
			var form		= $('.fl-builder-settings'),
				style 	= form.find('select[name=style]').val(),
				tab_style 	= form.find('select[name=tab_style]').val();

			
			if( style != 'linebox' && style != 'iconfall' ){
				form.find('#fl-field-tab_style').show();
				if( tab_style == 'inline'  ){
					form.find('#fl-field-tab_style_alignment').show();
				} else {
					form.find('#fl-field-tab_style_alignment').hide();
				}

			} else {
				form.find('#fl-field-tab_style').hide();
				form.find('#fl-field-tab_style_alignment').hide();
			}

			if( style == 'iconfall' ) {
				form.find('#fl-field-show_icon').hide();
				form.find('#fl-field-icon_position').hide();
				form.find('#fl-field-icon_hover_color').hide();
				form.find('#fl-field-icon_active_color').hide();
				form.find('#fl-field-icon_color').show();
			} else {
				form.find('#fl-field-show_icon').show();
				var val = form.find('select[name=show_icon]').val();
				if( val == 'yes' ) {
					form.find('#fl-field-icon_position').show();
					form.find('#fl-field-icon_color').show();
					form.find('#fl-field-icon_hover_color').show();
					form.find('#fl-field-icon_active_color').show();
				} else {
					form.find('#fl-field-icon_position').hide();
					form.find('#fl-field-icon_color').hide();
					form.find('#fl-field-icon_hover_color').hide();
					form.find('#fl-field-icon_active_color').hide();
				}
			}
		},

		_iconStyleChanged: function() {
			var form		= $('.fl-builder-settings'),
				val = form.find('select[name=show_icon]').val(),
				style 	= form.find('select[name=style]').val();
			if( val == 'yes' ) {
				if( style == 'iconfall' ) {
					console.log('into iconfall');
					form.find('#fl-field-icon_color').show();
					form.find('#fl-field-icon_position').hide();
					form.find('#fl-field-icon_hover_color').hide();
					form.find('#fl-field-icon_active_color').hide();
				} else {
					form.find('#fl-field-icon_position').show();
					form.find('#fl-field-icon_color').show();
					form.find('#fl-field-icon_hover_color').show();
					form.find('#fl-field-icon_active_color').show();
				}
			} else {
				form.find('#fl-field-icon_position').hide();
				form.find('#fl-field-icon_color').hide();
				form.find('#fl-field-icon_hover_color').hide();
				form.find('#fl-field-icon_active_color').hide();
			}
		},
	});

})(jQuery);