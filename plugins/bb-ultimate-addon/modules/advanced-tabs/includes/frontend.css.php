<?php
    $settings->title_color = UABB_Helper::uabb_colorpicker( $settings, 'title_color' );
    $settings->title_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'title_hover_color' );
    $settings->title_active_color = UABB_Helper::uabb_colorpicker( $settings, 'title_active_color' );

    $settings->title_background_color = UABB_Helper::uabb_colorpicker( $settings, 'title_background_color', true );
    $settings->title_background_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'title_background_hover_color', true );
    $settings->title_active_background_color = UABB_Helper::uabb_colorpicker( $settings, 'title_active_background_color', true );
    
    $settings->underline_border_color = UABB_Helper::uabb_colorpicker( $settings, 'underline_border_color' );
    $settings->underline_separation_color = UABB_Helper::uabb_colorpicker( $settings, 'underline_separation_color' );
    
    $settings->icon_color = UABB_Helper::uabb_colorpicker( $settings, 'icon_color');
    $settings->icon_hover_color = UABB_Helper::uabb_colorpicker( $settings, 'icon_hover_color' );
    $settings->icon_active_color = UABB_Helper::uabb_colorpicker( $settings, 'icon_active_color' );

    $settings->content_color = UABB_Helper::uabb_colorpicker( $settings, 'content_color' );
    $settings->content_background_color = UABB_Helper::uabb_colorpicker( $settings, 'content_background_color', true );

    $settings->content_border_color = UABB_Helper::uabb_colorpicker( $settings, 'content_border_color' );

?>

 <?php
if( $settings->style != 'iconfall' && $settings->style != 'linebox' ) {
    if( $settings->tab_style == 'inline' ) {
?>
    .fl-node-<?php echo $id; ?> .uabb-tabs nav ul {
        <?php echo ( $settings->tab_style_alignment != 'left' ) ? ( ( $settings->tab_style_alignment != 'center' ) ? 'justify-content: flex-end;' : 'justify-content: center;' ) : 'justify-content: flex-start;'; ?>
        
    }
<?php
    } else {
?>
    .fl-node-<?php echo $id; ?> .uabb-tabs nav ul li {
        text-align: center;
        -webkit-flex: 1;
        -moz-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }
<?php
    }
?>

<?php
} else {
?>
    .fl-node-<?php echo $id; ?> .uabb-tabs nav ul li {
        text-align: center;
        -webkit-flex: 1;
        -moz-flex: 1;
        -ms-flex: 1;
        flex: 1;
    }
<?php
}
?>


<?php
if( $settings->style != 'iconfall' ) {
    if( $settings->icon_position == 'right' ) {
    ?>
    .fl-node-<?php echo $id; ?> .uabb-tabs ul li a {
        direction: rtl;
    }
    .fl-node-<?php echo $id; ?> .uabb-tabs ul li .uabb-tabs-icon {
        margin-left: 0.4em ;
        display: inline-block;
    }

    <?php
    } else if( $settings->icon_position == 'top' ) {
    ?>
    .fl-node-<?php echo $id; ?> .uabb-tabs ul li .uabb-tabs-icon {
        display: block;
        margin-bottom: 0.4em;
    }

    <?php
    } else if( $settings->icon_position == 'left' ) {
    ?>
    .fl-node-<?php echo $id; ?> .uabb-tabs ul li .uabb-tabs-icon {
        margin-right: 0.4em;
        display: inline-block;
    }
    <?php
    }
}
?>

.fl-node-<?php echo $id; ?> .uabb-tab-title {
    color: <?php echo uabb_theme_text_color( $settings->title_color ); ?>;
}

.fl-node-<?php echo $id; ?> .uabb-tabs ul li a {
    color: <?php echo uabb_theme_text_color( $settings->title_color ); ?>;
    <?php 
    if( $settings->title_font_family['family'] != 'Default' ) { 
        UABB_Helper::uabb_font_css( $settings->title_font_family );
    }
    echo ( $settings->title_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->title_font_size['desktop'] . 'px;' : '';
    //echo ( $settings->title_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->title_line_height['desktop'] . 'px;' : 'line-height: ' . ( $settings->title_font_size['desktop'] + 2 ) . 'px;';
    echo ( $settings->title_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->title_line_height['desktop'] . 'px;' : '';
    ?>
}

<?php
if( $settings->icon_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a .uabb-tabs-icon i {
    color: <?php echo $settings->icon_color; ?>;
}
<?php
}
if( $settings->style != 'iconfall' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a {
    <?php echo ( $settings->tab_padding != '' ) ? $settings->tab_padding : 'padding: 15px;'; ?>
}
<?php
}
if( $settings->style == 'underline' || $settings->style == 'topline' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a {
    <?php
    $border_size = ( $settings->underline_border_size != '' ) ? $settings->underline_border_size : 6;
    $base_padding = 0;
    if( isset( $settings->uabb_tab_padding ) ) {
        if( $settings->uabb_tab_padding['bottom'] != '' ) {
            $base_padding = $settings->uabb_tab_padding['bottom'] + $border_size;
        } else {
            $base_padding = $settings->uabb_tab_padding['all'] + $border_size;
        }
    } else if( isset( $settings->tab_padding ) ) {
        $base_padding = 15 + $border_size;
    }
    ?>
    <?php
    if( $settings->style == 'topline' && $settings->line_position == 'top' ) {
    ?>
    padding-top: <?php echo $base_padding; ?>px;
    <?php
    } else {
    ?>
    padding-bottom: <?php echo $base_padding; ?>px;
    <?php
    }
    ?>
}
<?php
}
?>


<?php
if( $settings->title_background_color != '' && $settings->style != 'iconfall' && $settings->style != 'simple' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li {
    background-color: <?php echo $settings->title_background_color; ?>;
}
<?php
}

if( $settings->title_hover_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a:hover,
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a:hover .uabb-tab-title {
    color: <?php echo $settings->title_hover_color; ?>;
}
<?php
}

if( $settings->title_background_hover_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a:hover {
    <?php
    echo ( $settings->title_background_hover_color != ''  && $settings->style != 'iconfall' && $settings->style != 'linebox' ) ? 'background-color:' . $settings->title_background_hover_color . ';' : '';
    ?>
}
<?php
}
if( $settings->icon_hover_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs ul li a:hover .uabb-tabs-icon i {
    <?php echo ( $settings->icon_hover_color != '' ) ? 'color:' . $settings->icon_hover_color . ';' : ''; ?>
}
<?php
}

if( $settings->title_active_color != '' ) {
?>

.fl-node-<?php echo $id; ?> .uabb-tabs ul .uabb-tab-current a .uabb-tab-title,
.fl-node-<?php echo $id; ?> .uabb-tabs ul .uabb-tab-current a:hover .uabb-tab-title {
    color: <?php echo $settings->title_active_color; ?>;
}

<?php
}

if( $settings->icon_active_color != '' ) {
?>

.fl-node-<?php echo $id; ?> .uabb-tabs ul li.uabb-tab-current a .uabb-tabs-icon i,
.fl-node-<?php echo $id; ?> .uabb-tabs ul li.uabb-tab-current a .uabb-tabs-icon i:hover,
.fl-node-<?php echo $id; ?> .uabb-tabs ul li.uabb-tab-current a:hover .uabb-tabs-icon i:hover {
    color : <?php echo $settings->icon_active_color; ?>;
}

<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content,
.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content-current {
    <?php
    echo ( $settings->content_padding != '' ) ? $settings->content_padding : 'padding: 12px;';
    $content_border_size = ( $settings->content_border_size != '' ) ? $settings->content_border_size : 1;
    echo ( $settings->content_border_style != 'none' ) ? 'border: ' . $content_border_size . 'px ' . $settings->content_border_style . ' ' . $settings->content_border_color . ';' : '';
    if( $settings->content_border_style == 'none' && $settings->content_border_radius != '' ) {
        echo 'border-radius: ' . $settings->content_border_radius . 'px;';
    }
    
    echo 'text-align: ' . $settings->content_alignment . ';';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-tabs ul li {
    <?php
    if( $settings->style == 'bar' ) {
        $tab_border_size = ( $settings->tab_border_size != '' ) ? $settings->tab_border_size : 1;
        echo ( $settings->tab_border_style != 'none' ) ? 'border: ' . $tab_border_size . 'px ' . $settings->tab_border_style . ' ' . $settings->tab_border_color . ';' : '';
        echo ( $settings->tab_border_radius != '' && $settings->tab_border_style == 'none' ) ? 'border-radius: ' . $settings->tab_border_radius . 'px;' : '';
    }
    ?>
}

<?php
if( $settings->tab_border_style == 'none' && $settings->tab_border_radius != '' ) {
?>

.fl-node-<?php echo $id; ?> .uabb-tabs li:first-child {
    border-top-left-radius: <?php echo $settings->tab_border_radius; ?>px;
    border-bottom-left-radius: <?php echo $settings->tab_border_radius; ?>px;
} 

.fl-node-<?php echo $id; ?> .uabb-tabs li:last-child { 
    border-top-right-radius: <?php echo $settings->tab_border_radius; ?>px; 
    border-bottom-right-radius: <?php echo $settings->tab_border_radius; ?>px;
}

<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content,
.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content-current,
.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content p,
.fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content-current p {
    color: <?php echo uabb_theme_text_color( $settings->content_color ); ?>;
    <?php
    if( $settings->content_font_family['family'] != 'Default' ) {
        UABB_Helper::uabb_font_css( $settings->content_font_family );        
    }
    echo ( $settings->content_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->content_font_size['desktop'] . 'px;' : '';
    echo ( $settings->content_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->content_line_height['desktop'] . 'px;' : '';
    ?>
}

.fl-node-<?php echo $id; ?> .uabb-content-wrap {
    background-color: <?php echo $settings->content_background_color; ?>;
    <?php echo ( $settings->content_border_radius != '' && $settings->content_border_style == 'none' ) ? 'border-radius: ' . $settings->content_border_radius . 'px;' : ''; ?>
}

/* Style Dependent CSS Start */
/* _____________________________________________________________________ */

/* Top Line Style */
/* _____________________________________________________________________ */

.fl-node-<?php echo $id; ?> .uabb-tabs-style-topline nav li.uabb-tab-current a {
    background: none;
    <?php
    $border_size = ( $settings->underline_border_size != '' ) ? $settings->underline_border_size : 6;
    $border_size = ( $settings->line_position == 'bottom' ) ? ( $border_size * -1 ) : $border_size;
    ?>
    <?php
    $color_default = ( uabb_theme_base_color( $settings->underline_border_color ) != '' ) ? uabb_theme_base_color( $settings->underline_border_color ) : '#a7a7a7';
    ?>
    box-shadow: inset 0 <?php echo $border_size; ?>px 0 <?php echo $color_default; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-tabs-style-topline nav li.uabb-tab-current {
    border-top-color: <?php echo uabb_theme_base_color( $color_default ); ?>;
}
<?php
if( $settings->title_hover_color != '' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-tabs-style-topline nav a:hover,
.fl-node-<?php echo $id; ?> .uabb-tabs-style-topline nav a:hover * {
    color: <?php echo $settings->title_hover_color; ?>;
}
<?php
}
?>

/* _____________________________________________________________________ */

/* Top Style Bar */
/* _____________________________________________________________________ */

.fl-node-<?php echo $id; ?> .uabb-tabs-style-bar ul li.uabb-tab-current a {
    color: <?php echo $settings->title_active_color; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-tabs-style-bar ul li.uabb-tab-current {
    <?php
    $color_default = ( uabb_theme_base_color( $settings->title_active_background_color ) != '' ) ? uabb_theme_base_color( $settings->title_active_background_color ) : '#a7a7a7';
    ?>
    background-color: <?php echo uabb_theme_base_color( $color_default ); ?>;
}
/* _____________________________________________________________________ */

/* Underline */
/* _____________________________________________________________________ */

.fl-node-<?php echo $id; ?> .uabb-tabs-style-underline nav li a::after {
    <?php
    $color_default = ( uabb_theme_base_color( $settings->underline_border_color ) != '' ) ? uabb_theme_base_color( $settings->underline_border_color ) : '#a7a7a7';
    ?>
    background: <?php echo $color_default; ?>;
    height: <?php echo ( $settings->underline_border_size != '' ) ? $settings->underline_border_size : 6 ; ?>px;
}

.fl-node-<?php echo $id; ?> .uabb-tabs-style-underline nav a {
    border-left: 1px solid <?php echo $settings->underline_separation_color; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-tabs-style-underline nav li:last-child a {
    border-right: 1px solid <?php echo $settings->underline_separation_color; ?>;
}

/* _____________________________________________________________________ */

/* Icon Fall */
/* _____________________________________________________________________ */

.fl-node-<?php echo $id; ?> .uabb-tabs-style-iconfall nav li::before {
    <?php
    $color_default = ( uabb_theme_base_color( $settings->underline_border_color ) != '' ) ? uabb_theme_base_color( $settings->underline_border_color ) : '#a7a7a7';
    ?>
    background: <?php echo $color_default; ?>;
    height: <?php echo ( $settings->underline_border_size != '' ) ? $settings->underline_border_size : 6 ; ?>px;
}


/* _____________________________________________________________________ */

/* Line Box */
/* _____________________________________________________________________ */

.fl-node-<?php echo $id; ?> .uabb-tabs-style-linebox nav a::after,
.fl-node-<?php echo $id; ?> .uabb-tabs-style-linebox nav a:hover::after,
.fl-node-<?php echo $id; ?> .uabb-tabs-style-linebox nav a:focus::after,
.fl-node-<?php echo $id; ?> .uabb-tabs-style-linebox nav li.uabb-tab-current a::after {
    <?php
    $color_default = ( uabb_theme_base_color( $settings->title_background_hover_color ) != '' ) ? uabb_theme_base_color( $settings->title_background_hover_color ) : '#a7a7a7';
    ?>
    background: <?php echo $color_default; ?>;
}


/* _____________________________________________________________________ */

/* Style Dependent CSS End */
/* _____________________________________________________________________ */

<?php
if( $global_settings->responsive_enabled ) { // Global Setting If started
?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {

        .fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content p,
        .fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content-current p {
            <?php
            echo ( $settings->content_font_size['medium'] != '' ) ? 'font-size: ' . $settings->content_font_size['medium'] . 'px;' : '';
            echo ( $settings->content_line_height['medium'] != '' ) ? 'line-height: ' . $settings->content_line_height['medium'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-tabs ul li a .uabb-tab-title {
            <?php
            echo ( $settings->title_font_size['medium'] != '' ) ? 'font-size: ' . $settings->title_font_size['medium'] . 'px;' : '';
            //echo ( $settings->title_line_height['medium'] != '' ) ? 'line-height: ' . $settings->title_line_height['medium'] . 'px;' : 'line-height: ' . ( $settings->title_font_size['medium'] + 2 ) . 'px;';
            echo ( $settings->title_line_height['medium'] != '' ) ? 'line-height: ' . $settings->title_line_height['medium'] . 'px;' : '';
            ?>
        }
        
    }
 
     @media ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {

        .fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content p,
        .fl-node-<?php echo $id; ?> .uabb-content-wrap .uabb-content-current p {
            <?php
            echo ( $settings->content_font_size['small'] != '' ) ? 'font-size: ' . $settings->content_font_size['small'] . 'px;' : '';
            echo ( $settings->content_line_height['small'] != '' ) ? 'line-height: ' . $settings->content_line_height['small'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-tabs ul li a .uabb-tab-title {
            <?php
            echo ( $settings->title_font_size['small'] != '' ) ? 'font-size: ' . $settings->title_font_size['small'] . 'px;' : '';
            //echo ( $settings->title_line_height['small'] != '' ) ? 'line-height: ' . $settings->title_line_height['small'] . 'px;' : 'line-height: ' . ( $settings->title_font_size['small'] + 2 ) . 'px;';
            echo ( $settings->title_line_height['small'] != '' ) ? 'line-height: ' . $settings->title_line_height['small'] . 'px;' : '';
            ?>
        }

    }
<?php
}
?>