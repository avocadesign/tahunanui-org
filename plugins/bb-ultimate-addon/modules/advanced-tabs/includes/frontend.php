<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */
//echo '<pre>'; print_r($module); echo '</pre>';
//echo '<pre>'; print_r($settings); echo '</pre>';
global $wp_embed;
?>
<section>
	<div class="uabb-tabs uabb-tabs-style-<?php echo $settings->style; ?>">
		<nav>
			<ul>
				<?php
				for($i = 0; $i < count($settings->items); $i++) : if(!is_object($settings->items[$i])) continue;
					$class = ( $settings->show_icon == 'yes' ) ? '<span class="uabb-tabs-icon"><i class= " ' . $settings->items[$i]->tab_icon . '"></i></span>' : ( ( $settings->style == 'iconfall' ) ? '<span  class="uabb-tabs-icon"><iuabb-tabs-icon class= " ' . $settings->items[$i]->tab_icon . '"></i></span>' : '' );
				?>
				<li><a href="#section-<?php echo $settings->style; ?>-<?php echo $i; ?>" class=""><?php echo $class; ?><span class="uabb-tab-title"><?php echo $settings->items[$i]->label; ?></span></a></li>
				<?php endfor; ?>
			</ul>
		</nav>
		<div class="uabb-content-wrap">
			<?php for($i = 0; $i < count($settings->items); $i++) : if(!is_object($settings->items[$i])) continue; ?>
			<section class="uabb-content uabb-text-editor" id="section-<?php echo $settings->style; ?>-<?php echo $i; ?>"><?php echo wpautop( $wp_embed->autoembed( $settings->items[$i]->content ) ); ?></section>
			<?php endfor; ?>
		</div><!-- /content -->
	</div><!-- /tabs -->
</section>