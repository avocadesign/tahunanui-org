<?php 
    $settings->color = UABB_Helper::uabb_colorpicker( $settings, 'color' );
?>
.fl-node-<?php echo $id; ?> {
	width: 100%
}

.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading {
	text-align: <?php echo $settings->alignment; ?>;
	<?php if($settings->font_size == 'custom') : ?>
	font-size: <?php echo $settings->custom_font_size; ?>px;
	<?php endif; ?>
}
<?php if(!empty($settings->color)) : ?>
.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading a,
.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading .uabb-heading-text,
.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading .uabb-heading-text * {
	color: <?php echo $settings->color; ?>;
}
<?php endif; ?>
<?php if( !empty($settings->font) && $settings->font['family'] != 'Default' ) : ?>
.fl-node-<?php echo $id; ?> .uabb-heading .uabb-heading-text{
	<?php UABB_Helper::uabb_font_css( $settings->font ); ?>
}
<?php endif; ?>
<?php if($global_settings->responsive_enabled && ($settings->r_font_size == 'custom' || $settings->r_alignment == 'custom')) : ?>
@media (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	
	<?php if($settings->r_font_size == 'custom') : ?>
	.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading {
		font-size: <?php echo $settings->r_custom_font_size; ?>px;
	}
	<?php endif; ?>
	
	<?php if($settings->r_alignment == 'custom') : ?>
	.fl-node-<?php echo $id; ?> <?php echo $settings->tag; ?>.uabb-heading {
		text-align: <?php echo $settings->r_custom_alignment; ?>;
	}
	<?php endif; ?>
}    
<?php endif; ?>
