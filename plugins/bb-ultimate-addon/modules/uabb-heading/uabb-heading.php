<?php

/**
 * @class UABBHeadingModule
 */
class UABBHeadingModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Heading', 'uabb'),
			'description'   	=> __('Display a title/page heading.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/uabb-heading/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/uabb-heading/',
            'partial_refresh'	=> true
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBHeadingModule', array(
	'general'       => array(
		'title'         => __('General', 'uabb'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'heading'        => array(
						'type'            => 'text',
						'label'           => __('Heading', 'uabb'),
						'default'         => '',
						'preview'         => array(
							'type'            => 'text',
							'selector'        => '.uabb-heading-text'
						)
					)
				)
			),
			'link'          => array(
				'title'         => __('Link', 'uabb'),
				'fields'        => array(
					'link'          => array(
						'type'          => 'link',
						'label'         => __('Link', 'uabb'),
						'preview'         => array(
							'type'            => 'none'
						)
					),
					'link_target'   => array(
						'type'          => 'select',
						'label'         => __('Link Target', 'uabb'),
						'default'       => '_self',
						'options'       => array(
							'_self'         => __('Same Window', 'uabb'),
							'_blank'        => __('New Window', 'uabb')
						),
						'preview'         => array(
							'type'            => 'none'
						)
					)
				)
			)
		)
	),
	'style'         => array(
		'title'         => __('Style', 'uabb'),
		'sections'      => array(
			'colors'        => array(
				'title'         => __('Colors', 'uabb'),
				'fields'        => array(
					'color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Text Color', 'uabb'),
							'preview'		=> array(
								'type' => 'css',
								'property' => 'color',
								'selector' => '.uabb-heading  .uabb-heading-text'
							)
						)
                    ),
				)
			),
			'structure'     => array(
				'title'         => __('Structure', 'uabb'),
				'fields'        => array(
					'alignment'     => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'left'      =>  __('Left', 'uabb'),
							'center'    =>  __('Center', 'uabb'),
							'right'     =>  __('Right', 'uabb')
						),
						'preview'         => array(
							'type'            => 'css',
							'selector'        => '.uabb-heading',
							'property'        => 'text-align'
						)
					),
					'tag'           => array(
						'type'          => 'select',
						'label'         => __( 'HTML Tag', 'uabb' ),
						'default'       => 'h3',
						'options'       => array(
							'h1'            =>  'h1',
							'h2'            =>  'h2',
							'h3'            =>  'h3',
							'h4'            =>  'h4',
							'h5'            =>  'h5',
							'h6'            =>  'h6'
						)
					),
					'font'          => array(
						'type'          => 'font',
						'default'		=> array(
							'family'		=> 'Default',
							'weight'		=> 300
						),
						'label'         => __('Font', 'uabb'),
						'preview'         => array(
							'type'            => 'font',
							'selector'        => '.uabb-heading-text'
						)
					),
					'font_size'     => array(
						'type'          => 'select',
						'label'         => __('Font Size', 'uabb'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'uabb'),
							'custom'        =>  __('Custom', 'uabb')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('custom_font_size')
							)
						)
					),
					'custom_font_size' => array(
						'type'          => 'text',
						'label'         => __('Custom Font Size', 'uabb'),
						'default'       => '24',
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px'
					)
				)
			),
			'r_structure'   => array(
				'title'         => __( 'Mobile Structure', 'uabb' ),
				'fields'        => array(
					'r_alignment'   => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'uabb'),
							'custom'        =>  __('Custom', 'uabb')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('r_custom_alignment')
							)
						),
						'preview'         => array(
							'type'            => 'none'
						)
					),
					'r_custom_alignment' => array(
						'type'          => 'select',
						'label'         => __('Custom Alignment', 'uabb'),
						'default'       => 'center',
						'options'       => array(
							'left'      =>  __('Left', 'uabb'),
							'center'    =>  __('Center', 'uabb'),
							'right'     =>  __('Right', 'uabb')
						),
						'preview'         => array(
							'type'            => 'none'
						)
					),
					'r_font_size'   => array(
						'type'          => 'select',
						'label'         => __('Font Size', 'uabb'),
						'default'       => 'default',
						'options'       => array(
							'default'       =>  __('Default', 'uabb'),
							'custom'        =>  __('Custom', 'uabb')
						),
						'toggle'        => array(
							'custom'        => array(
								'fields'        => array('r_custom_font_size')
							)
						),
						'preview'         => array(
							'type'            => 'none'
						)
					),
					'r_custom_font_size' => array(
						'type'          => 'text',
						'label'         => __('Custom Font Size', 'uabb'),
						'default'       => '24',
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
						'preview'         => array(
							'type'            => 'none'
						)
					)
				)
			)
		)
	)
));
