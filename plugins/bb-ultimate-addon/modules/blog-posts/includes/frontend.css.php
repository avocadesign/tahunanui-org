<?php

$settings->content_background_color = UABB_Helper::uabb_colorpicker( $settings, 'content_background_color', true );

$settings->arrow_color = UABB_Helper::uabb_colorpicker( $settings, 'arrow_color' );
$settings->arrow_background_color = UABB_Helper::uabb_colorpicker( $settings, 'arrow_background_color', true);
$settings->arrow_color_border = UABB_Helper::uabb_colorpicker( $settings, 'arrow_color_border' );

$settings->date_color = UABB_Helper::uabb_colorpicker( $settings, 'date_color' );
$settings->date_background_color = UABB_Helper::uabb_colorpicker( $settings, 'date_background_color', true );
$settings->meta_color = UABB_Helper::uabb_colorpicker( $settings, 'meta_color' );

$settings->link_color = UABB_Helper::uabb_colorpicker( $settings, 'link_color' );
$settings->link_more_arrow_color = UABB_Helper::uabb_colorpicker( $settings, 'link_more_arrow_color' );


?>

<?php
if( $settings->cta_type == 'button' ) {

	FLBuilder::render_module_css('uabb-button', $id , array(
		/* General Section */
        'text'              => $settings->btn_text,
        
        /* Link Section */
        'link'              => $settings->btn_link,
        'link_target'       => $settings->btn_link_target,
        
        /* Style Section */
        'style'             => $settings->btn_style,
        'border_size'       => $settings->btn_border_size,
        'transparent_button_options' => $settings->btn_transparent_button_options,
        'threed_button_options'      => $settings->btn_threed_button_options,
        'flat_button_options'        => $settings->btn_flat_button_options,

        /* Colors */
        'bg_color'          => $settings->btn_bg_color,
        'bg_color_opc'          => $settings->btn_bg_color_opc,
        'bg_hover_color'    => $settings->btn_bg_hover_color,
        'bg_hover_color_opc'    => $settings->btn_bg_hover_color_opc,
        'text_color'        => $settings->btn_text_color,
        'text_hover_color'  => $settings->btn_text_hover_color,

        /* Icon */
        'icon'              => $settings->btn_icon,
        'icon_position'     => $settings->btn_icon_position,
        
        /* Structure */
        'width'              => $settings->btn_width,
        'custom_width'       => $settings->btn_custom_width,
        'custom_height'      => $settings->btn_custom_height,
        'padding_top_bottom' => $settings->btn_padding_top_bottom,
        'padding_left_right' => $settings->btn_padding_left_right,
        'border_radius'      => $settings->btn_border_radius,
        'align'              => $settings->overall_alignment,
        'mob_align'          => '',

        /* Typography */
        'font_size'         => $settings->btn_font_size,
        'line_height'       => $settings->btn_line_height,
        'font_family'       => $settings->btn_font_family,
	));

}
?>
<?php
if( $settings->is_carousel == 'feed' ) {
?>
.fl-node-<?php echo $id; ?> .uabb-blog-posts .uabb-post-thumbnail img {
     width: 100%;
}
<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-blog-posts .uabb-post-wrapper {
	<?php
	if( $settings->is_carousel == 'feed' ) {
		echo ( $settings->element_space != '' ) ? 'margin-bottom: ' . ( $settings->element_space ) . 'px;' : 'margin-bottom: 15px;';
	} else {
		echo ( $settings->element_space != '' ) ? 'padding-left: ' . ( $settings->element_space / 2 ) . 'px;' : 'padding-left: 7.5px;';
		echo ( $settings->element_space != '' ) ? 'padding-right: ' . ( $settings->element_space / 2 ) . 'px;' : 'padding-right: 7.5px;';
	}
	?>
}

.fl-node-<?php echo $id; ?> .uabb-post-wrapper .uabb-content {
	<?php echo $settings->content_padding; ?>
	<?php echo ( $settings->content_background_color != '' ) ? 'background: ' . $settings->content_background_color : ''; ?>
}

.fl-node-<?php echo $id; ?> .uabb-posted-on {
	<?php
	$color = uabb_theme_base_color( $settings->date_background_color );
	$date_background_color = ( $color != '' ) ? $color : '#EFEFEF';
	?>
	background: <?php echo $date_background_color; ?>;
	<?php $left = ( $settings->is_carousel != 'feed' ) ? ( ( $settings->element_space != '' ) ? $settings->element_space / 2 : 7.5 ) : 0; ?>
	left: <?php echo $left; ?>px;
	
}

<?php
if( $settings->meta_color != '' ) {
?>
	.fl-node-<?php echo $id; ?> .uabb-content .uabb-post-meta a,
	.fl-node-<?php echo $id; ?> .uabb-content .uabb-post-meta a:hover,
	.fl-node-<?php echo $id; ?> .uabb-content .uabb-post-meta a:focus,
	.fl-node-<?php echo $id; ?> .uabb-content .uabb-post-meta a:active {
		color: <?php echo $settings->meta_color; ?>;
	}
<?php
}

if( $settings->show_box_shadow == 'yes' ) {
?>
	.fl-node-<?php echo $id; ?> .uabb-blog-posts-shadow {
    	box-shadow: 0 4px 1px rgba(197, 197, 197, 0.2);
	}

<?php
}


if( $settings->is_carousel == 'grid' ) {
?>
@media all and ( min-width: 992px ) {
	.fl-node-<?php echo $id; ?> .uabb-post-wrapper:nth-child(<?php echo $settings->post_per_grid; ?>n+1){
		clear: left;
		padding-left: 0;
	}
	.fl-node-<?php echo $id; ?> .uabb-post-wrapper:nth-child(<?php echo $settings->post_per_grid; ?>n+0) {
		clear: right;
		padding-right: 0;
	}
	.fl-node-<?php echo $id; ?> .uabb-post-wrapper:nth-child(<?php echo $settings->post_per_grid; ?>n+1) .uabb-posted-on {
		left: 0;
	}
}

<?php
}
?>


.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text span,
.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text:visited * {
	color: <?php echo ( uabb_theme_base_color( $settings->link_more_arrow_color ) == '' ) ? '#f7f7f7' : uabb_theme_base_color( $settings->link_more_arrow_color ); ?>;
}

<?php
if( $settings->is_carousel == 'carousel' ) {
?>
.fl-node-<?php echo $id; ?> .slick-prev i,
.fl-node-<?php echo $id; ?> .slick-next i,
.fl-node-<?php echo $id; ?> .slick-prev i:hover,
.fl-node-<?php echo $id; ?> .slick-next i:hover,
.fl-node-<?php echo $id; ?> .slick-prev i:focus,
.fl-node-<?php echo $id; ?> .slick-next i:focus {
	outline: none;
	<?php
	$color = uabb_theme_base_color( $settings->arrow_color );
	$arrow_color = ( $color != '' ) ? $color : '#fff';
	?>
	color: <?php echo $arrow_color; ?>;
	<?php
	switch ( $settings->arrow_style ) {
		case 'square':
	?>
	background: <?php echo $settings->arrow_background_color; ?>;
	<?php
			break;
		
		case 'circle':
	?>
	border-radius: 50%;
	background: <?php echo $settings->arrow_background_color; ?>;
	<?php
			break;

		case 'square-border':
	?>
	border: <?php echo $settings->arrow_border_size; ?>px solid <?php echo $settings->arrow_color_border ?>;
	<?php
			break;

		case 'circle-border':
	?>
	border: <?php echo $settings->arrow_border_size; ?>px solid <?php echo $settings->arrow_color_border ?>;
	border-radius: 50%;
	<?php
			break;
	}
	?>
}


/*.fl-node-<?php echo $id; ?> .uabb-blog-posts-carousel .slick-active:first-of-type {
    padding-left: 0;
}
.fl-node-<?php echo $id; ?> .uabb-blog-posts-carousel .slick-active:last-of-type {
    padding-right: 0;
}*/

<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-content {
	text-align: <?php echo $settings->overall_alignment; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text,
.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a,
.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:visited,
.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:hover {
	<?php
	echo 'color: ' . uabb_theme_text_color( $settings->link_color ) . ';' ;
	echo ( $settings->link_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->link_line_height['desktop'] . 'px;' : '';
	echo ( $settings->link_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->link_font_size['desktop'] . 'px;' : '';
	
	if( $settings->link_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->link_font_family );
	}
	?>
}

.fl-node-<?php echo $id; ?> .uabb-entry-date span {
	<?php echo 'color: ' . uabb_theme_text_color( $settings->date_color ) . ';' ; ?>
}

.fl-node-<?php echo $id; ?> .uabb-blog-posts-description {
	<?php
	echo 'color: ' . uabb_theme_text_color( $settings->desc_color ) . ';' ;
	echo ( $settings->desc_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->desc_line_height['desktop'] . 'px;' : '';
	echo ( $settings->desc_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->desc_font_size['desktop'] . 'px;' : '';
	
	if( $settings->desc_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->desc_font_family );
	}
	
	?>
}

.fl-node-<?php echo $id; ?> .uabb-post-heading,
.fl-node-<?php echo $id; ?> .uabb-post-heading a,
.fl-node-<?php echo $id; ?> .uabb-post-heading a:hover,
.fl-node-<?php echo $id; ?> .uabb-post-heading a:focus,
.fl-node-<?php echo $id; ?> .uabb-post-heading a:visited {
	<?php
	echo ( $settings->title_color != '' ) ? 'color: ' . $settings->title_color . ';' : '';
	echo ( $settings->title_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->title_line_height['desktop'] . 'px;' : '';
	echo ( $settings->title_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->title_font_size['desktop'] . 'px;' : '';

	if( $settings->title_font_family['family'] != 'Default' ) {
		UABB_Helper::uabb_font_css( $settings->title_font_family );
	}
	?>
}


/* Need to delete <?php
if( $settings->is_carousel != 'carousel' ) {
?>

@media all and ( max-width: 992px ) {
	.fl-node-<?php echo $id; ?> .uabb-post-wrapper {
		width: 100%;
		padding-bottom: 50px;
	}
	.fl-node-<?php echo $id; ?> .uabb-blog-posts .uabb-post-thumbnail img {
	    width: 100%;
	}
}

<?php
}
?> Delete upto here*/

<?php
if( $global_settings->responsive_enabled ) { // Global Setting If started
?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {
    	<?php
    	if( $settings->link_line_height['medium'] != '' || $settings->link_font_size['medium'] != '' ) {
    	?>

    	.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:visited,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:hover {
			<?php
			echo ( $settings->link_line_height['medium'] != '' ) ? 'line-height: ' . $settings->link_line_height['medium'] . 'px;' : '';
			echo ( $settings->link_font_size['medium'] != '' ) ? 'font-size: ' . $settings->link_font_size['medium'] . 'px;' : '';
			?>
		}
		<?php
		}

		if( $settings->desc_line_height['medium'] != '' || $settings->desc_font_size['medium'] != '' ) {
		?>

		.fl-node-<?php echo $id; ?> .uabb-blog-posts-description {
			<?php
			echo ( $settings->desc_line_height['medium'] != '' ) ? 'line-height: ' . $settings->desc_line_height['medium'] . 'px;' : '';
			echo ( $settings->desc_font_size['medium'] != '' ) ? 'font-size: ' . $settings->desc_font_size['medium'] . 'px;' : '';
			?>
		}

		<?php
		}

		if( $settings->title_line_height['medium'] != '' || $settings->title_font_size['medium'] != '' ) {
		?>

		.fl-node-<?php echo $id; ?> .uabb-post-heading,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:hover,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:focus,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:visited {
			<?php
			echo ( $settings->title_line_height['medium'] != '' ) ? 'line-height: ' . $settings->title_line_height['medium'] . 'px;' : '';
			echo ( $settings->title_font_size['medium'] != '' ) ? 'font-size: ' . $settings->title_font_size['medium'] . 'px;' : '';
			?>
		}

		<?php
		}
		?>

    }
 
     @media ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {

     	<?php
    	if( $settings->link_line_height['small'] != '' || $settings->link_font_size['small'] != '' ) {
    	?>
     	.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:visited,
		.fl-node-<?php echo $id; ?> .uabb-content .uabb-read-more-text a:hover {
			<?php
			echo ( $settings->link_line_height['small'] != '' ) ? 'line-height: ' . $settings->link_line_height['small'] . 'px;' : '';
			echo ( $settings->link_font_size['small'] != '' ) ? 'font-size: ' . $settings->link_font_size['small'] . 'px;' : '';
			?>
		}
		<?php
		}

		if( $settings->desc_line_height['small'] != '' || $settings->desc_font_size['small'] != '' ) {
		?>

		.fl-node-<?php echo $id; ?> .uabb-blog-posts-description {
			<?php
			echo ( $settings->desc_line_height['small'] != '' ) ? 'line-height: ' . $settings->desc_line_height['small'] . 'px;' : '';
			echo ( $settings->desc_font_size['small'] != '' ) ? 'font-size: ' . $settings->desc_font_size['small'] . 'px;' : '';
			?>
		}

		<?php
		}

		if( $settings->title_line_height['small'] != '' || $settings->title_font_size['small'] != '' ) {
		?>

		.fl-node-<?php echo $id; ?> .uabb-post-heading,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:hover,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:focus,
		.fl-node-<?php echo $id; ?> .uabb-post-heading a:visited {
			<?php
			echo ( $settings->title_line_height['small'] != '' ) ? 'line-height: ' . $settings->title_line_height['small'] . 'px;' : '';
			echo ( $settings->title_font_size['small'] != '' ) ? 'font-size: ' . $settings->title_font_size['small'] . 'px;' : '';
			?>
		}
		<?php
		}
		?>
    }
<?php
}
?>