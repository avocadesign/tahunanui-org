<?php
//echo "<pre>"; print_r($settings); echo "</pre>";

$args = $module->render_args();
$the_query = new WP_Query( $args );
//echo "<pre>"; print_r($the_query); echo "</pre>";
$col = ( $settings->is_carousel != 'carousel' ) ? ( ( $settings->is_carousel == 'feed' ) ? 1 : $settings->post_per_grid ) : $settings->post_per_grid_desktop;

?>
<div class="uabb-blog-posts <?php echo ( $settings->is_carousel == 'carousel' ) ? 'uabb-blog-posts-carousel' : ''; ?> uabb-post-grid-<?php echo $col; ?>">
	<?php
	for ( $i=0; $i < $the_query->post_count; $i++ ) {
		setup_postdata( $the_query->posts[$i] );
	?>
	<div class="uabb-blog-posts-col-<?php echo $col; ?> uabb-post-wrapper">
		<div class="uabb-blog-posts-shadow">
			<?php
			$show_featured_image = ( isset( $settings->show_featured_image ) ) ? $settings->show_featured_image : 'yes';
			if( $show_featured_image == 'yes' ) {
				$img_url = $module->render_image_url( $i, $the_query->posts[$i]->ID );
				
				if ( $settings->is_carousel == 'carousel' && $settings->lazyload == 'yes' ) {
					$img_url = 'data-lazy="'.$img_url.'"';
				}else{
					$img_url = 'src="'.$img_url.'"';
				}
			?>

			<div class="uabb-post-thumbnail">
				<img <?php echo $img_url; ?> />
				<?php
				if( $settings->show_date_box == 'yes' ) {
				?>
				<div class="uabb-next-date-meta">
					<span class="uabb-posted-on">
						<time class="uabb-entry-date uabb-published uabb-updated" datetime="<?php echo date( 'c', strtotime( $the_query->posts[$i]->post_date ) ); ?>">
							<span class="uabb-date-month"><?php echo date( 'M', strtotime( $the_query->posts[$i]->post_date ) ); ?></span>
							<span class="uabb-date-day"><?php echo date( 'j', strtotime( $the_query->posts[$i]->post_date ) ); ?></span>
							<span class="uabb-date-year"><?php echo date( 'Y', strtotime( $the_query->posts[$i]->post_date ) ); ?></span>
						</time>
					</span>
				</div>
				<?php
				}
				?>
			</div>

			<?php
			}
			?>

			<div class="uabb-content">
				<<?php echo $settings->title_tag_selection; ?> class="uabb-post-heading">
					<a href="<?php echo get_permalink( $the_query->posts[$i]->ID ); ?>" title="<?php echo $the_query->posts[$i]->post_title; ?>" tabindex="0" class=""><?php echo $the_query->posts[$i]->post_title; ?></a>
				</<?php echo $settings->title_tag_selection; ?>>
				
				<?php
				$show_meta = ( isset( $settings->show_meta ) ) ? $settings->show_meta : 'yes';
				$show_author = ( isset( $settings->show_author ) ) ? $settings->show_author : 'yes';
				$show_categories = ( isset( $settings->show_categories ) ) ? $settings->show_categories : 'yes';
				$show_tags = ( isset( $settings->show_tags ) ) ? $settings->show_tags : 'yes';
				$show_comments = ( isset( $settings->show_comments ) ) ? $settings->show_comments : 'yes';
				$show_date = ( isset( $settings->show_date ) ) ? $settings->show_date : 'yes';

				if( $show_meta == 'yes' ) {
					if( $show_author == 'yes' || $show_categories == 'yes' || $show_tags == 'yes' || $show_comments == 'yes'  ) {
				?>
				<div class="uabb-post-meta">
				<?php
				if( $show_author == 'yes' ) {
				?>
					By <span class="uabb-posted-by"> <a class="url fn n" href="<?php echo get_author_posts_url( $the_query->posts[$i]->post_author ); ?>"><?php

						$author = ( get_the_author_meta( 'display_name', $the_query->posts[$i]->post_author ) != '' ) ? get_the_author_meta( 'display_name', $the_query->posts[$i]->post_author ) : get_the_author_meta( 'user_nicename', $the_query->posts[$i]->post_author );

						echo $author; ?></a>
					</span> 
					<?php
				}
				if( $show_date == 'yes' ){
					echo ( $show_author == 'yes' ) ? ' / ' : '';
				?>
					<span class="uabb-meta-date"><?php echo date( 'M', strtotime( $the_query->posts[$i]->post_date ) ) . ' ' . date( 'j', strtotime( $the_query->posts[$i]->post_date ) ) . ' ' . date( 'Y', strtotime( $the_query->posts[$i]->post_date ) ); ?>
					</span>
				<?php
				}
				if( $show_categories == 'yes' ) {
					$post_type = ( isset( $setting->post_type ) ) ? $settings->post_type : 'post';
					$object_taxonomies = get_object_taxonomies( $post_type );
					if( !empty( $object_taxonomies ) ) {
						$category_detail = wp_get_post_terms( $the_query->posts[$i]->ID, $object_taxonomies[0] );
						//echo '<pre>'; print_r($category_detail); echo '</pre>';
						//$category_detail = get_the_category( $the_query->posts[$i]->ID );
						if( count( $category_detail ) > 0 ) {
							echo ( $show_author == 'yes' || $show_date == 'yes' ) ? ' / ' : '';
							for( $j = 0; $j < count( $category_detail ); $j++ ){
						?><span class="uabb-cat-links"><a href="<?php echo site_url(); ?>/<?php echo $object_taxonomies[0]; ?>/<?php echo $category_detail[$j]->slug; ?>/" rel="category tag"><?php echo $category_detail[$j]->name; ?></a></span><?php
								echo ( $j+1 != count( $category_detail ) ) ? trim(',&nbsp;') : '';
							}
						}
					}
				}
				if( $show_tags == 'yes' ) {

					$tag_detail = get_the_tags( $the_query->posts[$i]->ID );
					if( $tag_detail != '' ) {
						echo ( $show_author == 'yes' || $show_categories == 'yes' || $show_date == 'yes' ) ? ' / ' : '';
						for( $k = 0; $k < count( $tag_detail ); $k++ ){
					?><span class="uabb-tag-links"><a href="<?php echo site_url(); ?>/tag/<?php echo $tag_detail[$k]->slug; ?>/" rel="category tag"><?php echo $tag_detail[$k]->name; ?></a></span><?php
							echo ( $k+1 != count( $tag_detail ) ) ? trim(',&nbsp;') : '';
						}
					}
				}
				if( $show_comments == 'yes' ) {

					if( $the_query->posts[$i]->comment_count > 0 ) {
						echo ( $show_author == 'yes' || $show_categories == 'yes' || $show_tags == 'yes' || $show_date == 'yes' ) ? ' / ' : '';
					?><span class="uabb-comments-link"><a href="<?php echo get_permalink( $the_query->posts[$i]->ID ); ?>#comments"><?php echo $the_query->posts[$i]->comment_count; ?> Comment<?php echo ( $the_query->posts[$i]->comment_count > 1 ) ? 's' : ''; ?></a></span>
					<?php
					}
				}
					?>
				</div>
				<?php
					}
				}
				$txt = ( $the_query->posts[$i]->post_excerpt != '' ) ? $the_query->posts[$i]->post_excerpt : $the_query->posts[$i]->post_content;
				$txt = do_shortcode ( $txt );
				$content = strip_tags( wp_trim_words ( $txt ) );
				
				$excerpt_count = strlen( $content );
				if( isset( $settings->excerpt_count ) ) {
					$excerpt_count = ( $settings->excerpt_count != '' ) ? $settings->excerpt_count : strlen( $content );
				} else {
					$excerpt_count = 300;
				}
				?>
				<div class="uabb-blog-posts-description"><?php echo ( strlen( $content ) <= $excerpt_count ) ? $content : substr( $content, 0, $excerpt_count ) . '....'; ?></div>
				<?php
				$link = get_permalink( $the_query->posts[$i]->ID );
				$module->render_button( $link, $settings->link_target );
				?>
			</div>
		</div>
	</div>
	<?php
	}
	wp_reset_postdata();
	?>
	
</div>