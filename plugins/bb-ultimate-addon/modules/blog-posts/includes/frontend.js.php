<?php
if( $settings->is_carousel == 'carousel' ) {
?>
	jQuery(document).ready( function() {

		jQuery( '.fl-node-<?php echo $id ?>' ).find( '.uabb-blog-posts-carousel' ).uabbslick({
			dots: false,
			infinite: <?php echo ( $settings->infinite_loop == 'yes' ) ? 'true' : 'false'; ?>,
			arrows: true,
			lazyLoad: 'ondemand',
			slidesToShow: <?php echo ( $settings->post_per_grid_desktop != '' ) ? $settings->post_per_grid_desktop : 1; ?>,
			slidesToScroll: 1,
			autoplay: <?php echo ( $settings->autoplay == 'yes' ) ? 'true' : 'false'; ?>,
	  		autoplaySpeed: <?php echo ( $settings->animation_speed != '' ) ? $settings->animation_speed : '1000'; ?>,
  			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: <?php echo ( $settings->post_per_grid_medium != '' ) ? $settings->post_per_grid_medium : 1; ?>
					}
				},
			    {
					breakpoint: 768,
					settings: {
						slidesToShow: <?php echo ( $settings->post_per_grid_small != '' ) ? $settings->post_per_grid_small : 1; ?>
					}
			    }
  			]
		});
	});
		
<?php
}
?>