(function($){

    FLBuilder.registerModuleHelper('blog-posts', {

        init: function()
        {
            var form        = $('.fl-builder-settings'),
                is_carousel = form.find('select[name=is_carousel]'),
                img_size = form.find('select[name=featured_image_size]'),
                show_featured_image = form.find('select[name=show_featured_image]'),
                carousal_type  = form.find('select[name=total_posts_switch]');

            carousal_type.on('change', $.proxy( this._toggle, this ) );
            is_carousel.on('change', $.proxy( this._toggleSection, this ) );
            show_featured_image.on('change', $.proxy( this._toggleFeaturedImageSection, this ) );
            img_size.on('change', $.proxy( this._toggleImageSize, this ) );
            $( this._toggleImageSize, this );
            $( this._toggleFeaturedImageSection, this );
            $( this._toggle, this );
            $( this._toggleSection, this );
        },

        _toggle: function() {
            var form        = $('.fl-builder-settings'),
                carousal_type    = form.find('select[name=total_posts_switch]').val();
            if( carousal_type == 'all' ) {
                form.find('#fl-field-total_posts').hide();
            } else {
                form.find('#fl-field-total_posts').show();
            }
        },

        _toggleImageSize: function() {
            var form        = $('.fl-builder-settings'),
                featured_image_size    = form.find('select[name=featured_image_size]').val();
            if( featured_image_size == 'custom' ) {
                form.find('#fl-field-featured_image_size_width').show();
                form.find('#fl-field-featured_image_size_height').show();
            } else {
                form.find('#fl-field-featured_image_size_width').hide();
                form.find('#fl-field-featured_image_size_height').hide();
            }
        },

        _toggleFeaturedImageSection: function() {
            var form        = $('.fl-builder-settings'),
                custom    = form.find('select[name=featured_image_size]').val(),
                show_featured_image    = form.find('select[name=show_featured_image]').val();
            if( show_featured_image == 'yes' ) {
                form.find('#fl-field-featured_image_size').show();
                if( custom == 'custom' ) {
                    form.find('#fl-field-featured_image_size_width').show();
                    form.find('#fl-field-featured_image_size_height').show();
                } else {
                    form.find('#fl-field-featured_image_size_width').hide();
                    form.find('#fl-field-featured_image_size_height').hide();
                }
            } else {
                form.find('#fl-field-featured_image_size').hide();
                form.find('#fl-field-featured_image_size_width').hide();
                form.find('#fl-field-featured_image_size_height').hide();
            }
        },

        _toggleSection: function() {
            var form        = $('.fl-builder-settings'),
                is_carousel    = form.find('select[name=is_carousel]').val(),
                carousal_type    = form.find('select[name=total_posts_switch]').val();
            if( is_carousel == 'grid' ) {
                form.find('#fl-builder-settings-section-carousel_filter').hide();
                form.find('#fl-builder-settings-section-carousel_style').hide();
                form.find('#fl-builder-settings-section-filter').show();
                form.find('#fl-field-post_per_grid').show();

                if( carousal_type == 'all' ) {
                    form.find('#fl-field-total_posts').hide();
                } else {
                    form.find('#fl-field-total_posts').show();
                }
            } else if( is_carousel == 'feed' ) {
                form.find('#fl-builder-settings-section-carousel_filter').hide();
                form.find('#fl-builder-settings-section-carousel_style').hide();
                form.find('#fl-field-post_per_grid').hide();
                form.find('#fl-builder-settings-section-filter').show();

                if( carousal_type == 'all' ) {
                    form.find('#fl-field-total_posts').hide();
                } else {
                    form.find('#fl-field-total_posts').show();
                }
            } else {
                form.find('#fl-builder-settings-section-carousel_filter').show();
                form.find('#fl-builder-settings-section-carousel_style').show();
                form.find('#fl-builder-settings-section-filter').hide();
            }
        },

    });

})(jQuery);
