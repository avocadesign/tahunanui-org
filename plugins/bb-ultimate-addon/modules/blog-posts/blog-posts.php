<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class BlogPostsModule
 */
class BlogPostsModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public static $post_types;

    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Blog Posts', 'uabb'),
            'description'   => __('Blog Posts', 'uabb'),
            'category'      => __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/blog-posts/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/blog-posts/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
        $this->add_css( 'font-awesome' );
        BlogPostsModule::$post_types = get_post_types( array( 'exclude_from_search' => false ) );
        
    }

    /**
     * @method _get_editor
     * @protected
     */
    protected function _get_editor( $url )
    {
        $url_path  = $url;
        $file_path = str_ireplace(home_url(), ABSPATH, $url_path);

        if(file_exists($file_path)) {
            $this->_editor = wp_get_image_editor($file_path);
        }else {
            $this->_editor = wp_get_image_editor($url_path);
        }

        return $this->_editor;
    }

    /**
     * @method _get_cropped_path
     * @protected
     */
    protected function _get_cropped_path( $i, $url )
    {
        $crop        = 'custom_crop';
        $cache_dir   = FLBuilderModel::get_cache_dir();

        if(empty($url)) {
            $filename    = uniqid(); // Return a file that doesn't exist.
        }
        else {
            
            if ( stristr( $url, '?' ) ) {
                $parts = explode( '?', $url );
                $url   = $parts[0];
            }
            
            $pathinfo    = pathinfo($url);
            $dir         = $pathinfo['dirname'];
            $ext         = $pathinfo['extension'];
            $name        = wp_basename($url, ".$ext");
            $new_ext     = strtolower($ext);
            $filename    = "{$name}-{$crop}.{$new_ext}";
        }

        return array(
            'filename' => $filename,
            'path'     => $cache_dir['path'] . $filename,
            'url'      => $cache_dir['url'] . $filename
        );
    }

    /**
     * @method delete
     */
    public function deleteUrl( $i, $url )
    {
        $cropped_path = $this->_get_cropped_path( $i, $url );
        if(file_exists($cropped_path['path'])) {
            unlink($cropped_path['path']);
        }
    }


    /**
     * @method crop
     */
    public function crop( $i, $url, $width, $height )
    {
        // Delete an existing crop if it exists.
        $this->deleteUrl( $i, $url );

        $editor = $this->_get_editor( $url );

        if(!$editor || is_wp_error($editor)) {
            return false;
        }

        $cropped_path = $this->_get_cropped_path( $i, $url );
        $new_width    = $width;
        $new_height   = $height;


        // Make sure we have enough memory to crop.
        @ini_set('memory_limit', '300M');

        // Crop the photo.
        $editor->resize($new_width, $new_height, true);

        // Save the photo.
        $editor->save($cropped_path['path']);
        // Return the new url.
        return $cropped_path['url'];
    }

    public function render_args() {

        $args['post_type'] = ( isset( $this->settings->post_type ) ) ? $this->settings->post_type : 'post';

        $show_total_post = ( $this->settings->is_carousel == 'carousel' ) ? '-1' : ( ( $this->settings->total_posts_switch == 'all' ) ? '-1' : $this->settings->total_posts );

        $taxonomies = get_object_taxonomies( $args['post_type'], 'objects' );
        $data       = array();

        foreach($taxonomies as $tax_slug => $tax) {

            if(!$tax->public || !$tax->show_ui) {
                continue;
            }
            
            $data[$tax_slug] = $tax;
        }

        $taxonomies = $data;

        foreach($taxonomies as $tax_slug => $tax) {
                    
            $tax_value = '';

            // New settings slug.
            if(isset($this->settings->{'tax_' . $args['post_type'] . '_' . $tax_slug})) {
                $tax_value = $this->settings->{'tax_' . $args['post_type'] . '_' . $tax_slug};
            }
            // Legacy settings slug.
            else if(isset($this->settings->{'tax_' . $tax_slug})) {
                $tax_value = $this->settings->{'tax_' . $tax_slug};
            }
                
            if(!empty($tax_value)) {
             
                $args['tax_query'][] = array(
                    'taxonomy'  => $tax_slug,
                    'field'     => 'id',
                    'terms'     => explode(',', $tax_value)
                );
            }
        }

        $post_type_var = 'posts_' . $args['post_type'];
        
        $args['post_status'] = 'any';
        $args['order'] = ( isset( $this->settings->order ) ) ? $this->settings->order : '';
        $args['orderby'] = ( isset( $this->settings->orderby ) ) ? $this->settings->orderby : '';
        $args['showposts'] = ( $this->settings->is_carousel == 'carousel' ) ? '-1' : ( ( $this->settings->total_posts_switch == 'all' ) ? '-1' : $this->settings->total_posts );
        $args['author'] = ( isset( $this->settings->users ) ) ? $this->settings->users : '';
        if( isset( $this->settings->$post_type_var ) ) {
            if( $this->settings->$post_type_var != '' ) {
                $args['post__in'] = explode( ',', $this->settings->$post_type_var );
            }
        }
        return $args;
    }

    /**
     * @method render_button
     */
    public function render_button( $link = '', $link_target = '_blank' ) {

        if( $this->settings->cta_type == 'button' ) {

            $btn_settings = array(
                /* General Section */
                'text'              => $this->settings->btn_text,
                
                /* Link Section */
                'link'              => $link,
                'link_target'       => $link_target,
                
                /* Style Section */
                'style'             => $this->settings->btn_style,
                'border_size'       => $this->settings->btn_border_size,
                'transparent_button_options' => $this->settings->btn_transparent_button_options,
                'threed_button_options'      => $this->settings->btn_threed_button_options,
                'flat_button_options'        => $this->settings->btn_flat_button_options,

                /* Colors */
                'bg_color'          => $this->settings->btn_bg_color,
                'bg_hover_color'    => $this->settings->btn_bg_hover_color,
                'text_color'        => $this->settings->btn_text_color,
                'text_hover_color'  => $this->settings->btn_text_hover_color,

                /* Icon */
                'icon'              => $this->settings->btn_icon,
                'icon_position'     => $this->settings->btn_icon_position,
                
                /* Structure */
                'width'              => $this->settings->btn_width,
                'custom_width'       => $this->settings->btn_custom_width,
                'custom_height'      => $this->settings->btn_custom_height,
                'padding_top_bottom' => $this->settings->btn_padding_top_bottom,
                'padding_left_right' => $this->settings->btn_padding_left_right,
                'border_radius'      => $this->settings->btn_border_radius,
                'align'             => $this->settings->overall_alignment,
                'mob_align'          => '',
                
                /* Typography */
                'font_size'         => $this->settings->btn_font_size,
                'line_height'       => $this->settings->btn_line_height,
                'font_family'       => $this->settings->btn_font_family,
            );

            FLBuilder::render_module_html('uabb-button', $btn_settings);

            //FLBuilder::render_module_html( 'uabb-button', $btn_settings );
        } else if( $this->settings->cta_type == 'link' ) {
            echo '<span class="uabb-read-more-text"><a href="' . $link . '" target="' . $link_target . '" >' . $this->settings->cta_text . ' <span class="uabb-next-right-arrow">&#8594;</span></a></span>';
        }     
    }

    public function render_image_url( $i, $post_attachment_id ) {
        $id = -1;

        $id = get_post_thumbnail_id( $post_attachment_id );

        $size = ( isset( $this->settings->featured_image_size ) ) ? $this->settings->featured_image_size : 'full';

        if( $id != -1 && $id != "" ) {
            if( $size != 'custom' ) {
                $temp = wp_get_attachment_image_src( $id, $size );
                $image = $temp[0]; 
            } else {
                $temp = wp_get_attachment_image_src( $id, 'full' );
                $image = $this->crop( $i, $temp[0], $this->settings->featured_image_size_width, $this->settings->featured_image_size_height );
            }
        } else {
            if( $size == 'custom' ) {
                $image = $this->crop( $i, BB_ULTIMATE_ADDON_URL . 'modules/blog-posts/images/placeholder.jpg', $this->settings->featured_image_size_width, $this->settings->featured_image_size_height );
            } else {
                $image = BB_ULTIMATE_ADDON_URL . 'modules/blog-posts/images/placeholder.jpg';
            }
        }

        return $image;
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('BlogPostsModule', array(
    'general'       => array( // Tab
        'title'         => __('General', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('', 'uabb'), // Section Title
                'fields'        => array(
                    'is_carousel'     => array(
                        'type'          => 'select',
                        'label'         => __( 'Show in Carousel', 'uabb' ),
                        'default'       => 'carousel',
                        'options'       => array(
                            'carousel'  => 'Carousel',
                            'grid'    => 'Grid',
                            'feed'    => 'Feeds',
                        ),
                        /*'toggle' => array(
                            'carousel' => array(
                                'sections' => array( 'carousel_filter', 'carousel_style' )
                            ),
                            'grid' => array(
                                'fields' => array( 'total_posts_switch', 'total_posts', 'post_per_grid' ),
                                'sections' => array( 'filter' )
                            ),
                            'feed' => array(
                                'fields' => array( 'total_posts_switch', 'total_posts' ),
                                'sections' => array( 'filter' )
                            )
                        )*/
                    ),
                )
            ),
            'carousel_filter' => array(
                'title' => __( 'Carousel Filter', 'uabb' ),
                'fields' =>  array(
                    'infinite_loop'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Infinite Loop', 'uabb' ),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                    ),
                    'post_per_grid_desktop' => array(
                        'type'          => 'select',
                        'label' => __('Grid Column - Desktop', 'uabb'),
                        'default' => '3',
                        'options'       => array(
                            '1'      => __('1 Column', 'uabb'),
                            '2'      => __('2 Column', 'uabb'),
                            '3'      => __('3 Column', 'uabb'),
                            '4'      => __('4 Column', 'uabb'),
                        ),
                    ),
                    'post_per_grid_medium' => array(
                        'type'          => 'select',
                        'label' => __('Grid Column - Tabs', 'uabb'),
                        'default' => '2',
                        'options'       => array(
                            '1'      => __('1 Column', 'uabb'),
                            '2'      => __('2 Column', 'uabb'),
                            '3'      => __('3 Column', 'uabb'),
                            '4'      => __('4 Column', 'uabb'),
                        ),
                    ),
                    'post_per_grid_small' => array(
                        'type'          => 'select',
                        'label' => __('Grid Column - Mobile', 'uabb'),
                        'default' => '1',
                        'options'       => array(
                            '1'      => __('1 Column', 'uabb'),
                            '2'      => __('2 Column', 'uabb'),
                            '3'      => __('3 Column', 'uabb'),
                            '4'      => __('4 Column', 'uabb'),
                        ),
                    ),
                    'lazyload'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Enable Lazy Load', 'uabb' ),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                    ),
                    'autoplay'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Autoplay Slides', 'uabb' ),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                        'toggle' => array(
                            'yes' => array(
                                'fields' => array( 'animation_speed' )
                            )
                        )
                    ),
                    'animation_speed' => array(
                        'type' => 'text',
                        'label' => __('Animation Speed', 'uabb'),
                        'placeholder' => '1000',
                        'size' => '8',
                        'description' => 'ms'
                    ),
                )
            ),
            'filter' => array(
                'title' => __( 'Grid Settings', 'uabb' ),
                'fields' => array(
                    'total_posts_switch'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Total Posts', 'uabb' ),
                        'default'       => 'all',
                        'options'       => array(
                            'all'       => 'All',
                            'custom'    => 'Custom',
                        ),
                        'toggle'    => array(
                            'custom'    => array(
                                'fields'    => array( 'total_posts' ),
                            ),
                            'all'    => array(
                                'fields'    => array(),
                            )
                        ),
                    ),
                    'total_posts' => array(
                        'type' => 'text',
                        'label' => __('Custom Post Count', 'uabb'),
                        'size' => '8',
                    ),
                    'post_per_grid' => array(
                        'type'          => 'select',
                        'label' => __('Grid Column', 'uabb'),
                        'default' => '2',
                        'options'       => array(
                            '1'      => __('1 Column', 'uabb'),
                            '2'      => __('2 Column', 'uabb'),
                            '3'      => __('3 Column', 'uabb'),
                            '4'      => __('4 Column', 'uabb'),
                        ),
                    ),
                )
            )
        )
    ),
    'post_type_filter' => array(
        'title'         => __('Content', 'fl-builder'),
        'file'          => BB_ULTIMATE_ADDON_DIR . 'includes/loop-settings.php',
    ),
    'style'       => array( // Tab
        'title'         => __('Style', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'alignment' => array(
                'title' => __( '', 'uabb' ),
                'fields' => array(
                    'overall_alignment' => array(
                        'type'          => 'select',
                        'label' => __('Overall Alignment', 'uabb'),
                        'default'       => 'center',
                        'options'       => array(
                            'center' => __( 'Center', 'uabb' ),
                            'left' => __( 'Left', 'uabb' ),
                            'right' => __( 'Right', 'uabb' ),
                        ),
                    ),
                    'element_space' => array(
                        'type' => 'text',
                        'label' => __('Space between Elements', 'uabb'),
                        'size' => '8',
                        'placeholder' => '15',
                        'description' => 'px',
                        'help'         => __('Spcing between two grids', 'uabb')
                    ),
                    'show_box_shadow'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Show Box Shadow', 'uabb' ),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                    ),
                    'show_date_box'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Show Date Box', 'uabb' ),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'       => 'Yes',
                            'no'        => 'No',
                        ),
                    ),
                )
            ),
            'style' => array(
                'title' => __( 'Content Style', 'uabb' ),
                'fields' => array(
                    'content_padding' => array(
                        'type'      => 'uabb-spacing',
                        'label'     => __( 'Content Padding', 'uabb' ),
                        'default'   => 'padding: 25px;',    //optional
                        'mode'      => 'padding',
                    ),
                    'content_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Content Background Color', 'uabb'),
                        )
                    ),
                    'content_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                )
            ),
            'carousel_style' => array(
                'title' => __( 'Carousel Styles', 'uabb' ),
                'fields' => array(
                    'arrow_style'       => array(
                        'type'          => 'select',
                        'label'         => __('Arrow Style', 'fl-builder'),
                        'default'       => 'circle',
                        'options'       => array(
                            'square'             => __('Square Background', 'fl-builder'),
                            'circle'             => __('Circle Background', 'fl-builder'),
                            'square-border'      => __('Square Border', 'fl-builder'),
                            'circle-border'      => __('Circle Border', 'fl-builder')
                        ),
                        'toggle'        => array(
                            'square-border' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_border', 'arrow_border_size' )
                            ),
                            'circle-border' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_border', 'arrow_border_size' )
                            ),
                            'square' => array(
                                'fields'        => array( 'arrow_color', 'arrow_background_color', 'arrow_background_color_opc' )
                            ),
                            'circle' => array(
                                'fields'        => array( 'arrow_color', 'arrow_background_color', 'arrow_background_color_opc' )
                            ),
                        )
                    ),
                    'arrow_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Arrow Color', 'uabb'),
                        )
                    ),
                    'arrow_background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Arrow Background Color', 'uabb'),
                            'default'       => '#efefef',
                        )
                    ),
                    'arrow_background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'arrow_color_border' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Arrow Border Color', 'fl-builder'),
                        )
                    ),                  
                    'arrow_border_size'       => array(
                        'type'          => 'text',
                        'label'         => __('Border Size', 'fl-builder'),
                        'default'       => '1',
                        'description'   => 'px',
                        'size'          => '8',
                        'max_lenght'    => '3'
                    ),
                )
            ),
        ),
    ),
    'cta'           => array(
        'title'         => __('Link', 'uabb'),
        'sections'      => array(
            'cta'           => array(
                'title'         => __('Call to Action', 'uabb'),
                'fields'        => array(
                    'cta_type'      => array(
                        'type'          => 'select',
                        'label'         => __('Type', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => _x( 'None', 'Call to action.', 'uabb' ),
                            'link'          => __('Text', 'uabb'),
                            'button'        => __('Button', 'uabb'),
                        ),
                        'toggle'        => array(
                            'none'          => array(),
                            'link'          => array(
                                'fields'        => array( 'cta_text' ),
                                'sections'      => array( 'link_typography' )
                            ),
                            'button'        => array(
                                'sections'      => array( 'btn-general', 'btn-colors', 'btn-icon', 'btn-style', 'btn-structure', 'btn_typography')
                            ),

                        )
                    ),
                    'cta_text'      => array(
                        'type'          => 'text',
                        'label'         => __('Text', 'uabb'),
                        'default'       => __('Read More', 'uabb'),
                    ),
                )
            ),
            'btn-general'    => BB_Ultimate_Addon::uabb_section_get( 'btn-general' ),
            'btn-link'          => array(
                'title'         => __('Link', 'uabb'),
                'fields'        => array(
                    'link_target'   => array(
                        'type'          => 'select',
                        'label'         => __('Link Target', 'uabb'),
                        'default'       => '_self',
                        'options'       => array(
                            '_self'         => __('Same Window', 'uabb'),
                            '_blank'        => __('New Window', 'uabb')
                        ),
                        'preview'       => array(
                            'type'          => 'none'
                        )
                    )
                )
            ),
            /*'btn-link'       => BB_Ultimate_Addon::uabb_section_get( 'btn-link' ),*/
            'btn-style'      => BB_Ultimate_Addon::uabb_section_get( 'btn-style' ),
            'btn-icon'       => BB_Ultimate_Addon::uabb_section_get( 'btn-icon' ),
            'btn-colors'     => BB_Ultimate_Addon::uabb_section_get( 'btn-colors' ),
            'btn-structure'  => BB_Ultimate_Addon::uabb_section_get( 'btn-structure', array(), array('btn_align', 'btn_mob_align') ),
        )
    ),
    'typography'       => array( // Tab
        'title'         => __('Typography', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Title', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-post-heading'
                            )
                        ),
                        'tag_selection'   => array(
                            'default'       => 'h3',
                        ),
                    ), 
                ),
                array(),
                'title'
            ),
            'desc_typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                    'title' => __('Description', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-blog-posts-description'
                            )
                        ),
                        'color' => array(
                            'label'         => __('Description Color', 'uabb'),
                        ),
                    ),
                ), 
                array('tag_selection'),
                'desc'
            ),
            'btn_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __( 'Button Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => 'a.uabb-button'
                            ),
                        ),
                    ),
                ), 
                array( 'color','tag_selection' ),
                'btn'
            ),
            'date_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __( 'Date', 'uabb' ),
                    'fields'   => array(
                        'color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Date Color', 'uabb'),
                            )
                        ),
                        'background_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Date Background Color', 'uabb'),
                            )
                        ),
                        'background_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    ),
                 ), 
                array( 'tag_selection', 'line_height', 'font_family', 'font_size' ),
                'date'
            ),
            'link_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 'title' => __( 'Link Text', 'uabb' ),
                    'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-infobox-cta-link'
                            ),
                        ),
                        'color' => array(
                            'label'         => __('Link Color', 'uabb'),
                        ),
                        'more_arrow_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Arrow Color', 'uabb'),
                            )
                        ),
                    ),
                 ), 
                array( 'tag_selection' ),
                'link'
            ),
        )
    ),
));
