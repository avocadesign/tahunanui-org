<?php

/**
 * @class FLHtmlModule
 */
class UABBContactFormModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'           	=> __('Contact Form', 'uabb'),
			'description'    	=> __('A very simple contact form.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/uabb-contact-form/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/uabb-contact-form/',
			'editor_export'  	=> false,
			'partial_refresh'	=> true
		));

		add_action('wp_ajax_uabb_builder_email', array($this, 'send_mail'));
		add_action('wp_ajax_nopriv_uabb_builder_email', array($this, 'send_mail'));
	}

	/**
	 * @method send_mail
	 */
	static public function send_mail($params = array()) {

    	global $uabb_contact_from_name, $uabb_contact_from_email;

		// Get the contact form post data

		$subject = (isset($_POST['subject']) ? $_POST['subject'] : __('Contact Form Submission', 'uabb'));
		$mailto = get_option('admin_email');

		if ( isset($settings->mailto_email) && !empty($settings->mailto_email) ) {
			$mailto   = $settings->mailto_email;
		}

		$uabb_contact_from_email = (isset($_POST['email']) ? $_POST['email'] : null);
		$uabb_contact_from_name = (isset($_POST['name']) ? $_POST['name'] : null);

		add_filter('wp_mail_from', 'UABBContactFormModule::mail_from');
		add_filter('wp_mail_from_name', 'UABBContactFormModule::from_name');

		// Build the email
		$template = "";

		if (isset($_POST['name']))  $template .= "Name: $_POST[name] \r\n";
		if (isset($_POST['email'])) $template .= "Email: $_POST[email] \r\n";
		if (isset($_POST['phone'])) $template .= "Phone: $_POST[phone] \r\n";

		$template .= __('Message', 'uabb') . ": \r\n" . $_POST['message'];

		// Double check the mailto email is proper and send
		if ($mailto) {
			wp_mail($mailto, $subject, $template);
			die('1');
		} else {
			die($mailto);
		}
	}

	static public function mail_from($original_email_address) {
		global $uabb_contact_from_email;
		return ($uabb_contact_from_email != '') ? $uabb_contact_from_email : $original_email_address;
	}

	static public function from_name($original_name) {
		global $uabb_contact_from_name;
		return ($uabb_contact_from_name != '') ? $uabb_contact_from_name : $original_name;
	}

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBContactFormModule', array(
	'general'       => array(
		'title'         => __('General', 'uabb'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'mailto_email'     => array(
						'type'          => 'text',
						'label'         => __('Send To Email', 'uabb'),
						'default'       => '',
						'placeholder'   => __('example@mail.com', 'uabb'),
						'help'          => __('The contact form will send to this e-mail', 'uabb'),
						'preview'       => array(
							'type'          => 'none'
						)
					),
				)
			),
			'name_section'       => array(
				'title'         => __('Name Field'),
				'fields'        => array(
					'name_toggle'   => array(
						'type'          => 'select',
						'label'         => __('Name', 'uabb'),
						'default'       => 'show',
						'options'       => array(
							'show'      => __('Show', 'uabb'),
							'hide'      => __('Hide', 'uabb'),
						),
						'toggle'		=> array(
							'show'		=> array(
								'fields'	=> array( 'name_width', 'name_label', 'name_placeholder' ),
							)
						)
					),
					'name_width'   => array(
						'type'          => 'select',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'options'       => array(
							'100'      	=> __('100%', 'uabb'),
							'50'      	=> __('50%', 'uabb'),
						)
					),
					'name_label'          => array(
						'type'          => 'text',
						'label'         => __('Label', 'uabb'),
						'default'       => __('Name', 'uabb'),
					),
					'name_placeholder'          => array(
						'type'          => 'text',
						'label'         => __('Placeholder', 'uabb'),
						'default'       => __('Name', 'uabb'),
					),
				)
			),
			'email_section'       => array(
				'title'         => __('Email Field'),
				'fields'        => array(
					'email_toggle'   => array(
						'type'          => 'select',
						'label'         => __('Email', 'uabb'),
						'default'       => 'show',
						'options'       => array(
							'show'      => __('Show', 'uabb'),
							'hide'      => __('Hide', 'uabb'),
						),
						'toggle'		=> array(
							'show'		=> array(
								'fields'	=> array( 'email_width', 'email_label', 'email_placeholder' ),
							)
						)
					),
					'email_width'   => array(
						'type'          => 'select',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'options'       => array(
							'100'      	=> __('100%', 'uabb'),
							'50'      	=> __('50%', 'uabb'),
						)
					),
					'email_label'          => array(
						'type'          => 'text',
						'label'         => __('Label', 'uabb'),
						'default'       => __('Email', 'uabb'),
					),
					'email_placeholder'          => array(
						'type'          => 'text',
						'label'         => __('Placeholder', 'uabb'),
						'default'       => __('Email', 'uabb'),
					),
				)
			),
			'subject_section'       => array(
				'title'         => __('Subject Field'),
				'fields'        => array(
					'subject_toggle'   => array(
						'type'          => 'select',
						'label'         => __('Subject', 'uabb'),
						'default'       => 'hide',
						'options'       => array(
							'show'      => __('Show', 'uabb'),
							'hide'      => __('Hide', 'uabb'),
						),
						'toggle'		=> array(
							'show'		=> array(
								'fields'	=> array( 'subject_width', 'subject_label', 'subject_placeholder' ),
							)
						)
					),
					'subject_width'   => array(
						'type'          => 'select',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'options'       => array(
							'100'      	=> __('100%', 'uabb'),
							'50'      	=> __('50%', 'uabb'),
						)
					),
					'subject_label'          => array(
						'type'          => 'text',
						'label'         => __('Label', 'uabb'),
						'default'       => __('Subject', 'uabb'),
					),
					'subject_placeholder'          => array(
						'type'          => 'text',
						'label'         => __('Placeholder', 'uabb'),
						'default'       => __('Subject', 'uabb'),
					),
				)
			),
			'phone_section'       => array(
				'title'         => __('Phone Field'),
				'fields'        => array(
					'phone_toggle'   => array(
						'type'          => 'select',
						'label'         => __('Phone', 'uabb'),
						'default'       => 'hide',
						'options'       => array(
							'show'      => __('Show', 'uabb'),
							'hide'      => __('Hide', 'uabb'),
						),
						'toggle'		=> array(
							'show'		=> array(
								'fields'	=> array( 'phone_width', 'phone_label', 'phone_placeholder' ),
							)
						)
					),
					'phone_width'   => array(
						'type'          => 'select',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'options'       => array(
							'100'      	=> __('100%', 'uabb'),
							'50'      	=> __('50%', 'uabb'),
						)
					),
					'phone_label'          => array(
						'type'          => 'text',
						'label'         => __('Label', 'uabb'),
						'default'       => __('Phone', 'uabb'),
					),
					'phone_placeholder'          => array(
						'type'          => 'text',
						'label'         => __('Placeholder', 'uabb'),
						'default'       => __('Phone', 'uabb'),
					),
				)
			),
			'msg_section'       => array(
				'title'         => __('Message Field'),
				'fields'        => array(
					'msg_toggle'   => array(
						'type'          => 'select',
						'label'         => __('Message', 'uabb'),
						'default'       => 'show',
						'options'       => array(
							'show'      => __('Show', 'uabb'),
							'hide'      => __('Hide', 'uabb'),
						),
						'toggle'		=> array(
							'show'		=> array(
								'fields'	=> array( 'msg_width' ),
							)
						)
					),
					'msg_width'   => array(
						'type'          => 'select',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'options'       => array(
							'100'      	=> __('100%', 'uabb'),
							'50'      	=> __('50%', 'uabb'),
						)
					),
					'msg_label'          => array(
						'type'          => 'text',
						'label'         => __('Label', 'uabb'),
						'default'       => __('Your Message', 'uabb'),
					),
					'msg_placeholder'          => array(
						'type'          => 'text',
						'label'         => __('Placeholder', 'uabb'),
						'default'       => __('Message', 'uabb'),
					),
				)
			),
			'success'       => array(
				'title'         => __( 'Success', 'uabb' ),
				'fields'        => array(
					'success_action' => array(
						'type'          => 'select',
						'label'         => __( 'Success Action', 'uabb' ),
						'options'       => array(
							'none'          => __( 'None', 'uabb' ),
							'show_message'  => __( 'Show Message', 'uabb' ),
							'redirect'      => __( 'Redirect', 'uabb' )
						),
						'toggle'        => array(
							'show_message'       => array(
								'fields'        => array( 'success_message' )
							),
							'redirect'      => array(
								'fields'        => array( 'success_url' )
							)
						),
						'preview'       => array(
							'type'             => 'none'
						)
					),
					'success_message' => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'rows'          => 8,
						'default'       => __( 'Thanks for your message! We’ll be in touch soon.', 'uabb' ),
						'preview'       => array(
							'type'             => 'none'
						)
					),
					'success_url'  => array(
						'type'          => 'link',
						'label'         => __( 'Success URL', 'uabb' ),
						'preview'       => array(
							'type'             => 'none'
						)
					)
				)
			)
		)
	),
	'style'       => array(
		'title'         => __('Style', 'uabb'),
		'sections'      => array(
			'form-general'       => array(
				'title'         => '',
				'fields'        => array(
					'form_style'   => array(
						'type'          => 'select',
						'label'         => __('Form Style', 'uabb'),
						'default'       => 'style1',
						'options'       => array(
							'style1'      => __('Style 1', 'uabb'),
							'style2'      => __('Style 2', 'uabb'),
						),
						'toggle'		=> array(
							'style1'	  => array(
								'fields'	=> array( 'enable_label' )
							)
						)
					),
					'enable_label'   => array(
						'type'          => 'select',
						'label'         => __('Enable Label', 'uabb'),
						'default'       => 'yes',
						'options'       => array(
							'yes'     => __('Yes', 'uabb'),
							'no'      => __('No', 'uabb'),
						)
					),
					'enable_placeholder'   => array(
						'type'          => 'select',
						'label'         => __('Enable Placeholder', 'uabb'),
						'default'       => 'yes',
						'options'       => array(
							'yes'     => __('Yes', 'uabb'),
							'no'      => __('No', 'uabb'),
						)
					),
				)
			),
			'input-fields'       => array(
				'title'         => __('Form Fields', 'uabb'),
				'fields'        => array(
					'input_text_align'   => array(
						'type'          => 'select',
						'label'         => __('Text Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'left'      => __('Left', 'uabb'),
							'center'    => __('Center', 'uabb'),
							'right'    => __('Right', 'uabb'),
						)
					),
					
					'input_text_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Text Color', 'uabb'),
						)
                    ),
                    
                    'input_background_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Color', 'uabb'),
						)
                    ),
                    'input_background_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'input_border_width'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Width', 'uabb'),
		                'default'       => '',
		                'placeholder'	=> '2',
		                'description'   => 'px',
		                'maxlength'     => '2',
		                'size'          => '6',
		            ),
                    
                    'input_border_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Border Color', 'uabb'),
						)
                    ),
                    'input_border_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'input_border_active_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Border Active Color', 'uabb'),
                        	'preview'		=> array(
	                        	'type'	=> 'none'
	                        )
						)
                    ),
                    'input_border_active_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity', array(
                        	'preview'		=> array(
	                        	'type'	=> 'none'
	                        )
						) 
                    ),
                    
				)
			),
			'button-style'       => array(
				'title'         => __('Submit Button', 'uabb'),
				'fields'        => array(
					'btn_text'	=> array(
						'type'          => 'text',
						'label'         => __('Text', 'uabb'),
						'default'       => 'SEND A MESSAGE',
					),
					'btn_align'   => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'full'      => __('Full', 'uabb'),
							'left'      => __('Left', 'uabb'),
							'center'    => __('Center', 'uabb'),
							'right'    => __('Right', 'uabb'),
						)
					),
					'btn_style'   => array(
						'type'          => 'select',
						'label'         => __('Style', 'uabb'),
						'default'       => 'flat',
						'options'       => array(
							'flat'      	=> __('Flat', 'uabb'),
							'transparent'   => __('Transparent', 'uabb'),
							'gradient'    	=> __('Gradient', 'uabb'),
							'3d'    		=> __('3D', 'uabb'),
						),
						'toggle'		=> array(
							'flat'        => array(
								'fields'	=> array( 'btn_background_hover_color', 'btn_text_hover_color' )
							),
							'transparent' => array( 
								'fields'	=> array( 'btn_border_width', 'btn_background_hover_color', 'btn_text_hover_color' )
							),
							'gradient'	  => array( 
								'fields'	=> array( 'btn_background_hover_color', 'btn_text_hover_color' )
							),
						)
					),
					'btn_border_width'	=> array(
						'type'          => 'text',
						'label'         => __('Border Width', 'uabb'),
						'default'       => '2',
						'maxlength'     => '3',
						'size'          => '6',
						'description'   => 'px',
					),
                   
                    'btn_background_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Color', 'uabb'),
						)
                    ),
                    'btn_background_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
                    'btn_text_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Text Color', 'uabb'),
						)
                    ),

                    'btn_background_hover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Hover Color', 'uabb'),
                        	'preview'		=> array(
	                        	'type'	=> 'none'
	                        )
						)
                    ),
                    'btn_background_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity', array(
                        	'preview'		=> array(
	                        	'type'	=> 'none'
	                        )
						) 
                    ),
                    
                    'btn_text_hover_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Text Hover Color', 'uabb'),
                        	'preview'		=> array(
	                        	'type'	=> 'none'
	                        )
						)
                    ),
                    'btn_radius'	=> array(
						'type'          => 'text',
						'label'         => __('Border Radius', 'uabb'),
						'default'       => '',
						'maxlength'     => '3',
						'size'          => '6',
						'placeholder'	=> '4',
						'description'   => 'px',
					),
					'btn_vertical_padding'	=> array(
						'type'          => 'text',
						'label'         => __('Vertical Padding', 'uabb'),
						'default'       => '',
						'maxlength'     => '4',
						'size'          => '6',
						'description'   => 'px',
						//'placeholder'	=> '12',
					),
					'btn_horizontal_padding'	=> array(
						'type'          => 'text',
						'label'         => __('Horizontal Padding', 'uabb'),
						'default'       => '',
						'maxlength'     => '4',
						'size'          => '6',
						'description'   => 'px',
						//'placeholder'	=> '24',
					),
				)
			),
		)
	),
	'typography'         => array(
		'title'         => __('Typography', 'uabb'),
		'sections'      => array(
			'input_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Input Text', 'uabb' ),
					'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => 'input, textarea'
                            ),
                        ),
                        'font_size' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => 'input, textarea',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => 'input, textarea',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        ),
                        'input_top_margin'   => array(
							'type'          => 'text',
							'label'         => __('Input Top Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '0',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
						'input_bottom_margin'   => array(
							'type'          => 'text',
							'label'         => __('Input Bottom Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '10',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
						'textarea_top_margin'   => array(
							'type'          => 'text',
							'label'         => __('Textarea Top Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '0',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
						'textarea_bottom_margin'   => array(
							'type'          => 'text',
							'label'         => __('Textarea Bottom Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '10',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
                    )
				), 
				array('color','tag_selection')
			),
			'button_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Button Text', 'uabb' ),
					'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-contact-form-submit'
                            ),
                        ),
                        'font_size' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-contact-form-submit',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-contact-form-submit',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        )
                    )
				), 
				array('color','tag_selection'),
				'btn' 
			),
			'label_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
				'typography', 
				array( 'title' => __('Label Text', 'uabb' ),
					'fields'   => array(
                        'font_family' => array(
                            'preview'   => array(
                                'type'      => 'font',
                                'selector'  => '.uabb-contact-form label'
                            ),
                        ),
                        'font_size' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-contact-form label',
                                'property'  => 'font-size',
                                'unit'      => 'px'
                            ),
                        ),
                        'line_height' => array(
                        	'preview'   => array(
                                'type'      => 'css',
                                'selector'  => '.uabb-contact-form label',
                                'property'  => 'line-height',
                                'unit'      => 'px'
                            ),
                        ),
                        'top_margin'   => array(
							'type'          => 'text',
							'label'         => __('Label Top Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '0',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
						'bottom_margin'   => array(
							'type'          => 'text',
							'label'         => __('Label Bottom Margin', 'uabb'),
							'default'       => '',
							'placeholder'	=> '0',
							'description'   => 'px',
							'maxlength'     => '3',
							'size'          => '6',
						),
                    )
				), 
				array('tag_selection'),
				'label'
			),
		)
	)
));