(function($) {

	UABBContactForm = function( settings )
	{
		this.settings	= settings;
		this.nodeClass	= '.fl-node-' + settings.id;
		this.ajaxurl  	= settings.uabb_ajaxurl;
		this._init();
	};

	UABBContactForm.prototype = {
	
		settings	: {},
		nodeClass	: '',
		ajaxurl 	: '',
		
		_init: function()
		{
			$( this.nodeClass + ' .uabb-contact-form-submit' ).click( $.proxy( this._submit, this ) );
		},
		
		_submit: function( e )
		{
			var theForm	  	= $(this.nodeClass + ' .uabb-contact-form'),
				submit	  	= $(this.nodeClass + ' .uabb-contact-form-submit'),
				name	  	= $(this.nodeClass + ' .uabb-name input'),
				email		= $(this.nodeClass + ' .uabb-email input'),
				phone		= $(this.nodeClass + ' .uabb-phone input'),
				subject	  	= $(this.nodeClass + ' .uabb-subject input'),
				message	  	= $(this.nodeClass + ' .uabb-message textarea'),
				mailto	  	= $(this.nodeClass + ' .uabb-mailto'),
				ajaxurl	  	= this.ajaxurl, //FLBuilderLayoutConfig.paths.wpAjaxUrl,
				email_regex = /\S+@\S+\.\S+/,
				isValid	  	= true;
		  
			e.preventDefault();

			// End if button is disabled (sent already)
			if (submit.hasClass('uabb-disabled')) {
				return;
			}
			
			// validate the name
			if(name.length) {
				if (name.val() === '') {
					isValid = false;
					name.parent().addClass('uabb-error');
				} 
				else if (name.parent().hasClass('uabb-error')) {
					name.parent().removeClass('uabb-error');
				}
			}
			
			// validate the email
			if(email.length) {
				if (email.val() === '' || !email_regex.test(email.val())) {
					isValid = false;
					email.parent().addClass('uabb-error');
				} 
				else if (email.parent().hasClass('uabb-error')) {
					email.parent().removeClass('uabb-error');
				}
			}
			
			// validate the subject..just make sure it's there
			if(subject.length) {
				if (subject.val() === '') {
					isValid = false;
					subject.parent().addClass('uabb-error');
				} 
				else if (subject.parent().hasClass('uabb-error')) {
					subject.parent().removeClass('uabb-error');
				}
			}
			
			// validate the phone..just make sure it's there
			if(phone.length) {
				if (phone.val() === '') {
					isValid = false;
					phone.parent().addClass('uabb-error');
				} 
				else if (phone.parent().hasClass('uabb-error')) {
					phone.parent().removeClass('uabb-error');
				}
			}
			
			// validate the message..just make sure it's there
			if (message.val() === '') {
				isValid = false;
				message.parent().addClass('uabb-error');
			} 
			else if (message.parent().hasClass('uabb-error')) {
				message.parent().removeClass('uabb-error');
			}
			
			// end if we're invalid, otherwise go on..
			if (!isValid) {
				return false;
			} 
			else {
			
				// disable send button
				submit.addClass('uabb-disabled');
				
				// post the form data
				$.post(ajaxurl, {
					action	: 'uabb_builder_email',
					name	: name.val(),
					subject	: subject.val(),
					email	: email.val(),
					phone	: phone.val(),
					mailto	: mailto.val(),
					message	: message.val()
				}, $.proxy( this._submitComplete, this ) );
			}
		},
		
		_submitComplete: function( response )
		{
			var urlField 	= $( this.nodeClass + ' .uabb-success-url' ),
				noMessage 	= $( this.nodeClass + ' .uabb-success-none' );
			
			// On success show the success message
			if (response === '1') {
				
				$( this.nodeClass + ' .uabb-send-error' ).fadeOut();
				
				if ( urlField.length > 0 ) {
					window.location.href = urlField.val();
				} 
				else if ( noMessage.length > 0 ) {
					noMessage.fadeIn();
				}
				else {
					$( this.nodeClass + ' .uabb-contact-form' ).hide();
					$( this.nodeClass + ' .uabb-success-msg' ).fadeIn();
				}

				console.log( 'out' );
			} 
			// On failure show fail message and re-enable the send button
			else {
				$(this.nodeClass + ' .uabb-contact-form-submit').removeClass('uabb-disabled');
				$(this.nodeClass + ' .uabb-send-error').fadeIn();
				return false;
			}
		}
	};
	
})(jQuery);