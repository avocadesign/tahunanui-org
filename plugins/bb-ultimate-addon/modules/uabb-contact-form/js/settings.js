(function($){

	FLBuilder.registerModuleHelper('uabb-contact-form', {
    
		init: function()
		{
			var form      = $( '.fl-builder-settings' ),
				action    = form.find( 'select[name=success_action]' ),
				form_style = form.find( 'select[name=form_style]' ),
				enable_label = form.find( 'select[name=enable_label]' );
				
			this._actionChanged();
			this._labelTypography();
			
			action.on( 'change', this._actionChanged );
			form_style.on( 'change', this._labelTypography );
			enable_label.on( 'change', this._labelTypography );
		},
    
		_actionChanged: function()
		{
			var form      = $( '.fl-builder-settings' ),
				action    = form.find( 'select[name=success_action]' ).val(),
				url       = form.find( 'input[name=success_url]' );
				
			url.rules('remove');
				
			if ( 'redirect' == action ) {
				url.rules( 'add', { required: true } );
			}
		},
		_labelTypography: function()
		{
			var form      = $( '.fl-builder-settings' ),
				form_style = form.find( 'select[name=form_style]' ).val(),
				enable_label = form.find( 'select[name=enable_label]' ).val(),
				label_Section = form.find( '#fl-builder-settings-section-label_typography' );
				

			label_Section.hide();
			if ( form_style == 'style1' && enable_label == 'yes' ) {
				label_Section.show();
			}
						
		}
	});

})(jQuery);