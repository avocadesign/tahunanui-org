(function($) {

	$(function() {
	
		new UABBContactForm({
			id: '<?php echo $id ?>',
			uabb_ajaxurl: '<?php echo admin_url('admin-ajax.php'); ?>'
		});
	});
	
})(jQuery);