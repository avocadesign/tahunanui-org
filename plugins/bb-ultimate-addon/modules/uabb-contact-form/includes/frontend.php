<?php 
	$count 			= 0; 
	$name_class 	= '';
	$email_class 	= '';
	$subject_class	= '';
	$phone_class 	= '';
	$msg_class		= '';

	if( $settings->name_toggle == 'show' && $settings->name_width == '50' ) {
		$count = ++$count;
		$name_class = ' uabb-name-inline uabb-inline-group';
		if ( $count % 2 == 0 ) {
			$name_class .= ' uabb-io-padding-left';
		}else{
			$name_class .= ' uabb-io-padding-right';
		}

	} 

	if( $settings->email_toggle == 'show' && $settings->email_width == '50' ) {
		$count = ++$count;
		$email_class = ' uabb-email-inline uabb-inline-group';
		if ( $count % 2 == 0 ) {
			$email_class .= ' uabb-io-padding-left';
		}else{
			$email_class .= ' uabb-io-padding-right';
		}

	} 

	if( $settings->subject_toggle == 'show' && $settings->subject_width == '50' ) {
		$count = ++$count;
		$subject_class = ' uabb-subject-inline uabb-inline-group';
		if ( $count % 2 == 0 ) {
			$subject_class .= ' uabb-io-padding-left';
		}else{
			$subject_class .= ' uabb-io-padding-right';
		}

	}

	if( $settings->phone_toggle == 'show' && $settings->phone_width == '50' ) {
		$count = ++$count;
		$phone_class = ' uabb-phone-inline uabb-inline-group';
		if ( $count % 2 == 0 ) {
			$phone_class .= ' uabb-io-padding-left';
		}else{
			$phone_class .= ' uabb-io-padding-right';
		}

	}

	if( $settings->msg_toggle == 'show' && $settings->msg_width == '50' ) {
		$count = ++$count;
		$msg_class = ' uabb-message-inline uabb-inline-group';
		if ( $count % 2 == 0 ) {
			$msg_class .= ' uabb-io-padding-left';
		}else{
			$msg_class .= ' uabb-io-padding-right';
		}

	}
?>

<form class="uabb-contact-form <?php echo 'uabb-form-'.$settings->form_style; ?>">
  	<div class="uabb-input-group-wrap">
	<?php if ($settings->name_toggle == 'show') : ?>
	<div class="uabb-input-group uabb-name <?php echo $name_class; ?>">
		<?php if ( $settings->form_style == 'style1' && $settings->enable_label == 'yes' ) { ?>
		<label for="uabb-name"><?php echo $settings->name_label; ?></label>
		<?php } ?>
		<input type="text" name="uabb-name" value="" <?php if($settings->enable_placeholder == 'yes' ) { ?> placeholder="<?php echo $settings->name_placeholder; ?>" <?php } ?>/>
		<span class="uabb-contact-error"><?php _e('Please enter your name.', 'fl-builder');?></span>
	</div>
	<?php endif; ?>
	
	<?php if ($settings->email_toggle == 'show') : ?>
	<div class="uabb-input-group uabb-email <?php echo $email_class; ?>">
		<?php if ( $settings->form_style == 'style1' && $settings->enable_label == 'yes' ) { ?>
		<label for="uabb-email"><?php echo $settings->email_label; ?></label>
		<?php } ?>
		<input type="email" name="uabb-email" value="" <?php if($settings->enable_placeholder == 'yes' ) { ?>placeholder="<?php echo $settings->email_placeholder; ?>"<?php } ?>/>
		<span class="uabb-contact-error"><?php _e('Please enter a valid email.', 'fl-builder');?></span>
	</div>
	<?php endif; ?>

	<?php if ($settings->subject_toggle == 'show') : ?>
	<div class="uabb-input-group uabb-subject <?php echo $subject_class; ?>">
		<?php if ( $settings->form_style == 'style1' && $settings->enable_label == 'yes' ) { ?>
		<label for="uabb-subject"><?php echo $settings->subject_label; ?></label>
		<?php } ?>
		<input type="text" name="uabb-subject" value="" <?php if($settings->enable_placeholder == 'yes' ) { ?>placeholder="<?php echo $settings->subject_placeholder; ?>"<?php } ?>/>
		<span class="uabb-contact-error"><?php _e('Please enter a subject.', 'fl-builder');?></span>
	</div>
	<?php endif; ?>


	<?php if ($settings->phone_toggle == 'show') : ?>
	<div class="uabb-input-group uabb-phone <?php echo $phone_class; ?>">
		<?php if ( $settings->form_style == 'style1' && $settings->enable_label == 'yes' ) { ?>
		<label for="uabb-phone"><?php echo $settings->phone_label; ?></label>
		<?php } ?>
		<input type="tel" name="uabb-phone" value="" <?php if($settings->enable_placeholder == 'yes' ) { ?>placeholder="<?php echo $settings->phone_placeholder; ?>"<?php } ?> />
		<span class="uabb-contact-error"><?php _e('Please enter a valid phone number.', 'fl-builder');?></span>
	</div>
	<?php endif; ?>

	<?php if ($settings->msg_toggle == 'show') : ?>
	<div class="uabb-input-group uabb-message <?php echo $msg_class; ?>">
		<?php if ( $settings->form_style == 'style1' && $settings->enable_label == 'yes' ) { ?>
		<label for="uabb-message"><?php echo $settings->msg_label; ?></label>
		<?php } ?>
		<textarea name="uabb-message" <?php if($settings->enable_placeholder == 'yes' ) { ?>placeholder="<?php echo $settings->msg_placeholder; ?>"<?php } ?>></textarea>
		<span class="uabb-contact-error"><?php _e('Please enter a message.', 'fl-builder');?></span>
	</div>
	<?php endif; ?>
	</div>

	<div class="uabb-submit-btn">
		<button type="submit" class="uabb-contact-form-submit"><?php echo $settings->btn_text; ?></button>
		<!--<input type="submit" value="<?php esc_attr_e( 'Send', 'fl-builder' ); ?>" class="uabb-contact-form-submit"/>-->
	</div>
	<?php if ($settings->success_action == 'redirect') : ?>
		<input type="text" value="<?php echo $settings->success_url; ?>" style="display: none;" class="uabb-success-url">  
	<?php elseif($settings->success_action == 'none') : ?>  
		<span class="uabb-success-none" style="display:none;"><?php _e( 'Message Sent!', 'fl-builder' ); ?></span>
	<?php endif; ?>  
    
	<span class="uabb-send-error" style="display:none;"><?php _e( 'Message failed. Please try again.', 'fl-builder' ); ?></span>
</form>
<?php if($settings->success_action == 'show_message') : ?>  
  <span class="uabb-success-msg uabb-text-editor" style="display:none;"><?php echo $settings->success_message; ?></span>
<?php endif; ?>  

