<?php 
	$settings->color = UABB_Helper::uabb_colorpicker( $settings, 'color' );
?>
.fl-node-<?php echo $id; ?> .uabb-separator {
	border-top:<?php echo $settings->height; ?>px <?php echo $settings->style; ?> <?php echo ( $settings->color != '' ) ? $settings->color : '' ; ?>;
	width: <?php echo $settings->width; ?>%;
	display: inline-block;
}
.fl-node-<?php echo $id; ?> .uabb-separator-parent {
	text-align: <?php echo $settings->alignment; ?>;
}