<?php

/**
 * @class UABBSeparatorModule
 */
class UABBSeparatorModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('Separator', 'uabb'),
			'description'   	=> __('A divider line to separate content.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/uabb-separator/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/uabb-separator/',
            'editor_export' 	=> false,
			'partial_refresh'	=> true
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBSeparatorModule', array(
	'general'       => array( // Tab
		'title'         => __('General', 'uabb'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
	                        'label'         => __('Color', 'uabb'),
	                        'default'       => '#dadada',
	                        'preview'       => array(
								'type'          => 'css',
								'selector'      => '.uabb-separator',
								'property'      => 'border-top-color'
							)
                        ) 
                    ),

					'height'        => array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'default'       => '1',
						'maxlength'     => '2',
						'size'          => '3',
						'description'   => 'px',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-width',
							'unit'          => 'px'
						),
						'help'			=> __( 'Thickness of Border', 'uabb' )
					),
					'width'        => array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'default'       => '100',
						'maxlength'     => '3',
						'size'          => '5',
						'description'   => '%'
					),
					'alignment'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'center',
						'options'       => array(
							'center'      => _x( 'Center', 'uabb' ),
							'left'        => _x( 'Left', 'uabb' ),
							'right'       => _x( 'Right', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator-parent',
							'property'      => 'text-align'
						),
						'help'          => __('Align the border.', 'uabb'),
					),
					'style'         => array(
						'type'          => 'select',
						'label'         => __('Style', 'uabb'),
						'default'       => 'solid',
						'options'       => array(
							'solid'         => _x( 'Solid', 'Border type.', 'uabb' ),
							'dashed'        => _x( 'Dashed', 'Border type.', 'uabb' ),
							'dotted'        => _x( 'Dotted', 'Border type.', 'uabb' ),
							'double'        => _x( 'Double', 'Border type.', 'uabb' )
						),
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.uabb-separator',
							'property'      => 'border-top-style'
						),
						'help'          => __('The type of border to use. Double borders must have a height of at least 3px to render properly.', 'uabb'),
					)
				)
			)
		)
	)
));
