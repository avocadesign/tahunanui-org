(function($){

	FLBuilder.registerModuleHelper('uabb-separator', {

		rules: {
			height: {
				required: true,
				number: true
			}
		}
	});

})(jQuery);