<?php

/**
 * @class UABBNumbersModule
 */
class UABBNumbersModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          => __('Counter', 'uabb'),
			'description'   => __('Renders an animated number counter.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/uabb-numbers/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/uabb-numbers/',
		));

		$this->add_js( 'jquery-waypoints' );
		$this->add_css( 'font-awesome' );
	}

	/**
	 * @method update
	 * @param $settings {object}
	 */
	public function update($settings){
		// Cache the photo data.
		if(!empty($settings->photo)) {

			$data = FLBuilderPhoto::get_attachment_data($settings->photo);

			if($data) {
				$settings->photo_data = $data;
			}
		}

		return $settings;
	}


	public function render_number(){

		$number = $this->settings->number ? $this->settings->number : 0;
		$layout = $this->settings->layout ? $this->settings->layout : 'default';
		$type   = $this->settings->number_type ? $this->settings->number_type : 'percent';
		$prefix = $type == 'percent' ? '' : $this->settings->number_prefix;
		$suffix = $type == 'percent' ? '%' : $this->settings->number_suffix;
		$tag = ( $this->settings->num_tag_selection != 'default' ) ? $this->settings->num_tag_selection : 'div';


		$html = '<' . $tag . ' class="uabb-number-string">' . $prefix . '<span class="uabb-number-int">'. number_format( $number ) .'</span>' . $suffix . '</' . $tag . '>';

		echo $html;
	}

	public function render_before_number_text(){
		$html = '';
		if( !empty( $this->settings->before_number_text ) ) {
			$html .= '<span class="uabb-number-before-text">' . esc_html( $this->settings->before_number_text ) . '</span>';
		}
		echo $html;
	}

	public function render_after_number_text(){
		$html = '';
		if( !empty( $this->settings->after_number_text ) ) {
			$html .= '<span class="uabb-number-after-text">' . esc_html( $this->settings->after_number_text ) . '</span>';
		}
		echo $html;
	}

	public function render_circle_bar(){

		$width = !empty( $this->settings->circle_width ) ? $this->settings->circle_width : 100;
		$pos = ( $width / 2 );
		$radius = $pos - 10;
		$dash = number_format( ( ( M_PI * 2 ) * $radius ), 2, '.', '');

		$html = '<div class="svg-container">';
		$html .= '<svg class="svg" viewBox="0 0 '. $width .' '. $width .'" version="1.1" preserveAspectRatio="xMinYMin meet">
			<circle class="uabb-bar-bg" r="'. $radius .'" cx="'. $pos .'" cy="'. $pos .'" fill="transparent" stroke-dasharray="'. $dash .'" stroke-dashoffset="0"></circle>
			<circle class="uabb-bar" r="'. $radius .'" cx="'. $pos .'" cy="'. $pos .'" fill="transparent" stroke-dasharray="'. $dash .'" stroke-dashoffset="'. $dash .'" transform="rotate(-90 '. $pos .' '. $pos .')"></circle>
		</svg>';
		$html .= '</div>';

		echo $html;
	}

	/**
	 * @method render_button
	 */
	public function render_separator() {


		if( $this->settings->show_separator == 'yes' ) {
			$separator_settings = array(
				'color'			=> $this->settings->separator_color,
				'height'		=> $this->settings->separator_height,
				'width'			=> $this->settings->separator_width,
				'alignment'		=> $this->settings->separator_alignment,
				'style'			=> $this->settings->separator_style
			);

			echo '<div class="uabb-number-separator">';
			FLBuilder::render_module_html('uabb-separator', $separator_settings);
			echo '</div>';
		}		
	}

	/**
	 * @method render_image
	 */
	public function render_image( $position )
	{ 	/* Get setting pos according to image type */
		$set_pos = '';
		if( $this->settings->layout == 'circle' ){
			$set_pos 		= $this->settings->circle_position;
		}elseif( $this->settings->layout == 'default' ){
			$set_pos 		= $this->settings->img_icon_position;
		}

		/* Render Image / icon */
		if( $position == $set_pos ){
			$imageicon_array = array(
 
			    /* General Section */
			    'image_type' => $this->settings->image_type,
			 
			    /* Icon Basics */
			    'icon' => $this->settings->icon,
			    'icon_size' => $this->settings->icon_size,
			    'icon_align' => $this->settings->align,
			 
			    /* Image Basics */
			    'photo_source' => $this->settings->photo_source,
			    'photo' => $this->settings->photo,
			    'photo_url' => $this->settings->photo_url,
			    'img_size' => $this->settings->img_size,
			    'img_align' => $this->settings->align,
			    'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,
			 	
			 	/* Icon Style */
			    'icon_style' => $this->settings->icon_style,
			    'icon_bg_size' => $this->settings->icon_bg_size,
			    'icon_border_style' => $this->settings->icon_border_style,
			    'icon_border_width' => $this->settings->icon_border_width,
			    'icon_bg_border_radius' => $this->settings->icon_bg_border_radius,
			 	
			 	/* Image Style */
			    'image_style' => $this->settings->image_style,
			    'img_bg_size' => $this->settings->img_bg_size,
			    'img_border_style' => $this->settings->img_border_style,
			    'img_border_width' => $this->settings->img_border_width,
			    'img_bg_border_radius' => $this->settings->img_bg_border_radius,
			); 
			
			/* Render HTML Function */
			//echo '<div class="infobox-photo">';
			FLBuilder::render_module_html( 'image-icon', $imageicon_array );
			//echo '</div>';
		}

	}

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBNumbersModule', array(
	'general'       => array( // Tab
		'title'         => __('General', 'uabb'), // Tab title
		'sections'      => array( // Tab Sections
			/* Counter Genral Setting */
			'general'       => array( // Section
				'title'         => 'Number', // Section Title
				'fields'        => array( // Section Fields
					'layout' => array(
					    'type'          => 'select',
					    'label'         => __( 'Layout', 'uabb' ),
					    'default'       => 'default',
					    'options'       => array(
					    	'default'		=> __( 'Only Numbers', 'uabb' ),
					    	'circle'		=> __( 'Circle Counter', 'uabb' ),
					    	'bars'			=> __( 'Bars Counter', 'uabb' ),
					    ),
					    'toggle'		=> array(
					    	'default'	=> array(
					    		'sections'		=> array( 'separator', 'overall_structure' ),
					    		'tabs'          => array( 'imageicon' ),
					    		'fields'		=> array( )
					    	),
					    	'circle'		=> array(
					    		'sections'		=> array( 'circle_bar_style', 'separator', 'overall_structure' ),
					    		'tabs'          => array( 'imageicon' ),
					    		'fields'		=> array( )
					    	),
					    	'bars'			=> array(
					    		'sections'		=> array( 'bar_style', 'overall_structure' ),
					    		'fields'		=> array( 'number_position' ),
					    	),
					    ),
					),
					'number_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Number Type', 'uabb' ),
					    'default'       => 'percent',
					    'options'       => array(
					    	'percent'		=> __( 'Percent', 'uabb' ),
					    	'standard'		=> __( 'Standard', 'uabb' ),
					    ),
					    'toggle'		=> array(
					    	'standard'		=> array(
					    		'fields'		=> array( 'number_prefix', 'number_suffix' )
					    	),
					    ),
					),
					'number' => array(
						'type'          => 'text',
						'label'         => __('Number', 'uabb'),
						'size'          => '5',
						'default'		=> '100',
						'placeholder'	=> '100'
					),
					'max_number' => array(
						'type'          => 'text',
						'label'         => __('Total', 'uabb'),
						'size'          => '5',
						'help'			=> __( 'The total number of units for this counter. For example, if the Number is set to 250 and the Total is set to 500, the counter will animate to 50%.', 'uabb' ),
					),
					'number_position'	=> array(
						'type'          => 'select',
						'label'         => __('Number Position', 'uabb'),
						'size'          => '5',
						'help'			=> __( 'Where to display the number in relation to the bar.', 'uabb' ),
						'default'		=> 'default',
						 'options'       => array(
						 	'none'		=> __( 'None', 'uabb' ),
					    	'default'	=> __( 'Inside Bar', 'uabb' ),
					    	'above'		=> __( 'Above Bar', 'uabb' ),
					    	'below'		=> __( 'Below Bar', 'uabb' ),
					    ),
					),
					'before_number_text' => array(
						'type'          => 'text',
						'label'         => __('Text Before Number', 'uabb'),
						'size'          => '20',
						'help'			=> __( 'Text to appear above the number. Leave it empty for none.', 'uabb' )
					),
					'after_number_text' => array(
						'type'          => 'text',
						'label'         => __('Text After Number', 'uabb'),
						'size'          => '20',
						'help'			=> __( 'Text to appear after the number. Leave it empty for none.', 'uabb' )
					),
					'number_prefix' => array(
						'type'          => 'text',
						'label'         => __('Number Prefix', 'uabb'),
						'size'          => '10',
						'help'			=> __( 'For example, if your number is US$ 10, your prefix would be "US$ ".', 'uabb' )
					),
					'number_suffix' => array(
						'type'          => 'text',
						'label'         => __('Number Suffix', 'uabb'),
						'size'          => '10',
						'help'			=> __( 'For example, if your number is 10%, your prefix would be "%".', 'uabb' )
					),
				)
			),
			/* Separator Settings */
			'separator'       => array( // Section
				'title'         => 'Separator ( Below Number )', // Section Title
				'fields'        => array( // Section Fields
					'show_separator' => array(
					    'type'          => 'select',
					    'label'         => __( 'Show separator', 'uabb' ),
					    'default'       => 'no',
					    'options'       => array(
					        'yes'      => __( 'Yes', 'uabb' ),
					        'no'      => __( 'No', 'uabb' )
					    ),
					    'toggle'        => array(
					        'yes'      => array(
					            'fields'        => array( 'separator_color', 'separator_height', 'separator_width', 'separator_alignment', 'separator_style', 'separator_top_margin', 'separator_bottom_margin' ),
					        ),
					        'no'      => array()
					    )
					),
					'separator_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Separator Color', 'uabb'),
                        	'preview'      => array(
						        'type'         	=> 'css',
						        'selector'		=> '.uabb-separator',
						        'property'		=> 'border-top-color'
						    )
						)
                    ),
                    
                    'separator_height' => array(
						'type'          => 'text',
						'label'         => __('Thickness', 'uabb'),
						'size'          => '5',
						'default'		=> '1',
						'description'   => 'px',
					),
					'separator_width' => array(
						'type'          => 'text',
						'label'         => __('Width', 'uabb'),
						'size'          => '5',
						'default'		=> '100',
						'placeholder'	=> '',
						'description'   => '%',
					),
					'separator_alignment' => array(
					    'type'          => 'select',
					    'label'         => __( 'Alignment', 'uabb' ),
					    'default'       => 'inherit',
					    'options'       => array(
					    	'inherit'	=> __( 'Default', 'uabb' ),
					    	'left'		=> __( 'Left', 'uabb' ),
					    	'right'		=> __( 'Right', 'uabb' ),
					    	'center'	=> __( 'Center', 'uabb' ),
					    ),
					),
					'separator_style' => array(
					    'type'          => 'select',
					    'label'         => __( 'Style', 'uabb' ),
					    'default'       => 'solid',
					    'options'       => array(
					    	'solid'		=> __( 'Solid', 'uabb' ),
					    	'dashed'		=> __( 'Dashed', 'uabb' ),
					    	'dotted'		=> __( 'Dotted', 'uabb' ),
					    	'double'		=> __( 'Double', 'uabb' ),
					    ),
					),
				)
			),
			'animation'       => array( // Section
				'title'         => 'Animation', // Section Title
				'fields'        => array( // Section Fields
					'animation_speed' => array(
						'type'          => 'text',
						'label'         => __('Animation Speed', 'uabb'),
						'size'          => '5',
						'default'		=> '1',
						'placeholder'	=> '1',
						'description'	=> __( 'second(s)', 'uabb' ),
						'help'			=> __( 'Number of seconds to complete the animation.', 'uabb' )
					),
					'delay' 		 => array(
						'type'          => 'text',
						'label'         => __('Animation Delay', 'uabb'),
						'size'          => '5',
						'default'		=> '1',
						'placeholder'	=> '1',
						'description'	=> __( 'second(s)', 'uabb' )
					),
				)
			),
		)
	),
	/*'imageicon' => array(
		'title'         => __('Image / Icon', 'uabb'),
		'sections'      => array(
			'type_general' 		=> BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' ),
			'icon_basic' 	=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_basic',
				array( 'fields' => array(
						'icon_size'	=> array(
		 					'preview'	=> array(
                            	'type'		=> 'refresh',
                            ),
		 				),
		 				'icon_align'         => array(
							'default'       => 'inherit',
							'options'       => array(
								'inherit'       => __('Default', 'fl-builder'),
							),
						),
						'icon_position' => array(
							'type'          => 'select',
							'label'         => __('Position', 'uabb'),
							'default'       => 'above-title',
							'options'       => array(
								'above-title'   => __('Above Heading', 'uabb'),
								'below-title'   => __('Below Heading', 'uabb'),
								//'left-title'    => __( 'Left of Heading', 'uabb' ),
								//'right-title'   => __( 'Right of Heading', 'uabb' ),
								'left'          => __('Left of Text and Heading', 'uabb'),
								'right'         => __('Right of Text and Heading', 'uabb')
							)
						),
						'icon_circle_position' => array(
							'type'          => 'select',
							'label'         => __('Position', 'uabb'),
							'default'       => 'above-title',
							'options'       => array(
								'above-title'   => __('Above Heading', 'uabb'),
								'below-title'   => __('Below Heading', 'uabb'),
							)
						),
					)
				) 
			),
			'img_basic' 	=> 	BB_Ultimate_Addon::uabb_section_get( 'img_basic',
		 		array( 'fields' => array(
		 				'img_size'	=> array(
		 					'preview'	=> array(
                            	'type'		=> 'refresh',
                            ),
		 				),
		 				'img_align'         => array(
							'default'       => 'inherit',
							'options'       => array(
								'inherit'       => __('Default', 'fl-builder'),
							),
						),
						'image_position' => array(
							'type'          => 'select',
							'label'         => __('Position', 'uabb'),
							'default'       => 'above-title',
							'options'       => array(
								'above-title'   => __('Above Heading', 'uabb'),
								'below-title'   => __('Below Heading', 'uabb'),
								//'left-title'    => __( 'Left of Heading', 'uabb' ),
								//'right-title'   => __( 'Right of Heading', 'uabb' ),
								'left'          => __('Left of Text and Heading', 'uabb'),
								'right'         => __('Right of Text and Heading', 'uabb')
							)
						),
						'image_circle_position' => array(
							'type'          => 'select',
							'label'         => __('Position', 'uabb'),
							'default'       => 'above-title',
							'options'       => array(
								'above-title'   => __('Above Heading', 'uabb'),
								'below-title'   => __('Below Heading', 'uabb'),
							)
						),
					)
				) 
		 	),
			'icon_style'	=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_style',
				array( 'fields' => array(
						'icon_border_width'	=> array(
		 					'preview'	=> array(
                            	'type'		=> 'refresh',
                            ),
		 				),
		 			)
				)
			),
			'img_style'		=> 	BB_Ultimate_Addon::uabb_section_get( 'img_style',
				array( 'fields' =>
					array(
						'img_bg_size'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 				'img_border_width'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 			)
				)
			),
			'icon_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
		)
	),*/
	'imageicon' => array(
		'title'         => __('Image / Icon', 'uabb'),
		'sections'      => array(
			'type_general' 		=> BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' ,
				array( 'fields' => array(
						'image_type'	=> array(
		 					'toggle'        => array(
								'icon'          => array(
									'sections'	 => array( 'icon_basic',  'icon_style', 'icon_colors' ),
									//'fields'	 => array( 'img_icon_position' ),
								),
								'photo'         => array(
									'sections'	 => array( 'img_basic', 'img_style' ),
									//'fields'	 => array( 'img_icon_position' ),	
								)
							),
		 				),
		 			)
				)
			),
			'icon_basic'		=> array( // Section
				'title'         => 'Icon Basics', // Section Title
				'fields'        => array( // Section Fields
					'icon'          => array(
						'type'          => 'icon',
						'label'         => __('Icon', 'uabb')
					),
					'icon_size'     => array(
						'type'          => 'text',
						'label'         => __('Size', 'uabb'),
						'default'       => '30',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
						'preview'	=> array(
                        	'type'		=> 'refresh',
                        ),
					),
				)
			),
			/* Image Basic Setting */
			'img_basic'		=> array( // Section
				'title'         => 'Image Basics', // Section Title
				'fields'        => array( // Section Fields
					'photo_source'  => array(
						'type'          => 'select',
						'label'         => __('Photo Source', 'uabb'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'uabb'),
							'url'           => __('URL', 'uabb')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo')
							),
							'url'           => array(
								'fields'        => array('photo_url' )
							)
						)
					),
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'uabb')
					),
					'photo_url'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'uabb'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
					),
					'img_size'     => array(
						'type'          => 'text',
						'label'         => __('Size', 'uabb'),
						'default'       => '150',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
					),
				)
			),
			'icon_style'	=> 	BB_Ultimate_Addon::uabb_section_get( 'icon_style',
				array( 'fields' => array(
						'icon_border_width'	=> array(
		 					'preview'	=> array(
                            	'type'		=> 'refresh',
                            ),
		 				),
		 			)
				)
			),
			'img_style'		=> 	BB_Ultimate_Addon::uabb_section_get( 'img_style',
				array(
					'fields' => array(
						'img_bg_size'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 				'img_border_width'	=> array(
		 					'preview'	=> array(
	                        	'type'		=> 'refresh',
	                        ),
		 				),
		 			)
				)
			),
			'icon_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
		)
	),
	'style'         => array( // Tab
		'title'         => __('Style', 'uabb'), // Tab title
		'sections'      => array( // Tab Sections
			/*'structure'    => array(
				'title'         => __('Structure', 'uabb'),
				'fields'        => array(
					'overall_structure'	=> array(
						'type'          => 'select',
						'label'         => __('Overall Alignment', 'uabb'),
						'default'		=> 'center',
						'options'       => array(
							'center'	=> __( 'Center', 'uabb' ),
					    	'left'		=> __( 'Left', 'uabb' ),
					    	'right'		=> __( 'Right', 'uabb' ),
					    ),
					),
				)
			),*/
			'overall_structure' => array(
				'title'         => __('Structure', 'uabb'),
				'fields'        => array(
					'img_icon_position' => array(
						'type'          => 'select',
						'label'         => __('Position', 'uabb'),
						'default'       => 'above-title',
						'help'	=> __( 'Image Icon position', 'uabb' ),
						'options'       => array(
							'above-title'   => __('Above Heading', 'uabb'),
							'below-title'   => __('Below Heading', 'uabb'),
							'left-title'    => __( 'Left of Heading', 'uabb' ),
							'right-title'   => __( 'Right of Heading', 'uabb' ),
							'left'          => __('Left of Text and Heading', 'uabb'),
							'right'         => __('Right of Text and Heading', 'uabb')
						),
						'toggle'			=> array(
							'above-title'	=> array(
								'fields'	=> array( 'align' ),
							),
							'below-title'	=> array(
								'fields'	=> array( 'align' ),
							)
						)
					),
					'circle_position' => array(
						'type'          => 'select',
						'label'         => __('Position', 'uabb'),
						'default'       => 'above-title',
						'options'       => array(
							'above-title'   => __('Above Heading', 'uabb'),
							'below-title'   => __('Below Heading', 'uabb'),
						)
					),
					'align'         => array(
						'type'          => 'select',
						'label'         => __('Overall Alignment', 'uabb'),
						'default'       => 'left',
						'options'       => array(
							'center'        => __('Center', 'uabb'),
							'left'          => __('Left', 'uabb'),
							'right'         => __('Right', 'uabb')
						),
						'help'          => __('The alignment that will apply to all elements within the infobox.', 'uabb'),
					),
				)
			),
			'margin_style'    => array(
				'title'         => __('Margins', 'uabb'),
				'fields'        => array(
					'number_top_margin' => array(
						'type'          => 'text',
						'label'         => __('Number Top Margin', 'uabb'),
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
					),
					'number_bottom_margin' => array(
						'type'          => 'text',
						'label'         => __('Number Bottom Margin', 'uabb'),
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
					),
					'separator_top_margin' => array(
						'type'          => 'text',
						'label'         => __('Separator Top Margin', 'uabb'),
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
					),
					'separator_bottom_margin' => array(
						'type'          => 'text',
						'label'         => __('Separator Bottom Margin', 'uabb'),
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
					),
				)
			),
			'circle_bar_style'    => array(
				'title'         => __('Circle Bar Styles', 'uabb'),
				'fields'        => array(
					'circle_width' => array(
						'type'          => 'text',
						'label'         => __('Circle Size', 'uabb'),
						'default'       => '300',
						'maxlength'     => '4',
						'size'          => '4',
						'description'   => 'px',
					    'preview'      => array(
					        'type'         => 'css',
					        'rules'		   => array(
					        	array(
							        'selector'     => '.uabb-number-circle-container',
							        'property'     => 'max-width',
					        		'unit'		   => 'px'
					        	),
					        	array(
							        'selector'     => '.uabb-number-circle-container',
							        'property'     => 'max-height',
					        		'unit'		   => 'px'
					        	),
					        )
					    )

					),
					'circle_dash_width' => array(
						'type'          => 'text',
						'label'         => __('Circle Stroke Size', 'uabb'),
						'default'       => '10',
						'maxlength'     => '2',
						'size'          => '4',
						'description'   => 'px',
					    'preview'      => array(
					        'type'         => 'css',
					        'selector'     => '.svg circle',
					        'property'     => 'stroke-width',
					        'unit'		   => 'px'
					    )
					),

					'circle_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Circle Foreground Color', 'uabb'),
						)
                    ),
                    'circle_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                    
					'circle_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Circle Background Color', 'uabb'),
                        	'default'		=> '#fafafa',
						)
                    ),
                    'circle_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
				)
			),
			'bar_style'    => array(
				'title'         => __('Bar Styles', 'uabb'),
				'fields'        => array(
					'bar_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Bar Foreground Color', 'uabb'),
						)
                    ),
                    'bar_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

					'bar_bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Bar Background Color', 'uabb'),
                        	'default'		=> '#fafafa',
						)
                    ),
                    'bar_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
				)
			),
		)
	),
	'typography'	=> array(
		'title'			=> __('Typography', 'uabb'),
		'sections'		=> array(
			'number_typography' => BB_Ultimate_Addon::uabb_section_get(
							'typography',
							array( 'title' => __('Number Text', 'uabb' ),
									'fields'   => array(
										'font_family' => array(
	                                        'preview'	=> array(
	                                            'type'		=> 'font',
	                                            'selector'	=> '.uabb-number-string'
	                                    	),
                            			),
                            			/*'font_size'	=> array(
	                                        'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-string',
	                                    		'property'	=> 'font-size',
	                                    		'unit'		=> 'px'
	                                    	),
	                                    ),
	                                    'line_height'	=> array(
	                                        'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-string',
	                                    		'property'	=> 'line-height',
	                                    		'unit'		=> 'px'
	                                    	),
	                                    ),*/
                            			/*'color'	=> array(
	                                    	'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-string',
	                                            'property'	=> 'color'
	                                    	),
	                                    )*/
                            		),
								),
							array(),
							'num'
			),
			'ba_text_typography' => BB_Ultimate_Addon::uabb_section_get(
							'typography',
							array( 'title' => __('Befor - After Text', 'uabb' ),
									'fields'	=> array(
										'font_family' => array(
	                                        'preview'	=> array(
	                                            'type'		=> 'font',
	                                            'selector'	=> '.uabb-number-before-text, .uabb-number-after-text'
	                                    	),
	                                    ),
	                                    /*'font_size'	=> array(
	                                        'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-before-text, .uabb-number-after-text',
	                                    		'property'	=> 'font-size',
	                                    		'unit'		=> 'px'
	                                    	),
	                                    ),
	                                    'line_height'	=> array(
	                                        'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-before-text, .uabb-number-after-text',
	                                    		'property'	=> 'line-height',
	                                    		'unit'		=> 'px'
	                                    	),
	                                    ),*/
	                                    /*'color'	=> array(
	                                    	'preview'	=> array(
	                                            'type'		=> 'css',
	                                            'selector'	=> '.uabb-number-before-text, .uabb-number-after-text',
	                                            'property'	=> 'color'
	                                    	),
	                                    )*/
                                    ),
								),
							array( 'tag_selection'),
							'ba'
			),
		)
	)
));