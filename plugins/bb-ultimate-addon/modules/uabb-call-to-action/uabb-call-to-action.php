<?php

/**
 * @class UABBCtaModule
 */
class UABBCtaModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          => __('Call to Action', 'uabb'),
			'description'   => __('Display a heading, subheading and a button.', 'uabb'),
			'category'      => __('Ultimate Addons', 'uabb'),
			'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/uabb-call-to-action/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/uabb-call-to-action/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
		));
	}

	/**
	 * @method get_classname
	 */
	public function get_classname()
	{
		$classname = 'uabb-cta-wrap uabb-cta-' . $this->settings->layout;

		if($this->settings->layout == 'stacked') {
			$classname .= ' uabb-cta-' . $this->settings->alignment;
		}

		return $classname;
	}

	/**
	 * @method render_button
	 */
	public function render_button()
	{
		$btn_settings = array(
			
			/* General Section */
            'text'              => $this->settings->btn_text,
            
            /* Link Section */
            'link'              => $this->settings->btn_link,
            'link_target'       => $this->settings->btn_link_target,
            
            /* Style Section */
            'style'             => $this->settings->btn_style,
            'border_size'       => $this->settings->btn_border_size,
            'transparent_button_options' => $this->settings->btn_transparent_button_options,
            'threed_button_options'      => $this->settings->btn_threed_button_options,
            'flat_button_options'        => $this->settings->btn_flat_button_options,

            /* Colors */
            'bg_color'          => $this->settings->btn_bg_color,
            'bg_hover_color'    => $this->settings->btn_bg_hover_color,
            'text_color'        => $this->settings->btn_text_color,
            'text_hover_color'  => $this->settings->btn_text_hover_color,

            /* Icon */
            'icon'              => $this->settings->btn_icon,
            'icon_position'     => $this->settings->btn_icon_position,
            
            /* Structure */
			'width'             => $this->settings->layout == 'stacked' ? $this->settings->btn_width : 'full',
            'custom_width'       => $this->settings->btn_custom_width,
            'custom_height'      => $this->settings->btn_custom_height,
            'padding_top_bottom' => $this->settings->btn_padding_top_bottom,
            'padding_left_right' => $this->settings->btn_padding_left_right,
            'border_radius'      => $this->settings->btn_border_radius,
            'align'              => '',
            'mob_align'          => '',

            /* Typography */
            'font_size'         => $this->settings->btn_font_size,
            'line_height'       => $this->settings->btn_line_height,
            'font_family'       => $this->settings->btn_font_family,
		);

		FLBuilder::render_module_html('uabb-button', $btn_settings);
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBCtaModule', array(
	'general'       => array(
		'title'         => __('General', 'uabb'),
		'sections'      => array(
			'title'         => array(
				'title'         => '',
				'fields'        => array(
					'title'         => array(
						'type'          => 'text',
						'label'         => __('Heading', 'uabb'),
						'default'       => __('Call To Action', 'uabb'),
						'preview'       => array(
							'type'          => 'text',
							'selector'      => '.uabb-cta-title'
						)
					)
				)
			),
			'text'          => array(
				'title'         => __('Description', 'uabb'),
				'fields'        => array(
					'text'          => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'rows'			=> 8,
						'default'       => __('Enter description text here.', 'uabb'),
						/*'preview'       => array(
							'type'          => 'text',
							'selector'      => '.uabb-cta-text-content'
						)*/
					)
				)
			)
		)
	),
	'style'        => array(
		'title'         => __('Style', 'uabb'),
		'sections'      => array(
			'structure'     => array(
				'title'         => __('Structure', 'uabb'),
				'fields'        => array(
					'layout'        => array(
						'type'          => 'select',
						'label'         => __('Layout', 'uabb'),
						'default'       => 'inline',
						'options'       => array(
							'inline'        => __('Inline', 'uabb'),
							'stacked'       => __('Stacked', 'uabb')
						),
						'toggle'        => array(
							'inline'		=> array(
								'sections'		=> array( 'inline_btn_structure' )
							),
							'stacked'       => array(
								'fields'        => array( 'alignment' ),
								'sections'		=> array( 'btn_structure' )
							)
						)
					),
					'alignment'     => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'center',
						'options'       => array(
							'left'      =>  __('Left', 'uabb'),
							'center'    =>  __('Center', 'uabb'),
							'right'     =>  __('Right', 'uabb')
						)
					),
					'spacing'       => array(
						'type'          => 'text',
						'label'         => __('Spacing', 'uabb'),
						'default'       => '0',
						'maxlength'     => '3',
						'size'          => '4',
						'description'   => 'px',
						'help'			=> 'Apply padding to your element from all sides.',
						'preview'       => array(
							'type'          => 'css',
							'selector'      => '.fl-module-content',
							'property'      => 'padding',
							'unit'          => 'px'
						)
					),
					'bg_color'    => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                        	'label'         => __('Background Color', 'uabb'),
						)
                    ),
                    'bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
				)
			),
		)
	),
	//'button' => BB_Ultimate_Addon::uabb_object_get( 'button' ),
	'button'        => array(
		'title'         => __('Button', 'uabb'),
		'sections'      => array(
			'btn-general'    => BB_Ultimate_Addon::uabb_section_get( 'btn-general' ),
            'btn-link'       => BB_Ultimate_Addon::uabb_section_get( 'btn-link' ),
            'btn-style'      => BB_Ultimate_Addon::uabb_section_get( 'btn-style' ),
            'btn-icon'       => BB_Ultimate_Addon::uabb_section_get( 'btn-icon' ),
            'btn-colors'     => BB_Ultimate_Addon::uabb_section_get( 'btn-colors' ),
            'btn-structure'  => BB_Ultimate_Addon::uabb_section_get( 'btn-structure', array(), array('btn_align', 'btn_mob_align') ),
		)
	),
	'typography'         => array(
		'title'         => __('Typography', 'uabb'),
		'sections'      => array(
			'title_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
									'typography', 
									array(
										'title' => __('Title settings', 'uabb' ),
										'fields'   => array(
					                        'font_family' => array(
					                            'preview'         => array(
					                                'type'            => 'font',
					                                'selector'        => '.uabb-cta-title'
					                            )
					                        ),
					                    ), 
									), 
									array(),
									'title' 
								),
			'subhead_typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
									'typography', 
									array(
									'title' => __('Description settings', 'uabb' ),
									'fields' => array(
										'color' => array(
											'label' => 'Description Color'
										),
										'font_family' => array(
					                            'preview'         => array(
					                                'type'            => 'font',
					                                'selector'        => '.uabb-cta-text-content p'
					                            )
					                        ),
									) ), 
									array( 'tag_selection' ),
									'subhead' 
								),
			'typography'    =>  BB_Ultimate_Addon::uabb_section_get( 
									'typography', 
									array( 
										'title' => __( 'Button Settings', 'uabb' ),
										'fields'	=> array(
											'font_family' => array(
					                            'preview'         => array(
					                                'type'            => 'font',
					                                'selector'        => '.uabb-creative-button-wrap a, .uabb-creative-button-wrap a:visited'
					                            )
					                        ),
										)
										
									), 
									array( 'color','tag_selection' ),
									'btn'
								),
		)
	)
));
