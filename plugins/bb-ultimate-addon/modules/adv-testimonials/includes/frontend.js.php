<?php
if ( $settings->tetimonial_layout == "slider" ) {
?>
(function($) {

	// Clear the controls in case they were already created.
	$('.fl-node-<?php echo $id; ?> .uabb-slider-next').empty();
	$('.fl-node-<?php echo $id; ?> .uabb-slider-prev').empty();
	
	// Create the slider.
	$('.fl-node-<?php echo $id; ?> .uabb-testimonials').bxSlider({
		autoStart : <?php echo $settings->auto_play ?>,
		auto : true,
		adaptiveHeight: <?php echo $settings->adaptive_height; ?>,
		pause : <?php echo $settings->pause * 1000; ?>,
		mode : '<?php echo $settings->transition; ?>',
		speed : <?php echo $settings->speed * 1000;  ?>,
		pager : <?php echo ($settings->navigation == 'wide') ? 1 : 0; ?>,
		nextSelector : '.fl-node-<?php echo $id; ?> .uabb-slider-next',
		prevSelector : '.fl-node-<?php echo $id; ?> .uabb-slider-prev',
		nextText: '<i class="fa fa-chevron-right"></i>',
		prevText: '<i class="fa fa-chevron-left"></i>',
		controls : <?php echo ($settings->navigation == 'compact') ? 1 : 0; ?>,
		onSliderLoad: function() { 
			$('.fl-node-<?php echo $id; ?> .uabb-testimonials').addClass('uabb-testimonials-loaded'); 
		}
	});

})(jQuery);
<?php
}
?>