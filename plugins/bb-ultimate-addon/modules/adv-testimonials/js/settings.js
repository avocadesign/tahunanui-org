(function($){

	FLBuilder.registerModuleHelper('adv-testimonials', {

		rules: {
			'testimonials[]': {
				required: true
			},
			pause: {
				number: true,
				required: true
			},
			speed: {
				number: true,
				required: true
			}
		},
		init: function()
		{
			var form        = $('.fl-builder-settings'),
			testimonial_layout   = form.find('select[name=tetimonial_layout]'),
			navigation_opt   = form.find('select[name=navigation]');
			
			// Init validation events.
			this._testimonial_layoutChanged();
			this.navigation_changed();
			
			// Validation events.
			testimonial_layout.on('change', this._testimonial_layoutChanged);
			testimonial_layout.on('change', this.navigation_changed);
			//navigation_opt.on('change', this.navigation_changed);

			setTimeout(function(){
				testimonial_layout.trigger('change');
			},500);
		},

		_testimonial_layoutChanged: function()
		{
			var form        = $('.fl-builder-settings'),
			testimonial_layout   = form.find('select[name=tetimonial_layout]').val(),
			testimonial_img_icon   = form.find('select[name=image_type]').val(),
			imagetype_noslider = form.find('select[name=image_type_noslider]').val();
			if ( testimonial_layout == "slider" ) {
				$("#fl-builder-settings-section-testimonial_image_icon_width").css({"display" : "block"});
				$("#fl-builder-settings-section-testimonial_image_icon_width_noslider").css({"display" : "none"});

				// Hide Other image icon upload fields
				jQuery("#fl-builder-settings-section-img_basic_noslider").css({ "display" : "none" });
				jQuery("#fl-builder-settings-section-icon_basic_noslider").css({ "display" : "none" });
			}else if ( testimonial_layout == "box" && testimonial_img_icon != "none" ) {
				$("#fl-builder-settings-section-testimonial_image_icon_width_noslider").css({"display" : "block"});
				$("#fl-builder-settings-section-testimonial_image_icon_width").css({"display" : "none"});


				// check for Other image icon upload fields
				if ( imagetype_noslider == "photo" ) {
					jQuery("#fl-builder-settings-section-img_basic_noslider").css({ "display" : "block" });
					jQuery("#fl-builder-settings-section-icon_basic_noslider").css({ "display" : "none" });
				}
				else if ( imagetype_noslider == "icon" ) {
					jQuery("#fl-builder-settings-section-img_basic_noslider").css({ "display" : "none" });
					jQuery("#fl-builder-settings-section-icon_basic_noslider").css({ "display" : "block" });
				}
			}else{
				$("#fl-builder-settings-section-testimonial_image_icon_width").css({"display" : "none"});
				$("#fl-builder-settings-section-testimonial_image_icon_width_noslider").css({"display" : "none"});
			}
		},
		navigation_changed:function()
		{
			var form        = $('.fl-builder-settings'),
			testimonial_layout   = form.find('select[name=tetimonial_layout]').val(),
			navigation   = form.find('select[name=navigation]').val();
			if ( testimonial_layout == "slider" ) {
				if ( navigation == "compact" ) {
					$("#fl-builder-settings-section-slider_arrow").css({"display":"block"});
					$("#fl-builder-settings-section-slider_dots").css({"display":"none"});
				}else if ( navigation == "wide" ) {
					$("#fl-builder-settings-section-slider_arrow").css({"display":"none"});
					$("#fl-builder-settings-section-slider_dots").css({"display":"block"});
				}
			}else{
				$("#fl-builder-settings-section-slider_arrow").css({"display":"none"});
				$("#fl-builder-settings-section-slider_dots").css({"display":"none"});
			}
		},
	});

})(jQuery);

jQuery(document).on( 'change' , '.testimonial_over_all_position', function(){
	if ( jQuery(this).val() == "top" && jQuery(".uabb-testimonial-layout-selection").val() == "box" ) {
		jQuery(".testimonial_half_outside_opt").parent().parent().css({"display" : "table-row"});
	}else{
		jQuery(".testimonial_half_outside_opt").parent().parent().css({"display" : "none"});
	}
});

jQuery(document).on( 'change' , '.uabb-testimonial-layout-selection', function(){
	if ( jQuery(this).val() == "box" && jQuery(".testimonial_over_all_position").val() == "top" ) {
		jQuery(".testimonial_half_outside_opt").parent().parent().css({"display" : "table-row"});
	}else{
		jQuery(".testimonial_half_outside_opt").parent().parent().css({"display" : "none"});
	}
});
