<?php

/**
 * @class UABBAdvancedTestimonialsModule
 */
class UABBAdvancedTestimonialsModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          => __('Testimonials', 'fl-builder'),
			'description'   => __('An animated tesimonials area.', 'fl-builder'),
			'category'      => __('Ultimate Addons', 'fl-builder'),
			'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/adv-testimonials/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/adv-testimonials/',
		));

		$this->add_css('jquery-bxslider');
		$this->add_css('font-awesome');
		$this->add_js('jquery-bxslider');
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBAdvancedTestimonialsModule', array(
	'general'      => array( // Tab
		'title'         => __('General', 'fl-builder'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'tetimonial_layout'       => array(
						'type'          => 'select',
						'label'         => __('Testimonial Layouts', 'fl-builder'),
						'default'       => 'slider',
                        'class'         => 'uabb-testimonial-layout-selection',
						'options'       => array(
							'slider'             => __('Slider', 'fl-builder'),
							'box'             => __('Box Layout', 'fl-builder')
						),
						'toggle'        => array(
							'slider'      => array(
								'sections'      => array( 'slider', 'slider_navigation' ),
								'tabs'      => array( 'testimonials' ),
								'fields'	 => array('layout')
							),
							'box'      => array(
                                'sections'      => array( 'testimonial_title_section', 'testimonial_descr_section' ),
                                'tabs'      => array( 'testimonial_image_noslider' ),
                                'fields'     => array( 'layout_background', 'test_box_style' ), //, 'icon_position_half_box'
							)
						),
					),
                    
				)
			),
			'slider'       => array( // Section
				'title'         => __('Slider Settings', 'fl-builder'), // Section Title
				'fields'        => array( // Section Fields
					'auto_play'     => array(
						'type'          => 'select',
						'label'         => __('Auto Play', 'fl-builder'),
						'default'       => '1',
						'options'       => array(
							'0'             => __('No', 'fl-builder'),
							'1'             => __('Yes', 'fl-builder')
						),
                        'toggle'        => array(
                            '1'         => array(
                                'fields'        => array( 'pause' )
                            ),
                        )
					),
					'pause'         => array(
						'type'          => 'text',
						'label'         => __('Delay', 'fl-builder'),
						'default'       => '4',
						'maxlength'     => '4',
						'size'          => '5',
						'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'fl-builder' )
					),
					'transition'    => array(
						'type'          => 'select',
						'label'         => __('Transition', 'fl-builder'),
						'default'       => 'slide',
						'options'       => array(
							'horizontal'    => _x( 'Slide', 'Transition type.', 'fl-builder' ),
							'fade'          => __( 'Fade', 'fl-builder' )
						)
					),
					'speed'         => array(
						'type'          => 'text',
						'label'         => __('Transition Speed', 'fl-builder'),
						'default'       => '0.5',
						'maxlength'     => '4',
						'size'          => '5',
						'description'   => _x( 'seconds', 'Value unit for form field of time in seconds. Such as: "5 seconds"', 'fl-builder' )
					),
                    'adaptive_height'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Adaptive Height', 'fl-builder' ),
                        'default'       => 'true',
                        'options'       => array(
                            'false'       => 'No',
                            'true'       => 'Yes',
                        ),
                    ),
				)
			),
            'slider_navigation'       => array( // Section
                'title'         => __('Navigation', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'navigation'    =>array(
                        'type'      => 'select',
                        'label'         => __('Navigation', 'fl-builder'),
                        'default'       => '1',
                        'options'       => array(
                            'compact'             => __('Show Arrow', 'fl-builder'),
                            'wide'             => __('Show Dots', 'fl-builder')
                        ),
                        'toggle'        => array(
                            'compact'         => array(
                                'sections'        => array( 'slider_arrow' )
                            ),
                            'wide'         => array(
                                'sections'        => array('slider_dots')
                            ),
                        )
                    ),
                    
                    
                )
            ),
            'slider_arrow'       => array( // Section
                'title'         => __('', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    /* Arrow Fields */
                    'arrow_style'       => array(
                        'type'          => 'select',
                        'label'         => __('Arrow Style', 'fl-builder'),
                        'default'       => 'circle',
                        'options'       => array(
                            'square'             => __('Square Background', 'fl-builder'),
                            'circle'             => __('Circle Background', 'fl-builder'),
                            'square-border'      => __('Square Border', 'fl-builder'),
                            'circle-border'      => __('Circle Border', 'fl-builder')
                        ),
                        'toggle'        => array(
                            'square-border' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_border', 'arrow_border_size' )
                            ),
                            'circle-border' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_border', 'arrow_border_size' )
                            ),
                            'square' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_back', 'arrow_color_back_opc' )
                            ),
                            'circle' => array(
                                'fields'        => array( 'arrow_color', 'arrow_color_back', 'arrow_color_back_opc' )
                            ),
                        )
                    ),
                    'arrow_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                         array(
                            'label'         => __('Arrow Color', 'fl-builder'),
                            'default'       => '#ffffff',
                         )
                    ),
                    'arrow_color_back' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                         array(
                            'label'         => __('Arrow Background Color', 'fl-builder'),
                         )
                    ),
                    'arrow_color_back_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),
                   
                    'arrow_color_border' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                         array(
                            'label'         => __('Arrow Border Color', 'fl-builder'),
                         )
                    ),
                    
                    'arrow_border_size'       => array(
                        'type'          => 'text',
                        'label'         => __('Border Size', 'fl-builder'),
                        'default'       => '1',
                        'description'   => 'px',
                        'size'          => '8',
                        'max_lenght'    => '3'
                    ),
                )
            ),
            'slider_dots'       => array( // Section
                'title'         => __('', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'dot_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Dot Color', 'fl-builder'),
                        )
                    ),
                )
            ),
            
            /* Box Layout Options section */
            'testimonial_title_section'       => array( // Section
                'title'         => __('Title', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'testimonial_author_no_slider' => array(
                        'type'          => 'text',
                        'label'         => __('Author Name', 'fl-builder'),
                        'default'       => 'Author Name',
                        'placeholder'   => 'Author Name',
                        'description'   => '',
                    ),
                    'testimonial_designation_no_slider' => array(
                        'type'          => 'text',
                        'label'         => __('Designation', 'fl-builder'),
                        'placeholder'   => 'Designation',
                        'default'       => 'Designation',
                        'description'   => '',
                    ),
                ),
            ),
            'testimonial_descr_section'       => array( // Section
                'title'         => __('Testimonial', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'testimonial_description' => array(
                        'type'          => 'editor',
                        'description'   => '',
                        'default'       => 'Enter description text here.',
                    ),
                ),
            )
		)
	),



	'testimonials'      => array( // Tab
		'title'         => __('Testimonials', 'fl-builder'), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'title'         => '', // Section Title
				'fields'        => array( // Section Fields
					'testimonials'     => array(
						'type'          => 'form',
						'label'         => __('Testimonial', 'fl-builder'),
						'form'          => 'testimonials_form', // ID from registered form below
						'preview_text'  => 'testimonial_author', // Name of a field to use for the preview text
						'multiple'      => true
					),
				)
			)
		)
	),
    'testimonial_image_noslider'       => array( // Tab
        'title'         => __('Image', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            /* Icon param Code */
            'type_general_noslider'      => array( // Section
                'title'         => 'Image / Icon', // Section Title
                'fields'        => array( // Section Fields
                    'image_type_noslider'    => array(
                        'type'          => 'select',
                        'label'         => __('Image Type', 'uabb'),
                        'default'       => 'none',
                        'options'       => array(
                            'none'          => __( 'None', 'Image type.', 'uabb' ),
                            'icon'          => __('Icon', 'uabb'),
                            'photo'         => __('Photo', 'uabb'),
                        ),
                        'toggle'        => array(
                            'icon'          => array(
                                'sections'   => array( 'icon_basic_noslider' ),
                            ),
                            'photo'         => array(
                                'sections'   => array( 'img_basic_noslider' ),
                            )
                        ),
                    ),
                )
            ),

            /* Icon Basic Setting */
            'icon_basic_noslider'        => array( // Section
                'title'         => 'Icon', // Section Title
                'fields'        => array( // Section Fields
                    'icon_noslider'          => array(
                        'type'          => 'icon',
                        'label'         => __('Icon', 'uabb')
                    ),
                    /* Icon Color */
                    'icon_color_noslider' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Icon Color', 'uabb'),
                        )
                    ),
                )
            ),
            /* Image Basic Setting */
            'img_basic_noslider'     => array( // Section
                'title'         => 'Image', // Section Title
                'fields'        => array( // Section Fields
                    'photo_source_noslider'  => array(
                        'type'          => 'select',
                        'label'         => __('Photo Source', 'uabb'),
                        'default'       => 'library',
                        'options'       => array(
                            'library'       => __('Media Library', 'uabb'),
                            'url'           => __('URL', 'uabb')
                        ),
                        'toggle'        => array(
                            'library'       => array(
                                'fields'        => array('photo_noslider')
                            ),
                            'url'           => array(
                                'fields'        => array('photo_url_noslider' )
                            )
                        )
                    ),
                    'photo_noslider'         => array(
                        'type'          => 'photo',
                        'label'         => __('Photo', 'uabb')
                    ),
                    'photo_url_noslider'     => array(
                        'type'          => 'text',
                        'label'         => __('Photo URL', 'uabb'),
                        'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                    ),
                )
            ),

            /* Icon param Code Ends */
            'testimonial_image_icon_width_noslider' => array(
                'title'         => __('Image / Icon Style ', 'fl-builder'),
                'fields'        => array(

                    'testimonial_icon_image_size_noslider'     => array(
                        'type'          => 'text',
                        'label'         => __('Width', 'fl-builder'),
                        'default'       => '75',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'testimonial_icon_style_noslider'         => array(
                        'type'          => 'select',
                        'label'         => __('Image / Icon Background Style ', 'fl-builder'),
                        'default'       => 'square',
                        'description'   => '',
                        'options'       => array(
                            'circle'          => __('Circle Background', 'fl-builder'),
                            'square'         => __('Square Background', 'fl-builder'),
                            'custom'         => __('Design your own', 'fl-builder'),
                        ),
                        'toggle' => array(
                            'circle' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc' )
                            ),
                            'square' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc' )
                            ),
                            'custom' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc', 'testimonial_icon_bg_border_radius_noslider', 'testimonial_icon_bg_size_noslider' )
                            )
                        )
                    ),
                    
                    'testimonial_icon_bg_color_noslider' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'fl-builder'),
                        )
                    ),
                    'testimonial_icon_bg_color_noslider_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    
                    'testimonial_icon_bg_border_radius_noslider' => array(
                        'type'          => 'text',
                        'label'         => __('Border Radius ( For Background )', 'fl-builder'),
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),
                    'testimonial_icon_bg_size_noslider' => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'fl-builder'),
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),
                )
            ),
        )
    ),

	'testimonial_style'       => array( // Tab
        'title'         => __('Style', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'testimonial_styles'       => array( // Section
                'title'         => __('Style', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'testimonial_image_position' => array(
                        'type'          => 'select',
                        'label'         => __('Overall Position', 'fl-builder'),
                        'description'   => '',
                        'default'       => 'default',
                        'class'         => 'testimonial_over_all_position',
                        'help'          => __( 'Change the position of Image/Icon/Text', 'fl-builder' ),
                        'options'       => array(
                            'left'      => __('Left', 'fl-builder'),
                            'right'     => __('Right', 'fl-builder'),
                            'top'     => __('Top', 'fl-builder')
                        ),
                    ),
                    'icon_position_half_box'=> array(
                        'type'          => 'select',
                        'label'         => __('Shift Image', 'fl-builder'),
                        'class'         => 'testimonial_half_outside_opt',
                        'default'       => 'no',
                        'options'       => array(
                            'yes'       => __('Yes', 'fl-builder'),
                            'no'        => __('No', 'fl-builder'),
                        ),
                        'help'          => 'Show Half Image/Icon Outside The Box',
                    ),
                    'test_box_style' => array(
                        'type'          => 'select',
                        'label'         => __('Box Style', 'fl-builder'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'      => __('Yes', 'fl-builder'),
                            'no'     => __('No', 'fl-builder'),
                        )
                    ),
                    'author_name_position' => array(
                        'type'          => 'select',
                        'label'         => __('Author Name Position', 'fl-builder'),
                        'description'   => '',
                        'default'       => 'top',
                        'options'       => array(
                            'top'      => __('Above Description', 'fl-builder'),
                            'bottom'     => __('Below Description', 'fl-builder'),
                        )
                    ),
                    'layout_background' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Box Background Color', 'fl-builder'),
                        )
                    ),
                    'layout_background_opc' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker'),
                ),
            ),

			'testimonial_image_icon_width' => array(
                'title'         => __('Image / Icon Style ', 'fl-builder'),
                'fields'        => array(

                    'testimonial_icon_image_size'     => array(
                        'type'          => 'text',
                        'label'         => __('Width', 'fl-builder'),
                        'default'       => '75',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'description'   => 'px'
                    ),
                    'testimonial_icon_style'         => array(
                        'type'          => 'select',
                        'label'         => __('Image / Icon Style ', 'fl-builder'),
                        'default'       => 'square',
                        'description'   => '',
                        'options'       => array(
                            'simple'          => __('Simple', 'fl-builder'),
                            'circle'          => __('Circle Background', 'fl-builder'),
                            'square'         => __('Square Background', 'fl-builder'),
                            'custom'         => __('Design your own', 'fl-builder'),
                        ),
                        'toggle' => array(
                            'circle' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc' )
                            ),
                            'square' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc' )
                            ),
                            'custom' => array(
                                'fields' => array( 'testimonial_icon_bg_color', 'testimonial_icon_bg_color_opc', 'testimonial_icon_bg_border_radius', 'testimonial_icon_bg_size' )
                            )
                        )
                    ),
                    
                    'testimonial_icon_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                        array(
                            'label'         => __('Background Color', 'fl-builder'),
                        )
                    ),
                    'testimonial_icon_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
                    'testimonial_icon_bg_border_radius' => array(
                        'type'          => 'text',
                        'label'         => __('Border Radius ( For Background )', 'fl-builder'),
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),
                    'testimonial_icon_bg_size' => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'fl-builder'),
                        'default'       => '0',
                        'maxlength'     => '3',
                        'size'          => '4',
                        'placeholder'   => '10',
                        'description'   => 'px'
                    ),
                )
            ),
        ),
    ),
    
    'testimonial_typography'       => array( // Tab
        'title'         => __('Typography', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'testimonial_heading'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title'     => __('Heading Settings', 'uabb' ) ,
                    'fields'   => array(
                        'color'   => array(
                            'label' => __('Choose Color', 'uabb'),
                        ),
                        'tag_selection'   => array(
                            'label' => __('Select Tag', 'uabb'),
                            'default'   =>'h3',
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-testimonial-author-name'
                            )
                        ),
                        'margin_top' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Top', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        ),
                        'margin_bottom' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Bottom', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        )
                    )
                ), 
                array(),
                'testimonial_heading' 
            ),
            'testimonial_designation'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title'     => __('Designation Settings', 'uabb' ) ,
                    'fields'   => array(
                        'color'   => array(
                            'label' => __('Choose Color', 'uabb'),
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-testimonial-author-designation'
                            )
                        ),
                        'margin_top' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Top', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        ),
                        'margin_bottom' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Bottom', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        )
                    )
                ), 
                array('tag_selection'),
                'testimonial_designation' 
            ),
            'testimonial_description'    =>  BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array( 
                    'title'     => __('Testimonial Settings', 'uabb' ) ,
                    'fields'   => array(
                        'color'   => array(
                            'label' => __('Choose Color', 'uabb'),
                        ),
                        'tag_selection'   => array(
                            'label' => __('Select Tag', 'uabb'),
                        ),
                        'font_family' => array(
                            'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-testimonial-author-description *'
                            )
                        ),
                        'margin_top' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Top', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        ),
                        'margin_bottom' => array(
                            'type'      => 'text',
                            'label'     => __( 'Margin Bottom', 'fl-builder' ),
                            'size'      => '8',
                            'max-length'    => '6',
                            'description'   => 'px'
                        )
                    )
                ), 
                array('tag_selection'),
                'testimonial_description_opt' 
            ),
        )
    )
));


/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('testimonials_form', array(
	'title' => __('Add Testimonial', 'fl-builder'),
	'tabs'  => array(
		'general'      => array( // Tab
			'title'         => __('General', 'fl-builder'), // Tab title
			'sections'      => array( // Tab Sections
	            'testimonial_title_section'       => array( // Section
	                'title'         => __('Title', 'fl-builder'), // Section Title
	                'fields'        => array( // Section Fields
	                    'testimonial_author' => array(
	                        'type'          => 'text',
	                        'label'         => __('Author Name', 'fl-builder'),
	                        'placeholder'   => 'Author Name',
                            'default'       => 'Author Name',
	                        'description'   => '',
	                    ),
                        'testimonial_designation' => array(
                            'type'          => 'text',
                            'label'         => __('Designation', 'fl-builder'),
                            'placeholder'   => 'Designation',
                            'default'       => 'Designation',
                            'description'   => '',
                        ),
	                    
	                ),
	            ),
				'general'       => array( // Section
					'title'         => __('Testimonial', 'fl-builder'), // Section Title
					'fields'        => array( // Section Fields
	                    'testimonial'          => array(
                            'type'          => 'editor',
                            'label'         => '',
                            'default'       => 'Enter description text here.',
                        ),
					),
				),
			)
		),
        'photo_tab'      => array( // Tab
            'title'         => __('Image', 'fl-builder'), // Tab title
            'sections'      => array( // Tab Sections
                'type_general'      => array( // Section
                    'title'         => 'Image / Icon', // Section Title
                    'fields'        => array( // Section Fields
                        'image_type'    => array(
                            'type'          => 'select',
                            'label'         => __('Image Type', 'uabb'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'          => __( 'None', 'Image type.', 'uabb' ),
                                'icon'          => __('Icon', 'uabb'),
                                'photo'         => __('Photo', 'uabb'),
                            ),
                            'toggle'        => array(
                                'icon'          => array(
                                    'sections'   => array( 'icon_basic',  'icon_style', 'icon_colors' ),
                                ),
                                'photo'         => array(
                                    'sections'   => array( 'img_basic', 'img_style' ),
                                )
                            ),
                        ),
                    )
                ),

                /* Icon Basic Setting */
                'icon_basic'        => array( // Section
                    'title'         => 'Icon', // Section Title
                    'fields'        => array( // Section Fields
                        'icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb')
                        ),
                        /* Style Options */
                        'icon_color_preset'     => array(
                            'type'          => 'uabb-toggle-switch',
                            'label'         => __( 'Icon Color Presets', 'fl-builder' ),
                            'default'       => 'preset1',
                            'options'       => array(
                                'preset1'       => 'Preset 1',
                                'preset2'       => 'Preset 2',
                                /*'preset3'     => 'Preset 3',*/
                            ),
                            'help'          => __('Preset 1 => Icon : White, Background : Theme </br>Preset 2 => Icon : Theme, Background : #f3f3f3', 'uabb')
                        ),
                        /* Icon Color */
                        'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                            array(
                                'label'         => __('Icon Color', 'uabb'),
                            )
                        ),
                    )
                ),
                /* Image Basic Setting */
                'img_basic'     => array( // Section
                    'title'         => 'Image', // Section Title
                    'fields'        => array( // Section Fields
                        'photo_source'  => array(
                            'type'          => 'select',
                            'label'         => __('Photo Source', 'uabb'),
                            'default'       => 'library',
                            'options'       => array(
                                'library'       => __('Media Library', 'uabb'),
                                'url'           => __('URL', 'uabb')
                            ),
                            'toggle'        => array(
                                'library'       => array(
                                    'fields'        => array('photo')
                                ),
                                'url'           => array(
                                    'fields'        => array('photo_url' )
                                )
                            )
                        ),
                        'photo'         => array(
                            'type'          => 'photo',
                            'label'         => __('Photo', 'uabb')
                        ),
                        'photo_url'     => array(
                            'type'          => 'text',
                            'label'         => __('Photo URL', 'uabb'),
                            'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                        ),
                    )
                ),
            )
        ),
        //'object_icon' => BB_Ultimate_Addon::uabb_object_get( 'image-icon' ),
		
	)
));