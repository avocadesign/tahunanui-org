function init_map(){

	var isDraggable = jQuery(document).width() > 641 ? true : <?php echo $settings->dragging; ?>;
	var myOptions = {
			zoom: <?php echo $settings->map_zoom; ?>,
			center: new google.maps.LatLng( <?php echo $settings->map_lattitude ?>,<?php echo $settings->map_longitude ?> ),
			<?php echo ( $settings->map_expand == 'yes' ) ? 'scrollwheel: false,' : ''; ?>
			streetViewControl: <?php echo $settings->street_view . ','; ?>
			mapTypeControl: <?php echo $settings->map_type_control . ','; ?>
			zoomControl: <?php echo $settings->zoom . ','; ?>
			draggable: isDraggable,
			zoomControlOptions: {
				position: google.maps.ControlPosition.<?php echo $settings->zoom_control_position; ?>
			},
			mapTypeId: google.maps.MapTypeId.<?php echo $settings->map_type; ?>
		};
		//console.log(myOptions);
		
			map = new google.maps.Map( jQuery(".fl-node-<?php echo $id ?> #uabb-google-map")[0], myOptions);
			var image = '';
			<?php
			if( $settings->marker_point == 'custom' ) {
				if( isset( $settings->marker_img_src ) ) {
			?>
			image = {
			    url: '<?php echo $settings->marker_img_src; ?>'
			};
			<?php
				}
			}
			?>
			
			marker = new google.maps.Marker({
				map: map,
				<?php
				if( $settings->marker_point == 'custom' ) {
				?>
				icon: image,
				<?php
				}
				?>
				position: new google.maps.LatLng( <?php echo $settings->map_lattitude ?>,<?php echo $settings->map_longitude ?> )
			});

			<?php if ( $settings->info_window_text != '' ) { ?>

				infowindow = new google.maps.InfoWindow({
					content:"<?php echo $settings->info_window_text ?>"
				});

				google.maps.event.addListener(marker, 'click', function(){
					infowindow.open(map,marker);
				});
				<?php
				if ( $settings->open_marker == 'no' ) {
				?>
					infowindow.open(map,marker);
			<?php
				}
			}
			?>
}

init_map();