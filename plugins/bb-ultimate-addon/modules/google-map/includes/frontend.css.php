.fl-node-<?php echo $id; ?> {
    width: 100%;
}

.fl-node-<?php echo $id; ?> .uabb-google-map-wrapper {
	width: <?php echo $settings->map_width ?>%;
	height: <?php echo $settings->map_height ?>px;
	background-color: #CCC;
}