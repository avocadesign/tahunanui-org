<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class GoogleMapModule
 */
class GoogleMapModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Google Map', 'uabb'),
            'description'   => __('An basic example for coding new modules.', 'uabb'),
            'category'      => __('Ultimate Addons', 'uabb'),
            'dir'           => BB_ULTIMATE_ADDON_DIR . 'modules/google-map/',
            'url'           => BB_ULTIMATE_ADDON_URL . 'modules/google-map/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));

        $uabb_setting_options = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb', true );
        $google_api_key = '';

        if ( isset( $uabb_setting_options['uabb-google-map-api'] ) && !empty( $uabb_setting_options['uabb-google-map-api'] ) ) {
                         
           $google_api_key  = $uabb_setting_options['uabb-google-map-api'];
        }

        $url = 'https://maps.googleapis.com/maps/api/js';
        
        if( $google_api_key != false ) {
            $arr_params = array(
                'key' => $google_api_key
            );
            $url = esc_url( add_query_arg( $arr_params , $url ));
        }
        $this->add_js( 'google-map', $url, array(), '', true );



    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('GoogleMapModule', array(
    'general'       => array( // Tab
        'title'         => __('General', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'map_width'     => array(
                        'type'          => 'text',
                        'label'         => __('Width', 'uabb'),
                        'default'       => '100',
                        'size'			=> '6',
                        'description'	=> '%'
                    ),
                    'map_height'     => array(
                        'type'          => 'text',
                        'label'         => __('Height', 'uabb'),
                        'default'       => '300',
                        'size'			=> '6',
                        'description'	=> 'px'
                    ),
                    'map_type' => array(
                        'type'          => 'select',
                        'label'         => __('Map type', 'uabb'),
                        'default'       => 'ROADMAP',
                        'options'       => array(
                            'ROADMAP'      => __('Roadmap', 'uabb'),
                            'SATELLITE'      => __('Satellite', 'uabb'),
                            'HYBRID'      => __('Hybrid', 'uabb'),
                            'TERRAIN'      => __('Terrain', 'uabb'),
                        ),
                    ),
                    'map_lattitude'     => array(
                        'type'          => 'text',
                        'label'         => __('Lattitude', 'uabb'),
                        'default'		=> '18.591212',
                        'description'	=> '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">Here is a tool</a> where you can find Latitude & Longitude of your location'
                    ),
                    'map_longitude'     => array(
                        'type'          => 'text',
                        'label'         => __('Longitude', 'uabb'),
                        'default'		=> '73.741261',
                        'description'	=> '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">Here is a tool</a> where you can find Latitude & Longitude of your location'
                    ),
                    'map_zoom' => array(
                        'type'          => 'select',
                        'label'         => __('Map Zoom', 'uabb'),
                        'default'       => '12',
                        'options'       => array(
                            '1'      => __('1', 'uabb'),
                            '2'      => __('2', 'uabb'),
                            '3'      => __('3', 'uabb'),
                            '4'      => __('4', 'uabb'),
                            '5'      => __('5', 'uabb'),
                            '6'      => __('6', 'uabb'),
                            '7'      => __('7', 'uabb'),
                            '8'      => __('8', 'uabb'),
                            '9'      => __('9', 'uabb'),
                            '10'      => __('10', 'uabb'),
                            '11'      => __('11', 'uabb'),
                            '12'      => __('12', 'uabb'),
                            '13'      => __('13', 'uabb'),
                            '14'      => __('14', 'uabb'),
                            '15'      => __('15', 'uabb'),
                            '16'      => __('16', 'uabb'),
                            '17'      => __('17', 'uabb'),
                            '18'      => __('18', 'uabb'),
                            '19'      => __('19', 'uabb'),
                            '20'      => __('20', 'uabb'),
                        ),
                    ),
					'map_expand'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Disable map zoom on mouse wheel scroll', 'uabb' ),
                        'default'       => 'no',
                        'options'       => array(
                         	'yes'		=> 'Yes',
                          	'no'		=> 'No',
                        ),
                        'preview'       => array(
                            'type'  => 'none'
                        )
	                ),
                )
            ),
        )
    ),
	'info_window'       => array( // Tab
        'title'         => __('Info Window', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                	'info_window_text' => array(
                		'type'		=> 'editor',
                		'label'		=> '',
                        'media_buttons' => false
                	),
                	'open_marker'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Open on Marker Click', 'uabb' ),
                        'default'       => 'yes',
                        'options'       => array(
                         	'yes'		=> 'Yes',
                          	'no'		=> 'No',
                        )
	                ),
	            )
            ),
        )
    ),
    'control'       => array( // Tab
        'title'         => __('Controls', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('Advanced Controls', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'street_view'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Street view control', 'uabb' ),
                        'default'       => 'false',
                        'options'       => array(
                            'true'       => 'Yes',
                            'false'        => 'No',
                        )
                    ),
                    'map_type_control'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Map type control', 'uabb' ),
                        'default'       => 'false',
                        'options'       => array(
                            'true'       => 'Yes',
                            'false'        => 'No',
                        )
                    ),
                    'zoom'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Zoom control', 'uabb' ),
                        'default'       => 'false',
                        'options'       => array(
                            'true'       => 'Yes',
                            'false'        => 'No',
                        )
                    ),
                    'zoom_control_position'     => array(
                        'type'          => 'select',
                        'label'         => __( 'Zoom control position', 'uabb' ),
                        'default'       => 'RIGHT_BOTTOM',
                        'options'       => array(
                            'RIGHT_TOP'         => 'Right Top',
                            'RIGHT_CENTER'      => 'Right Center',
                            'RIGHT_BOTTOM'      => 'Right Bottom',
                            'LEFT_TOP'          => 'Left Top',
                            'LEFT_CENTER'       => 'Left Center',
                            'LEFT_BOTTOM'       => 'Left Bottom',
                        )
                    ),
                    'dragging'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Disable dragging on Mobile', 'uabb' ),
                        'default'       => 'false',
                        'options'       => array(
                            'true'       => 'Yes',
                            'false'        => 'No',
                        )
                    ),
                )
            ),
        )
    ),
    'marker'       => array( // Tab
        'title'         => __('Marker', 'uabb'), // Tab title
        'sections'      => array( // Tab Sections
            'title'       => array( // Section
                'title'         => __('Marker', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    'marker_point'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Marker Point Icon', 'uabb' ),
                        'default'       => 'default',
                        'options'       => array(
                            'default'       => 'Defaut',
                            'custom'        => 'Custom',
                        ),
                        'toggle' => array(
                            'custom' => array(
                                'fields' => array( 'marker_img' )
                            )
                        )
                    ),
                    'marker_img'         => array(
                        'type'          => 'photo',
                        'label'         => __('Custom Marker', 'uabb')
                    ),
                    /*'marker_width'     => array(
                        'type'          => 'text',
                        'label'         => __('Width', 'uabb'),
                        'size'          => '8',
                        'placeholder'   => __( '20', 'uabb' )
                    ),
                    'marker_height'     => array(
                        'type'          => 'text',
                        'label'         => __('Height', 'uabb'),
                        'size'          => '8',
                        'placeholder'   => __( '30', 'uabb' )
                    ),*/
                )
            ),
        )
    ),
));