(function ( $, window, undefined ) {
    // Hide until page load
    $( window ).load(function() {
        $('.uabb-ih-container').css({'visibility':'visible', 'opacity':1});
    });
    $(document).ready(function () {
        uabb_ihover_init();
		$(document).ajaxComplete(function(e, xhr, settings){
			uabb_ihover_init();
		});
    });
    $(window).resize(function(){
        uabb_ihover_init();
    });
    function responsive_sizes(el, ww, h, w, rh, rw) {
        if(ww!='') {
            if(ww>=768) {
                $(el).find('.uabb-ih-item, .uabb-ih-img, .uabb-ih-image-block, .uabb-ih-list-item').css({'height': h,'width': w});
            } else {
                $(el).find('.uabb-ih-item, .uabb-ih-img, .uabb-ih-image-block, .uabb-ih-list-item').css({'height': rh,'width': rw});
            }
        }
    }
    function uabb_ihover_init() {
        $('.uabb-ih-list').each(function(index, el){
            var s   = $(el).attr('data-shape');
            var h  = $(el).attr('data-height');
            var w   = $(el).attr('data-width');
            var rh = $(el).attr('data-res_height');
            var rw  = $(el).attr('data-res_width');
            var ww = jQuery(window).width() || '';
                
            $(el).find('li').each(function(){

                // Shape
                $(el).find('.uabb-ih-item').addClass('uabb-ih-' + s);

                // Responsive & Normal
                responsive_sizes(el, ww, h, w, rh, rw);

            });
        });
	}

}(jQuery, window));