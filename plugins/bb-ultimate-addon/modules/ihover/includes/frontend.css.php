<?php
$settings->border_color = UABB_Helper::uabb_colorpicker( $settings, 'border_color' );

$settings->background_color = UABB_Helper::uabb_colorpicker( $settings, 'background_color', true );

$settings->title_color = UABB_Helper::uabb_colorpicker( $settings, 'title_color' );
$settings->description_color = UABB_Helper::uabb_colorpicker( $settings, 'description_color' );
$settings->separator_color = UABB_Helper::uabb_colorpicker( $settings, 'separator_color' );

?>

<?php $height_width = ( $settings->height_width_options == 'custom' ) ? $settings->height_width : '250'; ?>

<?php
if( count( $settings->ihover_item ) > 0 ) {
    for( $i = 0; $i < count( $settings->ihover_item ); $i++ ) {
        if( is_object( $settings->ihover_item[$i] ) ) {

            if( $settings->ihover_item[$i]->border_style != 'none' ) {
            ?>
            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-wrapper {
                border-style: <?php echo $settings->ihover_item[$i]->border_style; ?>;
                border-width: <?php echo $settings->ihover_item[$i]->border_size; ?>px;
                border-color: <?php echo $settings->ihover_item[$i]->border_color; ?>;
            }
            <?php
            }
            ?>
            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-info-back {
                background-color: <?php echo uabb_theme_base_color( $settings->ihover_item[$i]->background_color ); ?>;
            }
            <?php
            if( $settings->ihover_item[$i]->title_color!= '' ) {
            ?>
                .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-heading,
                .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> a.uabb-ih-link .uabb-ih-heading {
                    <?php
                    echo ( $settings->ihover_item[$i]->title_color != '' ) ? 'color:' . $settings->ihover_item[$i]->title_color . ';' : '';
                    ?>
                }
            <?php
            }

            ?>
            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-description,
            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> a .uabb-ih-description {
                color: <?php echo uabb_theme_text_color( $settings->ihover_item[$i]->description_color ); ?>;
            }

            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-description *,
            .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> a .uabb-ih-description * {
                color: inherit;
                font-family: inherit;
                font-weight: inherit; 
                font-size: inherit;
                font-style: inherit; 
                line-height: inherit;
                margin: 0;
            }
            <?php
            if( $settings->ihover_item[$i]->separator_style != 'none' ) {
            ?>
                .fl-node-<?php echo $id; ?> .uabb-ih-item-<?php echo $i; ?> .uabb-ih-line {
                    border-style: <?php echo $settings->ihover_item[$i]->separator_style; ?>;
                    border-color: <?php echo $settings->ihover_item[$i]->separator_color; ?>;
                    width: <?php echo $settings->ihover_item[$i]->separator_width; ?>%;
                    border-top-width: <?php echo $settings->ihover_item[$i]->separator_size; ?>px;
                }

            <?php
            }
        }
    }
}
?>

.fl-node-<?php echo $id; ?> .uabb-ih-container ul.uabb-ih-list li.uabb-ih-list-item {
    margin: <?php echo ( $settings->spacing / 2 ); ?>px;
}

.fl-node-<?php echo $id; ?> .uabb-align-<?php echo $settings->align; ?> {
    text-align: <?php echo $settings->align; ?>;
}

.fl-node-<?php echo $id; ?> .uabb-ih-image-block,
.fl-node-<?php echo $id; ?> .uabb-ih-item,
.fl-node-<?php echo $id; ?> .uabb-ih-list-item {
    height: <?php echo $height_width; ?>px;
    width: <?php echo $height_width; ?>px;
}

<?php

if( $settings->title_typography_font_size['desktop'] != '' || $settings->title_typography_line_height['desktop'] != '' || $settings->title_typography_font_family['family'] != 'Default' || $settings->title_margin_top!= '' || $settings->title_margin_bottom!= '' ) {

?>

    .fl-node-<?php echo $id; ?> .uabb-ih-heading,
    .fl-node-<?php echo $id; ?> a.uabb-ih-link .uabb-ih-heading {
        <?php
        if( $settings->title_typography_font_family['family'] != 'Default' ) {
            UABB_Helper::uabb_font_css( $settings->title_typography_font_family );
        }
        
        echo ( $settings->title_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->title_typography_font_size['desktop'] . 'px;' : '';
        echo ( $settings->title_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->title_typography_line_height['desktop'] . 'px;' : '';
        echo ( $settings->title_margin_top != '' ) ? 'margin-top: ' . $settings->title_margin_top . 'px;' : '';
        echo ( $settings->title_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->title_margin_bottom . 'px;' : '';       
        ?>
    }

    .fl-node-<?php echo $id; ?> .uabb-ih-description,
    .fl-node-<?php echo $id; ?> a .uabb-ih-description {
        <?php
        if( $settings->desc_typography_font_family['family'] != 'Default' ) {
            UABB_Helper::uabb_font_css( $settings->desc_typography_font_family );
        }
        echo ( $settings->desc_typography_font_size['desktop'] != '' ) ? 'font-size: ' . $settings->desc_typography_font_size['desktop'] . 'px;' : '';
        echo ( $settings->desc_typography_line_height['desktop'] != '' ) ? 'line-height: ' . $settings->desc_typography_line_height['desktop'] . 'px;' : '';
        ?>
    }


<?php
}

if( $settings->desc_margin_top != '' || $settings->desc_margin_bottom != '' ) {
?>
    .fl-node-<?php echo $id; ?> .uabb-ih-description-block {
        margin-top: <?php echo $settings->desc_margin_top; ?>px;
        margin-bottom: <?php echo $settings->desc_margin_bottom; ?>px;
    }

<?php
}
?>

.fl-node-<?php echo $id; ?> .uabb-ih-divider-block {
    <?php
    echo ( $settings->separator_margin_top != '' ) ? 'margin-top: ' . $settings->separator_margin_top . 'px;' : '';
    echo ( $settings->separator_margin_bottom != '' ) ? 'margin-bottom: ' . $settings->separator_margin_bottom . 'px;' : '';
    ?>
}

<?php
if( $global_settings->responsive_enabled ) { // Global Setting If started
?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint; ?>px ) {
        <?php
        if( $settings->responsive_size == 'yes' ) {
        ?>
        .fl-node-<?php echo $id; ?> .uabb-ih-image-block,
        .fl-node-<?php echo $id; ?> .uabb-ih-item,
        .fl-node-<?php echo $id; ?> .uabb-ih-list-item {
            height: <?php echo $settings->height_width_responsive; ?>px;
            width: <?php echo $settings->height_width_responsive; ?>px;
        }
        <?php
        }
        ?>

        .fl-node-<?php echo $id; ?> .uabb-ih-description,
        .fl-node-<?php echo $id; ?> a .uabb-ih-description {
            <?php
            echo ( $settings->desc_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->desc_typography_line_height['medium'] . 'px;' : '';
            echo ( $settings->desc_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->desc_typography_font_size['medium'] . 'px;' : '';
            ?>
        }


        .fl-node-<?php echo $id; ?> .uabb-ih-heading,
        .fl-node-<?php echo $id; ?> a.uabb-ih-link .uabb-ih-heading {
            <?php
            echo ( $settings->title_typography_line_height['medium'] != '' ) ? 'line-height: ' . $settings->title_typography_line_height['medium'] . 'px;' : '';
            echo ( $settings->title_typography_font_size['medium'] != '' ) ? 'font-size: ' . $settings->title_typography_font_size['medium'] . 'px;' : '';
            ?>
        }
    }
 
     @media ( max-width: <?php echo $global_settings->responsive_breakpoint; ?>px ) {

        .fl-node-<?php echo $id; ?> .uabb-ih-description,
        .fl-node-<?php echo $id; ?> a .uabb-ih-description {
            <?php
            echo ( $settings->desc_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->desc_typography_line_height['small'] . 'px;' : '';
            echo ( $settings->desc_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->desc_typography_font_size['small'] . 'px;' : '';
            ?>
        }

        .fl-node-<?php echo $id; ?> .uabb-ih-heading,
        .fl-node-<?php echo $id; ?> a.uabb-ih-link .uabb-ih-heading {
            <?php
            echo ( $settings->title_typography_line_height['small'] != '' ) ? 'line-height: ' . $settings->title_typography_line_height['small'] . 'px;' : '';
            echo ( $settings->title_typography_font_size['small'] != '' ) ? 'font-size: ' . $settings->title_typography_font_size['small'] . 'px;' : '';
            ?>
        }

        <?php
        if( $settings->responsive_size == 'yes' ) {
        ?>
        .fl-node-<?php echo $id; ?> .uabb-ih-image-block,
        .fl-node-<?php echo $id; ?> .uabb-ih-item,
        .fl-node-<?php echo $id; ?> .uabb-ih-list-item {
            <?php
            echo ( $settings->height_width_responsive != '' ) ? 'height: ' . $settings->height_width_responsive . 'px;' : '';
            echo ( $settings->height_width_responsive != '' ) ? 'width: ' . $settings->height_width_responsive . 'px;' : '';
            ?>
            max-width: 100%;
        }
        <?php
        }
        ?>
    }
<?php
}
?>