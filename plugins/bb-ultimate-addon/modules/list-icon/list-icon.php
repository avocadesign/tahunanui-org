<?php

/**
 * @class UABBIconListModule
 */
class UABBIconListModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          	=> __('List Icon', 'uabb'),
			'description'   	=> __('Display a group of linked Font Awesome icons.', 'uabb'),
			'category'      	=> __('Ultimate Addons', 'uabb'),
			'dir'           	=> BB_ULTIMATE_ADDON_DIR . 'modules/list-icon/',
            'url'           	=> BB_ULTIMATE_ADDON_URL . 'modules/list-icon/',
			'editor_export' 	=> false,
			'partial_refresh'	=> true
		));
		$this->add_css( 'font-awesome' );
	}

	/**
	 * @method render_image
	 */
	public function render_image() {
		/* Render Html */
 
		/* Render HTML "$settings" Array */

		$imageicon_array = array(

			/* General Section */
			'image_type' => $this->settings->image_type,

			/* Icon Basics */
			'icon' => $this->settings->icon,
			'icon_size' => $this->settings->icon_size,
			'icon_align' => '',

			/* Image Basics */
			'photo_source' => $this->settings->photo_source,
			'photo' => $this->settings->photo,
			'photo_url' => $this->settings->photo_url,
			'img_size' => $this->settings->img_size,
			'img_align' => '',
			'photo_src' => ( isset( $this->settings->photo_src ) ) ? $this->settings->photo_src : '' ,

			/* Icon Style */
			'icon_style' => $this->settings->icon_style,
			'icon_bg_size' => $this->settings->icon_bg_size,
			'icon_border_style' => $this->settings->icon_border_style,
			'icon_border_width' => $this->settings->icon_border_width,
			'icon_bg_border_radius' => $this->settings->icon_bg_border_radius,

			/* Image Style */
			'image_style' => $this->settings->image_style,
			'img_bg_size' => $this->settings->img_bg_size,
			'img_border_style' => $this->settings->img_border_style,
			'img_border_width' => $this->settings->img_border_width,
			'img_bg_border_radius' => $this->settings->img_bg_border_radius,
		); 
		/* Render HTML Function */
		echo '<div class="uabb-callout-outter">';
		FLBuilder::render_module_html( 'image-icon', $imageicon_array );
		echo '</div>';
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('UABBIconListModule', array(
	'columns'      => array(
		'title'         => __('List Element', 'uabb'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'list_items'     => array(
						'type'         => 'form',
						'label'        => __('List Item', 'uabb'),
						'form'         => 'list-icon_list_item_form',
						'preview_text' => 'title',
						'multiple'     => true
					),
				)
			)
		)
	),
	//'icons' => BB_Ultimate_Addon::uabb_object_get( 'image-icon' ),
	'icons' => array(
		'title'         => __('Image / Icon', 'uabb'),
		'sections'      => array(
			'type_general' 		=> BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' ),
			'icon_basic' 	=> array(
				'title'         => 'Icon Basics', // Section Title
		        'fields'        => array( // Section Fields
		            'icon'          => array(
		                'type'          => 'icon',
		                'label'         => __('Icon', 'uabb')
		            ),
		            'icon_size'     => array(
		                'type'          => 'text',
		                'label'         => __('Size', 'uabb'),
		                'default'       => '30',
		                'maxlength'     => '5',
		                'size'          => '6',
		                'description'   => 'px',
		            ),
		        )
			),
			'img_basic' 	=> array(
				'title'         => 'Image Basics', // Section Title
				'fields'        => array( // Section Fields
					'photo_source'  => array(
						'type'          => 'select',
						'label'         => __('Photo Source', 'uabb'),
						'default'       => 'library',
						'options'       => array(
							'library'       => __('Media Library', 'uabb'),
							'url'           => __('URL', 'uabb')
						),
						'toggle'        => array(
							'library'       => array(
								'fields'        => array('photo')
							),
							'url'           => array(
								'fields'        => array('photo_url' )
							)
						)
					),
					'photo'         => array(
						'type'          => 'photo',
						'label'         => __('Photo', 'uabb')
					),
					'photo_url'     => array(
						'type'          => 'text',
						'label'         => __('Photo URL', 'uabb'),
						'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
					),
					'img_size'     => array(
						'type'          => 'text',
						'label'         => __('Size', 'uabb'),
						'default'       => '50',
						'maxlength'     => '5',
						'size'          => '6',
						'description'   => 'px',
					),
				)
			),

			/* Icon Style Section */
			'icon_style'			=> array(
				'title'			=> 'Style',
				'fields'		=> array(
					/* Icon Style */
					'icon_style'         => array(
                    	'type'          => 'select',
                    	'label'         => __('Icon Background Style', 'uabb'),
                    	'default'       => 'simple',
                    	'options'       => array(
                        	'simple'        => __('Simple', 'uabb'),
                        	'circle'          => __('Circle Background', 'uabb'),
                        	'square'         => __('Square Background', 'uabb'),
                        	'custom'         => __('Design your own', 'uabb'),
                    	),
                        'toggle' => array(
                            'simple' => array(
                                'fields' => array(),
                                /*'sections' => array( 'colors' )*/
                            ),
                            'circle' => array(
                                /*'sections' => array( 'colors' ),*/
                                'fields' => array( 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d' ),
                            ),
                            'square' => array(
                                /*'sections' => array( 'colors' ),*/
                                'fields' => array( 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d' ),
                            ),
                            'custom' => array(
                                /*'sections' => array( 'colors' ),*/
                                'fields' => array( 'icon_border_style', 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d', 'icon_bg_size', 'icon_bg_border_radius' ),
                            )
                        ),
						'trigger' => array(
							'custom' => array(
								'fields' => array( 'icon_border_style' ),
							)
						),
                	),
					
					/* Icon Background SIze */
					'icon_bg_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'uabb'),
                        'default'       => '',
                        'help'          => 'Spacing between Icon & Background edge',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'description'   => 'px'
                    ),

                    /* Border Style and Radius for Icon */
					'icon_border_style'   => array(
		                'type'          => 'select',
		                'label'         => __('Border Style', 'uabb'),
		                'default'       => 'none',
		                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
		                'options'       => array(
		                    'none'   => __( 'None', 'Border type.', 'uabb' ),
		                    'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
		                    'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
		                    'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
		                    'double' => __( 'Double', 'Border type.', 'uabb' )
		                ),
		                'toggle'        => array(
		                    'solid'         => array(
		                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
		                    ),
		                    'dashed'        => array(
		                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
		                    ),
		                    'dotted'        => array(
		                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
		                    ),
		                    'double'        => array(
		                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
		                    )
		                ),
		            ),
		            'icon_border_width'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',
		            ),
		            'icon_bg_border_radius'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Radius', 'uabb'),
		                'default'		=> '0',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',

		            ),
				)
			), 

			/* Image Style Section */
			'img_style'			=> array(
				'title'			=> 'Style',
				'fields'		=> array(
					/* Image Style */
					'image_style'         => array(
                    	'type'          => 'select',
                    	'label'         => __('Image Style', 'uabb'),
                    	'default'       => 'simple',
                    	'help'			=> __('Circle and Square style will crop your image in 1:1 ratio','uabb'),
                    	'options'       => array(
                        	'simple'        => __('Simple', 'uabb'),
                        	'circle'        => __('Circle', 'uabb'),
                        	'square'        => __('Square', 'uabb'),
                        	'custom'        => __('Design your own', 'uabb'),
                    	),
                        'toggle' => array(
                            'simple' => array(
                                'fields' => array()
                            ),
                            'circle' => array(
                                'fields' => array( ),
                            ),
                            'square' => array(
                                'fields' => array( ),
                            ),
                            'custom' => array(
                                'sections'  => array( 'img_colors' ),
                                'fields'	=> array( 'img_bg_size', 'img_border_style', 'img_border_width', 'img_bg_border_radius' ) 
                            )
                        ),
                        'trigger'       => array(
							'custom'           => array(
								'fields'        => array('img_border_style')
							),
							
						)
                	),

                    /* Image Background Size */
                    'img_bg_size'          => array(
                        'type'          => 'text',
                        'label'         => __('Background Size', 'uabb'),
                        'default'       => '',
                        'help'          => 'Spacing between Image edge & Background edge',
                        'maxlength'     => '3',
                        'size'          => '6',
                        'description'   => 'px',

                    ),

                    /* Border Style and Radius for Image */
					'img_border_style'   => array(
		                'type'          => 'select',
		                'label'         => __('Border Style', 'uabb'),
		                'default'       => 'none',
		                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
		                'options'       => array(
		                    'none'   => __( 'None', 'Border type.', 'uabb' ),
		                    'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
		                    'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
		                    'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
		                    'double' => __( 'Double', 'Border type.', 'uabb' )
		                ),
		                'toggle'        => array(
		                    'solid'         => array(
		                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
		                    ),
		                    'dashed'        => array(
		                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
		                    ),
		                    'dotted'        => array(
		                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
		                    ),
		                    'double'        => array(
		                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
		                    )
		                ),

		            ),
		            'img_border_width'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Width', 'uabb'),
		                'default'       => '1',
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',

		            ),
		            'img_bg_border_radius'    => array(
		                'type'          => 'text',
		                'label'         => __('Border Radius', 'uabb'),
		                'description'   => 'px',
		                'maxlength'     => '3',
		                'size'          => '6',
		                'placeholder'   => '0',

		            ),
				)
			),
			/* Icon Colors */
			'icon_colors'        => array( // Section
                'title'         => __('Colors', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    
                    'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'     => __('Icon Color', 'uabb'),
		                )
		            ),
		            'icon_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Icon Hover Color', 'uabb'),
		                    'preview'       => array(
		                            'type'      => 'none',
		                    )
		                )
		            ),

		            /* Background Color Dependent on Icon Style **/
		            'icon_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Color', 'uabb'),
		                )
		            ),
		            'icon_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
		            'icon_bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Hover Color', 'uabb'),
		                    'preview'       => array(
		                            'type'      => 'none',
		                    )
		                )
		            ),
		            'icon_bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),

		             /* Border Color Dependent on Border Style for ICon */
		            'icon_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Color', 'uabb'),
		                )
		            ),
		            'icon_border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Hover Color', 'uabb'),
		                )
		            ),

                    /* Gradient Color Option */
                    'icon_three_d'       => array(
                        'type'          => 'select',
                        'label'         => __('Gradient', 'uabb'),
                        'default'       => '0',
                        'options'       => array(
                            '0'             => __('No', 'uabb'),
                            '1'             => __('Yes', 'uabb')
                        )
                    ),
                )
            ),

			/* Image Colors */
			'img_colors'        => array( // Section
                'title'         => __('Colors', 'uabb'), // Section Title
                'fields'        => array( // Section Fields
                    /* Background Color Dependent on Icon Style **/
		            'img_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Color', 'uabb'),
		                )
		            ),
		            'img_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
		            'img_bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Background Hover Color', 'uabb'),
		                    'preview'       => array(
		                            'type'      => 'none',
		                    )
		                )
		            ),
		            'img_bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),


		             /* Border Color Dependent on Border Style for Image */
		            'img_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Color', 'uabb'),
		                )
		            ),
		            'img_border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
		                array(
		                    'label'         => __('Border Hover Color', 'uabb'),
		                )
		            ),
                )
            ),
		)
	),
	'style'         => array( // Tab
		'title'         => __('Typography', 'uabb'), // Tab title
		'sections'      => array( // Tab Sections
			'typography' => BB_Ultimate_Addon::uabb_section_get( 
                'typography', 
                array(
                	'title' => __('Typography', 'uabb' ),
                	'fields'        => array( // Section Fields
						'font_family'       => array(
							'label'         => __('Font Family', 'uabb'),
							'preview'         => array(
                                'type'            => 'font',
                                'selector'        => '.uabb-list-icon-text-heading'
                            )
						),
						'tag_selection'       => array(
							'default'       => 'h3'
						),
						'color' => array(
							'preview' => array(
								'type' => 'css',
								'selector' => '.uabb-list-icon .uabb-list-icon-text .uabb-list-icon-text-heading',
								'property' => 'color'
							)
						)
					)
                ), 
                array(),
                'typography'
            ),
			'structure'     => array( // Section
				'title'         => __('Structure', 'uabb'), // Section Title
				'fields'        => array( // Section Fields
					'icon_struc_align'     => array(
                        'type'          => 'uabb-toggle-switch',
                        'label'         => __( 'Icons Structure', 'fl-builder' ),
                        'default'       => 'vertical',
                        'options'       => array(
                         	'horizontal'		=> 'Horizontal',
                          	'vertical'			=> 'Vertical',
                        ),
                        'width'			=> '70px',
                    ),
					'align'         => array(
						'type'          => 'select',
						'label'         => __('Alignment', 'uabb'),
						'default'       => 'flex-start',
						'options'       => array(
							'center'		=> __('Center', 'uabb'),
							'flex-start'	=> __('Left', 'uabb'),
							'flex-end'		=> __('Right', 'uabb')
						),
					),
					'spacing'       => array(
						'type'          => 'text',
						'label'         => __('Space Between Two List Elements', 'uabb'),
						'default'       => '',
						'placeholder'	=> '10',
						'maxlength'     => '4',
						'size'          => '4',
						'description'   => 'px',
					),
					'icon_text_spacing'       => array(
						'type'          => 'text',
						'label'         => __('Space Between Icon & Text', 'uabb'),
						'default'       => '',
						'placeholder'	=> '10',
						'maxlength'     => '4',
						'size'          => '4',
						'description'   => 'px',
					),
				)
			),
			'mobile'     => array( // Section
				'title'         => __('Mobile', 'uabb'), // Section Title
				'fields'        => array( // Section Fields
					'mobile_spacing'       => array(
						'type'          => 'text',
						'label'         => __('Space Between Two List Elements', 'uabb'),
						'default'       => '',
						'placeholder'	=> '10',
						'maxlength'     => '4',
						'size'          => '4',
						'description'   => 'px',
						'preview'		=> array(
							'type'	=> 'none'
						)
					),
				)
			)
		)
	)
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('list-icon_list_item_form', array(
	'title' => __('Add List Item', 'uabb'),
	'tabs'  => array(
		'general'       => array( // Tab
			'title'         => __('General', 'uabb'), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array( // Section
					'title'         => '', // Section Title
					'fields'        => array( // Section Fields
						'title'          => array(
							'type'          => 'text',
							'label'         => __('Title', 'uabb')
						),
					)
				)
			)
		)
	)
));
