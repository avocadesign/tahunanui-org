<?php
/*
 * Param: Button
 * Description: This is a form for Button. All possible customization could be made in this form.
 *
*/

BB_Ultimate_Addon::uabb_section_register( 'border',   
    array(
        'title'     => __('Border', 'uabb'),
        'fields'    => array(
            'uabb_border_type'   => array(
                'type'          => 'select',
                'label'         => __('Type', 'uabb'),
                'default'       => 'none',
                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
                'options'       => array(
                    'none'   => _x( 'None', 'Border type.', 'uabb' ),
                    'solid'  => _x( 'Solid', 'Border type.', 'uabb' ),
                    'dashed' => _x( 'Dashed', 'Border type.', 'uabb' ),
                    'dotted' => _x( 'Dotted', 'Border type.', 'uabb' ),
                    'double' => _x( 'Double', 'Border type.', 'uabb' )
                ),
                'toggle'        => array(
                    ''              => array(
                        'fields'        => array()
                    ),
                    'solid'         => array(
                        'fields'        => array('uabb_border_color', 'uabb_border_top', 'uabb_border_bottom', 'uabb_border_left', 'uabb_border_right', 'responsive_border')
                    ),
                    'dashed'        => array(
                        'fields'        => array('uabb_border_color', 'uabb_border_top', 'uabb_border_bottom', 'uabb_border_left', 'uabb_border_right', 'responsive_border')
                    ),
                    'dotted'        => array(
                        'fields'        => array('uabb_border_color', 'uabb_border_top', 'uabb_border_bottom', 'uabb_border_left', 'uabb_border_right', 'responsive_border')
                    ),
                    'double'        => array(
                        'fields'        => array('uabb_border_color', 'uabb_border_top', 'uabb_border_bottom', 'uabb_border_left', 'uabb_border_right', 'responsive_border')
                    )
                )
            ),
            'uabb_border_top'    => array(
                'type'          => 'text',
                'label'         => __('Top Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '5',
                'placeholder'   => '0'
            ),
            'uabb_border_bottom' => array(
                'type'          => 'text',
                'label'         => __('Bottom Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '5',
                'placeholder'   => '0'
            ),
            'uabb_border_left'   => array(
                'type'          => 'text',
                'label'         => __('Left Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '5',
                'placeholder'   => '0'
            ),
            'uabb_border_right'  => array(
                'type'          => 'text',
                'label'         => __('Right Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '5',
                'placeholder'   => '0'
            ),
            'uabb_border_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                    /*'preview'       => array(
                        'type'          => 'css',
                        'property'      => 'border-color'
                    )*/
                )
            ),
            'responsive_border'   => array(
                'type'          => 'select',
                'label'         => __('Hide on Small Screen Devices', 'uabb'),
                'default'       => 'no',
                'options'       => array(
                    'yes'   => _x( 'Yes', 'Border type.', 'uabb' ),
                    'no'  => _x( 'No', 'Border type.', 'uabb' )
                )
            ),
        )
    )
);

BB_Ultimate_Addon::uabb_section_register( 'typography',
    array(
        'title'     => __('', 'uabb'),
        'fields'    => array(
            'tag_selection'   => array(
                'type'          => 'select',
                'label'         => __('Title Tag', 'uabb'),
                'default'       => 'h2',
                'options'       => array(
                    'h1'      => __('H1', 'uabb'),
                    'h2'      => __('H2', 'uabb'),
                    'h3'      => __('H3', 'uabb'),
                    'h4'      => __('H4', 'uabb'),
                    'h5'      => __('H5', 'uabb'),
                    'h6'      => __('H6', 'uabb'),
                    'div'     => __('Div', 'uabb'),
                    'p'       => __('p', 'uabb'),
                    'span'    => __('span', 'uabb'),
                )
            ),
            'font_family'       => array(
                'type'          => 'font',
                'label'         => __('Font Family', 'uabb'),
                'default'       => array(
                    'family'        => 'Default',
                    'weight'        => 'Default'
                )
            ),
            'font_size'     => array(
                'type'          => 'uabb-simplify',
                'label'         => __( 'Font Size', 'fl-builder' ),
                'default'       => array(
                    'desktop'       => '',
                    'medium'        => '',
                    'small'         => '',
                )
            ),
            'line_height'    => array(
                'type'          => 'uabb-simplify',
                'label'         => __( 'Line Height', 'fl-builder' ),
                'default'       => array(
                    'desktop'       => '',
                    'medium'        => '',
                    'small'         => '',
                )
            ),
            'color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                    'label'         => __('Color', 'uabb'),
                    'default'       => '',
                )
            ),
        )
    ) 
);

FLBuilder::register_settings_form('button_form_field', array(
    'title' => __('Button', 'uabb'),
    'tabs'  => array(
        'general'       => array(
            'title'         => __('General', 'uabb'),
            'sections'      => array(
                'general'       => array(
                    'title'         => '',
                    'fields'        => array(
                        'text'          => array(
                            'type'          => 'text',
                            'label'         => __('Text', 'uabb'),
                            'default'       => __('Click Here', 'uabb'),
                            'preview'         => array(
                                'type'            => 'text',
                                'selector'        => '.uabb-creative-button-text'
                            )
                        ),

                    )
                ),
                'link'          => array(
                    'title'         => __('Link', 'uabb'),
                    'fields'        => array(
                        'link'          => array(
                            'type'          => 'link',
                            'label'         => __('Link', 'uabb'),
                            'placeholder'   => __( 'http://www.example.com', 'uabb' ),
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'link_target'   => array(
                            'type'          => 'select',
                            'label'         => __('Link Target', 'uabb'),
                            'default'       => '_self',
                            'options'       => array(
                                '_self'         => __('Same Window', 'uabb'),
                                '_blank'        => __('New Window', 'uabb')
                            ),
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        )
                    )
                )
            )
        ),
        'style'         => array(
            'title'         => __('Style', 'uabb'),
            'sections'      => array(
                'style'         => array(
                    'title'         => __('Style', 'uabb'),
                    'fields'        => array(
                        'style'         => array(
                            'type'          => 'select',
                            'label'         => __('Style', 'uabb'),
                            'default'       => 'flat',
                            'class'         => 'creative_button_styles',
                            'options'       => array(
                                'flat'          => __('Flat', 'uabb'),
                                'gradient'      => __('Gradient', 'uabb'),
                                'transparent'   => __('Transparent', 'uabb'),
                                'threed'          => __('3D', 'uabb'),
                            ),
                            'toggle'        => array(
                                'transparent'   => array(
                                    'fields'    => array( 'border_size', 'transparent_button_options'),
                                ),
                                'threed'   => array(
                                    'fields'    => array( 'threed_button_options' )
                                ),
                                'flat'   => array(
                                    'fields'    => array( 'flat_button_options' )
                                ),
                                
                            )
                        ),
                        'border_size'   => array(
                            'type'          => 'text',
                            'label'         => __('Border Size', 'uabb'),
                            'default'       => '2',
                            'description'   => 'px',
                            'maxlength'     => '3',
                            'size'          => '5',
                            'placeholder'   => '0'
                        ),
                        'transparent_button_options'         => array(
                            'type'          => 'select',
                            'label'         => __('Hover Styles', 'uabb'),
                            'default'       => 'transparent-fade',
                            'options'       => array(
                                'none'          => __('None', 'uabb'),
                                'transparent-fade'          => __('Fade Background', 'uabb'),
                                'transparent-fill-top'      => __('Fill Background From Top', 'uabb'),
                                'transparent-fill-bottom'      => __('Fill Background From Bottom', 'uabb'),
                                'transparent-fill-left'     => __('Fill Background From Left', 'uabb'),
                                'transparent-fill-right'     => __('Fill Background From Right', 'uabb'),
                                'transparent-fill-center'       => __('Fill Background Vertical', 'uabb'),
                                'transparent-fill-diagonal'     => __('Fill Background Diagonal', 'uabb'),
                                'transparent-fill-horizontal'  => __('Fill Background Horizontal', 'uabb'),
                            ),
                        ),
                        'threed_button_options'         => array(
                            'type'          => 'select',
                            'label'         => __('Hover Styles', 'uabb'),
                            'default'       => 'threed_down',
                            'options'       => array(
                                'threed_down'          => __('Move Down', 'uabb'),
                                'threed_up'      => __('Move Up', 'uabb'),
                                'threed_left'      => __('Move Left', 'uabb'),
                                'threed_right'     => __('Move Right', 'uabb'),
                                'animate_top'     => __('Animate Top', 'uabb'),
                                'animate_bottom'     => __('Animate Bottom', 'uabb'),
                                /*'animate_left'     => __('Animate Left', 'uabb'),
                                'animate_right'     => __('Animate Right', 'uabb'),*/
                            ),
                        ),
                        'flat_button_options'         => array(
                            'type'          => 'select',
                            'label'         => __('Hover Styles', 'uabb'),
                            'default'       => 'none',
                            'options'       => array(
                                'none'          => __('None', 'uabb'),
                                'animate_to_left'      => __('Appear Icon From Right', 'uabb'),
                                'animate_to_right'          => __('Appear Icon From Left', 'uabb'),
                                'animate_from_top'      => __('Appear Icon From Top', 'uabb'),
                                'animate_from_bottom'     => __('Appear Icon From Bottom', 'uabb'),
                            ),
                        ),
                    )
                ),
                'icon'    => array(
                    'title'         => __('Icons', 'uabb'),
                    'fields'        => array(
                        'icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb'),
                            'show_remove'   => true
                        ),
                        'icon_position' => array(
                            'type'          => 'select',
                            'label'         => __('Icon Position', 'uabb'),
                            'default'       => 'before',
                            'options'       => array(
                                'before'        => __('Before Text', 'uabb'),
                                'after'         => __('After Text', 'uabb')
                            )
                        )
                    )
                ),
                'colors'        => array(
                    'title'         => __('Colors', 'uabb'),
                    'fields'        => array(
                        'bg_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => '',
                            )
                        ),
                        'bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

                        'bg_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Background Hover Color', 'uabb'),
                            'default'       => '',
                            'preview'       => array(
                                    'type'          => 'none'
                                )
                            ) 
                        ),
                        'bg_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

                        'text_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Text Color', 'uabb'),
                            'default'       => '',
                            ) 
                        ),

                        'text_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                            'label'         => __('Text Hover Color', 'uabb'),
                            'default'       => '',
                            'preview'       => array(
                                    'type'          => 'none'
                                )
                            ) 
                        ),
                    )
                ),
                'formatting'    => array(
                    'title'         => __('Structure', 'uabb'),
                    'fields'        => array(
                        'width'         => array(
                            'type'          => 'select',
                            'label'         => __('Width', 'uabb'),
                            'default'       => 'auto',
                            'options'       => array(
                                'auto'          => _x( 'Auto', 'Width.', 'uabb' ),
                                'full'          => __('Full Width', 'uabb'),
                                'custom'        => __('Custom', 'uabb')
                            ),
                            'toggle'        => array(
                                'auto'          => array(
                                    'fields'        => array('align', 'mob_align', 'line_height')
                                ),
                                'full'          => array(
                                    'fields'        => array( 'line_height' )
                                ),
                                'custom'        => array(
                                    'fields'        => array('align', 'mob_align', 'custom_width', 'custom_height', 'padding_top_bottom', 'padding_left_right' )
                                )
                            )
                        ),
                        'custom_width'  => array(
                            'type'          => 'text',
                            'label'         => __('Custom Width', 'uabb'),
                            'default'       => '200',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'custom_height'  => array(
                            'type'          => 'text',
                            'label'         => __('Custom Height', 'uabb'),
                            'default'       => '45',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'padding_top_bottom'       => array(
                            'type'          => 'text',
                            'label'         => __('Padding Top/Bottom', 'uabb'),
                            'default'       => '',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'padding_left_right'       => array(
                            'type'          => 'text',
                            'label'         => __('Padding Left/Right', 'uabb'),
                            'default'       => '',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'border_radius' => array(
                            'type'          => 'text',
                            'label'         => __('Round Corners', 'uabb'),
                            'default'       => '4',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'align'         => array(
                            'type'          => 'select',
                            'label'         => __('Alignment', 'uabb'),
                            'default'       => 'left',
                            'options'       => array(
                                'center'        => __('Center', 'uabb'),
                                'left'          => __('Left', 'uabb'),
                                'right'         => __('Right', 'uabb')
                            )
                        ),
                        'mob_align'         => array(
                            'type'          => 'select',
                            'label'         => __('Mobile Alignment', 'uabb'),
                            'default'       => 'center',
                            'options'       => array(
                                'center'        => __('Center', 'uabb'),
                                'left'          => __('Left', 'uabb'),
                                'right'         => __('Right', 'uabb')
                            )
                        ),
                    )
                )
            )
        ),
        'creative_typography'         => array(
            'title'         => __('Typography', 'uabb'),
            'sections'      => array(
                'typography'    =>  BB_Ultimate_Addon::uabb_section_get(
                                    'typography',
                                    array(
                                        'title'     => __('Button Settings', 'uabb' ) ,
                                        'fields'   => array(
                                            'color'   => array(
                                                'label' => __('Choose Color', 'uabb'),
                                            ),
                                            'tag_selection'   => array(
                                                'label' => __('Select Tag', 'uabb'),
                                            ),
                                            'font_family' => array(
                                                'preview'         => array(
                                                    'type'            => 'font',
                                                    'selector'        => '.uabb-creative-button'
                                                )
                                            ),
                                        )
                                    ),
                                    array('color','tag_selection')
                ),
            )
        )
    )
));

/**
 * Register the module and its form settings.
 */

FLBuilder::register_settings_form('icon_form_field', array(
    'title' => __('Icon', 'uabb'),
    'tabs'  => array(
        'general'       => array( // Tab
            'title'         => __('General', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'general'       => array( // Section
                    'title'         => '', // Section Title
                    'fields'        => array( // Section Fields
                        'icon'          => array(
                            'type'          => 'icon',
                            'label'         => __('Icon', 'uabb'),
                            'show_remove'   => true
                        )
                    )
                ),
                'link'          => array(
                    'title'         => 'Link',
                    'fields'        => array(
                        'link'          => array(
                            'type'          => 'link',
                            'label'         => __('Link', 'uabb'),
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'link_target'   => array(
                            'type'          => 'select',
                            'label'         => __('Link Target', 'uabb'),
                            'default'       => '_self',
                            'options'       => array(
                                '_self'         => __('Same Window', 'uabb'),
                                '_blank'        => __('New Window', 'uabb')
                            ),
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        )
                    )
                ),
                'text'          => array(
                    'title'         => 'Text',
                    'fields'        => array(
                        'text'          => array(
                            'type'          => 'editor',
                            'label'         => '',
                            'media_buttons' => false
                        )
                    )
                )
            )
        ),
        'style'         => array( // Tab
            'title'         => __('Style', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'style'     => array( // Section
                    'title'         => __('Style', 'uabb'), // Section Title
                    'fields'        => array( // Section Fields
                        'style'         => array(
                            'type'          => 'select',
                            'label'         => __('Icon Style', 'uabb'),
                            'default'       => 'simple',
                            'options'       => array(
                                'simple'        => __('Simple', 'uabb'),
                                'circle'          => __('Circle Background', 'uabb'),
                                'square'         => __('Square Background', 'uabb'),
                                'custom'         => __('Design your own', 'uabb'),
                            ),
                            'toggle' => array(
                                'simple' => array(
                                    'fields' => array()
                                ),
                                'circle' => array(
                                    'fields' => array( 'bg_color', 'bg_hover_color', 'three_d' ),
                                    'sections' => array( 'border' )
                                ),
                                'square' => array(
                                    'fields' => array( 'bg_color', 'bg_hover_color', 'three_d' ),
                                    'sections' => array( 'border' )
                                ),
                                'custom' => array(
                                    'fields' => array( 'bg_color', 'bg_hover_color', 'three_d', 'bg_size', 'bg_border_radius' ),
                                    'sections' => array( 'border' )
                                )
                            )
                        ),
                        /*'style_bg_color' => array(
                            'type'          => 'color',
                            'label'         => __('Color Option for Background', 'uabb'),
                            'show_reset'    => true,
                        ),*/
                        'bg_size'          => array(
                            'type'          => 'text',
                            'label'         => __('Background Size', 'uabb'),
                            'default'       => '',
                            'help'          => 'Space between icon and background',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'bg_border_radius'          => array(
                            'type'          => 'text',
                            'label'         => __('Border Radius ( For Background )', 'uabb'),
                            'default'       => '0',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                    )
                ),
                'colors'        => array( // Section
                    'title'         => __('Colors', 'uabb'), // Section Title
                    'fields'        => array( // Section Fields
                        'icon_color_options'         => array(
                            'type'          => 'select',
                            'label'         => __('Icon Color', 'uabb'),
                            'default'       => 'text',
                            'options'       => array(
                                'theme'          => __('Theme Color', 'uabb'),
                                'custom'         => __('Custom Color', 'uabb'),
                            ),
                            'toggle' => array(
                                'custom' => array(
                                    'fields' => array( 'color' ),
                                )
                            )
                        ),
                        'color'         => array(
                            'type'          => 'uabb-color',
                            'label'         => __('Color', 'uabb'),
                            'show_reset'    => true
                        ),
                        'hover_color' => array(
                            'type'          => 'uabb-color',
                            'label'         => __('Hover Color', 'uabb'),
                            'show_reset'    => true,
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        /*'bg_color_options'         => array(
                            'type'          => 'select',
                            'label'         => __('Icon Background Color', 'uabb'),
                            'default'       => 'custom',
                            'options'       => array(
                                'custom'         => __('Custom Color', 'uabb'),
                            ),
                            'toggle' => array(
                                'custom' => array(
                                    'fields' => array( 'bg_color' ),
                                )
                            )
                        ),*/
                        'bg_color'      => array(
                            'type'          => 'uabb-color',
                            'label'         => __('Background Color', 'uabb'),
                            'default'       => 'fafafa',
                            'show_reset'    => true
                        ),
                        'bg_hover_color' => array(
                            'type'          => 'uabb-color',
                            'label'         => __('Background Hover Color', 'uabb'),
                            'show_reset'    => true,
                            'preview'       => array(
                                'type'          => 'none'
                            )
                        ),
                        'three_d'       => array(
                            'type'          => 'select',
                            'label'         => __('Gradient', 'uabb'),
                            'default'       => '0',
                            'options'       => array(
                                '0'             => __('No', 'uabb'),
                                '1'             => __('Yes', 'uabb')
                            )
                        )
                    )
                ),
                'border' => BB_Ultimate_Addon::uabb_param_selector_filter('border','uabb-icon'),
                'structure'     => array( // Section
                    'title'         => __('Structure', 'uabb'), // Section Title
                    'fields'        => array( // Section Fields
                        'size'          => array(
                            'type'          => 'text',
                            'label'         => __('Size', 'uabb'),
                            'default'       => '30',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'align'         => array(
                            'type'          => 'select',
                            'label'         => __('Alignment', 'uabb'),
                            'default'       => 'left',
                            'options'       => array(
                                'center'        => __('Center', 'uabb'),
                                'left'          => __('Left', 'uabb'),
                                'right'         => __('Right', 'uabb')
                            ),
                            'preview'       => array(
                                'type'          => 'css',
                                'selector'      => '.fl-module-content ',
                                'property'      => 'text-align'
                            )
                        )
                    )
                )
            )
        )
    )
));


/**
 * Register the module and its form settings.
 */

FLBuilder::register_settings_form('photo_form_field', array(
    'title' => __('Photo', 'uabb'),
    'tabs'  => array(
        'general'       => array( // Tab
            'title'         => __('General', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'general'       => array( // Section
                    'title'         => '', // Section Title
                    'fields'        => array( // Section Fields
                        'photo_source'  => array(
                            'type'          => 'select',
                            'label'         => __('Photo Source', 'uabb'),
                            'default'       => 'library',
                            'options'       => array(
                                'library'       => __('Media Library', 'uabb'),
                                'url'           => __('URL', 'uabb')
                            ),
                            'toggle'        => array(
                                'library'       => array(
                                    'fields'        => array('photo')
                                ),
                                'url'           => array(
                                    'fields'        => array('photo_url', 'caption')
                                )
                            )
                        ),
                        'photo'         => array(
                            'type'          => 'photo',
                            'label'         => __('Photo', 'uabb')
                        ),
                        'photo_url'     => array(
                            'type'          => 'text',
                            'label'         => __('Photo URL', 'uabb'),
                            'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
                        ),
                        'align'         => array(
                            'type'          => 'select',
                            'label'         => __('Alignment', 'uabb'),
                            'default'       => 'center',
                            'options'       => array(
                                'left'          => __('Left', 'uabb'),
                                'center'        => __('Center', 'uabb'),
                                'right'         => __('Right', 'uabb')
                            )
                        ),
                        'responsive_align'         => array(
                            'type'          => 'select',
                            'label'         => __('Responsive Alignement', 'uabb'),
                            'default'       => 'center',
                            'options'       => array(
                                'left'          => __('Left', 'uabb'),
                                'center'        => __('Center', 'uabb'),
                                'right'         => __('Right', 'uabb')
                            )
                        )
                    )
                ),
                'caption'       => array(
                    'title'         => __('Caption', 'uabb'),
                    'fields'        => array(
                        'show_caption'  => array(
                            'type'          => 'select',
                            'label'         => __('Show Caption', 'uabb'),
                            'default'       => '0',
                            'options'       => array(
                                '0'             => __('Never', 'uabb'),
                                'hover'         => __('On Hover', 'uabb'),
                                'below'         => __('Below Photo', 'uabb')
                            )
                        ),
                        'caption'       => array(
                            'type'          => 'text',
                            'label'         => __('Caption', 'uabb')
                        )
                    )
                ),
                'link'          => array(
                    'title'         => __('Link', 'uabb'),
                    'fields'        => array(
                        'link_type'     => array(
                            'type'          => 'select',
                            'label'         => __('Link Type', 'uabb'),
                            'options'       => array(
                                ''              => _x( 'None', 'Link type.', 'uabb' ),
                                'url'           => __('URL', 'uabb'),
                                'lightbox'      => __('Lightbox', 'uabb'),
                                'file'          => __('Photo File', 'uabb'),
                                'page'          => __('Photo Page', 'uabb')
                            ),
                            'toggle'        => array(
                                ''              => array(),
                                'url'           => array(
                                    'fields'        => array('link_url', 'link_target')
                                ),
                                'file'          => array(),
                                'page'          => array()
                            ),
                            'help'          => __('Link type applies to how the image should be linked on click. You can choose a specific URL, the individual photo or a separate page with the photo.', 'uabb'),
                            'preview'         => array(
                                'type'            => 'none'
                            )
                        ),
                        'link_url'     => array(
                            'type'          => 'link',
                            'label'         => __('Link URL', 'uabb'),
                            'preview'         => array(
                                'type'            => 'none'
                            )
                        ),
                        'link_target'   => array(
                            'type'          => 'select',
                            'label'         => __('Link Target', 'uabb'),
                            'default'       => '_self',
                            'options'       => array(
                                '_self'         => __('Same Window', 'uabb'),
                                '_blank'        => __('New Window', 'uabb')
                            ),
                            'preview'         => array(
                                'type'            => 'none'
                            )
                        )
                    )
                )
            )
        ),
        'style'       => array( // Tab
            'title'         => __('Style', 'uabb'), // Tab title
            'sections'      => array( // Tab Sections
                'general'       => array( // Section
                    'title'         => '', // Section Title
                    'fields'        => array( // Section Fields
                        'style'         => array(
                            'type'          => 'select',
                            'label'         => __('Photo Style', 'uabb'),
                            'default'       => 'simple',
                            'options'       => array(
                                'simple'        => __('Simple', 'uabb'),
                                'circle'          => __('Circle Background', 'uabb'),
                                'square'         => __('Square Background', 'uabb'),
                                'landscape'     => __('Landscape', 'uabb'),
                                'panorama'      => __('Panorama', 'uabb'),
                                'portrait'      => __('Portrait', 'uabb'),
                                'custom'         => __('Custom', 'uabb'),
                            ),
                            'toggle' => array(
                                'simple' => array(
                                    'fields' => array()
                                ),
                                'circle' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size' )
                                ),
                                'square' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size' )
                                ),
                                'landscape' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size' )
                                ),
                                'panorama' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size' )
                                ),
                                'portrait' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size' )
                                ),
                                'custom' => array(
                                    'fields' => array( 'style_bg_color', 'bg_size', 'bg_border_radius' )
                                )
                            )
                        ),
                        'style_bg_color' => array(
                            'type'          => 'uabb-color',
                            'label'         => __('Color Option for Background', 'uabb'),
                            'show_reset'    => true,
                        ),
                        'bg_size'          => array(
                            'type'          => 'text',
                            'label'         => __('Background Size', 'uabb'),
                            'default'       => '',
                            'help'          => 'Space between icon and background',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'bg_border_radius'          => array(
                            'type'          => 'text',
                            'label'         => __('Border Radius ( For Background )', 'uabb'),
                            'default'       => '',
                            'maxlength'     => '3',
                            'size'          => '4',
                            'description'   => 'px'
                        ),
                        'hover_effect'         => array(
                            'type'          => 'select',
                            'label'         => __('Hover Effect', 'uabb'),
                            'default'       => 'style1',
                            'options'       => array(
                                'style1'        => __('Style 1', 'uabb'),
                                'style2'        => __('Style 2', 'uabb'),
                            ),
                            'toggle' => array(
                                'style1' => array(
                                    'fields' => array( 'opacity', 'hover_opacity' )
                                ),
                                'style2' => array(
                                    'fields' => array()
                                ),
                            )
                        ),
                        'opacity' => array(
                            'type'          => 'text',
                            'label'         => __('Image Opacity', 'uabb'),
                            'default'       => '100',
                            'description'   => '%',
                            'maxlength'     => '3',
                            'size'          => '5',
                            'placeholder'   => '100'
                        ),
                        'hover_opacity' => array(
                            'type'          => 'text',
                            'label'         => __('Image Hover Opacity', 'uabb'),
                            'default'       => '100',
                            'description'   => '%',
                            'maxlength'     => '3',
                            'size'          => '5',
                            'placeholder'   => '100'
                        )
                    )
                )
            )
        )
    )
));



