<?php
/**
 * Plugin Name: Ultimate Addon for Beaver Builder
 * Plugin URI: http://www.brainstormforce.com
 * Description: An example plugin for creating custom builder modules.
 * Version: 1.0.0
 * Author: Brainstorm Force
 * Author URI: http://www.brainstormforce.com
 */
define( 'BB_ULTIMATE_ADDON_DIR', plugin_dir_path( __FILE__ ) );
define( 'BB_ULTIMATE_ADDON_URL', plugins_url( '/', __FILE__ ) );

/**
 * Custom modules
 */
if( !class_exists( "BB_Ultimate_Addon" ) ) {
	class BB_Ultimate_Addon {

		//Class variables
		public static $uabb_field;
		public static $uabb_param;
		public static $uabb_object;

		/*
		* Constructor function that initializes required actions and hooks
		* @Since 1.0
		*/
		function __construct() {

			add_action( 'init', array( $this, 'fl_load_uabb' ) );
			add_action( 'admin_notices', array( $this, 'admin_notices' ) );
			add_action( 'network_admin_notices', array( $this, 'admin_notices' ) );

			add_action( 'after_setup_theme', array( $this, 'uabb_load_custom_bb_extensions' ) );

			//	Render JS & CSS
			add_filter( 'fl_builder_render_css', array( $this, 'fl_uabb_render_css' ), 10, 3 );
			//add_filter( 'fl_builder_render_js', array( $this, 'fl_uabb_render_js' ), 10, 3 );

			// Enqueue CSS & JS
			add_action( 'wp_enqueue_scripts', array( $this, 'uabb_builder_css_js' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'uabb_admin_assets' ) );

			//	Register IconFonts
			add_action( 'plugins_loaded', array( $this, 'init_hooks') );
			add_action( 'wp_ajax_uabb_reload_icons', array( $this, 'reload_icons' ) );
			add_action( 'fl_builder_admin_settings_save', array( $this, 'save' ) );

			add_filter( 'fl_builder_admin_settings_nav_items', array( $this, 'settings_nav_item' ) );
			add_action( 'fl_builder_admin_settings_render_forms', array( $this, 'settings_nav_form' ) );

			add_filter( 'uabb_colorpicker', array( $this, 'remove_uabb_colorpicker' ) );
		}

		/* Remove UABB Color Picker filter */
		function remove_uabb_colorpicker() {
			return true;
		}
		function admin_notices() {

			$all_plugins = (array) get_plugins();
			if( count( $all_plugins ) > 0 &&
				array_key_exists('bb-plugin-agency/fl-builder.php', $all_plugins ) ||
				array_key_exists('beaver-builder-lite-version/fl-builder.php', $all_plugins )
			) {
				$url = network_admin_url() . 'plugins.php?s=Beaver+Builder+Plugin';
			} else {
				$url = network_admin_url() . 'plugin-install.php?tab=search&type=term&s=Beaver Builder - WordPress Page Builder';
			}

			if( ! class_exists('FLBuilderModel') ) {
				echo '<div class="notice notice-error">';
                echo "<p>The <strong>Ultimate Addon for Beaver Builder</strong> " . __( 'plugin requires', 'uabb' )." <strong><a href='".$url."'>Beaver Builder</strong></a>" . __( ' plugin installed & activated.', 'uabb' ) . "</p>";
                echo '</div>';
			}
      	}

		function uabb_panel_config() {

			//	UABB panels - Enable / Disable
			if( class_exists('FLBuilderModel') ) {

				require_once 'classes/uabb-global-integration.php';

				$uabb = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb', true );
				if( !empty($uabb) && is_array($uabb) ) {
					if( array_key_exists( 'load_templates', $uabb ) ) {
						if( $uabb['load_templates'] == 1 ) {
							$this->load_templates();

							//	load 'panels'
							if( array_key_exists( 'load_panels', $uabb ) ) {
								if( $uabb['load_panels'] == 1 ) {

									// Enqueue CSS & JS
									add_action( 'wp_enqueue_scripts', array( $this, 'uabb_panel_css_js' ) );

									//	Add Panels & Buttons
									add_filter( 'fl_builder_render_ui_panel', '__return_false' );
									add_action( 'wp_footer', array( $this, 'render_ui') );
									add_filter( 'fl_builder_ui_bar_buttons', array( $this, 'builder_ui_bar_buttons') );
								}
							}
						}
					}

				} else {

					//	load 'templates.dat'
					$this->load_templates();

					// Enqueue CSS & JS
					add_action( 'wp_enqueue_scripts', array( $this, 'uabb_panel_css_js' ) );
					//	Add Panels & Buttons
					add_filter( 'fl_builder_render_ui_panel', '__return_false' );
					add_action( 'wp_footer', array( $this, 'render_ui') );
					add_filter( 'fl_builder_ui_bar_buttons', array( $this, 'builder_ui_bar_buttons') );
				}

			}
		}
		function save() {

			// Only admins can save settings.
			if(!current_user_can('delete_users')) {
				return;
			}

			if ( isset( $_POST['fl-uabb-nonce'] ) && wp_verify_nonce( $_POST['fl-uabb-nonce'], 'uabb' ) ) {

				$uabb['load_panels'] = false;
				if( isset( $_POST['uabb-enabled-panels'] ) ) {
					$uabb['load_panels'] = true;
				}

				$uabb['load_templates'] = false;
				if( isset( $_POST['uabb-load-templates'] ) ) {
					$uabb['load_templates'] = true;
				}

				if( isset( $_POST['uabb-google-map-api'] ) ) {
					$uabb['uabb-google-map-api'] = $_POST['uabb-google-map-api'];
				}

				$uabb['uabb-colorpicker'] = false;
				if( isset( $_POST['uabb-colorpicker'] ) ) {
					$uabb['uabb-colorpicker'] = true;
				}

				FLBuilderModel::update_admin_settings_option( '_fl_builder_uabb', $uabb, true );
			}

			if ( isset( $_POST['fl-uabb-global-nonce'] ) && wp_verify_nonce( $_POST['fl-uabb-global-nonce'], 'uabb-global' ) ) {

				$uabb_settings['uabb-global-enable'] = false;
				if( isset( $_POST['uabb-global-enable'] ) ) {
					$uabb_settings['uabb-global-enable'] = true;
				}

				/* Global Setting Options */
				$theme_options = array(
					'uabb-theme-color' 				=> '',
					'uabb-text-color' 				=> '',
					'uabb-link-color' 				=> '',
					'uabb-link-hover-color' 		=> '',
					'uabb-button-color' 			=> '',
					'uabb-button-hover-color' 		=> '',
					'uabb-button-bg-color' 			=> '',
					'uabb-button-bg-hover-color'	=> '',
					'uabb-button-font-size' 		=> '',
					'uabb-button-line-height' 		=> '',
					'uabb-button-letter-spacing' 	=> '',
					'uabb-button-text-transform' 	=> '',
					'uabb-button-border-radius' 	=> '',
					'uabb-button-v-padding' 		=> '',
					'uabb-button-h-padding' 		=> '',
				);

				foreach ($theme_options as $key => $value) {
					if( isset( $_POST[$key] )  ) {
						$uabb_settings[$key] = $_POST[$key];
					}
				}

				FLBuilderModel::update_admin_settings_option( '_fl_builder_uabb_global', $uabb_settings, true );
			}

			// Clear all asset cache.
			FLBuilderModel::delete_asset_cache_for_all_posts();
		}
		function reload_icons() {
			delete_option( '_uabb_enabled_icons' );
			echo 'success';
			die();
		}
		function uabb_admin_assets( $hook ) {

			wp_register_style(  'uabb-admin-css', BB_ULTIMATE_ADDON_URL . 'assets/css/uabb-admin.css', array() );
			wp_register_script( 'uabb-admin-js', BB_ULTIMATE_ADDON_URL . 'assets/js/uabb-admin.js', array('jquery'), '', true);
			wp_localize_script( 'uabb-admin-js', 'uabb', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

			if( 'settings_page_fl-builder-settings' == $hook || 'settings_page_fl-builder-multisite-settings' == $hook ) {
				wp_enqueue_style( 'wp-color-picker' );
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style(  'uabb-admin-css' );
				wp_enqueue_script( 'uabb-admin-js' );
			}

		}
		function init_hooks() {

			$this->register_icons();

			if ( isset( $_REQUEST['page'] ) && 'fl-builder-settings' == $_REQUEST['page'] ) {
				$this->save();
			}
		}
		static public function get_builder_uabb_global()
		{
			$uabb = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb_global', true );

			$defaults = array(
				'uabb-global-enable' 			=> false,
				'uabb-theme-color' 				=> '',
				'uabb-text-color' 				=> '',
				'uabb-link-color' 				=> '',
				'uabb-link-hover-color' 		=> '',
				'uabb-button-color' 			=> '',
				'uabb-button-hover-color' 		=> '',
				'uabb-button-bg-color' 			=> '',
				'uabb-button-bg-hover-color'	=> '',
				'uabb-button-font-size' 		=> '',
				'uabb-button-line-height' 		=> '',
				'uabb-button-letter-spacing' 	=> '',
				'uabb-button-text-transform' 	=> '',
				'uabb-button-border-radius' 	=> '',
				'uabb-button-v-padding' 		=> '',
				'uabb-button-h-padding' 		=> '',
			);

			//	if empty add all defaults
			if( empty( $uabb ) ) {
				$uabb = $defaults;
			} else {

				//	add new key
				foreach( $defaults as $key => $value ) {
					if( is_array( $uabb ) && !array_key_exists( $key, $uabb ) ) {
						$uabb[$key] = $value;
					} else {
						$uabb = wp_parse_args( $uabb, $defaults );
					}
				}
			}

			return apply_filters( 'uabb_get_builder_uabb_global', $uabb );
		}
		static public function get_builder_uabb()
		{
			$uabb = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb', true );

			$defaults = array(
				'load_panels'			=> 1,
				'load_templates' 		=> 1,
				'uabb-google-map-api'	=> '',
				'uabb-colorpicker'		=> 1,
			);

			//	if empty add all defaults
			if( empty( $uabb ) ) {
				$uabb = $defaults;
			} else {

				//	add new key
				foreach( $defaults as $key => $value ) {
					if( is_array( $uabb ) && !array_key_exists( $key, $uabb ) ) {
						$uabb[$key] = $value;
					} else {
						$uabb = wp_parse_args( $uabb, $defaults );
					}
				}
			}

			return apply_filters( 'uabb_get_builder_uabb', $uabb );
		}
		function register_icons() {

			//	REMOVE THIS COMMENT AFTER TEST
			// delete_option( '_uabb_enabled_icons', 0 );

			//	Update initially
		    $uabb_icons = get_option( '_uabb_enabled_icons', 0 );
		    if( 0 == $uabb_icons && class_exists('FLBuilderModel') ) {

				//	Copy IconFonts from UABB to BB
				$dir	=	FLBuilderModel::get_cache_dir( 'icons' );
				$src	=	BB_ULTIMATE_ADDON_DIR . 'includes/icons/';
				$dst	=	$dir['path'];
			    $this->recurse_copy($src,$dst);

				$enabled_icons = FLBuilderModel::get_enabled_icons();

				$folders = glob( BB_ULTIMATE_ADDON_DIR . 'includes/icons/' . '*' );
			    foreach ( $folders as $folder ) {
					$folder = trailingslashit( $folder );
					$key  = basename( $folder );
					if( is_array($enabled_icons) && !in_array( $key, $enabled_icons ) ) {
						$enabled_icons[]= $key;
					}
				}
				FLBuilderModel::update_admin_settings_option( '_fl_builder_enabled_icons', $enabled_icons, true );

				//	Trigger false
				update_option( '_uabb_enabled_icons', 1 );
		    }
		}
		function recurse_copy($src,$dst) {
		    $dir = opendir($src);

		    //	Create directory if not exist
		    if ( !is_dir($dst) ) {
		    	@mkdir( $dst );
		    }

		    while(false !== ( $file = readdir($dir)) ) {
		        if (( $file != '.' ) && ( $file != '..' )) {
		            if ( is_dir($src . '/' . $file) ) {
		                $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
		            } else {
		                copy($src . '/' . $file,$dst . '/' . $file);
		            }
		        }
		    }
		    closedir($dir);
		}
		function settings_nav_item( $items ) {

			$items['icons']['show'] = 1;
			if( FL_BUILDER_LITE === true ) {
				$items['icons']['title'] = __( 'UABB - Font Icon Manager', 'fl-builder' );
			}
			if( FL_BUILDER_LITE != true ) {
				$items['uabb-icons'] = array(
					'title' 	=> __( 'UABB - Font Icon Manager', 'fl-builder' ),
					'show'		=> true,
					'priority'	=> 504
				);				
			}
			$items['uabb'] = array(
				'title' 	=> __( 'UABB - General Settings', 'fl-builder' ),
				'show'		=> true,
				'priority'	=> 505
			);

			$items['uabb-global'] = array(
				'title' 	=> __( 'UABB - Global Styling', 'fl-builder' ),
				'show'		=> apply_filters( 'uabb_global_support', true ),
				'priority'	=> 510
			);

		    return $items;
		}
		function settings_nav_form( $items ) {

			include BB_ULTIMATE_ADDON_DIR . 'includes/admin-settings-icons.php';
			if( FL_BUILDER_LITE != true ) {
				include BB_ULTIMATE_ADDON_DIR . 'includes/admin-settings-uabb-icons.php';
			}
			include BB_ULTIMATE_ADDON_DIR . 'includes/admin-settings-uabb-general.php';

			if ( apply_filters( 'uabb_global_support', true ) ) {
				include BB_ULTIMATE_ADDON_DIR . 'includes/admin-settings-uabb-global.php';
			}
		}

		/**
		 * 	Load Templates
		 *
		 * Loaded Page, Row & Module templates from file 'templates.dat'
		 * @since 1.0
		 */
		function load_templates() {
			if ( ! class_exists( 'FLBuilder' ) || ! method_exists( 'FLBuilder', 'register_templates' ) ) {
				return;
			}
			FLBuilder::register_templates( BB_ULTIMATE_ADDON_DIR . '/includes/templates/templates.dat' );
		}

		/**
		 * 	Add Buttons to panel
		 *
		 * Row button added to the panel
		 * @since 1.0
		 */
		function builder_ui_bar_buttons( $buttons ) {
			if ( class_exists( 'FLBuilder' ) ) {

				$help_button 	= FLBuilderModel::get_help_button_settings();
				$simple_ui		= ! FLBuilderModel::current_user_has_editing_capability();

				$buttons = array(
					'help' => array(
						'label' => '<i class="fa fa-question-circle"></i>',
						'show'	=> $help_button['enabled'] && ! $simple_ui
					),
					/*'upgrade' => array(
						'label' => __( 'Upgrade!', 'fl-builder' ),
						'show'	=> true === FL_BUILDER_LITE
					),*/
					'buy' => array(
						'label' => __( 'Buy Now!', 'fl-builder' ),
						'show'	=> stristr( home_url(), 'demo.wpbeaverbuilder.com' )
					),
					'done' => array(
						'label' => __( 'Done', 'fl-builder' ),
						'class' => 'fl-builder-button-primary'
					),
					'tools' => array(
						'label' => __( 'Tools', 'fl-builder' ),
						'show'	=> ! $simple_ui
					),
					'templates' => array(
						'label' => __( 'Templates', 'fl-builder' ),
						'show'	=> FLBuilderModel::has_templates() && ! $simple_ui
					),
					'add-ultimate-rows' => array(
						'label' => __( 'Sections', 'fl-builder' ),
						'show'	=> ! $simple_ui,
					),
					'add-content' => array(
						'label' => __( 'Add Content', 'fl-builder' ),
						'show'	=> ! $simple_ui
					)
				);

				return $buttons;
			}
		}

		/**
		 * 	Load Rows Panel
		 *
		 * Row panel showing sections - rows & modules
		 * @since 1.0
		 */
		function render_ui() {
			if ( class_exists( 'FLBuilder' ) ) {


				global $wp_the_query;

				if ( FLBuilderModel::is_builder_active() ) {

					//	Panel
					$post_id            = $wp_the_query->post->ID;
					$simple_ui			= ! FLBuilderModel::current_user_has_editing_capability();
					$categories         = FLBuilderModel::get_categorized_modules();
					$render_panel       = apply_filters( 'uabb_fl_builder_render_ui_panel', FLBuilderModel::current_user_has_editing_capability() );

					//	Row Templates
					$is_row_template    = FLBuilderModel::is_post_user_template( 'row' );
					$is_module_template = FLBuilderModel::is_post_user_template( 'module' );
					$has_editing_cap    = FLBuilderModel::current_user_has_editing_capability();
					$row_templates      = FLBuilderModel::get_row_templates_data();

					//	Modules
					$is_module_template = FLBuilderModel::is_post_user_template( 'module' );
					$has_editing_cap    = FLBuilderModel::current_user_has_editing_capability();
					$module_templates   = FLBuilderModel::get_module_templates_data();

					if ( $render_panel ) {
						include BB_ULTIMATE_ADDON_DIR . 'includes/ui-panel.php';
					}

					include BB_ULTIMATE_ADDON_DIR . 'includes/ui-panel-sections.php';
				}
			}
		}

		function fl_load_uabb() {
			if ( class_exists( 'FLBuilder' ) ) {

				$this->uabb_panel_config();

				//	Helper
				require_once 'classes/uabb-global-functions.php';
				require_once 'classes/helper.php';

				// Custom Fields Config
				require_once 'fields/fields-config.php';
				
				// Module as Object
				require_once 'objects/objects-config.php';

				// Custom Params
				require_once 'params/params.php';


				// Ultimate Modules
			    require_once 'modules/flip-box/flip-box.php';
				require_once 'modules/interactive-banner-1/interactive-banner-1.php';
				require_once 'modules/interactive-banner-2/interactive-banner-2.php';
				require_once 'modules/info-table/info-table.php';
				require_once 'modules/info-banner/info-banner.php';
				require_once 'modules/dual-color-heading/dual-color-heading.php';
				require_once 'modules/spacer-gap/spacer-gap.php';
			    require_once 'modules/list-icon/list-icon.php';
			    require_once 'modules/info-list/info-list.php';
			    require_once 'modules/google-map/google-map.php';
			    require_once 'modules/slide-box/slide-box.php';
			    require_once 'modules/ihover/ihover.php';
			    require_once 'modules/dual-button/dual-button.php';
   			    require_once 'modules/info-box/info-box.php';
   			    require_once 'modules/fancy-text/fancy-text.php';
   			    require_once 'modules/image-icon/image-icon.php';
				require_once 'modules/advanced-tabs/advanced-tabs.php';
   			    require_once 'modules/blog-posts/blog-posts.php';
   			    require_once 'modules/advanced-separator/advanced-separator.php';
   			    require_once 'modules/advanced-icon/advanced-icon.php';
   			    require_once 'modules/advanced-accordion/advanced-accordion.php';
   			    require_once 'modules/progress-bar/progress-bar.php';
   			    require_once 'modules/creative-link/creative-link.php';
   			    require_once 'modules/team/team.php';
   			    require_once 'modules/image-separator/image-separator.php';


				// Agency Modules
				require_once 'modules/uabb-photo/uabb-photo.php';
			    require_once 'modules/uabb-call-to-action/uabb-call-to-action.php';
			    require_once 'modules/adv-testimonials/adv-testimonials.php';
			    require_once 'modules/uabb-heading/uabb-heading.php';
			    require_once 'modules/uabb-separator/uabb-separator.php';
			    require_once 'modules/uabb-contact-form/uabb-contact-form.php';
			    require_once 'modules/uabb-button/uabb-button.php';
			    require_once 'modules/uabb-numbers/uabb-numbers.php';
			}
		}

		/**
		 * Render Global uabb-layout-builder css
		 */
		function fl_uabb_render_css($css, $nodes, $global_settings) {
			if ( class_exists( 'FLBuilder' ) ) {
			    $css .= file_get_contents(BB_ULTIMATE_ADDON_DIR . 'assets/css/uabb-frontend.css');
				$css .= include('assets/dynamic-css/uabb-theme-dynamic-css.php');

	    		return $css;
    		}
		}

		/**
		 * Render Global uabb-layout-builder js
		 */
		/*function fl_uabb_render_js($js, $nodes, $global_settings) {
			if ( class_exists( 'FLBuilder' ) ) {

				return $js;
	    	}
		}*/

		/**
		 * Enqueue Builder CSS and Js
		 */
		function uabb_builder_css_js() {

			if ( class_exists( 'FLBuilder' ) && FLBuilderModel::is_builder_active() ) {
				wp_enqueue_style('uabb-builder-css', BB_ULTIMATE_ADDON_URL . 'assets/css/uabb-builder.css', array() );
				wp_enqueue_script('uabb-builder-js', BB_ULTIMATE_ADDON_URL . 'assets/js/uabb-builder.js', array('jquery'), '', true);

				if ( apply_filters( 'uabb_global_support', true ) ) {
					$uabb = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb_global', true );

					if( is_array( $uabb ) && array_key_exists( 'uabb-global-enable', $uabb ) ) {
						if ( !$uabb['uabb-global-enable'] ) {
							wp_localize_script( 'uabb-builder-js', 'uabb_presets', array( 'show_presets' => true ) );
						}
					}
				}
			}
		}

		/**
		 * Enqueue Panel CSS and JS
		 */
		function uabb_panel_css_js() {
			if ( class_exists( 'FLBuilder' ) && FLBuilderModel::is_builder_active() ) {

				wp_enqueue_script('uabb-panel-js', BB_ULTIMATE_ADDON_URL . 'assets/js/uabb-panel.js', array('jquery'), '', true);
			}
		}

		/**
		 * Function to load Extended Settings
		 */
		function uabb_load_custom_bb_extensions() {

			if(class_exists('FLBuilder')) {

				//	Extended Settings
				require_once 'includes/extended-settings.php';

				/* Register Row Setting */
				add_filter('fl_builder_register_settings_form', 'uabb_extend_column_settings', 10, 2);

				/* Register Column Setting */
				add_filter('fl_builder_register_settings_form', 'uabb_extend_row_settings', 10, 2);

				/* Add Class to row */
				add_filter( 'fl_builder_row_custom_class', array( $this, 'uabb_extended_setting_row_class' ), 10, 2);

				/* Row setting Html */
				add_action( 'fl_builder_before_render_row_bg', array( $this, 'uabb_extended_top_row_html' ), 10, 1 );
				add_action( 'fl_builder_after_render_row_bg', array( $this, 'uabb_extended_bottom_row_html' ), 10, 1 );

				/* Extended Settings CSS */
				add_filter('fl_builder_render_css', array( $this, 'uabb_extended_setting_css' ), 10, 3);
			}
		}


		/* Function to add row class according to setting */
		function uabb_extended_setting_row_class( $class, $row_object ) {
			$row = $row_object->settings;
			if ( $row->separator_shape != 'none' ) {
				$class = ' uabb-'.$row->separator_shape;
			}
			return $class;
		}

		/**
		 * Function to add Custom html of extended setting
		 */
		function uabb_extended_top_row_html( $row_object ) {
			$row = $row_object->settings;
			if( $row->separator_position =='top' || $row->separator_position == 'top_bottom' ) {
				$row->separator_flag = 'top';
				include 'includes/extended-row-html.php';
			}

		}

		function uabb_extended_bottom_row_html( $row_object ) {
			$row = $row_object->settings;
			if( $row->separator_position =='bottom' || $row->separator_position == 'top_bottom' ) {
				$row->separator_flag = 'bottom';
				include 'includes/extended-row-html.php';
			}
		}
		/**
		 * Function to append Custom CSS of extended settings
		 */
		function uabb_extended_setting_css( $css, $nodes, $global_settings) {

			ob_start();
			include BB_ULTIMATE_ADDON_DIR . 'assets/dynamic css/uabb-extended-setting-css.php';
			$css .= ob_get_clean();
			return $css;
		}

		// Register Field
		static function uabb_field_register( $field_id = '', $field_settings = array() ) {
			if( $field_id != '' && !empty( $field_settings ) ) {
				BB_Ultimate_Addon::$uabb_field[$field_id] = $field_settings;
			}
		}

		// Get field by ID
		static function uabb_field_get( $field_id, $field_new = NULL ) {
			if( isset( BB_Ultimate_Addon::$uabb_field[$field_id] ) ) {

				//	Initially set for return
				$field_updated = BB_Ultimate_Addon::$uabb_field[$field_id];


				//	Update / Replace existing array LABEL, DEFAULTs of fields
				if( $field_new != NULL && is_array($field_new) ) {
					$param_old = BB_Ultimate_Addon::$uabb_field[$field_id];
					$field_updated = array_replace_recursive( ( array )$param_old, ( array )$field_new );
				}

				$uabb_options = FLBuilderModel::get_admin_settings_option( '_fl_builder_uabb', true );
				$enable_colorpicker = true;

				if ( !empty( $uabb_options ) ) {
					$enable_colorpicker = true;
					if ( array_key_exists( 'uabb-colorpicker', $uabb_options ) ) {
						if ( $uabb_options['uabb-colorpicker'] == 1 ) {
							$enable_colorpicker = true;
						}else{
							$enable_colorpicker = false;
						}

					}
				}


				if ( apply_filters( 'uabb_colorpicker', true ) && $enable_colorpicker ) {
					if ( $field_id == 'coloropacity' ) {
						$field_updated['type']	= 'uabb-hide-field';
						unset( $field_updated['label'] );
						unset( $field_updated['description'] );
					}

				}else {
				 	if ( $field_id == 'colorpicker' ) {
						$field_updated['type']		= 'color';
					}
				}

				return $field_updated;
			}
		}

		// Register SECTION
		static function uabb_section_register( $param_id = '', $param_settings = array() ) {
			if( $param_id != '' && !empty( $param_settings ) ) {
				BB_Ultimate_Addon::$uabb_param[$param_id] = $param_settings;
			}
		}

		// Update SECTION
		static function uabb_section_get( $param_id, $param_new = NULL, $remove_fields = NULL, $prefix = '' ) {
			if( isset( BB_Ultimate_Addon::$uabb_param[$param_id] ) ) {

				//	Initially set for return
				$param_updated = BB_Ultimate_Addon::$uabb_param[$param_id];

				//	Update / Replace existing array LABEL, DEFAULTs of fields
				if( $param_new != NULL && is_array($param_new) ) {
					$param_old = BB_Ultimate_Addon::$uabb_param[$param_id];
					$param_updated = array_replace_recursive( ( array )$param_old, ( array )$param_new );
				}

				// Remove Fields
				if( $remove_fields != NULL ) {

					// is Array?
					if( is_array( $remove_fields ) ) {
						foreach ($remove_fields as $field) {
							unset( $param_updated['fields'][$field] );
						}
					} else {
						unset( $param_updated['fields'][$remove_fields] );
					}
				}

				if( isset($prefix) && !empty($prefix) ) {
					foreach ($param_updated['fields'] as $key => $field) {

						$param_updated['fields'][$prefix.'_'.$key] = $field;
						unset($param_updated['fields'][$key]);
					}
				}

				return $param_updated;
			}
		}

		static function uabb_param_selector_filter( $param_id, $selector ) {

			if( isset( BB_Ultimate_Addon::$uabb_param[$param_id] ) ) {

				foreach( BB_Ultimate_Addon::$uabb_param[$param_id]['fields'] as $key => $option ) {
					if( isset($option['preview']) )
					{
						$option['preview']['selector'] = '.' . $selector;
						BB_Ultimate_Addon::$uabb_param[$param_id]['fields'][$key] = $option;
					}
				}
				return BB_Ultimate_Addon::$uabb_param[$param_id];
			}
		}

		// Register Object
		static function uabb_object_register( $object_id = '', $object_settings = array() ) {
			if( $object_id != '' && !empty( $object_settings ) ) {
				BB_Ultimate_Addon::$uabb_object[$object_id] = $object_settings;
			}
		}

		static function uabb_object_get( $object_id ){
			//	Initially set for return
			$object_updated = BB_Ultimate_Addon::$uabb_object[$object_id];

			return $object_updated;
		}
	}
	new BB_Ultimate_Addon();
}

/**
 * Load brainstorm product updater
 */
$bsf_core_version_file = realpath( dirname( __FILE__ ) . '/admin/bsf-core/version.yml' );

if ( is_file( $bsf_core_version_file ) ) {
	global $bsf_core_version, $bsf_core_path;
	$bsf_core_dir = realpath( dirname( __FILE__ ) . '/admin/bsf-core/' );
	$version      = file_get_contents( $bsf_core_version_file );
	if ( version_compare( $version, $bsf_core_version, '>' ) ) {
		$bsf_core_version = $version;
		$bsf_core_path    = $bsf_core_dir;
	}
}

if ( ! function_exists( 'bsf_core_load' ) ) {
	function bsf_core_load() {
		global $bsf_core_version, $bsf_core_path;
		if ( is_file( realpath( $bsf_core_path . '/index.php' ) ) ) {
			include_once realpath( $bsf_core_path . '/index.php' );
		}
	}
}
add_action( 'init', 'bsf_core_load', 999 );
