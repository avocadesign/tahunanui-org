<?php /* 'general' */
BB_Ultimate_Addon::uabb_field_register( 'colorpicker',
	array( 
		'type'          => 'uabb-color',
        'label'         => __('Color', 'uabb'),
        'default'       => '',
        'show_reset'    => true,
	)
);

BB_Ultimate_Addon::uabb_field_register( 'coloropacity',
	array( 
		'type'          => 'text',
		'label'         => __('Opacity', 'uabb'),
		'default'       => '',
		'description'   => '%',
		'maxlength'     => '3',
		'size'          => '5',
	)
);