<?php

/* 'general' */
BB_Ultimate_Addon::uabb_section_register( 'image-icon-gen',
	array( // Section
		'title'         => 'Image / Icon', // Section Title
		'fields'        => array( // Section Fields
			'image_type'    => array(
				'type'          => 'select',
				'label'         => __('Image Type', 'uabb'),
				'default'       => 'none',
				'options'       => array(
					'none'          => __( 'None', 'Image type.', 'uabb' ),
					'icon'          => __('Icon', 'uabb'),
					'photo'         => __('Photo', 'uabb'),
				),
                'class'         => 'class_image_type',
				'toggle'        => array(
					'icon'          => array(
						'sections'	 => array( 'icon_basic',  'icon_style', 'icon_colors' ),
					),
					'photo'         => array(
						'sections'	 => array( 'img_basic', 'img_style' ),
					)
				),
			),
		)
	)
);

/* Icon Basic Setting */
BB_Ultimate_Addon::uabb_section_register( 'icon_basic',
	array( // Section
		'title'         => 'Icon Basics', // Section Title
        'fields'        => array( // Section Fields
            'icon'          => array(
                'type'          => 'icon',
                'label'         => __('Icon', 'uabb')
            ),
            'icon_size'     => array(
                'type'          => 'text',
                'label'         => __('Size', 'uabb'),
                'default'       => '30',
                'maxlength'     => '5',
                'size'          => '6',
                'description'   => 'px',
            ),
            'icon_align'         => array(
                'type'          => 'select',
                'label'         => __('Alignment', 'fl-builder'),
                'default'       => 'center',
                'options'       => array(
                    'left'          => __('Left', 'fl-builder'),
                    'center'        => __('Center', 'fl-builder'),
                    'right'         => __('Right', 'fl-builder')
                ),
            )
        )
	)
);

/* Image Basic Setting */

BB_Ultimate_Addon::uabb_section_register( 'img_basic',
	array( // Section
		'title'         => 'Image Basics', // Section Title
		'fields'        => array( // Section Fields
			'photo_source'  => array(
				'type'          => 'select',
				'label'         => __('Photo Source', 'uabb'),
				'default'       => 'library',
				'options'       => array(
					'library'       => __('Media Library', 'uabb'),
					'url'           => __('URL', 'uabb')
				),
				'toggle'        => array(
					'library'       => array(
						'fields'        => array('photo')
					),
					'url'           => array(
						'fields'        => array('photo_url' )
					)
				)
			),
			'photo'         => array(
				'type'          => 'photo',
				'label'         => __('Photo', 'uabb')
			),
			'photo_url'     => array(
				'type'          => 'text',
				'label'         => __('Photo URL', 'uabb'),
				'placeholder'   => __( 'http://www.example.com/my-photo.jpg', 'uabb' )
			),
			'img_size'     => array(
				'type'          => 'text',
				'label'         => __('Size', 'uabb'),
				'default'       => '',
				'maxlength'     => '5',
				'size'          => '6',
				'description'   => 'px',
			),
			'img_align'         => array(
				'type'          => 'select',
				'label'         => __('Alignment', 'fl-builder'),
				'default'       => 'center',
				'options'       => array(
					'left'          => __('Left', 'fl-builder'),
					'center'        => __('Center', 'fl-builder'),
					'right'         => __('Right', 'fl-builder')
				),
			)
		)
	)
);

/* Icon Style Section */
BB_Ultimate_Addon::uabb_section_register( 'icon_style',
	array(
		'title'           => 'Style',
        'fields'        => array(
            /* Icon Style */
           'icon_style'         => array(
                'type'          => 'select',
                'label'         => __('Icon Background Style', 'uabb'),
                'default'       => 'simple',
                'options'       => array(
                    'simple'        => __('Simple', 'uabb'),
                    'circle'          => __('Circle Background', 'uabb'),
                    'square'         => __('Square Background', 'uabb'),
                    'custom'         => __('Design your own', 'uabb'),
                ),
                'toggle' => array(
                    'simple' => array(
                        'fields' => array(),
                        /*'sections' => array( 'colors' )*/
                    ),
                    'circle' => array(
                        /*'sections' => array( 'colors' ),*/
                        'fields' => array( 'icon_color_preset', 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d' ),
                    ),
                    'square' => array(
                        /*'sections' => array( 'colors' ),*/
                        'fields' => array( 'icon_color_preset', 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d' ),
                    ),
                    'custom' => array(
                        /*'sections' => array( 'colors' ),*/
                        'fields' => array( 'icon_color_preset', 'icon_border_style', 'icon_bg_color', 'icon_bg_color_opc', 'icon_bg_hover_color', 'icon_bg_hover_color_opc', 'icon_three_d', 'icon_bg_size', 'icon_bg_border_radius' ),
                    )
                ),
                'trigger' => array(
                    'custom' => array(
                        'fields' => array( 'icon_border_style' ),
                    )
                ),
            ),
            
            /* Icon Background SIze */
            'icon_bg_size'          => array(
                'type'          => 'text',
                'label'         => __('Background Size', 'uabb'),
                'default'       => '',
                'help'          => 'Spacing between Icon & Background edge',
                'maxlength'     => '3',
                'size'          => '6',
                'description'   => 'px'
            ),

            /* Border Style and Radius for Icon */
            'icon_border_style'   => array(
                'type'          => 'select',
                'label'         => __('Border Style', 'uabb'),
                'default'       => 'none',
                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
                'options'       => array(
                    'none'   => __( 'None', 'Border type.', 'uabb' ),
                    'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
                    'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
                    'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
                    'double' => __( 'Double', 'Border type.', 'uabb' )
                ),
                'toggle'        => array(
                    'solid'         => array(
                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
                    ),
                    'dashed'        => array(
                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
                    ),
                    'dotted'        => array(
                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
                    ),
                    'double'        => array(
                        'fields'        => array('icon_border_width', 'icon_border_color','icon_border_hover_color' )
                    )
                ),
            ),
            'icon_border_width'    => array(
                'type'          => 'text',
                'label'         => __('Border Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '6',
                'placeholder'   => '0',
            ),
            'icon_bg_border_radius'    => array(
                'type'          => 'text',
                'label'         => __('Border Radius', 'uabb'),
                'default'       => '0',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '6',
                'placeholder'   => '0',
            ),
        )
	) 
);

/* Image Style Section */
BB_Ultimate_Addon::uabb_section_register( 'img_style',
	array(
		'title'			=> 'Style',
		'fields'		=> array(
			/* Image Style */
			'image_style'         => array(
            	'type'          => 'select',
            	'label'         => __('Image Style', 'uabb'),
            	'default'       => 'simple',
            	'help'			=> __('Circle and Square style will crop your image in 1:1 ratio','uabb'),
            	'options'       => array(
                	'simple'        => __('Simple', 'uabb'),
                	'circle'        => __('Circle', 'uabb'),
                	'square'        => __('Square', 'uabb'),
                	'custom'        => __('Design your own', 'uabb'),
            	),
                'class' => 'uabb-image-icon-style',
                'toggle' => array(
                    'simple' => array(
                        'fields' => array()
                    ),
                    'circle' => array(
                        'fields' => array( ),
                    ),
                    'square' => array(
                        'fields' => array( ),
                    ),
                    'custom' => array(
                        'sections'  => array( 'img_colors' ),
                        'fields'	=> array( 'img_bg_size', 'img_border_style', 'img_border_width', 'img_bg_border_radius' ) 
                    )
                ),
                'trigger'       => array(
					'custom'           => array(
						'fields'        => array('img_border_style')
					),
					
				)
        	),

            /* Image Background Size */
            'img_bg_size'          => array(
                'type'          => 'text',
                'label'         => __('Background Size', 'uabb'),
                'default'       => '',
                'help'          => 'Spacing between Image edge & Background edge',
                'maxlength'     => '3',
                'size'          => '6',
                'description'   => 'px',
            ),

            /* Border Style and Radius for Image */
			'img_border_style'   => array(
                'type'          => 'select',
                'label'         => __('Border Style', 'uabb'),
                'default'       => 'none',
                'help'          => __('The type of border to use. Double borders must have a width of at least 3px to render properly.', 'uabb'),
                'options'       => array(
                    'none'   => __( 'None', 'Border type.', 'uabb' ),
                    'solid'  => __( 'Solid', 'Border type.', 'uabb' ),
                    'dashed' => __( 'Dashed', 'Border type.', 'uabb' ),
                    'dotted' => __( 'Dotted', 'Border type.', 'uabb' ),
                    'double' => __( 'Double', 'Border type.', 'uabb' )
                ),
                'toggle'        => array(
                    'solid'         => array(
                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
                    ),
                    'dashed'        => array(
                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
                    ),
                    'dotted'        => array(
                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
                    ),
                    'double'        => array(
                        'fields'        => array('img_border_width', 'img_border_radius','img_border_color','img_border_hover_color' )
                    )
                ),
            ),
            'img_border_width'    => array(
                'type'          => 'text',
                'label'         => __('Border Width', 'uabb'),
                'default'       => '1',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '6',
                'placeholder'   => '0',
            ),
            'img_bg_border_radius'    => array(
                'type'          => 'text',
                'label'         => __('Border Radius', 'uabb'),
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '6',
                'placeholder'   => '0',
            ),
		)
	)
);

/* Icon Colors */
BB_Ultimate_Addon::uabb_section_register( 'icon_colors',
	array( // Section
        'title'         => __('Colors', 'uabb'), // Section Title
        'fields'        => array( // Section Fields
                    
            /* Style Options */
            'icon_color_preset'     => array(
                'type'          => 'uabb-toggle-switch',
                'label'         => __( 'Icon Color Presets', 'fl-builder' ),
                'default'       => 'preset1',
                'options'       => array(
                    'preset1'       => 'Preset 1',
                    'preset2'       => 'Preset 2',
                    /*'preset3'     => 'Preset 3',*/
                ),
                'help'          => __('Preset 1 => Icon : White, Background : Theme </br>Preset 2 => Icon : Theme, Background : #f3f3f3', 'uabb')
            ),
            /* Icon Color */
            'icon_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'     => __('Icon Color', 'uabb'),
                )
            ),
            'icon_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Icon Hover Color', 'uabb'),
                    'preview'       => array(
                            'type'      => 'none',
                    )
                )
            ),

            /* Background Color Dependent on Icon Style **/
            'icon_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Background Color', 'uabb'),
                )
            ),
            'icon_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
            'icon_bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Background Hover Color', 'uabb'),
                    'preview'       => array(
                            'type'      => 'none',
                    )
                )
            ),
            'icon_bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),

             /* Border Color Dependent on Border Style for ICon */
            'icon_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Border Color', 'uabb'),
                )
            ),
            'icon_border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Border Hover Color', 'uabb'),
                )
            ),
            
            /* Gradient Color Option */
            'icon_three_d'       => array(
                'type'          => 'select',
                'label'         => __('Gradient', 'uabb'),
                'default'       => '0',
                'options'       => array(
                    '0'             => __('No', 'uabb'),
                    '1'             => __('Yes', 'uabb')
                )
            ),
        )
    )
);

/* Image Colors */
BB_Ultimate_Addon::uabb_section_register( 'img_colors',
	array( // Section
        'title'         => __('Colors', 'uabb'), // Section Title
        'fields'        => array( // Section Fields
            /* Background Color Dependent on Icon Style **/
            'img_bg_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Background Color', 'uabb'),
                )
            ),
            'img_bg_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),
            'img_bg_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Background Hover Color', 'uabb'),
                    'preview'       => array(
                            'type'      => 'none',
                    )
                )
            ),
            'img_bg_hover_color_opc' => BB_Ultimate_Addon::uabb_field_get( 'coloropacity'),


             /* Border Color Dependent on Border Style for Image */
            'img_border_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Border Color', 'uabb'),
                )
            ),
            'img_border_hover_color' => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', 
                array(
                    'label'         => __('Border Hover Color', 'uabb'),
                )
            ),
        )
    )
);
/* Register Object */

BB_Ultimate_Addon::uabb_object_register( 'image-icon',

	array(
		'title'         => __('Image / Icon', 'uabb'),
		'sections'      => array(
			'type_general' 		=> BB_Ultimate_Addon::uabb_section_get( 'image-icon-gen' ),
			'icon_basic' 	=> BB_Ultimate_Addon::uabb_section_get( 'icon_basic' ),
			'img_basic' 	=> BB_Ultimate_Addon::uabb_section_get( 'img_basic' ),
			'icon_style'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_style' ),
			'img_style'		=> BB_Ultimate_Addon::uabb_section_get( 'img_style' ),
			'icon_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'icon_colors' ),
			'img_colors'	=> BB_Ultimate_Addon::uabb_section_get( 'img_colors' ),
		)
	)
);
