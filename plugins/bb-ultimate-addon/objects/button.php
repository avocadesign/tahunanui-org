<?php

/* Button General */
BB_Ultimate_Addon::uabb_section_register( 'btn-general',
	array( // Section
		'title'         => __( 'General', 'uabb' ),
        'fields'        => array(
            'btn_text'          => array(
                'type'          => 'text',
                'label'         => __('Text', 'uabb'),
                'default'       => __('Click Here', 'uabb'),
            ),
        )
	)
);

/* Button Link */
BB_Ultimate_Addon::uabb_section_register( 'btn-link',
	array( // Section
		'title'         => __('Link', 'uabb'),
        'fields'        => array(
            'btn_link'          => array(
                'type'          => 'link',
                'label'         => __('Link', 'uabb'),
                'placeholder'   => __( 'http://www.example.com', 'uabb' ),
                'preview'       => array(
                    'type'          => 'none'
                )
            ),
            'btn_link_target'   => array(
                'type'          => 'select',
                'label'         => __('Link Target', 'uabb'),
                'default'       => '_self',
                'options'       => array(
                    '_self'         => __('Same Window', 'uabb'),
                    '_blank'        => __('New Window', 'uabb')
                ),
                'preview'       => array(
                    'type'          => 'none'
                )
            )
        )
    )
);

/* Button Style */
BB_Ultimate_Addon::uabb_section_register( 'btn-style',
	array(
        'title'         => __('Style', 'uabb'),
        'fields'        => array(
            'btn_style'         => array(
                'type'          => 'select',
                'label'         => __('Style', 'uabb'),
                'default'       => 'flat',
                'class'         => 'creative_button_styles',
                'options'       => array(
                    'flat'          => __('Flat', 'uabb'),
                    'gradient'      => __('Gradient', 'uabb'),
                    'transparent'   => __('Transparent', 'uabb'),
                    'threed'          => __('3D', 'uabb'),
                ),
                'toggle'        => array(
                    'transparent'   => array(
                        'fields'    => array( 'btn_border_size', 'btn_transparent_button_options'),
                    ),
                    'threed'   => array(
                        'fields'    => array( 'btn_threed_button_options' )
                    ),
                    'flat'   => array(
                        'fields'    => array( 'btn_flat_button_options' )
                    ),
                    
                )
            ),
            'btn_border_size'   => array(
                'type'          => 'text',
                'label'         => __('Border Size', 'uabb'),
                'default'       => '2',
                'description'   => 'px',
                'maxlength'     => '3',
                'size'          => '5',
                'placeholder'   => '0'
            ),
            'btn_transparent_button_options'         => array(
                'type'          => 'select',
                'label'         => __('Hover Styles', 'uabb'),
                'default'       => 'transparent-fade',
                'options'       => array(
                    'none'          => __('None', 'uabb'),
                    'transparent-fade'          => __('Fade Background', 'uabb'),
                    'transparent-fill-top'      => __('Fill Background From Top', 'uabb'),
                    'transparent-fill-bottom'      => __('Fill Background From Bottom', 'uabb'),
                    'transparent-fill-left'     => __('Fill Background From Left', 'uabb'),
                    'transparent-fill-right'     => __('Fill Background From Right', 'uabb'),
                    'transparent-fill-center'       => __('Fill Background Vertical', 'uabb'),
                    'transparent-fill-diagonal'     => __('Fill Background Diagonal', 'uabb'),
                    'transparent-fill-horizontal'  => __('Fill Background Horizontal', 'uabb'),
                ),
            ),
            'btn_threed_button_options'         => array(
                'type'          => 'select',
                'label'         => __('Hover Styles', 'uabb'),
                'default'       => 'threed_down',
                'options'       => array(
                    'threed_down'          => __('Move Down', 'uabb'),
                    'threed_up'      => __('Move Up', 'uabb'),
                    'threed_left'      => __('Move Left', 'uabb'),
                    'threed_right'     => __('Move Right', 'uabb'),
                    'animate_top'     => __('Animate Top', 'uabb'),
                    'animate_bottom'     => __('Animate Bottom', 'uabb'),
                    /*'animate_left'     => __('Animate Left', 'uabb'),
                    'animate_right'     => __('Animate Right', 'uabb'),*/
                ),
            ),
            'btn_flat_button_options'         => array(
                'type'          => 'select',
                'label'         => __('Hover Styles', 'uabb'),
                'default'       => 'none',
                'options'       => array(
                    'none'          => __('None', 'uabb'),
                    'animate_to_left'      => __('Appear Icon From Right', 'uabb'),
                    'animate_to_right'          => __('Appear Icon From Left', 'uabb'),
                    'animate_from_top'      => __('Appear Icon From Top', 'uabb'),
                    'animate_from_bottom'     => __('Appear Icon From Bottom', 'uabb'),
                ),
            ),
        )
	) 
);

/* Button Colors */

BB_Ultimate_Addon::uabb_section_register( 'btn-colors',
    array( // Section
        'title'         => __('Colors', 'uabb'),
        'fields'        => array(
            'btn_bg_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                'label'         => __('Background Color', 'uabb'),
                'default'       => '',
                )
            ),
            'btn_bg_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

            'btn_bg_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                'label'         => __('Background Hover Color', 'uabb'),
                'default'       => '',
                'preview'       => array(
                        'type'          => 'none'
                    )
                ) 
            ),
            'btn_bg_hover_color_opc'    => BB_Ultimate_Addon::uabb_field_get( 'coloropacity' ),

            'btn_text_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                'label'         => __('Text Color', 'uabb'),
                'default'       => '',
                ) 
            ),

            'btn_text_hover_color'        => BB_Ultimate_Addon::uabb_field_get( 'colorpicker', array(
                'label'         => __('Text Hover Color', 'uabb'),
                'default'       => '',
                'preview'       => array(
                        'type'          => 'none'
                    )
                ) 
            ),
        )
    )
);


BB_Ultimate_Addon::uabb_section_register( 'btn-icon',
    array( // Section
        'title'         => __('Icons', 'uabb'),
        'fields'        => array(
            'btn_icon'          => array(
                'type'          => 'icon',
                'label'         => __('Icon', 'uabb'),
                'show_remove'   => true
            ),
            'btn_icon_position' => array(
                'type'          => 'select',
                'label'         => __('Icon Position', 'uabb'),
                'default'       => 'before',
                'options'       => array(
                    'before'        => __('Before Text', 'uabb'),
                    'after'         => __('After Text', 'uabb')
                )
            )
        )
    )
);

/* Button Structure */
BB_Ultimate_Addon::uabb_section_register( 'btn-structure',
	array(
        'title'         => __('Structure', 'uabb'),
        'fields'        => array(
            'btn_width'         => array(
                'type'          => 'select',
                'label'         => __('Width', 'uabb'),
                'default'       => 'auto',
                'options'       => array(
                    'auto'          => _x( 'Auto', 'Width.', 'uabb' ),
                    'full'          => __('Full Width', 'uabb'),
                    'custom'        => __('Custom', 'uabb')
                ),
                'toggle'        => array(
                    'auto'          => array(
                        'fields'        => array('btn_align', 'btn_mob_align', 'btn_line_height')
                    ),
                    'full'          => array(
                        'fields'        => array( 'btn_line_height' )
                    ),
                    'custom'        => array(
                        'fields'        => array('btn_align', 'btn_mob_align', 'btn_custom_width', 'btn_custom_height', 'btn_padding_top_bottom', 'btn_padding_left_right' )
                    )
                )
            ),
            'btn_custom_width'  => array(
                'type'          => 'text',
                'label'         => __('Custom Width', 'uabb'),
                'default'       => '200',
                'maxlength'     => '3',
                'size'          => '4',
                'description'   => 'px'
            ),
            'btn_custom_height'  => array(
                'type'          => 'text',
                'label'         => __('Custom Height', 'uabb'),
                'default'       => '45',
                'maxlength'     => '3',
                'size'          => '4',
                'description'   => 'px'
            ),
            'btn_padding_top_bottom'       => array(
                'type'          => 'text',
                'label'         => __('Padding Top/Bottom', 'uabb'),
                'default'       => '',
                'maxlength'     => '3',
                'size'          => '4',
                'description'   => 'px'
            ),
            'btn_padding_left_right'       => array(
                'type'          => 'text',
                'label'         => __('Padding Left/Right', 'uabb'),
                'default'       => '',
                'maxlength'     => '3',
                'size'          => '4',
                'description'   => 'px'
            ),
            'btn_border_radius' => array(
                'type'          => 'text',
                'label'         => __('Round Corners', 'uabb'),
                'default'       => '4',
                'maxlength'     => '3',
                'size'          => '4',
                'description'   => 'px'
            ),
            'btn_align'         => array(
                'type'          => 'select',
                'label'         => __('Alignment', 'uabb'),
                'default'       => 'left',
                'options'       => array(
                    'center'        => __('Center', 'uabb'),
                    'left'          => __('Left', 'uabb'),
                    'right'         => __('Right', 'uabb')
                )
            ),
            'btn_mob_align'         => array(
                'type'          => 'select',
                'label'         => __('Mobile Alignment', 'uabb'),
                'default'       => 'center',
                'options'       => array(
                    'center'        => __('Center', 'uabb'),
                    'left'          => __('Left', 'uabb'),
                    'right'         => __('Right', 'uabb')
                )
            ),
        )
	)
);

/* Register Object */

BB_Ultimate_Addon::uabb_object_register( 'button',

	array(
		'title'         => __('Button', 'uabb'),
		'sections'      => array(
			'btn-general'    => BB_Ultimate_Addon::uabb_section_get( 'btn-general' ),
			'btn-link'       => BB_Ultimate_Addon::uabb_section_get( 'btn-link' ),
			'btn-style'      => BB_Ultimate_Addon::uabb_section_get( 'btn-style' ),
            'btn-icon'       => BB_Ultimate_Addon::uabb_section_get( 'btn-icon' ),
            'btn-colors'     => BB_Ultimate_Addon::uabb_section_get( 'btn-colors' ),
			'btn-structure'  => BB_Ultimate_Addon::uabb_section_get( 'btn-structure' ),
		)
	)
);
