/*
* Config
*/

// Dev url
var devurl = "tahunanui.dev";

// Set Dev status true/false (creates souremaps if var dev = true)
var dev = true;

// Compiled CSS output style (options: nested, expanded, compact, compressed)
var outputStyle = 'nested';

// Theme Folder Name
var theme = 'agent-focused-pro';

// name of scss file and CSS file destination in Torlesse
var sass_file_name = 'custom.scss'; // e.g. style.scss
var primary_css_location = '/css/'; //e.g. '/' for themedir or '/css/' for the themedir/css folder

// name of scss file and CSS file destination in Other genesis child themes
//var sass_file_name = 'custom.scss'; // e.g. style.scss
//var primary_css_location = '/css/'; //e.g. '/' for themedir or '/css/' for the themedir/css folder

// helper variables
var css_folder = '/css/';
var theme_dir = 'themes/' + theme;
var sass_dir = theme_dir + '/sass/'
var sass_file = sass_dir + sass_file_name;
var sass_file_wildcard = sass_dir + '**/*.scss';
var primary_css_dir = theme_dir + primary_css_location;
var css_dir = theme_dir + css_folder;

// Prerequisites
var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var gulpif = require('gulp-if');

/*
* Tasks
*/

function customPlumber(errTitle) {
  return plumber({
    errorHandler: notify.onError({
      // Customizing error title
      title: errTitle || "Error running Gulp",
      message: "Error: <%= error.message %>",
      sound: "Glass"
    })
  });
}

gulp.task('sass', function(){
   return gulp.src( sass_file )
      .pipe(customPlumber('Error Running Sass'))
      .pipe(sourcemaps.init())
      .pipe(sass({
         // includes bower_components as a import location
         includePaths: ['bower_components'],
         outputStyle: outputStyle
      })) // Compiles Sass to CSS with gulp-sass
      .pipe(autoprefixer())
      .pipe(gulpif(dev, sourcemaps.write()))
      .pipe(gulp.dest( primary_css_dir ))
      .pipe(browserSync.reload({
         stream: true
      }))
      .pipe(notify('Sass compiled: <%= file.relative %>'));
});

gulp.task('sass-secondary', function(){
   return gulp.src([sass_file_wildcard, '!' + sass_file ])
      .pipe(customPlumber('Error Running Sass'))
      .pipe(sourcemaps.init())
      .pipe(sass({
         // includes bower_components as a import location
         includePaths: ['bower_components'],
         outputStyle: outputStyle
      }))
      .pipe(autoprefixer())
      .pipe(gulpif(dev, sourcemaps.write()))
      .pipe(gulp.dest( css_dir ))
      .pipe(browserSync.reload({
         stream: true
      }))
      .pipe(notify('Sass compiled: <%= file.relative %>'));
});

gulp.task('browserSync', function() {
   browserSync({
      // Use example.dev instead of spinning up a server
      proxy: devurl
   })
})

gulp.task('watch', ['browserSync', 'sass', 'sass-secondary'], function(){
  gulp.watch( sass_file_wildcard, ['sass', 'sass-secondary']);
  gulp.watch(['**/*.html', '**/*.php'], browserSync.reload);
  gulp.watch( theme_dir + '/js/**/*.js', browserSync.reload);
});
