var plan = require('flightplan');

/*
 * To deploy
 *
 * fly deploy:staging or fly deploy:production
 */

/*
 *	Configure servers
 *
 *	When setting up the flightplan file ->
 *	Check:
 *		Host,
 *		Username,
 *		Port
 *		path
 */

local_folder_name = 'tahunanui.dev';

// Set up staging server details
staging_server_host = 'c25123.sgvps.net';
staging_server_user = 'tahunanui';
staging_server_path = 'public_html/wp-content';

// Set up production server details
production_server_host = 'c25123.sgvps.net';
production_server_user = 'tahunanui';
production_server_path = 'public_html/wp-content';

// Migrate DB Pro Profile
staging_migrate_db_profile = 0;
production_migrate_db_profile = 1;

deployment_exclusions = [
'--exclude=themes/genesis ' +
'--exclude=uploads ' +
'--exclude=mu-plugins ' +
'--exclude=plugins ' +
'--exclude=cache ' +
'--exclude=w3tc-config ' +
'--exclude=advanced-cache.php ' +
'--exclude=object-cache.php ' +
'--exclude=.git* ' +
'--exclude=node_modules ' +
'--exclude=bower_components ' +
'--exclude=.sass-cache ' +
'--exclude=gulpfile.js ' +
'--exclude=flightplan.js ' +
'--exclude=package.json ' +
'--exclude=bower.json ' +
'--exclude=.DS_Store ' +
'--exclude=README.md ' +
'--exclude=config.rb ' +
'--exclude=.jshintrc'
];

plugin_exclusions = [
'--exclude=iwp-client '
];

media_exclusions = [
'--exclude=ithemes-security ' +
'--exclude=backupbuddy_backups ' +
'--exclude=backupbuddy_temp ' +
'--exclude=pb_backupbuddy ' +
'--exclude=wp-migrate-db ' +
'--exclude=wp-migrate-db-pro '
];

// Connection details for servers
staging_server_connect = staging_server_user + '@' + staging_server_host + ':';
production_server_connect = production_server_user + '@' + production_server_host + ':';

// Staging Server
plan.target('staging', {
  host: staging_server_host,
  username: staging_server_user,
  //port: staging_server_port,
  webRoot: staging_server_path,
  agent: process.env.SSH_AUTH_SOCK
});

// Production Server
plan.target('production', {
  host: production_server_host,
  username: production_server_user,
  //port: production_server_port,
  webRoot: production_server_path,
  agent: process.env.SSH_AUTH_SOCK
});

// fly deploy:<target>
plan.local('deploy', function(transport) {

   // Staging
   if(plan.runtime.target === 'staging') {
      var server_connect = staging_server_connect // get server connection details
      var server_path = staging_server_path   // get target WebRoot
   }

   // Production
   if(plan.runtime.target === 'production') {
      var server_connect = production_server_connect // get server connection details
      var server_path = production_server_path   // get target WebRoot
   }

   // Run deployment
   transport.exec( 'rsync ./ ' + server_connect + server_path + ' --recursive --delete --compress ' + deployment_exclusions + ' --verbose');

   transport.log('Successfully deployed');
});

// fly syncdown:<target>
plan.local('syncdown', function(transport) {

   // Staging
   if(plan.runtime.target === 'staging') {
      var server_connect = staging_server_connect // get server connection details
      var server_path = staging_server_path   // get target WebRoot
      var migrate_db_profile = staging_migrate_db_profile;
   }

   // Production
   if(plan.runtime.target === 'production') {
      var server_connect = production_server_connect // get server connection details
      var server_path = production_server_path   // get target WebRoot
      var migrate_db_profile = production_migrate_db_profile;
   }

   var input = transport.prompt('Pull down plugins? [y]');
   if(input.indexOf('y') !== -1) {

     // Pull down plugins
     transport.exec( 'rsync ' + server_connect + server_path + '/plugins ./ --recursive --delete --compress ' + plugin_exclusions + ' --verbose');

     // Commit plugin updates to Git
     transport.exec('git commit -m \'Update plugins\' -- plugins');
   }

   var input = transport.prompt('Pull down media? [y]');
   if(input.indexOf('y') !== -1) {

     // Pull down media
     transport.exec( 'rsync ' + server_connect + server_path + '/uploads ./ --recursive --delete --compress ' + media_exclusions + ' --verbose');
   }

   var input = transport.prompt('Pull down database? [y]');
   if(input.indexOf('y') !== -1) {

     // Pull down database
     transport.exec( 'vagrant ssh');
     transport.exec( 'cd /srv/www/' + local_folder_name);
     transport.exec( 'wp migratedb profile ' + migrate_db_profile );
   }

   transport.log('Successfully Synced');
});

// fly syncup:<target>
plan.local('syncup', function(transport) {

   // Staging
   if(plan.runtime.target === 'staging') {
      var server_connect = staging_server_connect // get server connection details
      var server_path = staging_server_path   // get target WebRoot
      //var server_port = staging_server_port // get port
   }

   // Production
   if(plan.runtime.target === 'production') {
      var server_connect = production_server_connect // get server connection details
      var server_path = production_server_path   // get target WebRoot
      //var server_port = production_server_port // get port
   }

   var input = transport.prompt('Push up plugins? [y]');
   if(input.indexOf('y') !== -1) {

     // Push up plugins
     transport.exec( 'rsync ./plugins ' + server_connect + server_path + ' --recursive --delete --compress ' + plugin_exclusions + ' --verbose');
   }

   var input = transport.prompt('Push up media? [y]');
   if(input.indexOf('y') !== -1) {

     // Push up media
     transport.exec( 'rsync ./uploads ' + server_connect + server_path + ' --recursive --delete --compress ' + media_exclusions + ' --verbose');
   }

   transport.log('Successfully Synced');
});

plan.remote('wp-config', function(transport) {
   transport.log(hostname);
   var dbname = transport.prompt('Database name?');
   var dbuser = transport.prompt('Database Username?');
   var dbpassword = transport.prompt('Database Password?');
   transport.exec('wp core config --dbname=' + dbname + ' --dbuser=' + dbuser + ' --dbpass=' + dbpassword );
})
