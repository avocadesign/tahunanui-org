<?php
/** Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) exit( 'Cheatin&#8217; uh?' );

/**
 * Plugin Name: Torlesse Functions
 * Plugin URI: http://www.avocadesign.co.nz
 * Description: This creates some default functions and options that are desirable for our clients. It also loads the client specific plugin if required
 * Author: Avoca Design
 * Author URI: http://www.avocadesign.co.nz
 * Version: 0.2.0
 */

$wp_content_dir = dirname( dirname(__FILE__) );

// Include Avoca Design Torlesse functions for Genesis themes
include_once( $wp_content_dir . '/avocadesign-mu-plugins/torlesse/torlesse-functions.php' );

// Include any client specific functions that need to reside outside the theme
//include_once( $wp_content_dir . '/avocadesign-mu-plugins/client/client-functions.php' );
